<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleType extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];

    public function src() {
        return $this->morphTo();
    }
}
