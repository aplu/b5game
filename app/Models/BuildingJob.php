<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class BuildingJob extends BaseModelOfAge
{
    // use SoftDeletes;

    protected $guarded = [];

    protected $dates = ['finish'];

    public function player()
    {
        return $this->belongsTo('App\Models\Player');
    }

    public function building()
    {
        return $this->belongsTo('App\Models\Building');
    }

    public function upgrade()
    {
        return $this->belongsTo('App\Models\Upgrade');
    }

    public function done() {
        /** @todo Process formula of related Buildable */
        // dd($this->building->formula);
        $this->delete();
    }
}
