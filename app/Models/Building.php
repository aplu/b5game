<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Building extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];
}
