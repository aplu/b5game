<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Support\Facades\Auth;

class Age extends BaseModel
{
    protected $guarded = [];

    protected $dates = ['since', 'until'];

    // protected $casts = [
        // 'rules' => 'array',
    // ];

    /**
     * Accessors & Mutators
     */

    public function getRulesAttribute($value) {
        return !empty($value) ? explode(',', $value) : [];
    }


    /**
     * Relationships
     */

    public function players() {
        return $this->hasMany('App\Models\Player');
    }

    public function user_players(User $user = null) {
        if (is_null($user))
            $user = Auth::user();

        $builder = $this->players();
        $builder->where('players.user_id', $user->id);
        return $builder;
    }

    public function buildings() {
        return $this->hasMany('App\Models\Building');
    }

    public function upgrades() {
        return $this->hasMany('App\Models\Upgrade');
    }

    public function resources() {
        return $this->hasMany('App\Models\Resource');
    }
}
