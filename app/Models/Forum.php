<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Forum extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];

    public function posts() {
        return $this->hasMany('App\Models\Post');
    }
}
