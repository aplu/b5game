<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Race extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function players()
    {
        return $this->hasMany('App\Models\Player');
    }
}
