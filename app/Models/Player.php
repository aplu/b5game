<?php
namespace App\Models;

// use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Player extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];

    protected $processedJobs;

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function race()
    {
        return $this->belongsTo('App\Models\Race');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function hw()
    {
        return $this->belongsTo('App\Models\Sector');
    }

    public function buildingJobs() {
        return $this->hasMany('App\Models\BuildingJob')->where('player_id', $this->id);
    }

    public function free_ships() {
        return $this->hangar * 10 - $this->ships_ti - $this->ships_q40 - $this->ships_cr;
    }

    public function profit($resources) {
        foreach ($resources as $resource => $amount) {
            $this->$resource += $amount;
        }
        $this->save();
    }

    public function pay($resources) {
        foreach ($resources as $key => $amount) {
            $resources[$key] = -$amount;
        }
        $this->profit($resources);
    }

    /**
     * Create BuildingJob(s)
     *
     * If player set larger amount than Building::at_once then create more BuildingJobs.
     *
     * First player's BuildingJob in queue has defined finish timestamp! See Player::processJobs().
     */
    public function build(Building $building, int $amount) {
        $jobs = $this->processJobs();

        $emptyJobs = empty($jobs) ? true : false;
        while ($amount > 0) {
            BuildingJob::create([
                'player_id' => $this->id,
                'type' => 'building',
                'building_id' => $building->id,
                'amount' => min($amount, $building->at_once),
                'finish' => $emptyJobs ? now()->addSeconds($building->time / ($this->age->build_speed ?? 1)) /*date('Y-m-d H:i:s', time() + $building->time)*/ : null,
            ]);
            if ($emptyJobs)
                $emptyJobs = false;
            $amount -= $building->at_once;
        }
    }

    public function upgrade(Upgrade $upgrade, int $amount = 1) {
        $jobs = $this->processJobs();

        $emptyJobs = empty($jobs) ? true : false;
        BuildingJob::create([
            'player_id' => $this->id,
            'type' => 'upgrade',
            'upgrade_id' => $upgrade->id,
            'amount' => $amount,
            'finish' => $emptyJobs ? now()->addSeconds($upgrade->time / ($this->age->build_speed ?? 1)) /*date('Y-m-d H:i:s', time() + $upgrade->time)*/ : null,
        ]);
    }

    /**
     * Process player's BuildingJobs.
     *
     * Load player's BuildingJobs. First BuildingJob have to be defined with finish timestamp by Player::build()
     * or by this processing method when older finished jobs are done.
     */
    public function processJobs() {
        if (!isset($this->processedJobs)) {
            $this->processedJobs = [];

            $now = time();
            $finishDefined = false;
            $save_player = false;

            foreach ($this->buildingJobs()->orderBy('id')->get() as $job) {
                /** @todo Replace by polymorphic BuildingJob */
                // $buildable = $job->buildable;
                if ($job->type == 'building') {
                    $buildable = Building::find($job->building_id);
                    //$buildable = Cache::rememberForever('building.'.$job->building_id, function() use ($job) { return $job->building; });
                } elseif ($job->type == 'upgrade') {
                    $buildable = Upgrade::find($job->upgrade_id);
                    //$buildable = Cache::rememberForever('upgrade.'.$job->upgrade_id, function() use ($job) { return $job->upgrade; });
                } else
                    throw new \InvalidArgumentException("Bad type '{$job->type}' of building job.");

                $start = !isset($start) ? $job->created_at->timestamp/*-time()*/ /*0*/ : $finish;

                if (!isset($finish)) {
                    if (!isset($job->finish)) {
                        throw new \RuntimeException('Next buildable job has no finish defined.');
                    }
                    $finish = $job->finish->timestamp;
                    // $finish = $job->created_at->timestamp /*- time()*/;
                    if ($finish > $now)
                        $finishDefined = true;
                } else {
                    $finish += $buildable->time / ($this->age->build_speed ?? 1);
                }

                if ($finish <= $now) {
                    // Increase amount of buildable
                    $this->{$buildable->code} += $job->amount;
                    // Execute formula
                    if (!empty($buildable->formula)) {
                        eval($buildable->formula . ';');
                    }
                    $job->done();
                    $save_player = true;
                    // unset($start, $finish);
                    continue;
                } elseif (!$finishDefined) {
                    $job->finish = date('Y-m-d H:i:s', $finish);
                    $job->save();
                    $finishDefined = true;
                }

                $this->processedJobs[$job->id] = [
                    'type' => $job->type,
                    'name' => $buildable->name,
                    'amount' => $job->amount,
                    'start' => $start - $now,
                    'finish' => $finish - $now,
                ];
            }

            if ($save_player) {
                $this->save();
            }
        }

        return $this->processedJobs;
    }
}
