<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleClass extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];

    public function types() {
        return $this->hasMany('App\Models\VehicleType', 'class_id')
                    ->orderBy('class_id')
                    ->orderBy('lvl')
                    // ->orderBy('src_id')
                    ->orderBy('max_hp');
    }
}
