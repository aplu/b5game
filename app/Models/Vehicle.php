<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];

    public function class() {
        return $this->belongsTo('App\Models\VehicleClass');
    }

    public function type() {
        return $this->belongsTo('App\Models\VehicleType');
    }
}
