<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];

    public function forum() {
        return $this->belongsTo('App\Models\Forum');
    }

    public function player() {
        return $this->belongsTo('App\Models\Player');
    }
}
