<?php
namespace App\Models;

// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
// use NotificationChannels\Discord\DiscordChannel;
// use NotificationChannels\Discord\DiscordMessage;

// class State extends BaseModelOfAge
class Channel
{
    // use SoftDeletes, Notifiable;
    use Notifiable;

    protected $id;

    public function __construct($id) {
        $this->id = $id;
    }

    public function routeNotificationForDiscord() {
        return $this->id;
    }
}
