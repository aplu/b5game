<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Sector extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];

    public function getRouteKeyName() {
    	return 'short';
    }

    public function ruler()
    {
        return $this->belongsTo('App\Models\State');
    }
}
