<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Resource extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];
}
