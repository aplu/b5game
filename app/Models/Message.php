<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];

    public function sender() {
        return $this->belongsTo('App\Models\Player');
    }

    public function recipient() {
        return $this->belongsTo('App\Models\Player');
    }
}
