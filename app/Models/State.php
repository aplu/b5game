<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
// use NotificationChannels\Discord\DiscordChannel;
// use NotificationChannels\Discord\DiscordMessage;

class State extends BaseModelOfAge {
    use SoftDeletes, Notifiable;

    protected $guarded = [];

    public function race() {
        return $this->hasOne('App\Models\Race');
    }

    public function players() {
        return $this->hasMany('App\Models\Player');
    }

    public function sectors() {
        return $this->hasMany('App\Models\Sector', 'ruler_id');
    }

    public function hw() {
        return $this->belongsTo('App\Models\Sector');
    }

    public function routeNotificationForDiscord() {
        return $this->discord;
    }
}
