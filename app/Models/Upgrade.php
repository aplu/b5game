<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Upgrade extends BaseModelOfAge
{
    use SoftDeletes;

    protected $guarded = [];

    public function calcResources($lvl) {
        $result = [];
        foreach ($this->age->resources as $resource) {
            $rescode = $resource->code;
            $amount = $this->{$rescode};
            if (!$amount) {
                continue;
            }
            $result[$rescode] = ceil(is_numeric($amount) ? $amount : eval('return ('.$amount.');'));
        }
        return $result;
    }
}
