<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class BaseModelOfAge extends BaseModel
{
    public static function ofAge($age_id = null) {
        return self::where('age_id', $age_id ?? player()->age_id);
    }

    public function age() {
        return $this->belongsTo('App\Models\Age');
    }
}
