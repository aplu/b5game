<?php

if (!function_exists('age')) {
    function age($id = null) {
        global $ages;

        if ($id) {
            if (!isset($ages[$id]))
                $ages[$id] = \App\Models\Age::find($id);
            return $ages[$id];
        } else {
            if (!isset($ages[0])) {
                $player = player();
                if (!$player)
                    $ages[0] = false;
                else
                    $ages[0] = \App\Models\Age::where('id', $player->id)->with('age')->first();
            }
            return $ages[0];
        }
    }
}

if (!function_exists('page')) {
    function page($name, $vars = []) {
        return view('b5gna.page', array_merge(['page' => $name], $vars));
    }
}

if (!function_exists('player')) {
    function player($id = null) {
        global $players;
        if ($id) {
            if (!isset($players[$id]))
                $players[$id] = \App\Models\Player::find($id);
            return $players[$id];
        } else {
            if (!isset($players[0])) {
                // $user = auth()->user();
                $player_id = session('player_id');
                if (!$player_id)
                    $players[0] = false;
                else
                    $players[0] = \App\Models\Player::where('id', $player_id)->first();
            }
            return $players[0];
        }
    }
}

if (!function_exists('theme')) {
    function theme($default = 'b5g') {
        return session('theme', $default);
    }
}

/**
 * Format numbers
 *
 * For amount of resources see func amount()
 */
if (!function_exists('num')) {
    function num($number, $decimals = 0) {
        return number_format($number, $decimals, ',', '.');
    }
}

/**
 * Amount of resources
 */
if (!function_exists('amount')) {
    function amount($num) {
        return number_format($num, 0, ',', '\'');
    }
}

if (!function_exists('time_unit')) {
    function time_unit($seconds, $long = false) {
        $result = '';
        if ($seconds >= 86400) {
            $days = floor($seconds / 86400);
            $result .= $days . ($long ? ($days > 1 ? ($days > 4 ? ' dnů' : ' dny') : ' den') : 'd');
            $seconds -= $days*86400;
        }
        if ($seconds >= 3600) {
            $hours = floor($seconds / 3600);
            $result .= (empty($result) ? '' : ' ') . $hours . ($long ? ' hod' : 'h');
            $seconds -= $hours*3600;
        }
        if ($seconds >= 60) {
            $hours = floor($seconds / 60);
            $result .= (empty($result) ? '' : ' ') . $hours . ($long ? ' min' : 'm');
            $seconds -= $hours*60;
        }
        if ($seconds > 0)
            $result .= (empty($result) ? '' : ' ') . $seconds . ($long ? ' sec' : 's');

        return $result;
    }
}
/**
 * Print number as 3 digits (with variable decimal point) and with ISO suffix
 *
 * Ugly implementation but working against /tests/testHelpers.php
 */
if (!function_exists('unit')) {
    function unit($num, $maxDecimals = 2, $debug = false) {
        // $num = (string) $num;
        /*if ($num < 1e3)
            return $num;
        elseif ($num < 1e6)
            return number_format($num/1e3, 1, ',', '\'') . 'k';
        elseif ($num < 1e9)
            return number_format($num/1e6, 1, ',', '\'') . 'M';
        else
            return number_format($num/1e9, 1, ',', '\'') . 'G';*/

        $suffix = ['', 'K', 'M', 'G', 'T', 'P', 'E'];

        $orderOfNum = (int) log10($num) /*+ ((substr(strtr((string) $num, ['.'=>'']), 0, 3) >= 995) ? 2 : 0)*/;
        $orderOfUnit = $orderOfNum%3;
        $order = $orderOfNum < 3 ? 0 : $maxDecimals-$orderOfUnit;

        // Store first digit before rounding
        $strNum = (string) $num;
        $firstDigit = $strNum[0];

        // Round
        $num = round($num/10**($orderOfNum-$orderOfUnit), $order);

        // If first digit was rounded up from 9 to 10 then $order of num is 0
        $strNum = (string) $num;
        if ($firstDigit == 9 && $strNum[0] == 1) {
            $order = 0;
            $orderOfNum++;
            if ($num == 1000)
                $num = 1;
        }

        // Format
        return number_format($num, $order, ',', "'") . $suffix[intdiv($orderOfNum, 3)];

        // Print number with 1 decimal point and ISO suffix
        //return number_format($num/10**($order-$order%3), $order >= 3 ? 1 : 0, ',', "'");

        // Print number as 3 digits (with variable decimal point) and with ISO suffix
        //return /*$result =*/number_format($num/10**($order-$intOrder), $order < 3 ? 0 : $maxDecimals-$intOrder, ',', "'")
        //    . /*($suf =*/ $suffix[intdiv($order, 3)]/*)*/;

        /*if ($debug)
            echo "\nnumber_format(\$num=$num / 10**( \$order=$order - \$intOrder=$intOrder), \$order < 3 ? 0 : \$maxDecimals=$maxDecimals-\$intOrder, ',', \"'\") . \$suffix[intdiv(\$order, 3)=".intdiv($order,3)."])\n\n";*/
            // return "maxDecimals:$maxDecimals order:$order intOrder:$intOrder suffix:$suf result:$result";
        // return $result;
    }
}
