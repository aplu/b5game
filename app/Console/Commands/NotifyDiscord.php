<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Channel;
use App\Models\State;
use App\Notifications\DiscordNotification;

use Illuminate\Console\Command;
// use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class NotifyDiscord extends Command
{
    protected $signature = 'notify:discord
                                {--channel= : Discord Channel ID}
                                {--age= : Age ID} {--state= : Shortcut of State}
                                {message}';

    protected $description = 'Notify State on Discord';

    public function handle() {
        $message = $this->argument('message');

        // `./artisan notify:discord --channel 818621120311590963 hello`
        if ($this->option('channel')) {
            $channel = new Channel($this->option('channel'));
            $notification = new DiscordNotification($message);
            Notification::send($channel, $notification);
            echo("The message was sent at channel {$this->option('channel')}.\n");

        // `./artisan notify:discord --state NR --age 8 hello`
        } elseif ($this->option('age') && $this->option('state')) {
            $state = State::where(['age_id' => $this->option('age'), 'short' => $this->option('state')])->first();
            if ($state && !empty($state->discord)) {
                $notification = new DiscordNotification($message);
                Notification::send($state, $notification);
                echo("The message was sent at state channel {$state->discord}.\n");
            } else {
                fwrite(STDERR, "Discord channel of specified state wasn't found.\n");
            }

        } else {
            fwrite(STDERR, "Either option --channel nor --age together with --state wasn't specified.\n");
        }

    }
}