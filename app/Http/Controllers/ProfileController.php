<?php
namespace App\Http\Controllers;

use App\Models\Age;
use App\Models\Player;
use App\Models\Race;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends BaseAuthController
{
    function show(Request $request) {
        $ages = Age::with(['user_players', 'user_players.race', 'user_players.state'])->get();
        return page('profile', compact('ages'));
    }

    function switchPlayer(Request $request, Player $player) {
        if ($player->user != Auth::user())
            dd("Pouze hráč vlastnící postavu se na ni může přepnout.");

        // Save Player and Age IDs to session
        session(['player_id' => $player->id, 'age_id' => $player->age->id]);
        // Save Player ID to db
        $user = Auth::user();
        $user->last_player_id = $player->id;
        $user->save();

        if (in_array('MODERN', $player->age->rules)) {
            return redirect()->route('sector', $player->hw);
        } else {
            return redirect()->route('outpost');
        }
    }

    function edit() {
        return page('profile-edit');
    }

    function changePassword(Request $request) {
        $user = Auth::user();

        $validated = $this->validate($request, [
            'password' => 'required|min:6|confirmed',
        ]);
        if (!Hash::check($validated['password'], $user->password)) {
            return redirect(route('edit-profile'))->withErrors([trans('auth.failed')]);
        }

        $user->update([
            'password' => Hash::make($validated['password']),
        ]);

        // return view('b5gna.page', ['page'=>'profile']);
        return redirect('profile');
    }

    function update(Request $request, User $user) {
        $this->validate($request, $this->rules($user));

        $building->update([
            'name' => $request->name,
            'url' => $request->url,
        ]);

        return redirect(route('profile'));
    }

    /*function destroy(Building $building) {
        $building->delete();

        return redirect(route('buildings'));
    }*/
}

