<?php
namespace App\Http\Controllers;

use App\Models\Sector;
use App\Models\State;
use Illuminate\Http\Request;

class StatesController extends BasePlayerController
{
	function index()
	{
		$states = State::ofAge()->withCount(['players', 'sectors'])->get();
		$sectors_count = Sector::ofAge()->count();

		// return view('states.index', compact('states'));
		return page('states', compact('states', 'sectors_count'));
	}

	function create()
	{
		return view('states.create');
	}

	function rules($state = null)
	{
		return [
			'short' => 'nullable|string',
			'name' => 'required|string',
			'color' => 'nullable|string',
			'bg' => 'nullable|string',
			'hw' => 'nullable|string',
			'active' => ''
		];
	}

	function store(Request $request)
	{
		$this->validate($request, $this->rules());

		State::create([
			'short' => $request->short,
			'name' => $request->name,
			'color' => $request->color,
			'bg' => $request->bg,
			'hw' => $request->hw,
			'active' => $request->active,
		]);

		return redirect(route('states'));
	}

	function show(State $state)
	{
		return view('states.show', compact('state'));
	}

	function edit(State $state)
	{
		return view('states.edit', compact('state'));
	}

	function update(Request $request, State $state)
	{
		$this->validate($request, $this->rules($state));

		$state->update([
			'short' => $request->short,
			'name' => $request->name,
			'color' => $request->color,
			'bg' => $request->bg,
			'hw' => $request->hw,
			'active' => $request->active,
		]);

		return redirect(route('state', $state));
	}

	function destroy(State $state)
	{
		$state->delete();

		return redirect(route('states'));
	}
}

