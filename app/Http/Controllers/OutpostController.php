<?php

namespace App\Http\Controllers;

use App\Models\Building;
use App\Models\BuildingJob;
use App\Models\Player;
use App\Models\Resource;
use App\Models\Sector;
use App\Models\Upgrade;
// use App\Http\Controllers\B5GBattles\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class OutpostController extends BasePlayerController
{
    public function show(Request $request) {
        /*$buildings = [];
        foreach (Building::ofAge()->get() as $building) {
            $buildings[] = $building;
        }*/
        $player = player();
        $age = $player->age;
        $buildings = $age->buildings;
        $upgrades = $age->upgrades;

        $jobs = $player->processJobs();
        /*foreach ($jobs as $id => $job) {
        }*/

        return page('outpost', compact('buildings', 'upgrades', 'jobs'));
    }

    public function build(Request $request, Building $building) {
        $amount = $request->amount;
        $redirect = redirect()->route('outpost');
        $error = ['title' => '<b>' . $building->name . '</b>', 'msg'=>''];

        // Syntax checks
        if (!isset(player()->{$building->code})) {
            $error['msg'] .= "<p>Není možné stavět ani bourat tyto budovy. Prosím kontaktuj TM kvůli opravě této chyby.</p>\n";
        } elseif (!is_numeric($amount)) {
            $error['msg'] .= "Zadal jsi '{$amount}', což není číslo.<br>\n";
        // Logic checks
        } else {
            $error['title'] .= ' [<b>' . $amount . ' ks</b>]';

            if ($amount == 0)
                return $redirect;
            elseif ($amount < 0)
                return $this->demolish($building, -$amount);

            if (isset($building->max) && $building->max <= player()->{$building->code} + $amount)
                $error['msg'] .= "Maximálny počet budov je {$building->max}. Můžeš postavit již jen <b>" . ($building->max - player()->{$building->code}) . " ks</b>.<br>\n";

            if ($building->space * $amount > player()->space)
                $error['msg'] .= "Nedostatok miesta na stanici! Je potreba <b>" . ($building->space * $amount) . " miesta</b>.<br>\n";

            /** @todo Dynamic resources */
            $resources = [];
            foreach (Resource::where('to_build', true)->get() as $resource) {
                if ($building->{$resource->code}) {
                    $resources[$resource->code] = $resource;
                }
            }

            $notEnough = [];
            foreach ($resources as $code => $resource) {
                if ($building->{$code} * $amount > player()->{$code})
                    $notEnough[] = '<b>' . $resource->short . ' <span style="color:white">' . amount($building->{$code} * $amount) . '</b></span>';
            }
            if (!empty($notEnough))
                $error['msg'] .= "<p>Nedostatok surovín na stavbu požadovaného počtu budov!<br>\nPotrebné: " . implode(', ', $notEnough) . '.</p>';

            /*if ($building->ti * $amount > player()->ti
                    || $building->q40 * $amount > player()->q40
                    || $building->cr * $amount > player()->cr
                )
                // $error['msg'] .= "Nedostatok surovín na stavbu požadovaného počtu budov!";
                $error['msg'] .= "<p>Nedostatok surovín na stavbu požadovaného počtu budov!<br>\nPotrebné: "
                                . ($building->ti ? '<b>Ti <span style="color:white">' . amount($building->ti * $amount) . '</b></span>, ' : '')
                                . ($building->q40 ? '<b>Q40 <span style="color:white">' . amount($building->q40 * $amount) . '</b></span>, ' : '')
                                . ($building->cr ? '<b>Cr <span style="color:white">' . amount($building->cr * $amount) . '</b></span>.</p>';*/

        }

        /** @todo Neni mozne postavit vice nez maximum budov */

        if (!empty($error['msg']))
            return $redirect->with('outpost.error', $error);

        // Build
        player()->build($building, $amount);

        $pay = [];
        /** @todo Dynamic resources */
        if ($building->ti)
            $pay['ti'] = $building->ti * $amount;
        if ($building->q40)
            $pay['q40'] = $building->q40 * $amount;
        if ($building->cr)
            $pay['cr'] = $building->cr * $amount;
        if ($building->space)
            $pay['space'] = $building->space * $amount;

        player()->pay($pay);

        return $redirect;
    }

    public function upgrade(Request $request, Upgrade $upgrade) {
        $redirect = redirect()->route('outpost');
        $error = ['title' => $upgrade->name, 'msg'=>''];

        $player = player();
        /** @todo $lvl + same upgrades in jobs queue */
        $lvl = $player->{$upgrade->code};

        // Syntax checks
        if (is_null($lvl)) {
            $error['msg'] .= "<p>Není možné používat tento upgrade. Prosím kontaktuj TM kvůli opravě této chyby.</p>\n";
        // Logic checks
        } else {
            /*if ($upgrade->space > $player->space) {
                $error['msg'] .= "Nedostatok miesta na stanici! Potrebné miesto: " . ($upgrade->space) . "<br>\n";
            }
            if ($upgrade->ti > $player->ti
                    || $upgrade->q40 > $player->q40
                    || $upgrade->cr > $player->cr
                )
                // $error['msg'] .= "Nedostatok surovín na stavbu požadovaného počtu budov!";
                $error['msg'] .= "<p>Nedostatok surovín na stavbu požadovaného počtu budov!<br>\nPotrebné: "
                                . '<b>Ti <span style="color:white">' . amount($upgrade->ti) . '</b></span>, '
                                . '<b>Q40 <span style="color:white">' . amount($upgrade->q40) . '</b></span>, '
                                . '<b>Cr <span style="color:white">' . amount($upgrade->cr) . '</b></span>.</p>';
            */

            // Dynamic resources
            $rss = $upgrade->calcResources($lvl);

            $missing_rss = [];
            foreach ($rss as $res => $amount) {
                $available = $player->{$res};
                if ($amount > $available) {
                    $missing_rss[$res] = $amount - $available;
                }
            }

            if (!empty($missing_rss)) {
                // $resources = Resource::all()->pluck('short', 'code')->toArray();
                $resources = Resource::where('age_id', $player->age->id)->whereIn('code', array_keys($missing_rss))->pluck('short', 'code')->toArray();
                $missing_errors = [];
                foreach ($missing_rss as $res => $missing) {
                    // $missing_errors[] = '<b>' . $resources[$res] . ' <span style="color:white">' . amount($rss[$res]) . '</b></span>';
                    $missing_errors[] = '<b>' . $resources[$res] . ' <span style="color:white">' . amount($rss[$res]) . '</b></span> (chybí ' . amount($missing_rss[$res]) . ')';
                }
                $error['msg'] .= "<p>Nedostatok surovín na stavbu požadovaného počtu budov!<br>\nPotrebné: "
                               . implode(', ', $missing_errors) . '.';
                /*dump($rss);
                dump($missing_rss);
                dump($missing_errors);
                dd($resources);*/
                unset($missing_errors);
            }

        }

        if (!empty($error['msg']))
            return $redirect->with('outpost.error', $error);

        // Upgrade
        $player->upgrade($upgrade);

        // $pay = [];
        // if ($upgrade->ti)
        //     $pay['ti'] = $upgrade->ti;
        // if ($upgrade->q40)
        //     $pay['q40'] = $upgrade->q40;
        // if ($upgrade->cr)
        //     $pay['cr'] = $upgrade->cr;
        // if ($upgrade->space)
        //     $pay['space'] = $upgrade->space;
        // $player->pay($pay);
        $player->pay($rss);

        return $redirect;
    }

    public function demolish(Building $building, $amount) {
        $redirect = redirect()->route('outpost');
        $error = ['title' => $building->name . ' [-' . $amount . 'ks]', 'msg'=>''];

        /** @todo Neni mozne bourat tyto budovy */

        if (player()->{$building->code} < $amount) {
            $amount = player()->{$building->code};
            $error['msg'] = "Tolik budov nemáš. Bourá se <b>{$amount} ks</b>.";
        }

        if (!empty($error['msg']))
            $redirect = $redirect->with('outpost.error', $error);

        player()->profit([
            $building->code => -$amount,
            'space' => $building->space * $amount,
        ]);

        return $redirect;
    }

    public function stopJob(Request $request, BuildingJob $job) {
        if ($job->player->id != player()->id)
            return false;

        $building = $job->building;
        $amount = $job->amount;

        $job->delete();

        $payback = [];
        if ($building->ti)
            $payback['ti'] = $building->ti * $amount;
        if ($building->q40)
            $payback['q40'] = $building->q40 * $amount;
        if ($building->cr)
            $payback['cr'] = $building->cr * $amount;
        if ($building->space)
            $payback['space'] = $building->space * $amount;

        player()->profit($payback);

        return redirect()->route('outpost');
    }

    public function sectorsToRelocate() {
        /** @todo Only mine sectors */
        $my_sectors = Sector::ofAge()->get();
        dump($my_sectors);

        return page('relocate');
    }

    public function relocate() {
    }
}
