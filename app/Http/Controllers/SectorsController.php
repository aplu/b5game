<?php
namespace App\Http\Controllers;

use App\Models\Sector;
use Illuminate\Http\Request;

class SectorsController extends BasePlayerController
{
	function index()
	{
		$sectors = Sector::ofAge()->with('ruler')->get();

		return view('b5gna.page', [
			'page' => 'sectors',
			'sectors' => $sectors,
		]);
	}

	function select(Request $request) {
		return redirect()->route('sector', $request->sector);
	}

	function show(Sector $sector = null)
	{
		$player = player();
        $age = $player->age;

        if (in_array('MODERN', $age->rules)) {
			return view('b5gna.page', [
				'page' => 'sector-modern',
				'sector' => $sector,
				// 'buildings' => $age->buildings,
				'upgrades' => $age->upgrades,
				// 'jobs' => $jobs = $player->processJobs(),
			]);

		} else {
			return view('b5gna.page', [
				'page' => 'sector',
				'sector' => $sector,
			]);
		}
	}
}
