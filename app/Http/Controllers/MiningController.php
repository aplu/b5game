<?php

namespace App\Http\Controllers;

use App\Models\Player;
// use App\Http\Controllers\B5GBattles\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class MiningController extends BasePlayerController
{
    public function mining(Request $request) {
        return page('mining');
    }

    public function miners(Request $request) {
        return page('miners');
    }
}
