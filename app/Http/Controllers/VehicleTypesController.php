<?php
namespace App\Http\Controllers;

use App\Models\VehicleClass;
use Illuminate\Http\Request;

class VehicleTypesController extends BasePlayerController
{
    function index(/*Request $request*/) {
        $vehicle_classes = VehicleClass::ofAge()->with(['types', 'types.src'])/*->orderBy('vehicle_types.lvl')*/->get();
        /*foreach ($vehicle_classes as $class) {
            $vehicle_types = $class->types()->orderBy('lvl')->get();
            dump($vehicle_types->pluck('name', 'short')->toArray());
            foreach ($vehicle_types as $type) {
                dump($type->src);
            }
        }
        dd($vehicle_classes->pluck('name', 'short')->toArray());*/

        return page('vehicle-types', compact('vehicle_classes'));
    }

    function research(/*Request $request*/) {
        return page('technology');
    }
}
