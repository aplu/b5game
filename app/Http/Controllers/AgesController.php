<?php
namespace App\Http\Controllers;

use App\Models\Age;
use App\Models\Building;
use App\Models\Player;
use App\Models\Race;
use App\Models\Resource;
// use App\Models\Sector;
// use App\Models\State;
use App\Models\Upgrade;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgesController extends BaseAuthController
{
    function index()
    {
        $ages = Age::/*where('type', 'public')->get()*/all();
        return view('b5gna.ages.index', compact('ages'));
    }

    function chooseAge(Request $request/*, Age $age = null*/) {
        $user = Auth::user();

        if (!$request->age)
            // return redirect()->route('profile');
            return view('b5gna.ages.index', ['ages' => Age::all()]);
            //return page('profile', ['ages' => Age::with('user_players')->get()]);

        $age = Age::find($request->age);

        return redirect()->route('choose-race', ['age' => $age]);
    }

    /*function joinToAge(Request $request) {
        if (!$request->age)
            return page('profile');

        $age = Age::find($request->age);

        return redirect()->route('join-race', ['age' => $age]);
    }*/

    function chooseRace(Request $request, Age $age, Race $race = null)
    {
        if ($age->players_per_user && ($age->user_players()->count() >= $age->players_per_user)) {
            dd("Nemůžeš v tomto věku vytvořit postavu. Max postav hráče ({$age->players_per_user}) jsi v tomto věku již dosáhl.");
            // return redirect(route('profile'));
        } elseif ($age->max_players && ($age->players()->count() >= $age->max_players)) {
            dd("Nemůžeš v tomto věku vytvořit postavu. Max postav všech hráčů ({$age->max_players}) dosaženo.");
            // return redirect(route('profile'));
        } elseif ($age->since && $age->since > now()) {
            dd("Nemůžeš v tomto věku vytvořit postavu. Věk ještě nezačal.");
        } elseif ($age->until && $age->until < now()) {
            dd("Nemůžeš v tomto věku vytvořit postavu. Věk již skončil.");
        } elseif ($age->type != 'PUBLIC' && !Auth::user()->tester) {
            dd("Nemůžeš v tomto věku vytvořit postavu. Vstup pouze pro testery.");
        }

        if (!$request->race)
            // return page('choose-race', compact('age'));
            return view('b5gna.ages.choose-race', compact('age'));

        // $state = State::where('short', $race->states)->first();
        // $sector = Sector::where('name', $state->hw)->first();

        if (!isset($race)) {
            $race = Race::find($request->race);
        }

        $user = Auth::user();

        $player = new Player();
        $player->user_id = $user->id;
        $player->age_id = $age->id; //$request->age;
        $player->name = $request->name /*$user->name*/ ?? 'Player' . rand(1E6,9999999);
        $player->race_id = $race->id ?? $request->race;
        $player->state_id = $race->state->id;
        $player->hw_id = $race->state->hw->id;
        $player->save();
        $player->refresh();

        // Save Player ID to db
        $user->last_player_id = $player->id;
        // $user->last_player = $player;
        $user->save();

        session(['player_id' => $player->id, 'age_id' => $age->id]);

        foreach (Resource::ofAge()->get() as $resource) {
            if (isset($player->{$resource->code})) {
                $player->{$resource->code} = $resource->init;
            }
            if (isset($resource->max) && isset($player->{'max_'.$resource->code})) {
                $player->{'max_'.$resource->code} = $resource->max;
            }
        }

        foreach (Building::all() as $building) {
            if (isset($player->{$building->code})) {
                $player->{$building->code} = $building->init;
            }
        }

        foreach (Upgrade::all() as $upgrade) {
            if (isset($player->{$upgrade->code})) {
                $player->{$upgrade->code} = $upgrade->init;
            }
        }

        $player->save();

        return redirect()->route('outpost');
    }

    /*function create()
    {
        return view('b5gna.ages.create');
    }

    function rules($age = null)
    {
        return [
            'since' => '',
            'until' => '',
            'name' => 'string'
        ];
    }

    function store(Request $request)
    {
        $this->validate($request, $this->rules());

        Age::create([
            'since' => $request->since,
            'until' => $request->until,
            'name' => $request->name,
        ]);

        return redirect(route('ages'));
    }

    function show(Age $age)
    {
        return view('ages.show', compact('age'));
    }

    function edit(Age $age)
    {
        return view('ages.edit', compact('age'));
    }

    function update(Request $request, Age $age)
    {
        $this->validate($request, $this->rules($age));

        $age->update([
            'since' => $request->since,
            'until' => $request->until,
            'name' => $request->name,
        ]);

        return redirect(route('age', $age));
    }

    function destroy(Age $age)
    {
        $age->delete();

        return redirect(route('ages'));
    }*/
}

