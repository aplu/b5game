<?php
namespace App\Http\Controllers;

use App\Models\Building;
use Illuminate\Http\Request;

class BuildingsController extends BaseAdminController
{
	function index()
	{
		$buildings = Building::all();
		return view('buildings.index', compact('buildings'));
	}

	function create()
	{
		return view('buildings.create');
	}

	function rules($building = null)
	{
		return [
			'name' => 'string',
			'url' => 'string|url'
		];
	}

	function store(Request $request)
	{
		$this->validate($request, $this->rules());

		Building::create([
			'name' => $request->name,
			'url' => $request->url,
		]);

		return redirect(route('buildings'));
	}

	function show(Building $building)
	{
		return view('buildings.show', compact('building'));
	}

	function edit(Building $building)
	{
		return view('buildings.edit', compact('building'));
	}

	function update(Request $request, Building $building)
	{
		$this->validate($request, $this->rules($building));

		$building->update([
			'name' => $request->name,
			'url' => $request->url,
		]);

		return redirect(route('building', $building));
	}

	function destroy(Building $building)
	{
		$building->delete();

		return redirect(route('buildings'));
	}
}

