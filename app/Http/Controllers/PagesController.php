<?php

namespace App\Http\Controllers;

use App\Models\Age;
use App\Models\Player;
use App\Models\Sector;
use App\Models\State;
// use App\Http\Controllers\B5GBattles\Controller;
use Illuminate\Http\Request;

class PagesController extends BaseController
{
    /**
     * Use Blade Template as page.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($page = 'about') {
        // return view('b5gna.pages.' . $page);
        return view('b5gna.page', compact('page'));
    }

    public function all() {
        $player = player();

        // return view('b5gna.pages.' . $page);
        return view('b5gna.all', ['pages' => [
                // 'startup',
                'citizenship',
                'profile',

                'station',
                'relocate',
                'miners',
                'mining',
                'technology',

                'fleet-build',
                'repair',
                'fleet-groups',
                'fleet-wings',
                'fleet-overview',
                'move',
                'move-plan',

                'map',
                'map-sharing',
                'stations',
                'sectors',
                'sector',
                'intel',

                'fight-detail',
                'fight-mine',
                'fight-summary',

                'transfer',
                'market',
                'market-log',
                'balance',
                'cis',
                'taxes',
                'relations',
                'parties',
                'election',

                'citizens',
                'players',

                'notice-board',
                'vote',
                'vote-create',
                'forums',
                'forum',
                'forum-create',
                'msg',

                'statistics',
                'fight-points',
                'states',

                'states-eco',
                'macroeco',
                'crystals',

                'fleet-params',
                'acclog',
                'penalty',
            ],

            // ProfileController::show
            'ages' => Age::with(['user_players', 'user_players.race', 'user_players.state'])->get(),

            // StatesController::index
            'states' => State::ofAge()->withCount(['players', 'sectors'])->get(),
            'sectors_count' => Sector::ofAge()->count(),

            // PlayersController::index
            'players' => Player::ofAge()->get(),

            // SectorsController::index
            'sectors' => Sector::ofAge()->with('ruler')->get(),
            // SectorsController::show
            'sector' => $player->hw,

            // StationController::index
            'age' => $player->age,
            'buildings' => $player->age->buildings,
            'upgrades' => $player->age->upgrades,
            'jobs' => $player->processJobs(),
        ]);
    }
}
