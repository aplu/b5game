<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Base controller for authenticated user
 */
class BaseAuthController extends BaseController
{
    public function __construct() {
        $this->middleware('auth');
    }
}
