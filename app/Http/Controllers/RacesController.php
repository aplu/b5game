<?php
namespace App\Http\Controllers;

use App\Models\Race;
use Illuminate\Http\Request;

class RacesController extends BasePlayerController
{
	function index()
	{
		$races = Race::all();
		return view('races.index', compact('races'));
	}

	function create()
	{
		return view('races.create');
	}

	function rules($race = null)
	{
		return [
			'name' => 'required|string'
		];
	}

	function store(Request $request)
	{
		$this->validate($request, $this->rules());

		Race::create([
			'name' => $request->name,
		]);

		return redirect(route('races'));
	}

	function show(Race $race)
	{
		return view('races.show', compact('race'));
	}

	function edit(Race $race)
	{
		return view('races.edit', compact('race'));
	}

	function update(Request $request, Race $race)
	{
		$this->validate($request, $this->rules($race));

		$race->update([
			'name' => $request->name,
		]);

		return redirect(route('race', $race));
	}

	function destroy(Race $race)
	{
		$race->delete();

		return redirect(route('races'));
	}
}

