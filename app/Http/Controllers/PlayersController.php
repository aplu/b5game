<?php
namespace App\Http\Controllers;

use App\Models\Player;
use Illuminate\Http\Request;

class PlayersController extends BasePlayerController
{
	function index()
	{
		$players = Player::ofAge()->get();

		// return view('players.index', compact('players'));
		return page('players', compact('players'));
	}

	function create()
	{
		return view('players.create');
	}

	function rules($player = null)
	{
		return [
			'name' => 'required|string',
			'email' => 'required|email|unique',
			'email_verified_at' => 'nullable',
			'password' => 'required|string|password',
			'token' => '',
			'born' => 'date',
			'sex' => '',
			'url' => 'string|url',
			'bio' => 'nullable',
			'hw' => 'string'
		];
	}

	function store(Request $request)
	{
		$this->validate($request, $this->rules());

		Player::create([
			'name' => $request->name,
			'email' => $request->email,
			'email_verified_at' => $request->email_verified_at,
			'password' => $request->password,
			'token' => $request->token,
			'born' => $request->born,
			'sex' => $request->sex,
			'url' => $request->url,
			'bio' => $request->bio,
			'hw' => $request->hw,
		]);

		return redirect(route('players'));
	}

	function show(Player $player)
	{
		return view('players.show', compact('player'));
	}

	function edit(Player $player)
	{
		return view('players.edit', compact('player'));
	}

	function update(Request $request, Player $player)
	{
		$this->validate($request, $this->rules($player));

		$player->update([
			'name' => $request->name,
			'email' => $request->email,
			'email_verified_at' => $request->email_verified_at,
			'password' => $request->password,
			'token' => $request->token,
			'born' => $request->born,
			'sex' => $request->sex,
			'url' => $request->url,
			'bio' => $request->bio,
			'hw' => $request->hw,
		]);

		return redirect(route('player', $player));
	}

	function destroy(Player $player)
	{
		$player->delete();

		return redirect(route('players'));
	}
}

