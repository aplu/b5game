<?php
namespace App\Http\Controllers;

use App\Models\VehicleClass;
use Illuminate\Http\Request;

/**
Produkce na max upgradech pri indexech sektoru 110
==================================================
```
1 TL = 6600 rTi/24h
1 TA = 1100 rQ40/24h

pri 100 TP = +72'000 Ti / ( 100*2h/20 + 2h mista + (100'000 rTi / 6'600 rTi)*1h/10 hangaru*1.2 misto) = +5'210 Ti/h
pri 60 TP = +43'200 Ti / ( 60*2h/20 + 60*2h mista/100 + (60'000 rTi / 6'600 rTi)*1h/10 hangaru*1.2) = +5'210 Ti/h
pri 1 TP = +720 Ti / ( 2h/20 + 2h mista/100 + (1'000 rTi / 6'600 rTi tezba)*1h/10 hangaru*1.2) = +720 Ti / 0.16545454 h = +5'210,5263 Ti/h

pro 50 Raf = +18'000 Q40 / ( 50*3h/5 + 300*2h mista/100 + (50'000 rQ40 / 1'100 rQ40)*1h/10 hangaru*1.2 misto) = +434.2105 Q40/h
pro 10 Raf = +4'430 Q40/6h
1 Raf = +360 Q40 / ( 3h/5 + 6*2h mista/100 + (1'000 rQ40 / 1'100 rQ40)*1h/10 hangaru*1.2) = +434.2105 Q40/h

pro 40 OC = +72m Cr / ( 40*3h/8 + 200*2h mista/100 + 40*1h/10 hangaru * 1.2 s mistem) = +3'025'210,0840 Cr/h
pro 16 OC = 
16 OC = +49'230'768 Cr/6h
pro 1 OC = +1.8m Cr / ( 3h/8 + 5*2h mista/100 + 1h/10 hangaru*1.2) = +1.8m Cr / 0.57500 h = +3'025'210.0840 Cr/h

1800000 / ( 3/8 + 5*2/100 + 1/10*1.2 ) / (720 / ( 2/20 + 2/100 + 1000 / 6600*1/10*1.2 ))
580.5958747135214285

1800000 / ( 3/8 + 5*2/100 + 1/10*1.2 ) / (360 / ( 3/5 + 6*2/100 + 1000/1100*1/10*1.2 ))
6967.1504965622605057

se skoky 24/(12+skoky*0.5)

```

1 hodinovy prirustek produkce
=============================
```
100 TP (10h & 100 mista) = 100'000 rTi -> 72'000 Ti = 15.1515 TL
    + 1 lvl mista (2h)
    + 1.515 hangaru (1.515h)
        + upgrade mista
    = 72'000 Ti / 13.515 h = 5'327,354266 Ti/h

50 Raf (30h & 300 mista) = 50'000 rQ40 -> 18'000 Q40 = 45.4545 TA
    + 3 lvl mista (6h)
    + 4.545 hangaru (4.545h)
        + upgrade mista
    = 18'000 Q40 / 40.545 h = 443,946188 Q40/h

40 OC&OL (15h & 200 mista) = 72m Cr
    + 2 lvl mista (4h)
    + 4 hangary (4h)
        + upgrade mista 40/100 (0.8h)
    = 72m Cr / 27 h = 3'076'923 Cr/h
    2'666'666 Cr/h
    3'130'434,782 Cr/h
```

# 5'327,354266 Ti = 443,946188 Q40 = 2'666'666 Cr

```
1 Ti = 2'666'666 / 5'327,354266 = 587.61528 Cr
1 Q40 = 2'666'666 / 443,946188 = 7051.383 Cr
```


```
1 TP = +720 Ti / ( 2h/20 + 2h mista/100 + (1000 Ti / 6600 Ti) * 1h/10 hangaru) = +720 Ti / 0.165454 h = +5'327 Ti/h

```


za 24 se da vyrobit s 50ti tovarnama
-------------------
S
K
12.97x  thnor (3.7h * 0.5 = 1.85h) = 1.4m Ti; 240k Q40; 1b Cr
15x     toreth (3.2h * 0.5 = 1.6h) = 2.17m Ti; 104k Q40; 1.32b Cr
L


6.5x thnor + 7.5x toreth = 1.8m Ti; 172k q40; 1.16b Cr
    ((24/3.7)*76836 Ti + (24/3.2)*87382 Ti) = (/0.72) 1602437 rTi = (/1.1) 1603 TP + (/6) 267 TL
    ((24/3.7)*18355 + (24/3.2)*6918) = 170943 Q40
        (/0.36) = 474842 rQ40 = 475 Raf
            (/1.1) = 432 TA
    (((24/3.7)*76.84m Cr + (24/3.2)*87.38m Cr)/0.9) = 1282m Cr
        (/1.1/2) = 583 OC & OL

*/
class EcoSimController extends BasePlayerController
{
    function index(Request $request) {
        $index_ti = $request->old('index.ti') ?? 70;
        $index_q40 = $request->old('index.q40') ?? 70;
        $index_cr = $request->old('index.cr') ?? 70;
        $jumps = $request->old('jumps') ?? 0;
        $effect_tp = $request->old('effect.tp') ?? 36;
        $effect_raf = $request->old('effect.raf') ?? 18;
        $effect_oc = $request->old('effect.oc') ?? 45;

        return page('tests.ecosim', compact('index_ti', 'index_q40', 'index_cr', 'jumps', 'effect_tp', 'effect_raf', 'effect_oc'));
    }

    function calc(Request $request) {
        // At least minimal validation is needed to store old() data to session to retrieve it later by old() helper.
        // @TODO But it doesn't work for empty input and more situations.
        $validated = $request->validate([
            'index.ti'  => 'required|numeric',
            'index.q40' => 'required|numeric',
            'index.cr'  => 'required|numeric',
            'jumps'     => 'required|numeric',
            'effect.tp'  => 'required|numeric',
            'effect.raf' => 'required|numeric',
            'effect.oc'  => 'required|numeric',
        ]);
        // This doesn't work as validate() above
        // $request->validate(['' => '']);

        /*dump($index);
        dump($validated);
        dump(session()->all());

        dump($request->old('index.ti'));
        dd($request->input());*/

        // return view('b5gna.pages.ecosim');
        // return page('tests.ecosim');
        // return redirect(route('ecosim'));
        return redirect(route('ecosim'))->withInput();
    }
}
