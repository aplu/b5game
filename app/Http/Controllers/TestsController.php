<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestsController extends BaseController
{
    public function index() {
        return page('tests.index');
    }

    public function session() {
        return view('b5gna.users.session');
    }

    public function xhtml() {
        return page('tests.xhtml');
    }
}
