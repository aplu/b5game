<?php

namespace App\Http\Controllers;

// use App\Http\Controllers\B5GBattles\Controller;
use Illuminate\Http\Request;

/**
 * Base controller for player joined in age
 */
class BasePlayerController extends BaseController
{
    // protected $player;

    public function __construct() {
        $this->middleware(['auth', 'playing']);
    }

    /*public function player() {
        if (!isset($this->player)) {
            $this->player = player();
            if (!$this->player)
            	return redirect()->route('ages');
        }

        return $this->player;
    }*/
}
