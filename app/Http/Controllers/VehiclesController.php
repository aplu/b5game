<?php
namespace App\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Http\Request;

class VehiclesController extends BasePlayerController
{
    function index(/*Request $request*/) {
        $vehicles = Vehicle::all();
        // return view('vehicles.index', compact('vehicles'));
        return page('fleet-params', compact('vehicles'));
    }

    function build(/*Request $request*/) {
        return page('fleet-build');
    }

    function repair(/*Request $request*/) {
        return page('repair');
    }

    function create(/*Request $request*/) {
        return view('ships.create');
    }

    function rules($ship = null) {
        return [
            'name' => 'string',
            'short' => 'string',
            'class' => '',
            'lvl' => '',
            'src_type' => '',
            'src' => 'string',
            'ti' => '',
            'q40' => '',
            'cr' => '',
            'time' => '',
            'slots' => 'string',
            'hp' => '',
            'us' => '',
            'ul' => ''
        ];
    }

    function store(Request $request) {
        $this->validate($request, $this->rules());

        Ship::create([
            'name' => $request->name,
            'short' => $request->short,
            'class' => $request->class,
            'lvl' => $request->lvl,
            'src_type' => $request->src_type,
            'src' => $request->src,
            'ti' => $request->ti,
            'q40' => $request->q40,
            'cr' => $request->cr,
            'time' => $request->time,
            'slots' => $request->slots,
            'hp' => $request->hp,
            'us' => $request->us,
            'ul' => $request->ul,
        ]);

        return redirect(route('ships'));
    }

    function show(/*Request $request,*/ Ship $ship) {
        return view('ships.show', compact('ship'));
    }

    function edit(/*Request $request,*/ Ship $ship) {
        return view('ships.edit', compact('ship'));
    }

    function update(Request $request, Ship $ship) {
        $this->validate($request, $this->rules($ship));

        $ship->update([
            'name' => $request->name,
            'short' => $request->short,
            'class' => $request->class,
            'lvl' => $request->lvl,
            'src_type' => $request->src_type,
            'src' => $request->src,
            'ti' => $request->ti,
            'q40' => $request->q40,
            'cr' => $request->cr,
            'time' => $request->time,
            'slots' => $request->slots,
            'hp' => $request->hp,
            'us' => $request->us,
            'ul' => $request->ul,
        ]);

        return redirect(route('ship', $ship));
    }

    function destroy(/*Request $request,*/ Ship $ship) {
        $ship->delete();

        return redirect(route('ships'));
    }
}
