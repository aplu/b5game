<?php
namespace App\Http\Controllers;

use App\Models\Building;
use Illuminate\Http\Request;

class BattleSimController extends BasePlayerController
{
    public function index()
    {
        $sumUS = $sumUL = 0;
        $sumHP = ['S' => 0, 'K' => 0, 'L' => 0, 'D' => 0];

        // Sum before fight
        /*foreach ($ships as $ship) {
            $sumUS += $ship['us'] * $ship['hpPart'] * $ship['acc'];
            $sumUL += $ship['ul'] * $ship['hpPart'] * $ship['acc'];
            $sumHP[$ship['class']] += $ship['hp'];
        }*/

        // Missing templates!
        // return view('b5gna.page', ['page'=>'fight-control']);
        // return view('b5gna.page', ['page'=>'fight-attack']);

        // return view('b5gna.page', ['page'=>'fight-detail']);
        // return view('b5gna.page', ['page'=>'fight-overview']);
        // return view('b5gna.page', ['page'=>'fight-mine']);
        // return view('b5gna.page', ['page'=>'sector']);

        return page('tests.battlesim');
    }
}
