<?php
namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostsController extends BaseAuthController
{
	function index()
	{
		$posts = Post::all();
		return view('posts.index', compact('posts'));
	}

	function create()
	{
		return view('posts.create');
	}

	function rules($post = null)
	{
		return [
			'content' => 'required'
		];
	}

	function store(Request $request)
	{
		$this->validate($request, $this->rules());

		Post::create([
			'content' => $request->content,
		]);

		return redirect(route('posts'));
	}

	function show(Post $post)
	{
		return view('posts.show', compact('post'));
	}

	function edit(Post $post)
	{
		return view('posts.edit', compact('post'));
	}

	function update(Request $request, Post $post)
	{
		$this->validate($request, $this->rules($post));

		$post->update([
			'content' => $request->content,
		]);

		return redirect(route('post', $post));
	}

	function destroy(Post $post)
	{
		$post->delete();

		return redirect(route('posts'));
	}
}

