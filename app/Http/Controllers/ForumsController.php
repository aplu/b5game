<?php
namespace App\Http\Controllers;

use App\Models\Forum;
use Illuminate\Http\Request;

class ForumsController extends BaseAuthController
{
	function index()
	{
		$forums = Forum::all();
		return view('forums.index', compact('forums'));
	}

	function create()
	{
		return view('forums.create');
	}

	function rules($forum = null)
	{
		return [
		];
	}

	function store(Request $request)
	{
		$this->validate($request, $this->rules());

		Forum::create([
		]);

		return redirect(route('forums'));
	}

	function show(Forum $forum)
	{
		return view('forums.show', compact('forum'));
	}

	function edit(Forum $forum)
	{
		return view('forums.edit', compact('forum'));
	}

	function update(Request $request, Forum $forum)
	{
		$this->validate($request, $this->rules($forum));

		$forum->update([
		]);

		return redirect(route('forum', $forum));
	}

	function destroy(Forum $forum)
	{
		$forum->delete();

		return redirect(route('forums'));
	}
}

