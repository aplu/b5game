<?php

namespace App\Http\Controllers;

use App\Models\Player;
// use App\Http\Controllers\B5GBattles\Controller;
use Illuminate\Http\Request;

class GameManagementController extends BaseController
{
    /*public function __construct() {
        $this->middleware('auth');
    }*/

    public function recalc($amount = 1000) {
        foreach (Player::all() as $player) {
            // 1000 per day, 41.6666 per hour
            $rtiSub = min($player->rti, $player->furnace * $amount);
            $tiEffect = 0.36 + $player->lvl_ti * 0.04;
            $tiAdd = $rtiSub * $tiEffect;

            $player->rti -= $rtiSub;
            $player->ti += round($tiAdd);

            $rq40Sub = min($player->rq40, $player->rafinery * $amount);
            $q40Effect = 0.18 + $player->lvl_q40 * 0.02;
            $q40Add = $rq40Sub * $q40Effect;

            $player->rq40 -= $rq40Sub;
            $player->q40 += round($q40Add);

            dump([
                'player' => $player->name,
                'rti' => amount($player->rti) . ' - ' . amount($rtiSub) . ' = ' . amount($player->rti - $rtiSub),
                'ti' => amount($player->ti) . ' + ' . amount($tiAdd) . ' = ' . amount($player->ti + $tiAdd),
                'lvl_ti' => $player->lvl_ti . ' = ' . $tiEffect . '%',
                'rq40' => amount($player->rq40) . ' - ' . amount($rq40Sub) . ' = ' . amount($player->rq40 - $rq40Sub),
                'q40' => amount($player->q40) . ' + ' . amount($q40Add) . ' = ' . amount($player->q40 + $q40Add),
                'lvl_q40' => $player->lvl_q40 . ' = ' . $q40Effect . '%',
            ]);

            $player->save();
        }

        // return redirect()->route('intro');
    }
}
