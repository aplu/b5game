<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'name' => ['nullable', 'string', 'max:255'],
            'sex' => Rule::in(['', 'm', 'f']),
            'birth_year' => ['nullable', 'numeric'],
        ];
    }

    public function attributes()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Heslo',
            'name' => 'Meno',
            'sex' => 'Pohlavie',
            'birth_year' => 'Rok narodenia',
        ];
    }
}
