<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Shorter strings for older MariaDB at AWS
        Schema::defaultStringLength(191);

        Relation::morphMap([
            'race' => 'App\Models\Race',
            'state' => 'App\Models\State',
        ]);
    }
}
