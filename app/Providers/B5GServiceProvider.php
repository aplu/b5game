<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;

class B5GServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        /*$this->publishes([
            __DIR__.'/path/to/config/courier.php' => config_path('courier.php'),
        ]);*/

        //Blade::if('env', function ($environment) {
        //    return app()->environment($environment);
        //});
        // ### Allows
        // @env('local') The application is in the local environment...
        // @elseenv('testing') The application is in the testing environment...
        // @else The application is not in the local or testing environment...
        // @endenv

        //Blade::directive('datetime', function ($expression) {
            /*return "<?php echo ($expression)->format('m/d/Y H:i'); ?>";*/
            /*return "<?= date('d M Y - H:i:s', $expression)); ?>";*/
        // });

        // View::share('testVar', 'testValue');

        View::composer('*', function($view) /*use ($auth)*/ {
            $player = player();
            $view->with('player', $player);
            // $view->with('age', age());
            $view->with('rules', $player->age->rules ?? []);
            // $view->with('race', race());
            // $view->with('state', state());
        });

        Blade::directive('amount', function($num) {
            return "<?= amount($num) ?>";
        });

        Blade::directive('unit', function($num) {
            return "<?= unit($num) ?>";
        });
    }
}
