Production Queue
================

Fronty
------
- vystavba (budovy a upgrady)
- vyroba tezby (fronty s 1 ukolem)
- vyroba lodi
- burza
- boj
- lety
- skenovani
- vystavba stanic v sektorech
? prepocet

Pozadavky na fronty ukolu
-------------------------
- kombinace realtime s delayed zpracovanim (pri vypadku sluzby nebo nepravidelnem spousteni) bez sebekratsich casovych der
- zavislosti ukolu mezi ruznymi frontami (napr. vystavba tovaren s vyrobou lodi)
- jednoduchy dotaz na pocet nekterych zaznamu ve fronte, napr. pocet upgradu konkretniho typu kvuli vypoctu ceny dalsiho upgradu

Implementace
------------

# Realtime processing
Je zpracovavana serverovou sluzbou presne v case, kdy ma byt ukol zpracovan.

Vyhody:
- ukol je nejjednodussim algoritmem zpracovan okamzite s veskerymi aktualnimi daty

Nevyhody:
- vyzaduje nonstop bezici serverovou sluzbu s casovanim max 1s, ale spise 100ms ci mene (1ms)
- pri vypadku

# Delayed processing
Ukoly jsou zpracovany az ve chvili, kdy je potreba aktualizovat data. Napr. kdyz se hrac prepne na stranku stanice (zpracovani vystavby) nebo jiny hrac sleduje detail sektoru (zpracovani vyroby lodi vsech hracu v sektoru).

Vyhody:
- nevyzaduje realtime zpracovani

Nevyhody:
- pri zpracovani nemusi byt k dispozici aktualni data a pri zpracovani v minulosti muze dojit k nepresnemu vyhodnoceni co kolizi zpracovani paralelnich front