Zacatek veku
============
200'000 Ti, 2'500 Q40, 200'000'000 Cr
20 TL, 5 TA, 20 OL

40 TP, 5 Raf, 20 OC, 5 Hang, 1 Tov, 0 VD
eTP (1/10) 36%, eRaf (1/10) 18%, eOL (1/10) 45%, misto (3) 60/300

# Ceny budov

20 TP  =  20 mista,  9'860 Ti,   140 Q,  9'349'960 Cr
5 Raf  =  30 mista, 40'800 Ti,     0 Q,  4'249'980 Cr
8 OC   =  40 mista, 37'128 Ti,   712 Q,  4'759'984 Cr
2 Hang =  20 mista, 20'008 Ti,   814 Q,  7'929'948 Cr
3 Tov  =  60 mista,  7'650 Ti, 1'701 Q,  9'562'455 Cr
1 VD   = 200 mista, 96'002 Ti, 3'840 Q, 20'800'000 Cr

Efektivita tavicich peci (+4%, 6h)
==================================

1(36%)->2(40%)    22'950 Ti, 797 Q, 2'390'615 Cr
2(40%)->3(44%)

Misto na stanici
================

lvl3->4 Ti 10'873, Cr 1'510'163


# Postup
1. poslat tezbu, vyrobit 5 TL
2. postavit 2 hangary, vyrobit 10 TL + 10 TA

## cmd. Kakarrot  | 29 May - 21:25
Den 1: 21:00
- vyroba 6 tazobnych lodi
Den 1: 23:00
- do fronty stavba 100 taviacich peci (dokonci sa 11:01)
- vyslat 6 tazobnych lodi
Den 1: do 11:00
- do fronty stavba 30 taviacich peci (dokonci sa 15:01)
- do fronty stavba 4 rafinerii (dokonci sa 18:01)
Den 2: 20:01
- 1 hangar (dokonci sa 22:01)
- upgrade efektivity tavenia rudy Ti (dokonci sa 04:01)
- 1 rafineria
- do fronty 36 taviacich peci resp kolko komu maxinalne vyjde (dokonci sa 11:01)
Den 2: 22:01
- 7 tazobnych lodi + vyslat ich (najneskor 07:59)

## SOAD  | 23 Jun - 09:18
Kroky bod po bode a hodina po hodine ako co najefektivnejsie stavat ekonomiku
Den zacina a konci prepoctom. Nulty den je den startu veku

Den 0: co najskor po spusteni veku tj okolo 17:01 !!!
- stavba 2 hangare (dokoncia sa cca 19:01)
- vyroba 5 tazobnych lodi
- vyslat tazbu (20 TP, 5 T, 20 OL ... doporucujem oddelit TP a T od OL kvoli kroku v 19:56)
- do fronty stavba LEN 1 hangaru (dokonci sa 21:01)
- do fronty upgrade miesta (dokonci sa 23:01)
- do fronty upgrade miesta druhykrat (dokonci sa 01:01)
Den 0: 19:00
- v pripade, ze som 2 hangare stihol postavit do 17:03, tak vyroba 8 tazobnych lodi a 8 tankerov
- v pripade, ze som 2 hangare NEstihol postavit do 17:03, tak vyroba 7 tazobnych lodi a 7 tankerov
- vyslat tazbu 5 tazobne lode
- priprava tazobnej skupiny pre 8 resp 7 tazobnych lodi a tankerov, ktoru vysleme okolo 19:56 (bude to tesne, preto si tazobnu skupinu pripravte dopredu)
Den 0: okolo 19:56
- vyslat tazbu 8 resp 7 tazobnych lodi a tankerov este pred prepoctom !!!
- vyroba tazobnych lodi (4 ak som predtym o 19:00 vyrobil 8+8 resp 6 ked som predtym vyrobil 7+7)
- stiahnut tazbu rTi a rQ40 a hned ju znova nahodit (ziskate rudy, ktore hned v nultom prepocte pretavite)

Den 1: 21:00
- vyroba 10 tazobnych lodi
Den 1: 23:00
- do fronty stavba 100 taviacich peci (dokonci sa 11:01)
- vyslat 10 tazobnych lodi
Den 1: do 11:00
- do fronty stavba 40 taviacich peci (dokonci sa 15:01)
- do fronty stavba 5 rafinerii (dokonci sa 18:01)
- do fronty upgrade miesta (dokonci sa 20:01)

Den 2: 20:01
- upgrade efektivity tavenia rudy Ti (dokonci sa 02:01)
- do fronty 95 taviacich peci resp kolko komu maxinalne vyjde (dokonci sa 12:01)

Den 3: 20:01
- upgrade miesta (dokonci sa 22:01)
Den 3: 22:01
- stavba 2 hangare (dokonci sa 00:01)
- do fronty upgrade efektivity tavenia rudy Ti (dokonci sa 06:01)
Den 3: 00:01 resp nejneskor 5:30
- vyroba 20 tazobnych lodi
Den 3: do 8:00 !!!
- vyslanie tazit 20 tazobnych lodi
- stavba 7 taviacich peci a 2 rafinerie (resp tolko aby ste vsetku rudu pretavili)

Odporucania pre dalsie dni:
----------------------------------------------------------------------------------------
- snazim sa co najskor po prepocte mat vylepsene miesto, aby som cez noc mohol stavat a nestal
- upgrade efektivit davam vzdy po vyrobeni 30 tazobnych plavidiel daneho typu (tj mam 60 tazobnych lodi, takze by som mal mat 2x vylepsenu efektivitu tj level 3)
- snazim sa po prepocte mat na sklade co najmensie mnozstvo rudy tj vsetko by som mal pretavit (NEROBIM SI ZASOBY ASPON PRVYCH 8 DNI ani viac taviacich budov ako mam rudy !!!)
- ak nemam miesto a musim ist spat, tak dam upgrade, aby som aspon nejako vyuzil cas (toto ale plati az niekedy 7.-8. den, ked uz mozem budovat nonstop !)