<?php
//$user = Auth::user();
$player = player();
if (!$player)
	return false;
?>
<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<ajax>
<resources>
<res-ti><?= number_format($player->ti, 0, ',', '\'') ?></res-ti>
<res-q40><?= number_format($player->q40, 0, ',', '\'') ?></res-q40>
<res-cr><?= number_format($player->cr, 0, ',', '\'') ?></res-cr>
<res-rti><?= number_format($player->rti, 0, ',', '\'') ?></res-rti>
<res-rq40><?= number_format($player->rq40, 0, ',', '\'') ?></res-rq40>
</resources>
<indicators>
<msg_count><?= $player->msg_count ?></msg_count>
<cis_count><?= $player->cis_count ?></cis_count>
<fight_count><?= $player->fight_count ?></fight_count>
</indicators>
<users><?= 0 ?></users>
<online><?= 0 ?></online>
<notify></notify>
<alert><?= '0' ?></alert>
</ajax>