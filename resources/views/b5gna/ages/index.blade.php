@extends('b5gna.layout')

@section('content')

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="{{ route('ages') }}">Věky</a></div>
@else
<div class="mainTitle">Věky</div>
@endif
<div class="mainTitleFade"></div>
<div class="content">

	@if (!($debug['all'] ?? false))
		@include('b5gna.menu.dummy-profile')
	@endif

	<div class="narrowBox">

@php
//dump($ages);
@endphp

	@if (!count($ages))
		<div class="window" style="text-align:center; padding:2em; font-size:150%">
			Momentálně není spuštěn žádný věk.
		</div>
	@else
		@foreach ($ages as $age)
			<form action="{{ route('choose-age') }}" method="POST">
			@csrf
			<input type="hidden" name="age" value="{{ $age->id }}">
			<div style="float:right; width:200px">
				<img src="{{ $age->image }}" style="border-radius:10px; border:1px solid blue">
			</div>
			<table width="70%" class="Tab center" style="margin:2em auto">
				<tr>
					<th colspan="2" style="font-size:150%">
						Věk {!! '#'.$age->id . ' <strong>' . $age->name . '</strong>' !!}
					</th>
				</tr>
				<tr>
					<th>Začal</th>
					<td>{{ $age->since->format('j.n.Y H:i') }}</td>
				</tr>
				<tr>
					<th>Končí</th>
					<td>{{ $age->until ? $age->until->format('j.n.Y H:i') : '-' }}</td>
				</tr>
				{{-- <tr>
					<th>Typ</th>
					<td>{{ $age->type }}</td>
				</tr>
				<tr>
					<th>Pravidla</th>
					<td>{{ $age->rules }}</td>
				</tr> --}}
				<tr>
					<th colspan="2">
						@if ($age->type == 'PUBLIC' || Auth::user()->tester)
						<input type="submit" value="Vstoupit do věku">
						@else
						<strong>Vstup pouze pro testery</strong>
						@endif
					</th>
				</tr>
			</table>
			<div style="clear:both"></div>
			</form>
		@endforeach
	@endif

	</div>
				{{-- <div align="left" style="margin:10px;">
					<b style="color:red">Momentálně jsi v tomto herním světě bloudící duší bez těla.<br>
					Pro získání hráčské postavy si musíš vybrat rasu, do jejíhož zástupce se chceš nainkarnovat.<br>
					Tvá postava se vzápětí stane vládcem vlastní malé vesmírné základny, kterou bude moci vybudovat od základů...</b><br>
					<br>
					Výběr věku:
					<select name="age_id" style="font-size:150%; line-height:150%; height:150%">
						<option>-- Zvol si věk --</option>
					@foreach (App\Models\Age::get() as $age)
						<option value="{{ $age->id }}"{{ old('age_id') == $age->id ? ' selected' : '' }}>{{ '#'.$age->id . ' ' . $age->name }}</option>
					@endforeach
					</select>

					<br><br>
				</div>
			</div>
		</div> --}}

	<div style="clear:both"></div>
</div>

@endsection
