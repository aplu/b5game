@extends('b5gna.layout')

@section('content')

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="{{ route('edit-profile') }}">Volba rasy</a></div>
@else
<div class="mainTitle">Volba rasy</div>
@endif
<div class="mainTitleFade"></div>
<div class="content">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-profile')
    @endif

    {{-- <form action="" method="post"> --}}
        {{-- @method('PATCH') --}}
        {{-- @csrf --}}

    <form action="{{ route('choose-race', compact('age')) }}" method="POST">
    @csrf

    <div class="narrowBox">

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">Volba rasy</div>
            <div align="left" style="margin:10px;">
                {{-- <b style="color:red">Momentálně jsi v tomto herním světě bloudící duší bez těla.<br>
                Pro získání hráčské postavy si musíš vybrat rasu, do jejíhož zástupce se chceš nainkarnovat.<br>
                Tvá postava se vzápětí stane vládcem vlastní malé vesmírné základny, kterou bude moci vybudovat od základů...</b><br>--}}

                Vybraný věk: <b class="hl">{{ $age->name }}</b><br>

                <br><br>

                Jméno postavy: <input name="name" value="{{ old('name', Auth::user()->name) }}">

                <br><br>

                Výběr rasy:
                <select name="race" onchange="$('#intro').html($('#intro-'+this.value).html())" style="font-size:150%; line-height:150%; height:150%">
                    <option>-- Zvol si rasu --</option>
                @foreach (App\Models\Race::ofAge($age->id)->where('playable', 1)->get() as $race)
                    <option value="{{ $race->id }}">{{ $race->name }}</option>
                @endforeach
                </select>

                <br><br>

                <div id="intro">
                </div>

            @foreach (App\Models\Race::where('playable', 1)->get() as $race)
                <div id="intro-{{ $race->id }}" style="display:none">
                    {{-- <form action="{{ route('choose-race', $race) }}" method="GET">
                        @csrf --}}
                        <input type="submit" value="Chci hrát za {{ $race->name }}">
                    {{-- </form> --}}
                </div>
            @endforeach

                </form>

            </div>
        </div>
    </div>
    </form>

    <div style="clear:both"></div>
</div>

@endsection