@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/taxes">Dane</a></div>
@else
<div class="mainTitle">Dane</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br><br><br>
    <form action="" method="post">
        <table width="70%" cellspacing="0" class="Tab center">
            <tr style="font-size: 12px; color: #6984f9;">
                <td colspan="4"><b>Interné dane: Pozemská Aliancia</b></td>
            </tr>
            <tr>
                <th>Druh dane</th>
                <th>Výška</th>
                <th>Tvoje dane</th>
                <th>Jednotka</th>
            </tr>
            <tr>
                <td>Daň zo stanice</td>
                <td>200 mil</td>
                <td>200 mil</td>
                <td>Cr / deň</td>
            </tr>
            <tr>
                <td>Daň z ťažobnej lode</td>
                <td>200</td>
                <td>200</td>
                <td>Ti / deň</td>
            </tr>
            <tr>
                <td>Daň z obchodnej lode</td>
                <td>200'000</td>
                <td>200,2 mil</td>
                <td>Cr / deň</td>
            </tr>
            <tr>
                <td>Daň z tankera</td>
                <td>200</td>
                <td>200</td>
                <td>Q40 / deň</td>
            </tr>
            <tr>
                <td>Daň z vojnovej lode</td>
                <td>200 mil</td>
                <td>0</td>
                <td>Cr / deň</td>
            </tr>
    </form>
    </table><br><br>
    <div style="clear:both"></div>
</div>