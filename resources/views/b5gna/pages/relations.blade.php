@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/relations">Politické vzťahy</a></div>
@else
<div class="mainTitle">Politické vzťahy</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br>
    <form action="" method="post">
        <table width="70%" cellspacing="0" class="Tab center">
            <tr>
                <th rowspan="1">S kým</th>
                <th colspan="1">Náš vzťah k ním</th>
                <th colspan="1">Ich vzťah k nám</th>
            </tr>
            <tr>
                <td style="color: #dd9090">Centaurská Republika</td>
                <td><b style="color: #fb0000">V</b></td>
                <td><b style="color: #fb0000">V</b></td>
            </tr>
            <tr>
                <td style="color: #787811">Kolónia Drakhov</td>
                <td><b style="color: #00eaff">N</b></td>
                <td><b style="color: #00eaff">N</b></td>
            </tr>
            <tr>
                <td style="color: #30bba9">Kruh Technomágov</td>
                <td><b style="color: #00eaff">N</b></td>
                <td><b style="color: #00eaff">N</b></td>
            </tr>
            <tr>
                <td style="color: #8a68a5">Minbarská Federácia</td>
                <td><b style="color: #00eaff">N</b></td>
                <td><b style="color: #00eaff">N</b></td>
            </tr>
            <tr>
                <td style="color: #a03b37">Narnský Režim</td>
                <td><b style="color: #fbcf00">A</b></td>
                <td><b style="color: #fbcf00">A</b></td>
            </tr>
            <tr>
                <td style="color: #60e090">Bez občianstva</td>
                <td><b style="color: #fb0000">V</b></td>
                <td><b style="color: #fb0000">V</b></td>
            </tr>
        </table>
    </form><br>
    <div style="clear:both"></div>
</div>