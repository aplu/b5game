@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="{{ route('tests') }}">Testy</a></div>
@else
<div class="mainTitle">Testy</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content">

    <div class="narrowBox">

        <ul>
            <li><a href="{{ route('test-session') }}">Session</a></li>
            <li><a href="{{ route('test-xhtml') }}">XHTML šablony</a></li>
        </ul>

        <h2>Simulátory</h2>
        <ul>
            <li><a href="{{ route('ecosim') }}">EcoSim</a></li>
            <li><a href="{{ route('battlesim') }}">BattleSim</a></li>
        </ul>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vestibulum, velit quis ornare imperdiet, ex est laoreet nisl, vitae ultricies erat felis ac augue. Phasellus ultricies urna et pellentesque condimentum. Mauris sed leo efficitur, accumsan neque ut, lacinia mauris. Donec tristique euismod egestas. Suspendisse convallis diam hendrerit felis aliquet, eu volutpat lacus imperdiet. Nullam nec pharetra lacus. Curabitur malesuada faucibus massa, accumsan sollicitudin ligula. Vivamus imperdiet, nisl sit amet eleifend maximus, eros lectus sollicitudin lacus, in congue nunc ipsum eget diam. Nullam sagittis tincidunt eros ac scelerisque. Nam volutpat neque at cursus pretium. Donec varius volutpat sapien, in vehicula sapien gravida sit amet. Quisque et sollicitudin sem. Vestibulum sodales placerat nibh sed sodales.</p>
        <p>Curabitur fermentum nec nisi id porttitor. Pellentesque consectetur sagittis accumsan. Cras nec pellentesque ex, quis interdum sapien. Nulla sit amet auctor ipsum, vel ultrices sapien. Nam a tellus a erat tempus ultrices. Vivamus rutrum risus non arcu convallis sagittis. Vestibulum fermentum sagittis magna, eu sagittis ante fermentum a.</p>
        <p>Maecenas vel nisi nec arcu semper tincidunt. Curabitur ultricies felis ut lectus blandit congue. Suspendisse ut viverra nunc, id imperdiet ante. Mauris viverra, sem non porta tempus, metus nulla dapibus lectus, fringilla consectetur nisi justo quis sem. Aenean commodo mattis erat, vel ultricies eros posuere non. Vestibulum eu nunc nec erat scelerisque accumsan eget nec nunc. Nullam at imperdiet dui. Quisque mollis aliquam eleifend. Donec ornare, urna ac mattis interdum, quam arcu iaculis leo, non consequat urna quam sit amet metus. Mauris posuere arcu quis turpis viverra, ac consequat diam aliquet. Pellentesque nec porttitor ex. Mauris at libero eu urna pulvinar scelerisque in sit amet ex.</p>
    </div>

    <br><br>
</div>
