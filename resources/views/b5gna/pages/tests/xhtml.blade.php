@if ($debug['all'] ?? false)
<div class="mainTitle">
	<a href="{{ route('tests') }}">{{ __('Tests') }}</a>
	{!! __('bc-delim') !!}
	<a href="{{ route('test-xhtml') }}">.mainTitle</a></div>
@else
<div class="mainTitle">
	<a href="{{ route('tests') }}">{{ __('Tests') }}</a>
	{!! __('bc-delim') !!}
	.mainTitle
</div>
@endif
<div class="mainTitleFade">.mainTitleFade</div>
@include('b5gna.help.__button')
<div class="content">
	.content

	<p>p <big>big</big> <b>b</b> <i>i</i> <u>u</u> <strong>strong</strong> <a href="#">link</a> <em>em</em> <span class="hl">.hl</span></p>

	<ul>
		<li>ul &gt; li</li>
	</ul>

	<div class="stInfoBox">
		.stInfoBox<br>

		<b class="hl">b.hl</b>
		lorem ipsum
		<b class="colResDark">b.colResDark</b>
		<span class="colTi">.colTi</span>
		<span class="colQ40">.colQ40</span>
		<span class="colCr">.colCr</span>

		<input type="submit" value="input[type=submit]">

		<div class="stItemRes">
			.stItemRes<br>

			<label class="resLabel">.resLabel</label>
		</div>
	</div>
	<div style="clear:both"></div>

	table.Tab.center { width: 100% }
	<table width="100%" cellspacing="0" class="Tab center">
		<tr>
			<th>th</th>
			<td>td</td>
			<td class="TabH1">td.TabH1</td>
		</tr>
	</table>

	<div class="errorBox">
		.errorBox
		<div class="colLight" style="margin-bottom:5px;">
			.colLight { margin-bottom:5px }
		</div>
		after .colLight<br>2nd line
	</div>


	<div class="window" style="font-size:12px;">
		.window
		<div class="windowCaption">.windowCaption</div>
			<div align="left" style="margin:10px;">
				<select name="select123">
					<option>select &gt; option</option>
				</select>
			</div>
		</div>
	</div>

	<div class="narrowBox">
		.narrowBox

		<div class="stInfoBox">.stInfoBox</div>

		table
		<table>
			<tr>
				<th>th</th>
				<td>td</td>
				<td class="TabH1">td.TabH1</td>
			</tr>
		</table>

		table.Tab.center { width: 100% }
		<table width="100%" cellspacing="0" class="Tab center">
			<tr>
				<th>th</th>
				<td>td</td>
				<td class="TabH1">td.TabH1</td>
			</tr>
		</table>

		hr { border:0; border-top:1px solid #5a135c }
		<hr style="border:0; border-top:1px solid #5a135c">
	</div>

	<div class="center" style="width:50%">
		div.center{width:50%}
		<div class="stInfoBox">.stInfoBox</div>
	</div>

	<div class="levelBox" id="lvl_ti">
		#lvl_ti.levelBox
		<div class="levelPerc">.levelPerc</div>
		<div class="levelLevel">.levelLevel</div>
	</div>
	<div style="clear:both"></div>

	<div class="stItem">
		.stItem
		<div class="stItemTitle">
			.stItemTitle
		</div>
		<div class="stItemRes">
			.stItemRes
			<label class="resLabel">label.resLabel</label>
		</div>
	</div>

	<div class="sideBox">
		.sideBox
		<div class="sidePanelTitle">.sidePanelTitle</div>
		<div class="sidePanel">
			.sidePanel

			<label class="sideLabel">label.sideLabel</label>

			<span class="colResDark">.colResDark</span>
			<b>b</b>
			<b style="color:red">b{color:red}</b>

			<div class="toolbar">
				.toolbar
				<a class="smallBTN" href="#"><div>a.smallBTN</div></a>
			</div>
		</div>

		<div class="sidePanelTitle">.sidePanelTitle</div>
		<div class="sidePanel smallText">
			.sidePanel.smallText
			<label class="sideLabel">label.sideLabel</label>
			<div class="toolbar">
				.toolbar
			</div>
		</div>
	</div>

	<div class="mainContentBox">
		.mainContentBox

		<div class="stInfoBox">.stInfoBox</div>

		<div class="errorBox">
			.mainContentBox .errorBox
			<div class="colLight" style="margin-bottom:5px;">
				.colLight { margin-bottom:5px }
			</div>
			after .colLight
		</div>
	</div>

	next
	<div style="clear:both"></div>
</div>
