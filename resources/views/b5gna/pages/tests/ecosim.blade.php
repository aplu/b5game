@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="{{ route('ecosim') }}">Ecosim</a></div>
@else
<div class="mainTitle">Ecosim</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <p style="text-align:center">Pro zadané těžební indexy sektoru vypočítá cenu Ti a Q40 vzhledem k Cr.</p>

    <form action="{{ route('calc-ecosim') }}" method="post">
    @csrf
    <table style="margin:0 auto">
    <tr>
        <td style="text-align:right">Ti Index</td>
        <td>
            <input type="text" name="index[ti]" value="{{ old('index.ti', $index_ti) }}">
            @error('index.ti')
                <br>{{ $message }}
            @enderror
        </td>
    </tr>
    <tr>
        <td style="text-align:right">Q40 Index</td>
        <td>
            <input type="text" name="index[q40]" value="{{ old('index.q40', $index_q40) }}">
            @error('index.q40')
                <br>{{ $message }}
            @enderror
        </td>
    </tr>
    <tr>
        <td style="text-align:right">Cr Index</td>
        <td>
            <input type="text" name="index[cr]" value="{{ old('index.cr', $index_cr) }}">
            @error('index.cr')
                <br>{{ $message }}
            @enderror
        </td>
    </tr>
    <tr>
        <td style="text-align:right">Skoků</td>
        <td>
            <input type="text" name="jumps" value="{{ old('jumps', $jumps) }}">
            @error('jumps')
                <br>{{ $message }}
            @enderror
        </td>
    </tr>
    <tr>
        <td style="text-align:right">Efekt TP (%)</td>
        <td>
            <input type="text" name="effect[tp]" value="{{ old('effect.tp', $effect_tp) }}">
            @error('effect.tp')
                <br>{{ $message }}
            @enderror
        </td>
    </tr>
    <tr>
        <td style="text-align:right">Efekt Raf (%)</td>
        <td>
            <input type="text" name="effect[raf]" value="{{ old('effect.raf', $effect_raf) }}">
            @error('effect.raf')
                <br>{{ $message }}
            @enderror
        </td>
    </tr>
    <tr>
        <td style="text-align:right">Efekt OC (%)</td>
        <td>
            <input type="text" name="effect[oc]" value="{{ old('effect.oc', $effect_oc) }}">
            @error('effect.oc')
                <br>{{ $message }}
            @enderror
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align:center"><input type="submit"></td>
    </tr>
    </table>
    </form>

    <div class="narrowBox" style="text-align:left">
        <div class="stInfoBox">
            @php
                $trips_per_day = 24 / (12 + $jumps*0.5);
                $tl_income = 3000 * $trips_per_day * $index_ti / 100;
                $ta_income = 500 * $trips_per_day * $index_q40 / 100;
                $ol_income = 1E6 * $trips_per_day * $index_cr / 100;

                $tp1 = 1000 * ($effect_tp / 100);
                $tl_amount = 1000 / $tl_income;
                $tp100 = 100 * $tp1;
            @endphp
            <strong>1 TL</strong> = <b class="colResLight">{{ amount($tl_income) }}</b> rTi/den<br>
            <strong>1 TA</strong> = <b class="colResLight">{{ amount($ta_income) }}</b> rQ40/den<br>
            <strong>1 OL</strong> = <b class="colResLight">{{ amount($ol_income) }}</b> Cr/den<br>
            <br>
            <table>
                <tr>
                    <th>Budova</th>
                    <th>Přírůstek</th>
                    <th>Doba výstavby</th>
                    <th>Upgrade místa</th>
                    <th>Výstavba hangarů + Upgrade místa</th>
                    <th>Čas celkem</th>
                </tr>
                <tr>
                    <th>100 TP</th>
                    <td>{{ amount($tp100) }} Ti</td>
                    <td>100 * 2/20 h = 10h</td>
                    <td>100 * 2/100 h = 2h</td>
                    <td>(100'000 rTi / {{ amount($tl_income) }} rTi = {{ round(100 * $tl_amount, 2) }} TL) * 1/10 h * 1.2 = {{ round(12 * $tl_amount, 2) }} h</td>
                    <td>10 + 2 + {{ round(12 * $tl_amount, 2) }} h = {{ round(10 + 2 + 12 * $tl_amount, 2) }} h</td>
                </tr>
                <tr>
                    <th>1 TP</th>
                    <td>{{ amount($tp1) }} Ti</td>
                    <td>2/20 h = 0,1 h</td>
                    <td>2/100 h = 0,01 h</td>
                    <td>(1'000 rTi / {{ amount($tl_income) }} rTi = {{ round($tl_amount, 4) }} TL) * 1/10 h * 1.2 = {{ round(0.12 * $tl_amount, 4) }} h</td>
                </tr>
            </table>

        </div>
    </div>

    <br><br>
</div>

@php
    // dump($request);
@endphp