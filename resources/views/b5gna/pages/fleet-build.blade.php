@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/fleet-build">Výroba flotily</a></div>
@else
<div class="mainTitle">Výroba flotily</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br>
    <table width="40%" class="Tab center">
        <tr>
            <th>Kapacita hangárov<br>voľné/celkovo</th>
            <th>Počet tovární</th>
        </tr>
        <tr style="color: #6984f9">
            <td>999/999</td>
            <td>999</td>
        </tr>
    </table>
    <br><br>

    <form action="" method="post">
        <table width="90%" cellspacing="0" class="Tab center">
            <tr>
                <td colspan="3">
                    <h2>Zadaj stavbu</h2>
                </td>
            </tr>
            <tr>
                <th width="150">Názov<br>[postavených]</th>
                <th width="150">Zadaj počet/meno</th>
                <th>Vo výstavbe</th>
            </tr>
            <tr>
                <td colspan="3"><br><b style="color:red;">Nemôžeš stavať flotilu. Nie je k dispozícii žiadna technológia.</b><br><br><a href="/technology">Prejsť do sekcie technológií</a><br><br></td>
            </tr>
    </form>
    </table><br><br>
    <div style="text-align: center; width: 75%;" class="center"></div>
    <div style="clear:both"></div>
</div>