@php
$title = 'Moje fórum';
$description = 'Popis fóra...';
$type = 0;	// 0 - private, 1 - offtopic

// edit
$id = 1;
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/forum/create">Vytvoriť nové osobné fórum</a></div>
@else
<div class="mainTitle">Vytvoriť nové osobné fórum</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

	@if (!($debug['all'] ?? false))
		@include('b5gna.menu.dummy-forum')
	@endif

<br>
<form action="" method="post">
	<input type="hidden" name="idx" value="2927">
	<table width="55%" cellspacing="0" class="Tab center">
		<tr>
			<th>Názov fóra</th>
		</tr>
		<tr>
			<td><input type="text" name="Name" value="{{ $title }}" maxlength="30" style="width:450px;"></td>
		</tr>
		<tr>
			<th>Popis fóra</th>
		</tr>
		<tr>
			<td><textarea cols="70" rows="6" name="Popis">{{ $description }}</textarea></td>
		</tr>
		<tr>
			<th>Kategória</th>
		</tr>
		<tr>
			<td>
				<input type="radio" name="kateg" value="0"{!! $type === 0 ? ' checked' : '' !!}> osobné fórum
				<input type="radio" name="kateg" value="1"{!! $type === 1 ? ' checked' : '' !!}> off-topic fórum
			</td>
		</tr>
		<tr>
			<td class="TabH1"><input type="submit" name="Send" value="Odoslať"></td>
		</tr>
	</table>
</form>
<br><br>

@if ($id)
<table width="55%" cellspacing="0" class="Tab center">
	<tr>
		<td colspan="4" class="TabH1"><h2>Prístupové práva</h2></td>
	</tr>
	<tr>
		<th>Typ</th>
		<th>Subjekt</th>
		<th>Právo</th>
		<th></th>
	</tr>
	<tr>
		<td>všetci</td>
		<td><strong class="hl">všetci hráči</strong></td>
		<td>čítať a písať</td>
		<td><a href="?idx={{ $id }}&delTyp=a&link=">zrušiť</a></td>
	</tr>
</table>
<br><br>

<form action="/forum/edit" method="post">
	<input type="hidden" name="idx" value="{{ $id }}">
	<table width="55%" cellspacing="0" class="Tab center">
		<tr>
			<th colspan="2">Pridať prístup na fórum</th>
		</tr>
		<tr>
			<td><b class="hl">Pridať všetkých</b></td>
			<td><input type="checkbox" name="AAll" value="true" class="checkbox"> Pridať všetkých hráčov</td>
		</tr>
		<tr>
			<td><b class="hl">Pridať hráča</b></td>
			<td><select name="AUser" style="width: 190px;">
				<option value="">--- vyber hráča ---</option>
				<option value="827">Zathras</option>
			</select></td>
		</tr>
		<tr>
			<td><b class="hl">Pridať všetkých občanov štátu</b></td>
			<td><select name="AState" style="width: 190px;">
				<option value="none">--- vyber štát ---</option>
				<option value="cenrep">Centaurská Republika</option>
				<option value="drakh">Kolónia Drakhov</option>
				<option value="techno">Kruh Technomágov</option>
				<option value="minbar">Minbarská Federácia</option>
				<option value="narn">Narnský Režim</option>
				<option value="ea">Pozemská Aliancia</option>
				<option value="">Bez občianstva</option>
			</select></td>
		</tr>
		<tr>
			<td><b class="hl">Pridať členov vlády štátu</b></td>
			<td><select name="AGov" style="width: 190px;">
				<option value="none">--- vyber štát---</option>
				<option value="cenrep">Centaurská Republika</option>
				<option value="drakh">Kolónia Drakhov</option>
				<option value="techno">Kruh Technomágov</option>
				<option value="minbar">Minbarská Federácia</option>
				<option value="narn">Narnský Režim</option>
				<option value="ea">Pozemská Aliancia</option>
				<option value="">Bez občianstva</option>
			</select></td>
		</tr>
		<tr>
			<td><b class="hl">Typ prístupu</b></td>
			<td><select name="AType" style="width: 190px;">
				<option value="read">Len čítať</option>
				<option value="write">Čítať a písať</option>
			</select></td>
		</tr>
		<tr>
			<td colspan="2" class="TabH1"><input type="submit" name="ASend" value="Pridať prístup"></td>
		</tr>
	</table>
</form>
<br><br>

<form action="/forum/edit" method="post">
	<input type="hidden" name="idx" value="2927">
	<table width="55%" cellspacing="0" class="Tab center">
		<tr>
			<th>Zmazať fórum a jeho obsah</th>
		</tr>
		<tr>
			<td style="color:red;">Táto funkcia je nevratná!!!</td>
		</tr>
		<tr>
			<td class="TabH1"><input type="submit" name="DSend" value="Nenávratne zmazať fórum a jeho obsah"></td>
		</tr>
	</table>
</form>
<br><br>

@endif
	<div style="clear:both"></div>

</div>