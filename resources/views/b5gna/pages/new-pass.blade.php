@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/new-pass">Nové heslo</a></div>
@else
<div class="mainTitle">Nové heslo</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

	<br>
	<br>V prípade, že si zabudol/a svoje heslo, na tejto stránke si môžeš<br> nechať vygenerovať nové a poslať ho na svoj e-mail.<br>
	<br>
	<br>
	<br>

	<form action="" method="post">
		Tvoj login: <input type="text" name="NICK" maxlength="20">
		<input type="submit" name="Send" value="Odoslať">
	</form>

	<div style="clear:both"></div>
</div>