@php

$strategies = [
    '1;2;3;4' => 'S, K, L, D',
    '1;2;4;3' => 'S, K, D, L',
    '1;3;2;4' => 'S, L, K, D',
    '1;3;4;2' => 'S, L, D, K',
    '1;4;2;3' => 'S, D, K, L',
    '1;4;3;2' => 'S, D, L, K',
    '2;1;3;4' => 'K, S, L, D',
    '2;1;4;3' => 'K, S, D, L',
    '2;3;1;4' => 'K, L, S, D',
    '2;3;4;1' => 'K, L, D, S',
    '2;4;1;3' => 'K, D, S, L',
    '2;4;3;1' => 'K, D, L, S',
    '3;1;2;4' => 'L, S, K, D',
    '3;1;4;2' => 'L, S, D, K',
    '3;2;1;4' => 'L, K, S, D',
    '3;2;4;1' => 'L, K, D, S',
    '3;4;1;2' => 'L, D, S, K',
    '3;4;2;1' => 'L, D, K, S',
    '4;1;2;3' => 'D, S, K, L',
    '4;1;3;2' => 'D, S, L, K',
    '4;2;1;3' => 'D, K, S, L',
    '4;2;3;1' => 'D, K, L, S',
    '4;3;1;2' => 'D, L, S, K',
    '4;3;2;1' => 'D, L, K, S',
];

@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/fleet-wings">Správa letiek</a></div>
@else
<div class="mainTitle">Správa letiek</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <script language="JavaScript">
        SetCount = function(x) {
        	document.postform.Count.value=x;  
          }
    </script>
    <br>
    <form action="" method="post" name="postform">
        <table width="90%" cellspacing="0" class="Tab center">
            <tr>
                <th width="40" rowspan="2">Sektor</th>
                <th rowspan="2">Názov</th>
                <th rowspan="2">Model</th>
                <th rowspan="2">Počet</th>
                <th rowspan="2">Skupina<br>bojová/ťažobná</th>
                <th rowspan="2">Stav</th>
                <th rowspan="2">Zásoby<br>kyslíka</th>
                <th rowspan="2">Presnosť</th>
                <th rowspan="2" width="90px">Stratégia</th>
                <th colspan="2">Presun</th>
            </tr>
            <tr>
                <th width="40">Odkiaľ</th>
                <th width="40">Kam</th>
            </tr>
@if (rand(0,1))
            <tr>
                <td colspan="11"><br>nemáš žiadne letky<br><br></td>
            </tr>
@else
            <tr>
                <td rowspan="2" style="background-color: #8a68a5; vertical-align:middle;"><a href="/sector/cet" style="color:white"><b>cet</b></a></td>
                <td>Letka 271</td>
                <td style="color: #6984f9">Thunderbolt</td>
                <td><b>12</b></td>
                <td>W I</td>
                <td><span style="color: #669999">v pokoji</span></td>
                <td style="color:lime;">ok</td>
                <td>50%</td>
                <td><select name="target[271]">
                        <option value="1;2;3;4">S, K, L, D</option>
                        <option value="1;2;4;3">S, K, D, L</option>
                        <option value="1;3;2;4">S, L, K, D</option>
                        <option value="1;3;4;2">S, L, D, K</option>
                        <option value="1;4;2;3">S, D, K, L</option>
                        <option value="1;4;3;2">S, D, L, K</option>
                        <option value="2;1;3;4">K, S, L, D</option>
                        <option value="2;1;4;3">K, S, D, L</option>
                        <option value="2;3;1;4">K, L, S, D</option>
                        <option value="2;3;4;1">K, L, D, S</option>
                        <option value="2;4;1;3">K, D, S, L</option>
                        <option value="2;4;3;1">K, D, L, S</option>
                        <option value="3;1;2;4">L, S, K, D</option>
                        <option value="3;1;4;2">L, S, D, K</option>
                        <option value="3;2;1;4">L, K, S, D</option>
                        <option value="3;2;4;1">L, K, D, S</option>
                        <option value="3;4;1;2">L, D, S, K</option>
                        <option value="3;4;2;1">L, D, K, S</option>
                        <option value="4;1;2;3">D, S, K, L</option>
                        <option value="4;1;3;2">D, S, L, K</option>
                        <option value="4;2;1;3">D, K, S, L</option>
                        <option value="4;2;3;1">D, K, L, S</option>
                        <option value="4;3;1;2">D, L, S, K</option>
                        <option value="4;3;2;1" SELECTED>D, L, K, S</option>
                    </select></td>
                <td><input type="radio" name="from" value="271" onClick="SetCount(12);"></td>
                <td><input type="radio" name="to" value="271"></td>
            </tr>
            <tr>
                <td>Letka 316</td>
                <td style="color: #6984f9">Thunderbolt</td>
                <td><b>12</b></td>
                <td>W I</td>
                <td><span style="color: #669999">v pokoji</span></td>
                <td style="color:lime;">ok</td>
                <td>50%</td>
                <td><select name="target[316]">
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach
                    </select></td>
                <td><input type="radio" name="from" value="316" onClick="SetCount(12);"></td>
                <td><input type="radio" name="to" value="316"></td>
            </tr>
            <tr>
                <td rowspan="1" style="background-color: #8a68a5; vertical-align:middle;"><a href="/sector/tes" style="color:white"><b>tes</b></a></td>
                <td>Letka 516</td>
                <td style="color: #6984f9">Starfury</td>
                <td><b>1</b></td>
                <td>tt</td>
                <td><span style="color: #44dd66">ťažba</span></td>
                <td style="color:lime;">ok</td>
                <td>50%</td>
                <td><select name="target[516]">
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach
                    </select></td>
                <td colspan="2" style="color: red;">nepovolené</td>
            </tr>
            <tr>
                <td rowspan="1" style="background-color: #6984f9; vertical-align:middle;"><a href="/sector/ear" style="color:white"><b>ear</b></a></td>
                <td>Letka 443</td>
                <td style="color: #6984f9">Thunderbolt</td>
                <td><b>12</b></td>
                <td>
                    <font color="lime">voľná letka</font>
                </td>
                <td><span style="color: #669999">v pokoji</span></td>
                <td><span id="f443" title="01 Feb - 20:07"></span>
                    <script language="JavaScript">
    CountDown('f443', 31611)
                    </script>
                </td>
                <td>50%</td>
                <td><select name="target[443]">
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach
                    </select></td>
                <td colspan="2" style="color: red;">nepovolené</td>
            </tr>
            <tr>
                <td rowspan="2" style="background-color: #; vertical-align:middle;"><a href="/sector/HYP" style="color:white"><b>HYP</b></a></td>
                <td>Letka 411</td>
                <td style="color: #6984f9">Thunderbolt</td>
                <td><b>12</b></td>
                <td>W V</td>
                <td><span style="color: #ff99ff">v hyperpriestore</span></td>
                <td style="color:lime;">ok</td>
                <td>50%</td>
                <td><select name="target[411]">
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach
                    </select></td>
                <td colspan="2" style="color: red;">nepovolené</td>
            </tr>
            <tr>
                <td>Letka 425</td>
                <td style="color: #6984f9">Thunderbolt</td>
                <td><b>12</b></td>
                <td>W V</td>
                <td><span style="color: #ff99ff">v hyperpriestore</span></td>
                <td style="color:lime;">ok</td>
                <td>50%</td>
                <td><select name="target[425]">
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach
                    </select></td>
                <td colspan="2" style="color: red;">nepovolené</td>
            </tr>
            <tr>
                <th colspan="4">Do novej letky - názov</th>
                <td colspan="6" class="TabH1"><input type="text" name="NewWing" value="Letka" maxlength="35" style="width: 200px;"></td>
                <td><input type="radio" name="to" value="new" class="checkbox"></td>
            </tr>
            <tr>
                <th colspan="2">Počet stíhačov</th>
                <td colspan="5" class="TabH1"><input type="text" name="Count" value="" maxlength="4" style="width:45px;">&nbsp;&nbsp;<input type="submit" name="Send" value="Presunúť stíhače medzi letkami"></td>
                <td colspan="5" class="TabH1"><input type="submit" name="SendS" value="Zmeniť stratégiu"></td>
            </tr>
@endif
        </table>
    </form><br><br>
    <div style="clear:both"></div>
</div>