@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/miners">Výroba ťažby</a></div>
@else
<div class="mainTitle">Výroba ťažby</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br>
    <table width="60%" class="Tab center">
        <tr>
            <th>Kapacita hangárov<br>voľné/celkovo</th>
            <th>Štátny limit ťažby<br>voľný/celkový</th>
            <th>Limit obchodných lodí<br>voľný/celkový</th>
            <th>Počet tovární</th>
        </tr>
        <tr style="color: #6984f9">
            <td>100/1000</td>
            <td>999/9999</td>
            <td>100/1000</td>
            <td>999</td>
        </tr>
    </table><br><br>
    <form action="" method="post">
        <table width="80%" class="Tab center">
            <tr style="font-size: 12px; color:red;">
                <td colspan="4"><b>Zadaj stavbu</b></td>
            </tr>
            <tr>
                <th width="100">Názov</th>
                <th width="160">Stav<br>(ťažba/voľných/výstavba)</th>
                <th width="150">Zadaj počet</th>
                <th>Poznámka</th>
            </tr>
            <tr>
                <td>Ťažobné lode</td>
                <td>999 (999/0/0)</td>
@if (isset($inProgress))                
                <td><input type="text" name="build[1]" value="0"></td>
                <td></td>
@else
                <td>-</td>
                <td>prebieha výroba: <span id="t0" title="24 Oct - 22:17"></span>
                    <script> CountDown('t0', 3) </script>
                </td>
@endif
            </tr>
            <tr>
                <td>Tankery</td>
                <td>999 (999/0/0)</td>
                <td><input type="text" name="build[3]" value="0"></td>
                <td></td>
            </tr>
            <tr>
                <td>Obchodné lode</td>
                <td>999 (999/0/0)</td>
                <td><input type="text" name="build[2]" value="0"></td>
                <td></td>
            </tr>
            <tr>
                <td>Ťažobné plošiny</td>
                <td>999 (999/0/0)</td>
                <td>-</td>
                <td>na stavbu potrebuješ vesmírny dok</td>
            </tr>
            <tr>
                <td colspan="4"><input type="submit" value="Postaviť / Zbúrať"></td>
            </tr>
    </form>
    </table><br><br><br>

    <table width="80%" class="Tab2 center">
        <tr>
            <td colspan="6" class="TabH2">
                <h2>Parametre ťažobných lodí</h2>
            </td>
        </tr>
        <tr>
            <th width="120">Názov</th>
            <th>Titánium</th>
            <th>Quantium 40</th>
            <th>Kredity</th>
            <th>Búranie (kredity)</th>
        </tr>
        <tr>
            <td>Ťažobné lode</td>
            <td>350</td>
            <td>0</td>
            <td>400'000</td>
            <td>0</td>
        </tr>
        <tr>
            <td>Tankery</td>
            <td>600</td>
            <td>0</td>
            <td>600'000</td>
            <td>0</td>
        </tr>
        <tr>
            <td>Obchodné lode</td>
            <td>250</td>
            <td>0</td>
            <td>500'000</td>
            <td>0</td>
        </tr>
        <tr>
            <td>Ťažobné plošiny</td>
            <td>3'500</td>
            <td>500</td>
            <td>2'000'000</td>
            <td>0</td>
        </tr>
    </table><br><br>
    <div style="clear:both"></div>
</div>