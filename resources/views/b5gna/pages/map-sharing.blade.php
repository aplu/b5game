@php
$shareTo = ['ea'];
$shareFrom = [];
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/map-sharing">Nastavenie zdieľania informácií o sektoroch</a></div>
@else
<div class="mainTitle">Nastavenie zdieľania informácií o sektoroch</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-map')
    @endif

    <br><br>
    <div style="width:50%; float: left;">
        <table class="Tab2 center" style="width: 75%">
            <tr>
                <td colspan="3" class="TabH2">
                    <h2>Informácie sa zdieľajú s</h2>
                </td>
            </tr>
            <tr>
                <th>Typ</th>
                <th>Názov</th>
                <th></th>
            </tr>
@if (empty($shareTo))
            <tr>
                <td colspan="3">žiadne zdieľanie</td>
            </tr>
@else
            <tr>
                <td>štát</td>
                <td style="color:#6984f9">Pozemská Aliancia</td>
                <td style="padding:0px;">
                    <a href="?action=del&id=ea" title="Zrušiť zdieľanie"><img src="/images/b5gna/buttons/delete2.png" alt=""></a>
                </td>
            </tr>
@endif
        </table><br><br>
@if (empty($shareTo))
        <form action="/map-sharing" method="post">
            <table width="85%" cellspacing="0" class="Tab center">
                <tr>
                    <th colspan="2">Pridať zdieľanie informácií</th>
                </tr>
                <tr>
                    <td colspan="2" class="TabH1"><input type="submit" name="SendMyState" value="Zdieľať informácie s mojím štátom"></td>
                </tr>
            </table>
        </form><br><br>
@endif
    </div>
    <div style="width:50%; float: left;">
        <table class="Tab2 center" style="width: 75%">
            <tr>
                <td colspan="3" class="TabH2">
                    <h2>Informácie sa zdieľaju od</h2>
                </td>
            </tr>
            <tr>
                <th>Typ</th>
                <th>Názov</th>
                <th>Od</th>
            </tr>
@if (empty($shareFrom))
            <tr>
                <td colspan="3">žiadne zdieľanie</td>
            </tr>
@endif
        </table>
    </div>

    <div style="clear:both"></div><br>
</div>