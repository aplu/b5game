@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/citizens">Prehľad občanov štátu</a></div>
@else
<div class="mainTitle">Prehľad občanov štátu</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

    <br>
    <table width="97%" cellspacing="0" class="Tab center">
        <tr>
            <th>Meno</th>
            <th>Rasa</th>
            <th>Level</th>
            <th>Zdieľa<br>informácie</th>
            <th>Postavenie</th>
            <th>Strana/rod<br>kasta/kmeň</th>
            <th>Telefón</th>
            <th>Skype</th>
            <th width="75">ICQ</th>
        </tr>
        <tr>
            <td style="color: #6984f9">Apokalips</td>
            <td style="color: #6984f9">Ľudia</td>
            <td class="TabH1">0</td>
            <td><b style="color:lime">áno</b></td>
            <td>minister zahraničia</td>
            <td>NIE-My budeme vladnut :D</td>
            <td>+420123456789</td>
            <td>apokalips_cz</td>
            <td></td>
        </tr>
        <tr>
            <td style="color: #6984f9">aragok</td>
            <td style="color: #6984f9">Ľudia</td>
            <td class="TabH1"><b class="hl">1</b></td>
            <td><b style="color:lime">áno</b></td>
            <td>minister vnútra</td>
            <td>ANO - Budeme vládnout </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="color: #6984f9">Gaterinka</td>
            <td style="color: #6984f9">Ľudia</td>
            <td class="TabH1">0</td>
            <td><b style="color:lime">áno</b></td>
            <td>občan</td>
            <td>-</td>
            <td></td>
            <td>gaterinka</td>
            <td></td>
        </tr>
        <tr>
            <td style="color: #6984f9">GimliCZ</td>
            <td style="color: #6984f9">Ľudia</td>
            <td class="TabH1">0</td>
            <td><b style="color:lime">áno</b></td>
            <td>občan</td>
            <td>-</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="color: #6984f9">jjenda</td>
            <td style="color: #6984f9">Ľudia</td>
            <td class="TabH1"><b class="hl">1</b></td>
            <td><b style="color:lime">áno</b></td>
            <td>prezident</td>
            <td>ANO - Budeme vládnout <br>predseda</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="color: #6984f9">Rio</td>
            <td style="color: #6984f9">Ľudia</td>
            <td class="TabH1"><b class="hl">1</b></td>
            <td><b style="color:lime">áno</b></td>
            <td>minister financií</td>
            <td>NIE-My budeme vladnut :D<br>predseda</td>
            <td>+421 123 456 789</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="color: #6984f9">TomiYalkz</td>
            <td style="color: #6984f9">Ľudia</td>
            <td class="TabH1">0</td>
            <td><span style="color:red">nie</span></td>
            <td>občan</td>
            <td>-</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="color: #6984f9">Johny</td>
            <td style="color: #6984f9">Ľudia</td>
            <td class="TabH1">0</td>
            <td><span style="color:red">nie</span></td>
            <td>občan</td>
            <td>-</td>
            <td>&#128245; &#128580;</td>
            <td>&#129314; &#128465;</td>
            <td>260950617</td>
        </tr>
        <tr>
            <td style="color: #6984f9">zdroj</td>
            <td style="color: #6984f9">Ľudia</td>
            <td class="TabH1">0</td>
            <td><b style="color:lime">áno</b></td>
            <td>poslanec parlamentu</td>
            <td>ANO - Budeme vládnout </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <br>
    <div style="clear:both"></div>
</div>