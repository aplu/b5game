@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/crystals">TM kryštály</a></div>
@else
<div class="mainTitle">TM kryštály</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content">

    <br><br>
    <table class="Tab2" style="width: 280px; float:left; margin-left:18px;">
        <tr>
            <td colspan="3" class="TabH2">
                <h2>TOP10: celkom získaných</h2>
            </td>
        </tr>
        <tr>
            <th width="45">Poradie</th>
            <th>Meno</th>
            <th width="45">Počet</th>
        </tr>
        <tr>
            <td>1</td>
            <td style="color: #a03b37;">popocatepetI</td>
            <td class="hl"><b>61</b></td>
        </tr>
        <tr>
            <td>2</td>
            <td style="color: #dd9090;">Jerry</td>
            <td class="hl"><b>60</b></td>
        </tr>
        <tr>
            <td>3</td>
            <td style="color: #8a68a5;">DusKE</td>
            <td class="hl"><b>56</b></td>
        </tr>
        <tr>
            <td>4</td>
            <td style="color: #;">Defender</td>
            <td class="hl"><b>55</b></td>
        </tr>
        <tr>
            <td>5</td>
            <td style="color: #30bba9;">Shaade</td>
            <td class="hl"><b>55</b></td>
        </tr>
        <tr>
            <td>6</td>
            <td style="color: #dd9090;">Miso</td>
            <td class="hl"><b>54</b></td>
        </tr>
        <tr>
            <td>7</td>
            <td style="color: #8a68a5;">ShalTok</td>
            <td class="hl"><b>54</b></td>
        </tr>
        <tr>
            <td>8</td>
            <td style="color: #787811;">Ragnarök</td>
            <td class="hl"><b>51</b></td>
        </tr>
        <tr>
            <td>9</td>
            <td style="color: #8a68a5;">Richard B. Riddick</td>
            <td class="hl"><b>49</b></td>
        </tr>
        <tr>
            <td>10</td>
            <td style="color: #787811;">aquarius</td>
            <td class="hl"><b>49</b></td>
        </tr>
        <tr>
            <td>128</td>
            <td style="color: #6984f9;">wajrou</td>
            <td class="hl"><b>10</b></td>
        </tr>
    </table>
    <table class="Tab2" style="width: 280px; float:left; margin-left:30px;">
        <tr>
            <td colspan="3" class="TabH2">
                <h2>TOP10: získaných v aktuálnom veku</h2>
            </td>
        </tr>
        <tr>
            <th width="45">Poradie</th>
            <th>Meno</th>
            <th width="45">Počet</th>
        </tr>
        <tr>
            <td>1</td>
            <td style="color: #8a68a5;">hraničář</td>
            <td class="hl"><b>1</b></td>
        </tr>
        <tr>
            <td>2</td>
            <td style="color: #8a68a5;">Maverick</td>
            <td class="hl"><b>1</b></td>
        </tr>
        <tr>
            <td>3</td>
            <td style="color: #6984f9;">Itachi</td>
            <td class="hl"><b>1</b></td>
        </tr>
        <tr>
            <td>4</td>
            <td style="color: #dd9090;">Anne</td>
            <td class="hl"><b>1</b></td>
        </tr>
        <tr>
            <td>5</td>
            <td style="color: #787811;">Velký teplý Al</td>
            <td class="hl"><b>1</b></td>
        </tr>
        <tr>
            <td>6</td>
            <td style="color: #a03b37;">tlčhuba</td>
            <td class="hl"><b>1</b></td>
        </tr>
        <tr>
            <td>7</td>
            <td style="color: #dd9090;">Dark One</td>
            <td class="hl"><b>1</b></td>
        </tr>
        <tr>
            <td>8</td>
            <td style="color: #8a68a5;">voloda1024</td>
            <td class="hl"><b>1</b></td>
        </tr>
        <tr>
            <td>9</td>
            <td style="color: #a03b37;">ANALfabet_Hubakuk</td>
            <td class="hl"><b>1</b></td>
        </tr>
        <tr>
            <td>10</td>
            <td style="color: #6984f9;">TomiYalkz</td>
            <td class="hl"><b>1</b></td>
        </tr>
        <tr>
            <td>24</td>
            <td style="color: #6984f9;">wajrou</td>
            <td class="hl"><b>1</b></td>
        </tr>
    </table>
    <table class="Tab2" style="width: 280px; float:left; margin-left:30px;">
        <tr>
            <td colspan="3" class="TabH2">
                <h2>TOP10: priemer za vek</h2>
            </td>
        </tr>
        <tr>
            <th width="45">Poradie</th>
            <th>Meno</th>
            <th width="45">Počet</th>
        </tr>
        <tr>
            <td>1</td>
            <td style="color: #;">adokon</td>
            <td class="hl"><b>3.0000</b></td>
        </tr>
        <tr>
            <td>2</td>
            <td style="color: #;">Thar</td>
            <td class="hl"><b>2.8000</b></td>
        </tr>
        <tr>
            <td>3</td>
            <td style="color: #a03b37;">Kakarrot</td>
            <td class="hl"><b>2.6875</b></td>
        </tr>
        <tr>
            <td>4</td>
            <td style="color: #dd9090;">Dark One</td>
            <td class="hl"><b>2.5385</b></td>
        </tr>
        <tr>
            <td>5</td>
            <td style="color: #;">Witchery</td>
            <td class="hl"><b>2.5000</b></td>
        </tr>
        <tr>
            <td>6</td>
            <td style="color: #;">Defender</td>
            <td class="hl"><b>2.4762</b></td>
        </tr>
        <tr>
            <td>7</td>
            <td style="color: #787811;">Velký teplý Al</td>
            <td class="hl"><b>2.3636</b></td>
        </tr>
        <tr>
            <td>8</td>
            <td style="color: #dd9090;">Jerry</td>
            <td class="hl"><b>2.3600</b></td>
        </tr>
        <tr>
            <td>9</td>
            <td style="color: #;">roli</td>
            <td class="hl"><b>2.3333</b></td>
        </tr>
        <tr>
            <td>10</td>
            <td style="color: #;">dialer</td>
            <td class="hl"><b>2.3333</b></td>
        </tr>
        <tr>
            <td>151</td>
            <td style="color: #6984f9;">wajrou</td>
            <td class="hl"><b>1.2857</b></td>
        </tr>
    </table>
    <div style="clear:both;"></div><br><br>
    <table width="60%" cellspacing="0" class="Tab center">
        <tr>
            <td colspan="3">
                <h2>Denník získavania a použitia TM kryštálov</h2>
            </td>
        </tr>
        <tr>
            <th>Čas</th>
            <th>TM kryštály</th>
            <th>Pohyb</th>
        </tr>
        <tr>
            <td class="TabH1">16 Oct 2273 - 13:53:11</td>
            <td style='color:lime'>1</td>
            <td class="TabH1" style="text-align:left;"><b class="hl">zapojenie do veku:</b> Body IV.</td>
        </tr>
        <tr>
            <td class="TabH1">12 Jan 2273 - 18:57:40</td>
            <td style='color:lime'>1</td>
            <td class="TabH1" style="text-align:left;"><b class="hl">zapojenie do veku:</b> Body II.</td>
        </tr>
        <tr>
            <td class="TabH1">01 Apr 2272 - 06:31:58</td>
            <td style='color:lime'>1</td>
            <td class="TabH1" style="text-align:left;"><b class="hl">zapojenie do veku:</b> Územie V.</td>
        </tr>
        <tr>
            <td class="TabH1">12 Sep 2271 - 10:16:09</td>
            <td style='color:lime'>2</td>
            <td class="TabH1" style="text-align:left;"><b class="hl">umiestnenie vo veku (bonus od vlády)</b> </td>
        </tr>
        <tr>
            <td class="TabH1">12 Jul 2271 - 22:28:31</td>
            <td style='color:red'>-2</td>
            <td class="TabH1" style="text-align:left;"><b class="hl">zvýšenie indexu</b> </td>
        </tr>
        <tr>
            <td class="TabH1">22 Jun 2271 - 21:53:14</td>
            <td style='color:lime'>1</td>
            <td class="TabH1" style="text-align:left;"><b class="hl">zapojenie do veku:</b> Aktivita IV.</td>
        </tr>
        <tr>
            <td class="TabH1">27 Apr 2271 - 23:52:06</td>
            <td style='color:lime'>1</td>
            <td class="TabH1" style="text-align:left;"><b class="hl">zapojenie do veku:</b> 2vs2 IV.</td>
        </tr>
        <tr>
            <td class="TabH1">22 Apr 2271 - 23:47:23</td>
            <td style='color:lime'>1</td>
            <td class="TabH1" style="text-align:left;"><b class="hl">zapojenie do veku:</b> Územie IV.</td>
        </tr>
        <tr>
            <td class="TabH1">17 Mar 2269 - 04:56:03</td>
            <td style='color:lime'>1</td>
            <td class="TabH1" style="text-align:left;"><b class="hl">zapojenie do veku:</b> Body I.</td>
        </tr>
        <tr>
            <td class="TabH1">03 Dec 2265 - 01:59:20</td>
            <td style='color:lime'>1</td>
            <td class="TabH1" style="text-align:left;"><b class="hl">registrácia</b> </td>
        </tr>
    </table><br>
    <div style="clear:both"></div>
</div>