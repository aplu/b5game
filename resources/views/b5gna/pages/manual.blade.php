@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/manual">Manuál hry B5Game</a></div>
@else
<div class="mainTitle">Manuál hry B5Game</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

	<div class="narrowBox highLine" style="text-align: justify;">
		<h2 class="manual">Úvod do manuálu</h2>
		<div class="manualSub">
			<p>
			Vitaj na stránkach manuálu k hre <strong>Babylon 5 Game: New Age</strong>. Manuál je jedným zo šiestych miest v tejto hre, kde môžeš nájsť informácie o princípe, spôsoboch, postupoch a pravidlách hrania.
			Manuál sa zameriava najmä na vysvetlenie princípov virtuálneho sveta hry. Hra aj manuál sú neustále vo vývoji, preto môžu byť niektoré informácie v ňom uvedené neaktuálne alebo nekompletné.
			Preto odporúčame využiť aj ďalšie zdroje informácií:
			</p>
			
			<ul>
				<!--<li><strong>Úvod do hry</strong> - základné informácie o hre pre nováčikov. Klikni na tlačidlo nižšie.<br><br></li>-->

				<li><strong>Rýchly návod pre nováčikov</strong> - sprievodca prvými krokmi v hre. Klikni na tlačidlo nižšie.<br><br></li>

				<li><strong>Kontextová pomoc</strong> - nachádza sa na väčšine stránok hry. Obsahuje podrobné vysvetlenie funkcionality danej stránky a jej použitia. Z pravidla je aktuálna.
				Zobrazí sa po kliknutí na "? Pomoc" v pravom hornom rohu stránky (pod lištou s menu).<br><br></li>
				
				<li><strong>ChangeLog</strong> - obsahuje chronologický zoznam zmien, ktoré boli v hre vykonané. Otvorí sa cez hlavné menu.<br><br></li>
				
				<li><strong>Admin oznamy</strong> - informácie o dôležitých zmenách a pravidlách. Prístup cez Komunikácia -&gt; Admin oznamy<br><br> </li>
			
				<li><strong>Fórum pre nováčikov</strong> - miesto, kde sa môžeš opýtať na nejasnoti ostatných hráčov a administrátorov. Prístup cez Komunikácia -&gt; Pre nováčikov</li>
			</ul>
		</div>
	</div>

	<!--<form action="/intro" method="post"><input type="submit" value="Zobraziť úvod do hry"></form><br>-->

	<form action="/B5Game_rychly_navod.pdf" method="get" target="_blank"><input type="submit" value="Zobraziť rýchly návod pre nováčikov"></form><br>

	<form action="/dokuwiki" method="post" target="_blank"><input type="submit" value="Vstúpiť do manuálu"></form>

	<br><br><br>

	<div style="clear:both"></div>
</div>
