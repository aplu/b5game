@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/macroeco/graph">Makro ekonomika</a></div>
@else
<div class="mainTitle">Makro ekonomika</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

    <br>
    <div id="chart_macro1" style="width:850px; height:400px" class="center"></div><br><br>
    <div id="chart_macro1a" style="width:850px; height:400px" class="center"></div><br><br>
    <div id="chart_macro2" style="width:850px; height:300px" class="center"></div>
    <div id="chart_macro3" style="width:850px; height:300px" class="center"></div>
    <div id="chart_macro4" style="width:850px; height:300px" class="center"></div>
    <div id="chart_macro5" style="width:850px; height:300px" class="center"></div><br><br>
    <div id="chart_macro6" style="width:850px; height:400px" class="center"></div>

    <script type="text/javascript">
        google.load("visualization", "1", {packages:["corechart"]});
          google.setOnLoadCallback(drawChartMacro1);
          function drawChartMacro1() {
            var data = new google.visualization.DataTable();
            data.addColumn('number', 'deň');
            data.addColumn('number', 'Cr [mil.]');
            data.addColumn('number', 'Ti [tis.]');
            data.addColumn('number', 'Q40 [x100]');
            data.addColumn('number', 'rTi [tis.]');
            data.addColumn('number', 'rQ40 [tis.]');
        data.addRow([1,4,20,4,58,2]);data.addRow([2,10967,1376,354,80648,3156]);data.addRow([3,14546,2368,603,112443,4328]);data.addRow([4,14599,3162,712,114048,4406]);data.addRow([5,14653,3813,856,115305,4452]);data.addRow([6,14799,4484,1024,116880,4522]);data.addRow([7,15771,5114,1124,117839,4590]);data.addRow([8,16042,5530,1208,118624,4644]);data.addRow([9,16696,6024,1343,119567,4733]);data.addRow([10,18132,6403,1576,120104,4806]);data.addRow([11,18347,6982,1733,120772,4835]);data.addRow([12,18556,7591,1951,121273,4874]);data.addRow([13,18769,8004,2168,121707,4911]);data.addRow([14,19017,8540,2420,122796,4994]);data.addRow([15,19806,9438,2615,124157,5114]);data.addRow([16,20504,10181,2742,125079,5120]);data.addRow([17,20803,11116,3026,126219,5217]);data.addRow([18,21220,12313,3259,128504,5330]);data.addRow([19,21911,13418,3682,130733,5485]);data.addRow([20,22509,14361,4101,131196,5488]);data.addRow([21,23171,16173,4616,134227,9577]);data.addRow([22,24927,17891,4836,138745,13661]);data.addRow([23,26595,19808,5380,140362,13845]);data.addRow([24,28327,21407,5974,142906,13984]);data.addRow([25,29551,22936,6642,146447,14273]);data.addRow([26,29933,24376,7350,149997,14507]);data.addRow([27,30714,26458,7821,152403,14681]);data.addRow([28,31132,28869,8473,156159,22722]);data.addRow([29,31538,31219,8987,159138,22895]);data.addRow([30,31734,33194,9699,161771,23137]);data.addRow([31,32278,35364,10465,166357,23302]);data.addRow([32,32677,37252,11780,166450,23839]);data.addRow([33,33330,38875,12863,170004,24247]);data.addRow([34,33509,40446,14262,173378,24721]);data.addRow([35,34347,41995,15417,175390,24854]);data.addRow([36,34828,43675,15944,175085,24955]);data.addRow([37,35317,44763,17278,176642,25433]);data.addRow([38,35688,44951,17769,178499,25699]);data.addRow([39,36332,46257,18981,181127,25913]);data.addRow([40,36563,48031,20263,182379,26200]);data.addRow([41,36886,50050,21596,184022,26450]);data.addRow([42,37930,50097,22778,182328,26749]);data.addRow([43,38548,53471,24177,193337,26876]);data.addRow([44,39332,55091,24979,191332,27363]);data.addRow([45,39449,54383,24964,190054,27161]);data.addRow([46,39216,53920,25779,189235,27393]);data.addRow([47,40114,55712,26786,194422,27939]);data.addRow([48,40321,57169,27312,189419,28181]);data.addRow([49,40581,57306,28988,188256,28199]);data.addRow([50,68063,54736,29819,187876,28461]);data.addRow([51,74309,54839,30699,153173,22752]);
            var options = {
              title: 'Denná produkcia surovín',
              colors: ['#3366CC', '#ff9900', '#109618', '#dc3912', '#990099'],
              chartArea: {left: '6%', width: '80%', top: '15%', height: '70%'},
              fontName: 'Tahoma, Verdana, Geneva, Arial, Helvetica, sans-serif',
              fontSize: '13',
              backgroundColor: '#080808',
              focusTarget: 'category',
              titleTextStyle: {color: 'red', fontSize: '14'},
              legend: {textStyle: {color: '#f8f0ff'}},
              hAxis: {baselineColor: '#f8f0ff', gridlines: {color: '#444444'}, textStyle: {color: '#f8f0ff'}, titleTextStyle: {color: '#ffcc00'}, title: 'deň veku'},
              vAxis: {baselineColor: '#f8f0ff', gridlines: {color: '#444444'}, textStyle: {color: '#f8f0ff'}, titleTextStyle: {color: '#ffcc00'},}
            };
        
            var chart = new google.visualization.LineChart(document.getElementById('chart_macro1'));
            chart.draw(data, options);
            
        
        // =================================================    
            var data = new google.visualization.DataTable();
            data.addColumn('number', 'deň');
            data.addColumn('number', 'Cr');
            data.addColumn('number', 'Ti');
            data.addColumn('number', 'Q40');
            data.addColumn('number', 'rTi');
            data.addColumn('number', 'rQ40');
        data.addRow([1,0,0,0,0,0]);data.addRow([2,1096,57,17,1209,284]);data.addRow([3,1454,98,30,1686,389]);data.addRow([4,1459,131,35,1710,396]);data.addRow([5,1465,158,42,1729,400]);data.addRow([6,1479,186,51,1753,407]);data.addRow([7,1577,213,56,1767,413]);data.addRow([8,1604,230,60,1779,418]);data.addRow([9,1669,251,67,1793,425]);data.addRow([10,1813,266,78,1801,432]);data.addRow([11,1834,290,86,1811,435]);data.addRow([12,1855,316,97,1819,438]);data.addRow([13,1876,333,108,1825,441]);data.addRow([14,1901,355,121,1841,449]);data.addRow([15,1980,393,130,1862,460]);data.addRow([16,2050,424,137,1876,460]);data.addRow([17,2080,463,151,1893,469]);data.addRow([18,2122,513,162,1927,479]);data.addRow([19,2191,559,184,1960,493]);data.addRow([20,2250,598,205,1967,494]);data.addRow([21,2317,673,230,2013,861]);data.addRow([22,2492,745,241,2081,1229]);data.addRow([23,2659,825,269,2105,1246]);data.addRow([24,2832,891,298,2143,1258]);data.addRow([25,2955,955,332,2196,1284]);data.addRow([26,2993,1015,367,2249,1305]);data.addRow([27,3071,1102,391,2286,1321]);data.addRow([28,3113,1202,423,2342,2044]);data.addRow([29,3153,1300,449,2387,2060]);data.addRow([30,3173,1383,484,2426,2082]);data.addRow([31,3227,1473,523,2495,2097]);data.addRow([32,3267,1552,589,2496,2145]);data.addRow([33,3333,1619,643,2550,2182]);data.addRow([34,3350,1685,713,2600,2224]);data.addRow([35,3434,1749,770,2630,2236]);data.addRow([36,3482,1819,797,2626,2245]);data.addRow([37,3531,1865,863,2649,2288]);data.addRow([38,3568,1872,888,2677,2312]);data.addRow([39,3633,1927,949,2716,2332]);data.addRow([40,3656,2001,1013,2735,2358]);data.addRow([41,3688,2085,1079,2760,2380]);data.addRow([42,3793,2087,1138,2734,2407]);data.addRow([43,3854,2227,1208,2900,2418]);data.addRow([44,3933,2295,1248,2869,2462]);data.addRow([45,3944,2265,1248,2850,2444]);data.addRow([46,3921,2246,1288,2838,2465]);data.addRow([47,4011,2321,1339,2916,2514]);data.addRow([48,4032,2382,1365,2841,2536]);data.addRow([49,4058,2387,1449,2823,2537]);data.addRow([50,6806,2280,1490,2818,2561]);data.addRow([51,7430,2284,1534,2297,2047]);
            var options = {
              title: 'Denná produkcia surovín v UU [mil.]',
              colors: ['#3366CC', '#ff9900', '#109618', '#dc3912', '#990099'],
              chartArea: {left: '6%', width: '80%', top: '15%', height: '70%'},
              fontName: 'Tahoma, Verdana, Geneva, Arial, Helvetica, sans-serif',
              fontSize: '13',
              backgroundColor: '#080808',
              focusTarget: 'category',
              titleTextStyle: {color: 'red', fontSize: '14'},
              legend: {textStyle: {color: '#f8f0ff'}},
              hAxis: {baselineColor: '#f8f0ff', gridlines: {color: '#444444'}, textStyle: {color: '#f8f0ff'}, titleTextStyle: {color: '#ffcc00'}, title: 'deň veku'},
              vAxis: {baselineColor: '#f8f0ff', gridlines: {color: '#444444'}, textStyle: {color: '#f8f0ff'}, titleTextStyle: {color: '#ffcc00'},}
            };
        
            var chart = new google.visualization.LineChart(document.getElementById('chart_macro1a'));
            chart.draw(data, options);
            
        
        // =================================================   
            var data = new google.visualization.DataTable();
            data.addColumn('number', 'deň');
            data.addColumn('number', 'ekonomika');
            data.addColumn({type:'boolean',role:'certainty'});
            data.addColumn('number', 'burza');
            data.addColumn({type:'boolean',role:'certainty'});
        data.addRow([1,416.7,true,null,false]);data.addRow([2,416.7,true,null,false]);data.addRow([3,416.7,true,null,false]);data.addRow([4,416.7,true,null,false]);data.addRow([5,416.7,true,null,false]);data.addRow([6,416.7,true,null,false]);data.addRow([7,416.7,true,null,false]);data.addRow([8,416.7,true,null,false]);data.addRow([9,416.7,true,null,false]);data.addRow([10,416.7,true,null,false]);data.addRow([11,416.7,true,null,false]);data.addRow([12,416.7,true,null,false]);data.addRow([13,416.7,true,null,false]);data.addRow([14,416.7,true,null,false]);data.addRow([15,416.7,true,null,false]);data.addRow([16,416.7,true,null,false]);data.addRow([17,416.7,true,null,false]);data.addRow([18,416.7,true,null,false]);data.addRow([19,416.7,true,null,false]);data.addRow([20,416.7,true,null,false]);data.addRow([21,416.7,true,null,false]);data.addRow([22,416.7,true,null,false]);data.addRow([23,416.7,true,null,false]);data.addRow([24,416.7,true,null,false]);data.addRow([25,416.7,true,null,false]);data.addRow([26,416.7,true,null,false]);data.addRow([27,416.7,true,null,false]);data.addRow([28,416.7,true,null,false]);data.addRow([29,416.7,true,null,false]);data.addRow([30,416.7,true,null,false]);data.addRow([31,416.7,true,null,false]);data.addRow([32,416.7,true,null,false]);data.addRow([33,416.7,true,null,false]);data.addRow([34,416.7,true,null,false]);data.addRow([35,416.7,true,null,false]);data.addRow([36,416.7,true,null,false]);data.addRow([37,416.7,true,null,false]);data.addRow([38,416.7,true,null,false]);data.addRow([39,416.7,true,null,false]);data.addRow([40,416.7,true,null,false]);data.addRow([41,416.7,true,null,false]);data.addRow([42,416.7,true,null,false]);data.addRow([43,416.7,true,null,false]);data.addRow([44,416.7,true,null,false]);data.addRow([45,416.7,true,null,false]);data.addRow([46,416.7,true,null,false]);data.addRow([47,416.7,true,null,false]);data.addRow([48,416.7,true,null,false]);data.addRow([49,416.7,true,null,false]);data.addRow([50,416.7,true,null,false]);data.addRow([51,416.7,true,null,false]);    options.title='Kurz Ti/Cr';
            options.colors=['#ff9900', '#995500'];
        //    options.legend.position='none';
            var chart = new google.visualization.LineChart(document.getElementById('chart_macro2'));
            chart.draw(data, options);
            
        // =================================================    
            var data = new google.visualization.DataTable();
            data.addColumn('number', 'deň');
            data.addColumn('number', 'ekonomika');
            data.addColumn({type:'boolean',role:'certainty'});
            data.addColumn('number', 'burza');
            data.addColumn({type:'boolean',role:'certainty'});
        data.addRow([1,5000,true,null,false]);data.addRow([2,5000,true,null,false]);data.addRow([3,5000,true,null,false]);data.addRow([4,5000,true,null,false]);data.addRow([5,5000,true,null,false]);data.addRow([6,5000,true,null,false]);data.addRow([7,5000,true,null,false]);data.addRow([8,5000,true,null,false]);data.addRow([9,5000,true,null,false]);data.addRow([10,5000,true,null,false]);data.addRow([11,5000,true,null,false]);data.addRow([12,5000,true,null,false]);data.addRow([13,5000,true,null,false]);data.addRow([14,5000,true,null,false]);data.addRow([15,5000,true,null,false]);data.addRow([16,5000,true,null,false]);data.addRow([17,5000,true,null,false]);data.addRow([18,5000,true,null,false]);data.addRow([19,5000,true,null,false]);data.addRow([20,5000,true,null,false]);data.addRow([21,5000,true,null,false]);data.addRow([22,5000,true,null,false]);data.addRow([23,5000,true,null,false]);data.addRow([24,5000,true,null,false]);data.addRow([25,5000,true,null,false]);data.addRow([26,5000,true,null,false]);data.addRow([27,5000,true,null,false]);data.addRow([28,5000,true,null,false]);data.addRow([29,5000,true,null,false]);data.addRow([30,5000,true,null,false]);data.addRow([31,5000,true,null,false]);data.addRow([32,5000,true,null,false]);data.addRow([33,5000,true,null,false]);data.addRow([34,5000,true,null,false]);data.addRow([35,5000,true,null,false]);data.addRow([36,5000,true,null,false]);data.addRow([37,5000,true,null,false]);data.addRow([38,5000,true,null,false]);data.addRow([39,5000,true,null,false]);data.addRow([40,5000,true,null,false]);data.addRow([41,5000,true,null,false]);data.addRow([42,5000,true,null,false]);data.addRow([43,5000,true,null,false]);data.addRow([44,5000,true,null,false]);data.addRow([45,5000,true,null,false]);data.addRow([46,5000,true,null,false]);data.addRow([47,5000,true,null,false]);data.addRow([48,5000,true,null,false]);data.addRow([49,5000,true,null,false]);data.addRow([50,5000,true,null,false]);data.addRow([51,5000,true,null,false]);    options.title='Kurz Q40/Cr';
            options.colors=['#109618', '#08530b'];
            var chart = new google.visualization.LineChart(document.getElementById('chart_macro3'));
            chart.draw(data, options);
        
        // =================================================    
            var data = new google.visualization.DataTable();
            data.addColumn('number', 'deň');
            data.addColumn('number', 'ekonomika');
            data.addColumn({type:'boolean',role:'certainty'});
            data.addColumn('number', 'burza');
            data.addColumn({type:'boolean',role:'certainty'});
        data.addRow([1,150,true,null,false]);data.addRow([2,150,true,null,false]);data.addRow([3,150,true,null,false]);data.addRow([4,150,true,null,false]);data.addRow([5,150,true,null,false]);data.addRow([6,150,true,null,false]);data.addRow([7,150,true,null,false]);data.addRow([8,150,true,null,false]);data.addRow([9,150,true,null,false]);data.addRow([10,150,true,null,false]);data.addRow([11,150,true,null,false]);data.addRow([12,150,true,null,false]);data.addRow([13,150,true,null,false]);data.addRow([14,150,true,null,false]);data.addRow([15,150,true,null,false]);data.addRow([16,150,true,null,false]);data.addRow([17,150,true,null,false]);data.addRow([18,150,true,null,false]);data.addRow([19,150,true,null,false]);data.addRow([20,150,true,null,false]);data.addRow([21,150,true,null,false]);data.addRow([22,150,true,null,false]);data.addRow([23,150,true,null,false]);data.addRow([24,150,true,null,false]);data.addRow([25,150,true,null,false]);data.addRow([26,150,true,null,false]);data.addRow([27,150,true,null,false]);data.addRow([28,150,true,null,false]);data.addRow([29,150,true,null,false]);data.addRow([30,150,true,null,false]);data.addRow([31,150,true,null,false]);data.addRow([32,150,true,null,false]);data.addRow([33,150,true,null,false]);data.addRow([34,150,true,null,false]);data.addRow([35,150,true,null,false]);data.addRow([36,150,true,null,false]);data.addRow([37,150,true,null,false]);data.addRow([38,150,true,null,false]);data.addRow([39,150,true,null,false]);data.addRow([40,150,true,null,false]);data.addRow([41,150,true,null,false]);data.addRow([42,150,true,null,false]);data.addRow([43,150,true,null,false]);data.addRow([44,150,true,null,false]);data.addRow([45,150,true,null,false]);data.addRow([46,150,true,null,false]);data.addRow([47,150,true,null,false]);data.addRow([48,150,true,null,false]);data.addRow([49,150,true,null,false]);data.addRow([50,150,true,null,false]);data.addRow([51,150,true,null,false]);    options.title='Kurz rTi/Cr';
            options.colors=['#dc3912', '#862009'];
            var chart = new google.visualization.LineChart(document.getElementById('chart_macro4'));
            chart.draw(data, options);    
        
        // =================================================    
            var data = new google.visualization.DataTable();
            data.addColumn('number', 'deň');
            data.addColumn('number', 'ekonomika');
            data.addColumn({type:'boolean',role:'certainty'});
            data.addColumn('number', 'burza');
            data.addColumn({type:'boolean',role:'certainty'});
        data.addRow([1,900,true,null,false]);data.addRow([2,900,true,null,false]);data.addRow([3,900,true,null,false]);data.addRow([4,900,true,null,false]);data.addRow([5,900,true,null,false]);data.addRow([6,900,true,null,false]);data.addRow([7,900,true,null,false]);data.addRow([8,900,true,null,false]);data.addRow([9,900,true,null,false]);data.addRow([10,900,true,null,false]);data.addRow([11,900,true,null,false]);data.addRow([12,900,true,null,false]);data.addRow([13,900,true,null,false]);data.addRow([14,900,true,null,false]);data.addRow([15,900,true,null,false]);data.addRow([16,900,true,null,false]);data.addRow([17,900,true,null,false]);data.addRow([18,900,true,null,false]);data.addRow([19,900,true,null,false]);data.addRow([20,900,true,null,false]);data.addRow([21,900,true,null,false]);data.addRow([22,900,true,null,false]);data.addRow([23,900,true,null,false]);data.addRow([24,900,true,null,false]);data.addRow([25,900,true,null,false]);data.addRow([26,900,true,null,false]);data.addRow([27,900,true,null,false]);data.addRow([28,900,true,null,false]);data.addRow([29,900,true,null,false]);data.addRow([30,900,true,null,false]);data.addRow([31,900,true,null,false]);data.addRow([32,900,true,null,false]);data.addRow([33,900,true,null,false]);data.addRow([34,900,true,null,false]);data.addRow([35,900,true,null,false]);data.addRow([36,900,true,null,false]);data.addRow([37,900,true,null,false]);data.addRow([38,900,true,null,false]);data.addRow([39,900,true,null,false]);data.addRow([40,900,true,null,false]);data.addRow([41,900,true,null,false]);data.addRow([42,900,true,null,false]);data.addRow([43,900,true,null,false]);data.addRow([44,900,true,null,false]);data.addRow([45,900,true,null,false]);data.addRow([46,900,true,null,false]);data.addRow([47,900,true,null,false]);data.addRow([48,900,true,null,false]);data.addRow([49,900,true,null,false]);data.addRow([50,900,true,null,false]);data.addRow([51,900,true,null,false]);    options.title='Kurz rQ40/Cr';
            options.colors=['#990099', '#550055'];
            var chart = new google.visualization.LineChart(document.getElementById('chart_macro5'));
            chart.draw(data, options);    
        
            
            
            
        // =================================================    
            var data = new google.visualization.DataTable();
            data.addColumn('number', 'deň');
            data.addColumn('number', 'Cr [%]');
            data.addColumn('number', 'Ti [%]');
            data.addColumn('number', 'Q40 [%]');
            data.addColumn('number', 'rTi [%]');
            data.addColumn('number', 'rQ40 [%]');
        data.addRow([1,100,100,100,100,100]);data.addRow([2,100,100,100,100,100]);data.addRow([3,100,100,100,100,100]);data.addRow([4,100,100,100,100,100]);data.addRow([5,100,100,100,100,100]);data.addRow([6,100,100,100,100,100]);data.addRow([7,100,100,100,100,100]);data.addRow([8,100,100,100,100,100]);data.addRow([9,100,100,100,100,100]);data.addRow([10,100,100,100,100,100]);data.addRow([11,100,100,100,100,100]);data.addRow([12,100,100,100,100,100]);data.addRow([13,100,100,100,100,100]);data.addRow([14,100,100,100,100,100]);data.addRow([15,100,100,100,100,100]);data.addRow([16,100,100,100,100,100]);data.addRow([17,100,100,100,100,100]);data.addRow([18,100,100,100,100,100]);data.addRow([19,100,100,100,100,100]);data.addRow([20,100,100,100,100,100]);data.addRow([21,100,100,100,100,100]);data.addRow([22,100,100,100,100,100]);data.addRow([23,100,100,100,100,100]);data.addRow([24,100,100,100,100,100]);data.addRow([25,100,100,100,100,100]);data.addRow([26,100,100,100,100,100]);data.addRow([27,100,100,100,100,100]);data.addRow([28,100,100,100,100,100]);data.addRow([29,100,100,100,100,100]);data.addRow([30,100,100,100,100,100]);data.addRow([31,100,100,100,100,100]);data.addRow([32,100,100,100,100,100]);data.addRow([33,100,100,100,100,100]);data.addRow([34,100,100,100,100,100]);data.addRow([35,100,100,100,100,100]);data.addRow([36,100,100,100,100,100]);data.addRow([37,100,100,100,100,100]);data.addRow([38,100,100,100,100,100]);data.addRow([39,100,100,100,100,100]);data.addRow([40,100,100,100,100,100]);data.addRow([41,100,100,100,100,100]);data.addRow([42,100,100,100,100,100]);data.addRow([43,100,100,100,100,100]);data.addRow([44,100,100,100,100,100]);data.addRow([45,100,100,100,100,100]);data.addRow([46,100,100,100,100,100]);data.addRow([47,100,100,100,100,100]);data.addRow([48,100,100,100,100,100]);data.addRow([49,100,100,100,100,100]);data.addRow([50,100,100,100,100,100]);data.addRow([51,100,100,100,100,100]);    options.title='Relatívna hodnota surovín';
            options.colors=['#3366CC', '#ff9900', '#109618', '#dc3912', '#990099'];
        //    options.legend.position='right';
            var chart = new google.visualization.LineChart(document.getElementById('chart_macro6'));
            chart.draw(data, options);     
            
          }
    </script>
    <div style="clear:both"></div>
</div>