@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/rules-ambassador">Pravidlá hráčskeho ambasádora</a></div>
@else
<div class="mainTitle">Pravidlá hráčskeho ambasádora</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

    <div class="narrowBox" align="justify">

        <p>
            Ambasádor je volený zástupca hráčov, ktorého úlohou je viesť diskusiu medzi hráčmi o smerovaní a vylepšovaní hry a výsledky predkladať administrátorom (TM). Nasledovné pravidlá upravujú fukciu ambasádora:
        </p><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">I. Voľba ambasádora a funkčné obdobie</div>
            <div align="left" style="margin:10px;">
                <ol>
                    <li>Ambasádor je volený hráčmi.</li><br><br>

                    <li>Základné funkčné obdobie je jeden štandardný vek. Začína plynúť prvým dňom veku a končí deň pred začiatkom nasledujúceho štandardného veku. Medzivek sa nepovažuje za štandardný vek.</li><br><br>

                    <li>Ak by funkčné obdobie na základe článku I. bodu 2 alebo článku II. bod 5. malo byť kratšie ako 60 dní, predĺži sa o ďalší štandardný vek.</li><br><br>

                    <li>Jeden hráč nemôže byť zvolený za ambasádora dva razy po sebe.</li><br><br>

                    <li>Začiatok funkčného obdobia v prípade odvolania alebo odstúpenia ambasádora sa riadi článkom II. bod 5 týchto pravidiel.</li><br><br>

                    <li>Voľba ambasádora prebieha nasledovne:<br> Najmenej týždeň pred koncom funkčného obdobia ambasádora TM vyzve hráčov na kandidatúru. Hráči počas 2 dní od vyhlásenia môžu zasielať poštu TM a stanú sa kandidátmi. Po skončení lehoty TM vyhlásia anketu, v ktorej si hráči počas lehoty 2 dní vyberú z kandidátov nového ambasádora. Rozhodne počet hlasov. V prípade rovnosti hlasov sa 2 dňová lehota pre anketu predlžuje pokiaľ hráči nerozhodnú. Výsledok voľby vyhlasuje TM.</li><br><br>
                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">II. Odvolanie a odstúpenie ambasádora</div>
            <div align="left" style="margin:10px;">
                <ol>
                    <li>Ambasádora môžu odvolať TM, ak poruší tieto alebo všeobecné pravidlá, alebo ak ambasádor bude nečinný v rámci svojich úloh.</li><br><br>
                    <li>Ambasádora môžu odvolať hráči v hlasovaní o odvolaní prostredníctvom ankety. Hlasovanie môže iniciovať najmenej 5 hráčov zaslaním pošty TM. Hlasovanie trvá 2 dni.</li><br><br>
                    <li>Ambasádor môže sám odstúpiť.</li><br><br>
                    <li>Platí, že ambasádor z funkcie odsúpil, ak sa neprihlási do hry do 2 dní od deaktivácie jeho konta.</li><br><br>
                    <li>Vo všetkých prípadoch 1. až 4. sa volí nový ambasádor. Funkčné obdobie nového ambasádora sa v tomto prípade začína ihneď po vyhlásení výsledku voľby TM.</li><br><br>
                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">III. Práva a povinnosti ambasádora</div>
            <div align="left" style="margin:10px;">
                <ol>
                    <li>Ambasádor zastupuje hráčov. Prezentuje a predkladá návrhy, pripomienky, problémy hráčov (napr. z vývojového fóra) prostredníctvom zriadeného fóra priamo TM. Na fórum má prístup spolu s TM. Debata na fóre je vedená vyslovene v konštruktívnom duchu. Ambasádor má na tomto fóre od TM právo na odpoveď (vyjadrenie) na každú konštruktívnu otázku.</li><br><br>

                    <li>Dňom začatia jeho funkčného obdobia sa ambasádor stane občanom a Prezidentom štátu ISA so všetkými právomocami prezidenta a jeho stanica bude umiestnená v sektore Vorlon HW (spolu s TM) = bude mimo hernej mapy. Ambasádor získa za svoje zvolenie a pôsobenie TM kryštály v počte, ktorý stanovujú aktuálne pravidlá pre TM kryštály. TM mu môžu TM kryštály odobrať a to iba v prípade závažného porušenia pravidiel, alebo pri odvolaní z dôvodu nečinnosti.</li><br><br>

                    <li>Ak ambasádor odstúpi alebo je odvolaný prechádza k neobčanom a jeho stanica bude presunutá na hernú mapu. Ambasádor sa môže stať občanom iného štátu bez zaplatenia TM kryštálov (len raz, pri ďalšej zmene platí).</li><br><br>

                    <li>Politické rozhodnutia ambasádora (Prezidenta ISA) a jeho pôsobenie vo veku v rámci komunikácie so štátmi a hráčmi sú jeho slobodnou voľbou. TM nezasahujú do žiadneho vzťahu medzi ambasárodom a ostatnými hráčmi/štátmi s výnimkou porušenia týchto alebo iných pravidiel ambasádorom, prípadne v súvislosti s plnením jeho úloh.</li><br><br>

                    <li>Štát ISA nie je herným štátom, nevyhlasuje a nemá žiaden cieľ, nezaraďuje sa do tabuľky štátov, jeho ekonomické výsledky nie sú relevantné pre splnenie podmienok cieľa C6 hernými štátmi.</li><br><br>

                    <li>Pre ambasádora platia všetky pravidlá rovnako ako pre ostatných hráčov a ostatné štáty.</li><br><br>

                    <li>Úlohy ambasádora sú nasledovné:<br> - komunikuje s hráčmi na vývojovom fóre, prípadne iných fórach v rámci vývoja hry. Sleduje ToDo list a vyberá návrhy hráčov, sumarizuje ich a predkladá v sumári na spoločné fórum s TM spolu so svojimi vlastnými návrhmi,<br> - komunikuje s TM na spoločnom fóre a diskutuje o predložených návrhoch v rámci sumárov,<br> - riadi gestorov pre nováčikov na určenom fóre, prejednáva s nimi stav, pomáha gestorom, ak je potrebné. Na konci veku hodnotí pôsobenie gestorov a navrhuje TM odmenu pre najlepšieho gestora (TM kryštály),<br> - hodnotí príspevky do Kroniky a na konci veku navrhuje TM odmenu za najlepší príspevok,<br> - aktívne vyhodnocuje a riadi rozdelenie hráčov v rámci Predregistrácie do nového veku a rozhoduje o jej uzavretí pred začatím nového veku,<br> - vytvára admin ankety po porade s TM,<br> - cenzuruje príspevky hráčov pri porušení pravidiel komunikácie hráčmi, - riadi fórum "Hráčska komunita" a na základe podnetu od hráčov (predovšetkým z tohto fóra) navrhuje znenie hráčskych pravidiel a vytvára ankety o ich schvalovaní alebo zmenách ako aj k rozhodnutiam v rámci týchto pravidiel. Hráčske pravidlá nemôžu byť v rozpore s inými pravidlami (nemôžu povoliť niečo, čo je zakázané ani zrušiť niečo, čo je prikázané), ale môžu obmedziť niečo, čo je povolené (napr. upraviť podmienky prestupu medzi štátmi). TM si vyhradzujú právo veta (vo výnimočných prípadoch). Na schválenie treba 80% hlasov, pričom musí hlasovať aspoň 50% hračov.
                    </li><br><br>

                    <li>Ambasádor má v hre nasledovné oprávnenia:<br> - prístup na Politické fórum a na spoločné fórum s TM,<br> - vyhlasovanie admin ankiet všeobecne alebo admin ankiet týkajúcich sa hráčskych pravidiel,<br> - "páskovanie" (cenzurovanie) príspevkov hráčov pri jasnom porušení všeobecných pravidiel komunikácie,<br> - dočasné obmedzenie prístupu hráča na fóra hry pri jasnom porušení všeobecných pravidiel komunikácie,<br> - vyhlasovanie admin ankiet o vylúčení (ban) konkrétneho hráča z hry. Na vylúčenie treba 80% hlasov, pričom musí hlasovať aspoň 50% hračov.
                    </li><br><br>

                </ol>
            </div>
        </div><br>

    </div>
    <div style="clear:both"></div>
</div>