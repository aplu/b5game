@php
$party = 'Terran party';
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/election">Voľby</a></div>
@else
<div class="mainTitle">Voľby</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

	<table width="40%" cellspacing="0" class="Tab center">
		<tr>
			<td class="TabH1">
@if (!$party)
				Nie si členom strany.
@else
				Si členom strany:
				<font color="red">{{ $party }}</font>
@endif
			</td>
		</tr>
	</table><br><br>

	<table width="70%" cellspacing="0" class="Tab center">
		<tr>
			<th style="font-size: 12px; color:red;">Parlamentné voľby</td>
		</tr>
@if (rand(0,1))
		<tr>
			<td>Voľby trvajú do: <b>19 Dec 2273 - 09:01:50</b></td>
		</tr>
		<tr>
			<td style="text-align:left;"><a href="?main[vote]=12">StranaX</a> [1]<br></td>
		</tr>
@else
		<tr>
			<td>Momentálne neprebiehajú voľby.</td>
		</tr>
@endif
	</table><br>

	<table width="70%" cellspacing="0" class="Tab center">
		<tr>
			<th style="font-size: 12px; color:red;">Voľby prezidenta</td>
		</tr>
@if (rand(0,1))
		<tr>
			<td>Voľby trvajú do: <b>19 Dec 2273 - 09:01:50</b></td>
		</tr>
		<tr>
			<td style="text-align:left;">
				<a href="?president[vote]=123">Luis Santiago</a> [1]<br>
				<a href="?president[vote]=827">Morgan Clark</a> [0]<br>
			</td>
		</tr>
		<tr>
			<td class="TabH1"><a href="?president[add]=true">Kandidovať</a></td>
		</tr>
@else
		<tr>
			<td>Momentálne neprebiehajú voľby.</td>
		</tr>
@endif
	</table><br>

	<table width="70%" cellspacing="0" class="Tab center">
	<tr>
		<th style="font-size: 12px; color:red;">Vyhlásiť predčasné voľby do parlamentu</td>
	</tr>
	<tr>
		<td>Na úspech je potrebné získať: <b>4 hlasov.</b></td>
	</tr>
	<tr>
		<td style="text-align:left;"><a href="?early[vote]=1">Áno</a> [0]<br><a href="?early[vote]=2">Nie</a> [0]<br></td>
	</tr>
	</table><br>

	<form action="" method="post">
		<table width="60%" cellspacing="0" class="Tab center">
			<tr style="font-size: 12px; color:red;">
				<td colspan="2"><b>Založiť hlasovanie o odvolaní prezidenta alebo ministra</b></td>
			</tr>
			<tr>
				<td><select name="dismiss">
					<option value="1">Jeff</option>
					<option value="23">Johny</option>
					<option value="345">Delenn</option>
					<option value="4567">G'Kar</option>
				</select></td>
				<td><input type="submit" value="Založiť"></td>
			</tr>
		</table>
	</form><br>

	<div style="clear:both"></div>
</div>
