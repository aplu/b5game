@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/repair">Oprava flotily</a></div>
@else
<div class="mainTitle">Oprava flotily</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br>
    <form action="" method="post">
        <table width="95%" cellspacing="0" class="Tab center">
            <tr>
                <td colspan="5" class="TabH1">Parametre lode</td>
                <td colspan="5" class="TabH1">Cena opravy</td>
            </tr>
            <tr>
                <th width="42">Sektor</th>
                <th>Model</th>
                <th>Názov</th>
                <th>Presnosť</th>
                <th>HP</th>
                <th>Titánium</th>
                <th>Q40</th>
                <th>Kredity</th>
                <th>Čas</th>
                <th>Stav</th>
            </tr>

        @if (rand(0,1))
            <tr>
                <td colspan="10"><br>žiadna loď nie je poškodená<br><br></td>
            </tr>
        @else
            <tr>
                <td rowspan="2" style="background-color: #6984f9; vertical-align:middle;"><a href="../game/map_sector.php?sector=io" style="color:white"><b>io</b></a></td>
                <td style="text-align:left">[D] <span style="color: #6984f9">Warlock Destroyer</span></td>
                <td>1</td>
                <td>53,02%</td>
                <td style="color: #07f700">97%</td>
                <td colspan="5">prebieha oprava: <span id="rep8049"></span>
                    <script language="JavaScript">
                        CountDown('rep8049',986)
                    </script>
                </td>
            </tr>
            <tr>
                <td style="text-align:left">[D] <span style="color: #6984f9">Warlock Destroyer</span></td>
                <td>1</td>
                <td>50,95%</td>
                <td style="color: #0af400">96%</td>
                <td>11'477</td>
                <td>1'435</td>
                <td>3'985'012</td>
                <td>0,3 hod</td>
                <td><input type="submit" name="rep[9795]" value="Opraviť"></td>
            </tr>
            <tr>
                <td rowspan="1" style="background-color: #6984f9; vertical-align:middle;"><a href="../game/map_sector.php?sector=q32" style="color:white"><b>q32</b></a></td>
                <td style="text-align:left">[K] <span style="color: #6984f9">Hermes</span></td>
                <td>1</td>
                <td>57,88%</td>
                <td style="color: #ba4400">27%</td>
                <td>187'546</td>
                <td>6'252</td>
                <td>46'885'639</td>
                <td>1,7 hod</td>
                <td>
                    <font color="red">oprava v HW alebo OS</font>
                </td>
            </tr>
            <tr class="TabH2">
                <td colspan="5" class="TabH1">Spolu</td>
                <td>8'451</td>
                <td>1'056</td>
                <td>2'934'205</td>
                <td colspan="2" class="TabH1"></td>
            </tr>
        @endif
        </table>
    </form><br>


    <div style="clear:both"></div>
</div>