@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/fight-points">Bojové body</a></div>
@else
<div class="mainTitle">Bojové body</div>
@endif
<div class="mainTitleFade"></div>

<div class="content">
    <br><br>
    <div style="width: 280px; float:left; margin-left:18px;">
        <div>
            <table width="280px" class="Tab2">
                <tr>
                    <td colspan="3" class="TabH2">
                        <h2>TOP10: celkom získaných</h2>
                    </td>
                </tr>
                <tr>
                    <th>Poradie</th>
                    <th>Meno</th>
                    <th>Počet</th>
                </tr>
                <tr>
                    <td width="50px">1</td>
                    <td style="color: #dd9090;">Dark One</td>
                    <td width="45px" class="hl"><b>119'872</b></td>
                </tr>
                <tr>
                    <td width="50px">2</td>
                    <td style="color: #a03b37;">Kakarrot</td>
                    <td width="45px" class="hl"><b>115'762</b></td>
                </tr>
                <tr>
                    <td width="50px">3</td>
                    <td style="color: #a03b37;">ANALfabet_Hubakuk</td>
                    <td width="45px" class="hl"><b>110'746</b></td>
                </tr>
                <tr>
                    <td width="50px">4</td>
                    <td style="color: #787811;">Kosh VI</td>
                    <td width="45px" class="hl"><b>107'407</b></td>
                </tr>
                <tr>
                    <td width="50px">5</td>
                    <td style="color: #787811;">Ragnarök</td>
                    <td width="45px" class="hl"><b>105'406</b></td>
                </tr>
                <tr>
                    <td width="50px">6</td>
                    <td style="color: #787811;">aquarius</td>
                    <td width="45px" class="hl"><b>96'303</b></td>
                </tr>
                <tr>
                    <td width="50px">7</td>
                    <td style="color: #dd9090;">navinav</td>
                    <td width="45px" class="hl"><b>96'276</b></td>
                </tr>
                <tr>
                    <td width="50px">8</td>
                    <td style="color: #dd9090;">Jerry</td>
                    <td width="45px" class="hl"><b>88'885</b></td>
                </tr>
                <tr>
                    <td width="50px">9</td>
                    <td style="color: #dd9090;">ujoduro</td>
                    <td width="45px" class="hl"><b>87'404</b></td>
                </tr>
                <tr>
                    <td width="50px">10</td>
                    <td style="color: #6984f9;">Rio</td>
                    <td width="45px" class="hl"><b>86'902</b></td>
                </tr>
            </table>
        </div>
        <div id="fHidden" style="display:none">
            <table width="280px" class="Tab2">
                <tr>
                    <td width="50px">11</td>
                    <td style="color: #;">Defender</td>
                    <td width="45px" class="hl"><b>85'761</b></td>
                </tr>
                <tr>
                    <td width="50px">12</td>
                    <td style="color: #dd9090;">Anne</td>
                    <td width="45px" class="hl"><b>84'611</b></td>
                </tr>
                <tr>
                    <td width="50px">13</td>
                    <td style="color: #dd9090;">Miso</td>
                    <td width="45px" class="hl"><b>84'285</b></td>
                </tr>
                <tr>
                    <td width="50px">14</td>
                    <td style="color: #787811;">Velký teplý Al</td>
                    <td width="45px" class="hl"><b>80'543</b></td>
                </tr>
                <tr>
                    <td width="50px">15</td>
                    <td style="color: #787811;">froggy</td>
                    <td width="45px" class="hl"><b>76'000</b></td>
                </tr>
                <tr>
                    <td width="50px">16</td>
                    <td style="color: #;">Thar</td>
                    <td width="45px" class="hl"><b>75'606</b></td>
                </tr>
                <tr>
                    <td width="50px">17</td>
                    <td style="color: #a03b37;">popocatepetI</td>
                    <td width="45px" class="hl"><b>74'515</b></td>
                </tr>
                <tr>
                    <td width="50px">18</td>
                    <td style="color: #8a68a5;">voloda1024</td>
                    <td width="45px" class="hl"><b>71'038</b></td>
                </tr>
                <tr>
                    <td width="50px">19</td>
                    <td style="color: #8a68a5;">DusKE</td>
                    <td width="45px" class="hl"><b>70'780</b></td>
                </tr>
                <tr>
                    <td width="50px">20</td>
                    <td style="color: #8a68a5;">Richard B. Riddick</td>
                    <td width="45px" class="hl"><b>68'500</b></td>
                </tr>
                <tr>
                    <td width="50px">21</td>
                    <td style="color: #dd9090;">Caesar</td>
                    <td width="45px" class="hl"><b>68'160</b></td>
                </tr>
                <tr>
                    <td width="50px">22</td>
                    <td style="color: #787811;">yntums</td>
                    <td width="45px" class="hl"><b>63'240</b></td>
                </tr>
                <tr>
                    <td width="50px">23</td>
                    <td style="color: #787811;">Dobyvatel</td>
                    <td width="45px" class="hl"><b>59'618</b></td>
                </tr>
                <tr>
                    <td width="50px">24</td>
                    <td style="color: #dd9090;">Magog</td>
                    <td width="45px" class="hl"><b>56'191</b></td>
                </tr>
                <tr>
                    <td width="50px">25</td>
                    <td style="color: #;">DoomsDay</td>
                    <td width="45px" class="hl"><b>52'376</b></td>
                </tr>
                <tr>
                    <td width="50px">26</td>
                    <td style="color: #;">cmd. ADA</td>
                    <td width="45px" class="hl"><b>50'182</b></td>
                </tr>
                <tr>
                    <td width="50px">27</td>
                    <td style="color: #8a68a5;">Ulkesh</td>
                    <td width="45px" class="hl"><b>47'629</b></td>
                </tr>
                <tr>
                    <td width="50px">28</td>
                    <td style="color: #;">Cleve</td>
                    <td width="45px" class="hl"><b>47'485</b></td>
                </tr>
                <tr>
                    <td width="50px">29</td>
                    <td style="color: #8a68a5;">ShalTok</td>
                    <td width="45px" class="hl"><b>44'362</b></td>
                </tr>
                <tr>
                    <td width="50px">30</td>
                    <td style="color: #;">ApiDers</td>
                    <td width="45px" class="hl"><b>43'086</b></td>
                </tr>
                <tr>
                    <td width="50px">31</td>
                    <td style="color: #;">stocky</td>
                    <td width="45px" class="hl"><b>42'719</b></td>
                </tr>
                <tr>
                    <td width="50px">32</td>
                    <td style="color: #;">bukator</td>
                    <td width="45px" class="hl"><b>41'245</b></td>
                </tr>
                <tr>
                    <td width="50px">33</td>
                    <td style="color: #;">Ironhide</td>
                    <td width="45px" class="hl"><b>39'434</b></td>
                </tr>
                <tr>
                    <td width="50px">34</td>
                    <td style="color: #8a68a5;">Nusku1</td>
                    <td width="45px" class="hl"><b>38'014</b></td>
                </tr>
                <tr>
                    <td width="50px">35</td>
                    <td style="color: #;">psitron</td>
                    <td width="45px" class="hl"><b>36'820</b></td>
                </tr>
                <tr>
                    <td width="50px">36</td>
                    <td style="color: #a03b37;">Turgon III.</td>
                    <td width="45px" class="hl"><b>35'209</b></td>
                </tr>
                <tr>
                    <td width="50px">37</td>
                    <td style="color: #;">cmd. Flaming Fist</td>
                    <td width="45px" class="hl"><b>34'396</b></td>
                </tr>
                <tr>
                    <td width="50px">38</td>
                    <td style="color: #;">scorpion99</td>
                    <td width="45px" class="hl"><b>33'069</b></td>
                </tr>
                <tr>
                    <td width="50px">39</td>
                    <td style="color: #;">sido</td>
                    <td width="45px" class="hl"><b>31'935</b></td>
                </tr>
                <tr>
                    <td width="50px">40</td>
                    <td style="color: #;">stopo2</td>
                    <td width="45px" class="hl"><b>30'953</b></td>
                </tr>
                <tr>
                    <td width="50px">41</td>
                    <td style="color: #;">cmd. Alpus</td>
                    <td width="45px" class="hl"><b>30'780</b></td>
                </tr>
                <tr>
                    <td width="50px">42</td>
                    <td style="color: #6984f9;">zdroj</td>
                    <td width="45px" class="hl"><b>30'204</b></td>
                </tr>
                <tr>
                    <td width="50px">43</td>
                    <td style="color: #;">bobinius</td>
                    <td width="45px" class="hl"><b>29'854</b></td>
                </tr>
                <tr>
                    <td width="50px">44</td>
                    <td style="color: #;">mul</td>
                    <td width="45px" class="hl"><b>29'093</b></td>
                </tr>
                <tr>
                    <td width="50px">45</td>
                    <td style="color: #6984f9;">Itachi</td>
                    <td width="45px" class="hl"><b>28'590</b></td>
                </tr>
                <tr>
                    <td width="50px">46</td>
                    <td style="color: #;">sisli</td>
                    <td width="45px" class="hl"><b>27'618</b></td>
                </tr>
                <tr>
                    <td width="50px">47</td>
                    <td style="color: #;">TheNihilist</td>
                    <td width="45px" class="hl"><b>26'432</b></td>
                </tr>
                <tr>
                    <td width="50px">48</td>
                    <td style="color: #;">Sharp</td>
                    <td width="45px" class="hl"><b>26'280</b></td>
                </tr>
                <tr>
                    <td width="50px">49</td>
                    <td style="color: #a03b37;">klapacius</td>
                    <td width="45px" class="hl"><b>25'278</b></td>
                </tr>
                <tr>
                    <td width="50px">50</td>
                    <td style="color: #;">Wilhelm</td>
                    <td width="45px" class="hl"><b>25'063</b></td>
                </tr>
                <tr>
                    <td width="50px">51</td>
                    <td style="color: #a03b37;">haho</td>
                    <td width="45px" class="hl"><b>24'491</b></td>
                </tr>
                <tr>
                    <td width="50px">52</td>
                    <td style="color: #;">lukassssss</td>
                    <td width="45px" class="hl"><b>24'379</b></td>
                </tr>
                <tr>
                    <td width="50px">53</td>
                    <td style="color: #;">Cmd. Ivanovova</td>
                    <td width="45px" class="hl"><b>23'133</b></td>
                </tr>
                <tr>
                    <td width="50px">54</td>
                    <td style="color: #6984f9;">GimliCZ</td>
                    <td width="45px" class="hl"><b>22'678</b></td>
                </tr>
                <tr>
                    <td width="50px">55</td>
                    <td style="color: #;">20Markus</td>
                    <td width="45px" class="hl"><b>22'507</b></td>
                </tr>
                <tr>
                    <td width="50px">56</td>
                    <td style="color: #787811;">Mikiti</td>
                    <td width="45px" class="hl"><b>21'312</b></td>
                </tr>
                <tr>
                    <td width="50px">57</td>
                    <td style="color: #;">AresPJ</td>
                    <td width="45px" class="hl"><b>21'290</b></td>
                </tr>
                <tr>
                    <td width="50px">58</td>
                    <td style="color: #6984f9;">Apokalips</td>
                    <td width="45px" class="hl"><b>21'016</b></td>
                </tr>
                <tr>
                    <td width="50px">59</td>
                    <td style="color: #;">dialer</td>
                    <td width="45px" class="hl"><b>20'580</b></td>
                </tr>
                <tr>
                    <td width="50px">60</td>
                    <td style="color: #a03b37;">Emperor XXII</td>
                    <td width="45px" class="hl"><b>20'448</b></td>
                </tr>
                <tr>
                    <td width="50px">61</td>
                    <td style="color: #8a68a5;">Asgard12</td>
                    <td width="45px" class="hl"><b>20'073</b></td>
                </tr>
                <tr>
                    <td width="50px">62</td>
                    <td style="color: #;">Hadmen</td>
                    <td width="45px" class="hl"><b>19'866</b></td>
                </tr>
                <tr>
                    <td width="50px">63</td>
                    <td style="color: #;">Pepamor</td>
                    <td width="45px" class="hl"><b>18'646</b></td>
                </tr>
                <tr>
                    <td width="50px">64</td>
                    <td style="color: #;">brzda</td>
                    <td width="45px" class="hl"><b>18'317</b></td>
                </tr>
                <tr>
                    <td width="50px">65</td>
                    <td style="color: #;">hollock</td>
                    <td width="45px" class="hl"><b>18'165</b></td>
                </tr>
                <tr>
                    <td width="50px">66</td>
                    <td style="color: #;">StanoPK</td>
                    <td width="45px" class="hl"><b>17'676</b></td>
                </tr>
                <tr>
                    <td width="50px">67</td>
                    <td style="color: #;">Walis</td>
                    <td width="45px" class="hl"><b>15'740</b></td>
                </tr>
                <tr>
                    <td width="50px">68</td>
                    <td style="color: #;">Tanmed</td>
                    <td width="45px" class="hl"><b>15'047</b></td>
                </tr>
                <tr>
                    <td width="50px">69</td>
                    <td style="color: #;">dragon</td>
                    <td width="45px" class="hl"><b>14'868</b></td>
                </tr>
                <tr>
                    <td width="50px">70</td>
                    <td style="color: #;">Zasran</td>
                    <td width="45px" class="hl"><b>14'521</b></td>
                </tr>
                <tr>
                    <td width="50px">71</td>
                    <td style="color: #;">Flere Imsaho</td>
                    <td width="45px" class="hl"><b>13'540</b></td>
                </tr>
                <tr>
                    <td width="50px">72</td>
                    <td style="color: #a03b37;">Škvrnitý Démon</td>
                    <td width="45px" class="hl"><b>13'329</b></td>
                </tr>
                <tr>
                    <td width="50px">73</td>
                    <td style="color: #;">ivo15</td>
                    <td width="45px" class="hl"><b>12'447</b></td>
                </tr>
                <tr>
                    <td width="50px">74</td>
                    <td style="color: #;">jaro78</td>
                    <td width="45px" class="hl"><b>12'414</b></td>
                </tr>
                <tr>
                    <td width="50px">75</td>
                    <td style="color: #;">Jeliman</td>
                    <td width="45px" class="hl"><b>11'988</b></td>
                </tr>
                <tr>
                    <td width="50px">76</td>
                    <td style="color: #8a68a5;">volvo23</td>
                    <td width="45px" class="hl"><b>11'567</b></td>
                </tr>
                <tr>
                    <td width="50px">77</td>
                    <td style="color: #;">andrej176</td>
                    <td width="45px" class="hl"><b>11'341</b></td>
                </tr>
                <tr>
                    <td width="50px">78</td>
                    <td style="color: #;">NekromancerCZE</td>
                    <td width="45px" class="hl"><b>11'156</b></td>
                </tr>
                <tr>
                    <td width="50px">79</td>
                    <td style="color: #8a68a5;">Maverick</td>
                    <td width="45px" class="hl"><b>10'812</b></td>
                </tr>
                <tr class="TabH1">
                    <td width="50px">80</td>
                    <td style="color: #6984f9;">wajrou</td>
                    <td width="45px" class="hl"><b>10'748</b></td>
                </tr>
                <tr>
                    <td width="50px">81</td>
                    <td style="color: #dd9090;">ven0m</td>
                    <td width="45px" class="hl"><b>10'694</b></td>
                </tr>
                <tr>
                    <td width="50px">82</td>
                    <td style="color: #;">MORFEUS</td>
                    <td width="45px" class="hl"><b>9'947</b></td>
                </tr>
                <tr>
                    <td width="50px">83</td>
                    <td style="color: #;">Jofo</td>
                    <td width="45px" class="hl"><b>9'939</b></td>
                </tr>
                <tr>
                    <td width="50px">84</td>
                    <td style="color: #;">Aurannan</td>
                    <td width="45px" class="hl"><b>9'598</b></td>
                </tr>
                <tr>
                    <td width="50px">85</td>
                    <td style="color: #6984f9;">jjenda</td>
                    <td width="45px" class="hl"><b>9'305</b></td>
                </tr>
                <tr>
                    <td width="50px">86</td>
                    <td style="color: #;">rajny</td>
                    <td width="45px" class="hl"><b>9'292</b></td>
                </tr>
                <tr>
                    <td width="50px">87</td>
                    <td style="color: #;">bambulka</td>
                    <td width="45px" class="hl"><b>7'882</b></td>
                </tr>
                <tr>
                    <td width="50px">88</td>
                    <td style="color: #;">leFLEURdeMAL</td>
                    <td width="45px" class="hl"><b>7'656</b></td>
                </tr>
                <tr>
                    <td width="50px">89</td>
                    <td style="color: #;">RobViac</td>
                    <td width="45px" class="hl"><b>7'591</b></td>
                </tr>
                <tr>
                    <td width="50px">90</td>
                    <td style="color: #;">Lunil</td>
                    <td width="45px" class="hl"><b>6'827</b></td>
                </tr>
                <tr>
                    <td width="50px">91</td>
                    <td style="color: #;">Bear84</td>
                    <td width="45px" class="hl"><b>6'761</b></td>
                </tr>
                <tr>
                    <td width="50px">92</td>
                    <td style="color: #;">Termoska</td>
                    <td width="45px" class="hl"><b>6'193</b></td>
                </tr>
                <tr>
                    <td width="50px">93</td>
                    <td style="color: #6984f9;">aragok</td>
                    <td width="45px" class="hl"><b>6'049</b></td>
                </tr>
                <tr>
                    <td width="50px">94</td>
                    <td style="color: #;">kacabo</td>
                    <td width="45px" class="hl"><b>5'916</b></td>
                </tr>
                <tr>
                    <td width="50px">95</td>
                    <td style="color: #;">Krup</td>
                    <td width="45px" class="hl"><b>5'371</b></td>
                </tr>
                <tr>
                    <td width="50px">96</td>
                    <td style="color: #;">Thelvyn</td>
                    <td width="45px" class="hl"><b>5'350</b></td>
                </tr>
                <tr>
                    <td width="50px">97</td>
                    <td style="color: #;">digdag</td>
                    <td width="45px" class="hl"><b>5'325</b></td>
                </tr>
                <tr>
                    <td width="50px">98</td>
                    <td style="color: #;">ProK117</td>
                    <td width="45px" class="hl"><b>5'289</b></td>
                </tr>
                <tr>
                    <td width="50px">99</td>
                    <td style="color: #a03b37;">tlčhuba</td>
                    <td width="45px" class="hl"><b>5'261</b></td>
                </tr>
                <tr>
                    <td width="50px">100</td>
                    <td style="color: #;">AndrejKo</td>
                    <td width="45px" class="hl"><b>5'222</b></td>
                </tr>
                <tr>
                    <td width="50px">101</td>
                    <td style="color: #;">Cleo71</td>
                    <td width="45px" class="hl"><b>5'151</b></td>
                </tr>
                <tr>
                    <td width="50px">102</td>
                    <td style="color: #;">Clint</td>
                    <td width="45px" class="hl"><b>5'098</b></td>
                </tr>
                <tr>
                    <td width="50px">103</td>
                    <td style="color: #;">Tomin</td>
                    <td width="45px" class="hl"><b>4'790</b></td>
                </tr>
                <tr>
                    <td width="50px">104</td>
                    <td style="color: #;">Storman</td>
                    <td width="45px" class="hl"><b>3'274</b></td>
                </tr>
                <tr>
                    <td width="50px">105</td>
                    <td style="color: #;">Jaffa</td>
                    <td width="45px" class="hl"><b>2'980</b></td>
                </tr>
                <tr>
                    <td width="50px">106</td>
                    <td style="color: #;">owino</td>
                    <td width="45px" class="hl"><b>2'789</b></td>
                </tr>
                <tr>
                    <td width="50px">107</td>
                    <td style="color: #;">asuserin</td>
                    <td width="45px" class="hl"><b>2'766</b></td>
                </tr>
                <tr>
                    <td width="50px">108</td>
                    <td style="color: #;">Fear Factory</td>
                    <td width="45px" class="hl"><b>2'591</b></td>
                </tr>
                <tr>
                    <td width="50px">109</td>
                    <td style="color: #;">Elric</td>
                    <td width="45px" class="hl"><b>1'755</b></td>
                </tr>
                <tr>
                    <td width="50px">110</td>
                    <td style="color: #;">Merchant</td>
                    <td width="45px" class="hl"><b>1'755</b></td>
                </tr>
                <tr>
                    <td width="50px">111</td>
                    <td style="color: #;">Vycius</td>
                    <td width="45px" class="hl"><b>1'602</b></td>
                </tr>
                <tr>
                    <td width="50px">112</td>
                    <td style="color: #;">tomas1502</td>
                    <td width="45px" class="hl"><b>821</b></td>
                </tr>
                <tr>
                    <td width="50px">113</td>
                    <td style="color: #;">Tom</td>
                    <td width="45px" class="hl"><b>812</b></td>
                </tr>
                <tr>
                    <td width="50px">114</td>
                    <td style="color: #8a68a5;">Shafter</td>
                    <td width="45px" class="hl"><b>773</b></td>
                </tr>
                <tr>
                    <td width="50px">115</td>
                    <td style="color: #8a68a5;">makx</td>
                    <td width="45px" class="hl"><b>486</b></td>
                </tr>
                <tr>
                    <td width="50px">116</td>
                    <td style="color: #;">Dethecus_Etoile</td>
                    <td width="45px" class="hl"><b>190</b></td>
                </tr>
                <tr>
                    <td width="50px">117</td>
                    <td style="color: #;">Diablo</td>
                    <td width="45px" class="hl"><b>184</b></td>
                </tr>
                <tr>
                    <td width="50px">118</td>
                    <td style="color: #;">bedo26</td>
                    <td width="45px" class="hl"><b>106</b></td>
                </tr>
                <tr>
                    <td width="50px">119</td>
                    <td style="color: #;">Nofuture4U</td>
                    <td width="45px" class="hl"><b>90</b></td>
                </tr>
                <tr>
                    <td width="50px">120</td>
                    <td style="color: #;">jakop</td>
                    <td width="45px" class="hl"><b>55</b></td>
                </tr>
                <tr>
                    <td width="50px">121</td>
                    <td style="color: #;">zwinci</td>
                    <td width="45px" class="hl"><b>53</b></td>
                </tr>
                <tr>
                    <td width="50px">122</td>
                    <td style="color: #;">mjfox</td>
                    <td width="45px" class="hl"><b>40</b></td>
                </tr>
                <tr>
                    <td width="50px">123</td>
                    <td style="color: #6984f9;">Gaterinka</td>
                    <td width="45px" class="hl"><b>17</b></td>
                </tr>
                <tr>
                    <td width="50px">124</td>
                    <td style="color: #;">Zaskodnik</td>
                    <td width="45px" class="hl"><b>6</b></td>
                </tr>
                <tr>
                    <td width="50px">125</td>
                    <td style="color: #;">Stentch</td>
                    <td width="45px" class="hl"><b>2</b></td>
                </tr>
            </table>
        </div>
    </div>
    <div style="width: 280px; float:left; margin-left:30px;">
        <div>
            <table width="280px" class="Tab2">
                <tr>
                    <td colspan="3" class="TabH2">
                        <h2>TOP10: získaných v aktuálnom veku</h2>
                    </td>
                </tr>
                <tr>
                    <th>Poradie</th>
                    <th>Meno</th>
                    <th>Počet</th>
                </tr>
                <tr>
                    <td width="50px">1</td>
                    <td style="color: #8a68a5;">Asgard12</td>
                    <td width="45px" class="hl"><b>5'320</b></td>
                </tr>
                <tr>
                    <td width="50px">2</td>
                    <td style="color: #dd9090;">Jerry</td>
                    <td width="45px" class="hl"><b>4'070</b></td>
                </tr>
                <tr>
                    <td width="50px">3</td>
                    <td style="color: #787811;">Kosh VI</td>
                    <td width="45px" class="hl"><b>3'799</b></td>
                </tr>
                <tr>
                    <td width="50px">4</td>
                    <td style="color: #8a68a5;">Richard B. Riddick</td>
                    <td width="45px" class="hl"><b>3'322</b></td>
                </tr>
                <tr>
                    <td width="50px">5</td>
                    <td style="color: #dd9090;">Dark One</td>
                    <td width="45px" class="hl"><b>3'120</b></td>
                </tr>
                <tr>
                    <td width="50px">6</td>
                    <td style="color: #8a68a5;">Ulkesh</td>
                    <td width="45px" class="hl"><b>2'813</b></td>
                </tr>
                <tr>
                    <td width="50px">7</td>
                    <td style="color: #8a68a5;">ShalTok</td>
                    <td width="45px" class="hl"><b>2'651</b></td>
                </tr>
                <tr>
                    <td width="50px">8</td>
                    <td style="color: #787811;">froggy</td>
                    <td width="45px" class="hl"><b>2'580</b></td>
                </tr>
                <tr>
                    <td width="50px">9</td>
                    <td style="color: #dd9090;">Magog</td>
                    <td width="45px" class="hl"><b>2'519</b></td>
                </tr>
                <tr>
                    <td width="50px">10</td>
                    <td style="color: #8a68a5;">Maverick</td>
                    <td width="45px" class="hl"><b>2'241</b></td>
                </tr>
            </table>
        </div>
        <div id="fHidden2" style="display:none">
            <table width="280px" class="Tab2">
                <tr>
                    <td width="50px">11</td>
                    <td style="color: #dd9090;">ujoduro</td>
                    <td width="45px" class="hl"><b>2'040</b></td>
                </tr>
                <tr>
                    <td width="50px">12</td>
                    <td style="color: #dd9090;">Caesar</td>
                    <td width="45px" class="hl"><b>2'032</b></td>
                </tr>
                <tr>
                    <td width="50px">13</td>
                    <td style="color: #787811;">Ragnarök</td>
                    <td width="45px" class="hl"><b>1'952</b></td>
                </tr>
                <tr>
                    <td width="50px">14</td>
                    <td style="color: #dd9090;">navinav</td>
                    <td width="45px" class="hl"><b>1'842</b></td>
                </tr>
                <tr>
                    <td width="50px">15</td>
                    <td style="color: #787811;">aquarius</td>
                    <td width="45px" class="hl"><b>1'818</b></td>
                </tr>
                <tr>
                    <td width="50px">16</td>
                    <td style="color: #8a68a5;">DusKE</td>
                    <td width="45px" class="hl"><b>1'686</b></td>
                </tr>
                <tr>
                    <td width="50px">17</td>
                    <td style="color: #dd9090;">Miso</td>
                    <td width="45px" class="hl"><b>1'621</b></td>
                </tr>
                <tr>
                    <td width="50px">18</td>
                    <td style="color: #a03b37;">klapacius</td>
                    <td width="45px" class="hl"><b>1'597</b></td>
                </tr>
                <tr>
                    <td width="50px">19</td>
                    <td style="color: #787811;">yntums</td>
                    <td width="45px" class="hl"><b>1'564</b></td>
                </tr>
                <tr>
                    <td width="50px">20</td>
                    <td style="color: #dd9090;">Anne</td>
                    <td width="45px" class="hl"><b>1'478</b></td>
                </tr>
                <tr>
                    <td width="50px">21</td>
                    <td style="color: #8a68a5;">voloda1024</td>
                    <td width="45px" class="hl"><b>1'351</b></td>
                </tr>
                <tr>
                    <td width="50px">22</td>
                    <td style="color: #787811;">Dobyvatel</td>
                    <td width="45px" class="hl"><b>1'267</b></td>
                </tr>
                <tr>
                    <td width="50px">23</td>
                    <td style="color: #a03b37;">Škvrnitý Démon</td>
                    <td width="45px" class="hl"><b>1'266</b></td>
                </tr>
                <tr>
                    <td width="50px">24</td>
                    <td style="color: #787811;">Velký teplý Al</td>
                    <td width="45px" class="hl"><b>1'262</b></td>
                </tr>
                <tr>
                    <td width="50px">25</td>
                    <td style="color: #a03b37;">Turgon III.</td>
                    <td width="45px" class="hl"><b>1'246</b></td>
                </tr>
                <tr>
                    <td width="50px">26</td>
                    <td style="color: #8a68a5;">volvo23</td>
                    <td width="45px" class="hl"><b>1'017</b></td>
                </tr>
                <tr>
                    <td width="50px">27</td>
                    <td style="color: #a03b37;">ANALfabet_Hubakuk</td>
                    <td width="45px" class="hl"><b>965</b></td>
                </tr>
                <tr>
                    <td width="50px">28</td>
                    <td style="color: #a03b37;">Kakarrot</td>
                    <td width="45px" class="hl"><b>959</b></td>
                </tr>
                <tr>
                    <td width="50px">29</td>
                    <td style="color: #787811;">Mikiti</td>
                    <td width="45px" class="hl"><b>843</b></td>
                </tr>
                <tr>
                    <td width="50px">30</td>
                    <td style="color: #8a68a5;">Shafter</td>
                    <td width="45px" class="hl"><b>773</b></td>
                </tr>
                <tr>
                    <td width="50px">31</td>
                    <td style="color: #a03b37;">haho</td>
                    <td width="45px" class="hl"><b>664</b></td>
                </tr>
                <tr>
                    <td width="50px">32</td>
                    <td style="color: #a03b37;">Emperor XXII</td>
                    <td width="45px" class="hl"><b>586</b></td>
                </tr>
                <tr>
                    <td width="50px">33</td>
                    <td style="color: #a03b37;">tlčhuba</td>
                    <td width="45px" class="hl"><b>563</b></td>
                </tr>
                <tr>
                    <td width="50px">34</td>
                    <td style="color: #dd9090;">ven0m</td>
                    <td width="45px" class="hl"><b>526</b></td>
                </tr>
                <tr>
                    <td width="50px">35</td>
                    <td style="color: #a03b37;">popocatepetI</td>
                    <td width="45px" class="hl"><b>466</b></td>
                </tr>
                <tr>
                    <td width="50px">36</td>
                    <td style="color: #6984f9;">jjenda</td>
                    <td width="45px" class="hl"><b>153</b></td>
                </tr>
                <tr>
                    <td width="50px">37</td>
                    <td style="color: #6984f9;">Rio</td>
                    <td width="45px" class="hl"><b>96</b></td>
                </tr>
                <tr>
                    <td width="50px">38</td>
                    <td style="color: #6984f9;">aragok</td>
                    <td width="45px" class="hl"><b>74</b></td>
                </tr>
                <tr>
                    <td width="50px">39</td>
                    <td style="color: #6984f9;">Apokalips</td>
                    <td width="45px" class="hl"><b>43</b></td>
                </tr>
                <tr>
                    <td width="50px">40</td>
                    <td style="color: #6984f9;">Gaterinka</td>
                    <td width="45px" class="hl"><b>17</b></td>
                </tr>
            </table>
        </div>
    </div>
    <div style="width: 280px; float:left; margin-left:30px;">
        <div>
            <table width="280px" class="Tab2">
                <tr>
                    <td colspan="3" class="TabH2">
                        <h2>TOP10: priemer za vek</h2>
                    </td>
                </tr>
                <tr>
                    <th>Poradie</th>
                    <th>Meno</th>
                    <th>Počet</th>
                </tr>
                <tr>
                    <td width="50px">1</td>
                    <td style="color: #;">Sharp</td>
                    <td width="45px" class="hl"><b>26'280</b></td>
                </tr>
                <tr>
                    <td width="50px">2</td>
                    <td style="color: #;">dialer</td>
                    <td width="45px" class="hl"><b>20'580</b></td>
                </tr>
                <tr>
                    <td width="50px">3</td>
                    <td style="color: #;">Thar</td>
                    <td width="45px" class="hl"><b>18'902</b></td>
                </tr>
                <tr>
                    <td width="50px">4</td>
                    <td style="color: #;">dragon</td>
                    <td width="45px" class="hl"><b>14'868</b></td>
                </tr>
                <tr>
                    <td width="50px">5</td>
                    <td style="color: #a03b37;">ANALfabet_Hubakuk</td>
                    <td width="45px" class="hl"><b>13'843</b></td>
                </tr>
                <tr>
                    <td width="50px">6</td>
                    <td style="color: #;">psitron</td>
                    <td width="45px" class="hl"><b>12'273</b></td>
                </tr>
                <tr>
                    <td width="50px">7</td>
                    <td style="color: #dd9090;">Dark One</td>
                    <td width="45px" class="hl"><b>11'987</b></td>
                </tr>
                <tr>
                    <td width="50px">8</td>
                    <td style="color: #a03b37;">Kakarrot</td>
                    <td width="45px" class="hl"><b>11'576</b></td>
                </tr>
                <tr>
                    <td width="50px">9</td>
                    <td style="color: #787811;">Kosh VI</td>
                    <td width="45px" class="hl"><b>10'741</b></td>
                </tr>
                <tr>
                    <td width="50px">10</td>
                    <td style="color: #;">Defender</td>
                    <td width="45px" class="hl"><b>10'720</b></td>
                </tr>
            </table>
        </div>
        <div id="fHidden3" style="display:none">
            <table width="280px" class="Tab2">
                <tr>
                    <td width="50px">11</td>
                    <td style="color: #;">sido</td>
                    <td width="45px" class="hl"><b>10'645</b></td>
                </tr>
                <tr>
                    <td width="50px">12</td>
                    <td style="color: #dd9090;">Anne</td>
                    <td width="45px" class="hl"><b>10'576</b></td>
                </tr>
                <tr>
                    <td width="50px">13</td>
                    <td style="color: #787811;">Ragnarök</td>
                    <td width="45px" class="hl"><b>10'541</b></td>
                </tr>
                <tr>
                    <td width="50px">14</td>
                    <td style="color: #;">DoomsDay</td>
                    <td width="45px" class="hl"><b>10'475</b></td>
                </tr>
                <tr>
                    <td width="50px">15</td>
                    <td style="color: #;">stopo2</td>
                    <td width="45px" class="hl"><b>10'318</b></td>
                </tr>
                <tr>
                    <td width="50px">16</td>
                    <td style="color: #;">Ironhide</td>
                    <td width="45px" class="hl"><b>9'859</b></td>
                </tr>
                <tr>
                    <td width="50px">17</td>
                    <td style="color: #dd9090;">ujoduro</td>
                    <td width="45px" class="hl"><b>9'712</b></td>
                </tr>
                <tr>
                    <td width="50px">18</td>
                    <td style="color: #787811;">aquarius</td>
                    <td width="45px" class="hl"><b>9'630</b></td>
                </tr>
                <tr>
                    <td width="50px">19</td>
                    <td style="color: #dd9090;">navinav</td>
                    <td width="45px" class="hl"><b>9'628</b></td>
                </tr>
                <tr>
                    <td width="50px">20</td>
                    <td style="color: #dd9090;">Jerry</td>
                    <td width="45px" class="hl"><b>8'889</b></td>
                </tr>
                <tr>
                    <td width="50px">21</td>
                    <td style="color: #6984f9;">Rio</td>
                    <td width="45px" class="hl"><b>8'690</b></td>
                </tr>
                <tr>
                    <td width="50px">22</td>
                    <td style="color: #;">cmd. Flaming Fist</td>
                    <td width="45px" class="hl"><b>8'599</b></td>
                </tr>
                <tr>
                    <td width="50px">23</td>
                    <td style="color: #dd9090;">Miso</td>
                    <td width="45px" class="hl"><b>8'429</b></td>
                </tr>
                <tr>
                    <td width="50px">24</td>
                    <td style="color: #787811;">Velký teplý Al</td>
                    <td width="45px" class="hl"><b>8'054</b></td>
                </tr>
                <tr>
                    <td width="50px">25</td>
                    <td style="color: #;">bambulka</td>
                    <td width="45px" class="hl"><b>7'882</b></td>
                </tr>
                <tr>
                    <td width="50px">26</td>
                    <td style="color: #8a68a5;">Richard B. Riddick</td>
                    <td width="45px" class="hl"><b>7'611</b></td>
                </tr>
                <tr>
                    <td width="50px">27</td>
                    <td style="color: #787811;">froggy</td>
                    <td width="45px" class="hl"><b>7'600</b></td>
                </tr>
                <tr>
                    <td width="50px">28</td>
                    <td style="color: #a03b37;">popocatepetI</td>
                    <td width="45px" class="hl"><b>7'452</b></td>
                </tr>
                <tr>
                    <td width="50px">29</td>
                    <td style="color: #;">mul</td>
                    <td width="45px" class="hl"><b>7'273</b></td>
                </tr>
                <tr>
                    <td width="50px">30</td>
                    <td style="color: #;">cmd. ADA</td>
                    <td width="45px" class="hl"><b>7'169</b></td>
                </tr>
                <tr>
                    <td width="50px">31</td>
                    <td style="color: #787811;">Mikiti</td>
                    <td width="45px" class="hl"><b>7'104</b></td>
                </tr>
                <tr>
                    <td width="50px">32</td>
                    <td style="color: #8a68a5;">voloda1024</td>
                    <td width="45px" class="hl"><b>7'104</b></td>
                </tr>
                <tr>
                    <td width="50px">33</td>
                    <td style="color: #;">AresPJ</td>
                    <td width="45px" class="hl"><b>7'097</b></td>
                </tr>
                <tr>
                    <td width="50px">34</td>
                    <td style="color: #8a68a5;">DusKE</td>
                    <td width="45px" class="hl"><b>7'078</b></td>
                </tr>
                <tr>
                    <td width="50px">35</td>
                    <td style="color: #787811;">yntums</td>
                    <td width="45px" class="hl"><b>7'027</b></td>
                </tr>
                <tr>
                    <td width="50px">36</td>
                    <td style="color: #;">sisli</td>
                    <td width="45px" class="hl"><b>6'905</b></td>
                </tr>
                <tr>
                    <td width="50px">37</td>
                    <td style="color: #dd9090;">Caesar</td>
                    <td width="45px" class="hl"><b>6'816</b></td>
                </tr>
                <tr>
                    <td width="50px">38</td>
                    <td style="color: #;">Cleve</td>
                    <td width="45px" class="hl"><b>6'784</b></td>
                </tr>
                <tr>
                    <td width="50px">39</td>
                    <td style="color: #8a68a5;">Asgard12</td>
                    <td width="45px" class="hl"><b>6'691</b></td>
                </tr>
                <tr>
                    <td width="50px">40</td>
                    <td style="color: #;">Wilhelm</td>
                    <td width="45px" class="hl"><b>6'266</b></td>
                </tr>
                <tr>
                    <td width="50px">41</td>
                    <td style="color: #;">Termoska</td>
                    <td width="45px" class="hl"><b>6'193</b></td>
                </tr>
                <tr>
                    <td width="50px">42</td>
                    <td style="color: #;">ApiDers</td>
                    <td width="45px" class="hl"><b>6'155</b></td>
                </tr>
                <tr>
                    <td width="50px">43</td>
                    <td style="color: #;">brzda</td>
                    <td width="45px" class="hl"><b>6'106</b></td>
                </tr>
                <tr>
                    <td width="50px">44</td>
                    <td style="color: #;">stocky</td>
                    <td width="45px" class="hl"><b>6'103</b></td>
                </tr>
                <tr>
                    <td width="50px">45</td>
                    <td style="color: #;">bobinius</td>
                    <td width="45px" class="hl"><b>5'971</b></td>
                </tr>
                <tr>
                    <td width="50px">46</td>
                    <td style="color: #787811;">Dobyvatel</td>
                    <td width="45px" class="hl"><b>5'962</b></td>
                </tr>
                <tr>
                    <td width="50px">47</td>
                    <td style="color: #;">Cmd. Ivanovova</td>
                    <td width="45px" class="hl"><b>5'783</b></td>
                </tr>
                <tr>
                    <td width="50px">48</td>
                    <td style="color: #dd9090;">Magog</td>
                    <td width="45px" class="hl"><b>5'619</b></td>
                </tr>
                <tr class="TabH1">
                    <td width="50px">49</td>
                    <td style="color: #6984f9;">wajrou</td>
                    <td width="45px" class="hl"><b>5'374</b></td>
                </tr>
                <tr>
                    <td width="50px">50</td>
                    <td style="color: #;">Thelvyn</td>
                    <td width="45px" class="hl"><b>5'350</b></td>
                </tr>
                <tr>
                    <td width="50px">51</td>
                    <td style="color: #;">digdag</td>
                    <td width="45px" class="hl"><b>5'325</b></td>
                </tr>
                <tr>
                    <td width="50px">52</td>
                    <td style="color: #8a68a5;">Ulkesh</td>
                    <td width="45px" class="hl"><b>5'292</b></td>
                </tr>
                <tr>
                    <td width="50px">53</td>
                    <td style="color: #;">TheNihilist</td>
                    <td width="45px" class="hl"><b>5'286</b></td>
                </tr>
                <tr>
                    <td width="50px">54</td>
                    <td style="color: #;">AndrejKo</td>
                    <td width="45px" class="hl"><b>5'222</b></td>
                </tr>
                <tr>
                    <td width="50px">55</td>
                    <td style="color: #;">bukator</td>
                    <td width="45px" class="hl"><b>5'156</b></td>
                </tr>
                <tr>
                    <td width="50px">56</td>
                    <td style="color: #;">Cleo71</td>
                    <td width="45px" class="hl"><b>5'151</b></td>
                </tr>
                <tr>
                    <td width="50px">57</td>
                    <td style="color: #;">cmd. Alpus</td>
                    <td width="45px" class="hl"><b>5'130</b></td>
                </tr>
                <tr>
                    <td width="50px">58</td>
                    <td style="color: #;">Clint</td>
                    <td width="45px" class="hl"><b>5'098</b></td>
                </tr>
                <tr>
                    <td width="50px">59</td>
                    <td style="color: #;">Jofo</td>
                    <td width="45px" class="hl"><b>4'970</b></td>
                </tr>
                <tr>
                    <td width="50px">60</td>
                    <td style="color: #;">Zasran</td>
                    <td width="45px" class="hl"><b>4'840</b></td>
                </tr>
                <tr>
                    <td width="50px">61</td>
                    <td style="color: #;">Aurannan</td>
                    <td width="45px" class="hl"><b>4'799</b></td>
                </tr>
                <tr>
                    <td width="50px">62</td>
                    <td style="color: #6984f9;">Itachi</td>
                    <td width="45px" class="hl"><b>4'765</b></td>
                </tr>
                <tr>
                    <td width="50px">63</td>
                    <td style="color: #8a68a5;">Nusku1</td>
                    <td width="45px" class="hl"><b>4'752</b></td>
                </tr>
                <tr>
                    <td width="50px">64</td>
                    <td style="color: #;">Pepamor</td>
                    <td width="45px" class="hl"><b>4'662</b></td>
                </tr>
                <tr>
                    <td width="50px">65</td>
                    <td style="color: #;">hollock</td>
                    <td width="45px" class="hl"><b>4'541</b></td>
                </tr>
                <tr>
                    <td width="50px">66</td>
                    <td style="color: #;">Flere Imsaho</td>
                    <td width="45px" class="hl"><b>4'513</b></td>
                </tr>
                <tr>
                    <td width="50px">67</td>
                    <td style="color: #;">20Markus</td>
                    <td width="45px" class="hl"><b>4'501</b></td>
                </tr>
                <tr>
                    <td width="50px">68</td>
                    <td style="color: #a03b37;">Škvrnitý Démon</td>
                    <td width="45px" class="hl"><b>4'443</b></td>
                </tr>
                <tr>
                    <td width="50px">69</td>
                    <td style="color: #8a68a5;">ShalTok</td>
                    <td width="45px" class="hl"><b>4'436</b></td>
                </tr>
                <tr>
                    <td width="50px">70</td>
                    <td style="color: #;">StanoPK</td>
                    <td width="45px" class="hl"><b>4'419</b></td>
                </tr>
                <tr>
                    <td width="50px">71</td>
                    <td style="color: #;">Hadmen</td>
                    <td width="45px" class="hl"><b>3'973</b></td>
                </tr>
                <tr>
                    <td width="50px">72</td>
                    <td style="color: #;">Walis</td>
                    <td width="45px" class="hl"><b>3'935</b></td>
                </tr>
                <tr>
                    <td width="50px">73</td>
                    <td style="color: #6984f9;">zdroj</td>
                    <td width="45px" class="hl"><b>3'776</b></td>
                </tr>
                <tr>
                    <td width="50px">74</td>
                    <td style="color: #;">Tanmed</td>
                    <td width="45px" class="hl"><b>3'762</b></td>
                </tr>
                <tr>
                    <td width="50px">75</td>
                    <td style="color: #;">NekromancerCZE</td>
                    <td width="45px" class="hl"><b>3'719</b></td>
                </tr>
                <tr>
                    <td width="50px">76</td>
                    <td style="color: #;">scorpion99</td>
                    <td width="45px" class="hl"><b>3'674</b></td>
                </tr>
                <tr>
                    <td width="50px">77</td>
                    <td style="color: #a03b37;">klapacius</td>
                    <td width="45px" class="hl"><b>3'611</b></td>
                </tr>
                <tr>
                    <td width="50px">78</td>
                    <td style="color: #8a68a5;">Maverick</td>
                    <td width="45px" class="hl"><b>3'604</b></td>
                </tr>
                <tr>
                    <td width="50px">79</td>
                    <td style="color: #dd9090;">ven0m</td>
                    <td width="45px" class="hl"><b>3'565</b></td>
                </tr>
                <tr>
                    <td width="50px">80</td>
                    <td style="color: #a03b37;">Turgon III.</td>
                    <td width="45px" class="hl"><b>3'521</b></td>
                </tr>
                <tr>
                    <td width="50px">81</td>
                    <td style="color: #;">Lunil</td>
                    <td width="45px" class="hl"><b>3'414</b></td>
                </tr>
                <tr>
                    <td width="50px">82</td>
                    <td style="color: #;">ivo15</td>
                    <td width="45px" class="hl"><b>3'112</b></td>
                </tr>
                <tr>
                    <td width="50px">83</td>
                    <td style="color: #;">rajny</td>
                    <td width="45px" class="hl"><b>3'097</b></td>
                </tr>
                <tr>
                    <td width="50px">84</td>
                    <td style="color: #a03b37;">Emperor XXII</td>
                    <td width="45px" class="hl"><b>2'921</b></td>
                </tr>
                <tr>
                    <td width="50px">85</td>
                    <td style="color: #;">owino</td>
                    <td width="45px" class="hl"><b>2'789</b></td>
                </tr>
                <tr>
                    <td width="50px">86</td>
                    <td style="color: #;">asuserin</td>
                    <td width="45px" class="hl"><b>2'766</b></td>
                </tr>
                <tr>
                    <td width="50px">87</td>
                    <td style="color: #;">lukassssss</td>
                    <td width="45px" class="hl"><b>2'709</b></td>
                </tr>
                <tr>
                    <td width="50px">88</td>
                    <td style="color: #;">Krup</td>
                    <td width="45px" class="hl"><b>2'686</b></td>
                </tr>
                <tr>
                    <td width="50px">89</td>
                    <td style="color: #;">Fear Factory</td>
                    <td width="45px" class="hl"><b>2'591</b></td>
                </tr>
                <tr>
                    <td width="50px">90</td>
                    <td style="color: #;">RobViac</td>
                    <td width="45px" class="hl"><b>2'530</b></td>
                </tr>
                <tr>
                    <td width="50px">91</td>
                    <td style="color: #6984f9;">GimliCZ</td>
                    <td width="45px" class="hl"><b>2'520</b></td>
                </tr>
                <tr>
                    <td width="50px">92</td>
                    <td style="color: #;">MORFEUS</td>
                    <td width="45px" class="hl"><b>2'487</b></td>
                </tr>
                <tr>
                    <td width="50px">93</td>
                    <td style="color: #;">jaro78</td>
                    <td width="45px" class="hl"><b>2'483</b></td>
                </tr>
                <tr>
                    <td width="50px">94</td>
                    <td style="color: #a03b37;">haho</td>
                    <td width="45px" class="hl"><b>2'449</b></td>
                </tr>
                <tr>
                    <td width="50px">95</td>
                    <td style="color: #;">Tomin</td>
                    <td width="45px" class="hl"><b>2'395</b></td>
                </tr>
                <tr>
                    <td width="50px">96</td>
                    <td style="color: #;">andrej176</td>
                    <td width="45px" class="hl"><b>2'268</b></td>
                </tr>
                <tr>
                    <td width="50px">97</td>
                    <td style="color: #6984f9;">Apokalips</td>
                    <td width="45px" class="hl"><b>2'102</b></td>
                </tr>
                <tr>
                    <td width="50px">98</td>
                    <td style="color: #;">kacabo</td>
                    <td width="45px" class="hl"><b>1'972</b></td>
                </tr>
                <tr>
                    <td width="50px">99</td>
                    <td style="color: #;">leFLEURdeMAL</td>
                    <td width="45px" class="hl"><b>1'914</b></td>
                </tr>
                <tr>
                    <td width="50px">100</td>
                    <td style="color: #6984f9;">jjenda</td>
                    <td width="45px" class="hl"><b>1'861</b></td>
                </tr>
                <tr>
                    <td width="50px">101</td>
                    <td style="color: #;">ProK117</td>
                    <td width="45px" class="hl"><b>1'763</b></td>
                </tr>
                <tr>
                    <td width="50px">102</td>
                    <td style="color: #;">Merchant</td>
                    <td width="45px" class="hl"><b>1'755</b></td>
                </tr>
                <tr>
                    <td width="50px">103</td>
                    <td style="color: #;">Bear84</td>
                    <td width="45px" class="hl"><b>1'690</b></td>
                </tr>
                <tr>
                    <td width="50px">104</td>
                    <td style="color: #8a68a5;">volvo23</td>
                    <td width="45px" class="hl"><b>1'652</b></td>
                </tr>
                <tr>
                    <td width="50px">105</td>
                    <td style="color: #;">Storman</td>
                    <td width="45px" class="hl"><b>1'637</b></td>
                </tr>
                <tr>
                    <td width="50px">106</td>
                    <td style="color: #;">Vycius</td>
                    <td width="45px" class="hl"><b>1'602</b></td>
                </tr>
                <tr>
                    <td width="50px">107</td>
                    <td style="color: #;">Jeliman</td>
                    <td width="45px" class="hl"><b>1'499</b></td>
                </tr>
                <tr>
                    <td width="50px">108</td>
                    <td style="color: #;">Jaffa</td>
                    <td width="45px" class="hl"><b>993</b></td>
                </tr>
                <tr>
                    <td width="50px">109</td>
                    <td style="color: #;">tomas1502</td>
                    <td width="45px" class="hl"><b>821</b></td>
                </tr>
                <tr>
                    <td width="50px">110</td>
                    <td style="color: #8a68a5;">Shafter</td>
                    <td width="45px" class="hl"><b>773</b></td>
                </tr>
                <tr>
                    <td width="50px">111</td>
                    <td style="color: #a03b37;">tlčhuba</td>
                    <td width="45px" class="hl"><b>752</b></td>
                </tr>
                <tr>
                    <td width="50px">112</td>
                    <td style="color: #6984f9;">aragok</td>
                    <td width="45px" class="hl"><b>605</b></td>
                </tr>
                <tr>
                    <td width="50px">113</td>
                    <td style="color: #;">Elric</td>
                    <td width="45px" class="hl"><b>585</b></td>
                </tr>
                <tr>
                    <td width="50px">114</td>
                    <td style="color: #8a68a5;">makx</td>
                    <td width="45px" class="hl"><b>486</b></td>
                </tr>
                <tr>
                    <td width="50px">115</td>
                    <td style="color: #;">Tom</td>
                    <td width="45px" class="hl"><b>406</b></td>
                </tr>
                <tr>
                    <td width="50px">116</td>
                    <td style="color: #;">Dethecus_Etoile</td>
                    <td width="45px" class="hl"><b>190</b></td>
                </tr>
                <tr>
                    <td width="50px">117</td>
                    <td style="color: #;">Diablo</td>
                    <td width="45px" class="hl"><b>184</b></td>
                </tr>
                <tr>
                    <td width="50px">118</td>
                    <td style="color: #;">bedo26</td>
                    <td width="45px" class="hl"><b>106</b></td>
                </tr>
                <tr>
                    <td width="50px">119</td>
                    <td style="color: #;">Nofuture4U</td>
                    <td width="45px" class="hl"><b>90</b></td>
                </tr>
                <tr>
                    <td width="50px">120</td>
                    <td style="color: #;">jakop</td>
                    <td width="45px" class="hl"><b>55</b></td>
                </tr>
                <tr>
                    <td width="50px">121</td>
                    <td style="color: #;">zwinci</td>
                    <td width="45px" class="hl"><b>53</b></td>
                </tr>
                <tr>
                    <td width="50px">122</td>
                    <td style="color: #;">mjfox</td>
                    <td width="45px" class="hl"><b>40</b></td>
                </tr>
                <tr>
                    <td width="50px">123</td>
                    <td style="color: #6984f9;">Gaterinka</td>
                    <td width="45px" class="hl"><b>17</b></td>
                </tr>
                <tr>
                    <td width="50px">124</td>
                    <td style="color: #;">Zaskodnik</td>
                    <td width="45px" class="hl"><b>6</b></td>
                </tr>
                <tr>
                    <td width="50px">125</td>
                    <td style="color: #;">Stentch</td>
                    <td width="45px" class="hl"><b>2</b></td>
                </tr>
            </table>
        </div>
    </div>

    <div style="clear:both;"></div>
    <br>

    <div style="text-align:center;">
        <a class="btn" id="fHiddenBtn" onClick="slideDIV('#fHidden'); slideDIV('#fHidden2'); slideDIV('#fHidden3'); ToggleDIV('fHiddenBtn');">Zobraziť kompletné poradie</a>
    </div>
    <br>

    <div id="chart_state_fight_points" style="width:900px; height:350px" class="center"></div>
    <br>

    <script type="text/javascript">
        google.load("visualization", "1", {packages:["corechart"]});
          google.setOnLoadCallback(drawChartStateFightPoints);
          function drawChartStateFightPoints() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'štát');
        data.addColumn('number', 'Minbarská Federácia');data.addColumn('number', 'Centaurská Republika');data.addColumn('number', 'Kolónia Drakhov');data.addColumn('number', 'Narnský Režim');data.addColumn('number', 'Pozemská Aliancia');data.addRow(['', 21174, 19248, 15085, 8312, 383 ]);
            var options = {
              title: 'Body za štát',
              colors: [
        '#8a68a5','#dd9090','#787811','#a03b37','#6984f9'      ],
              chartArea: {left: '10%', width: '60%', top: '15%', height: '70%'},
              fontName: 'Tahoma, Verdana, Geneva, Arial, Helvetica, sans-serif',
              fontSize: '13',
              backgroundColor: '#080808',
              focusTarget: 'category',
              titleTextStyle: {color: 'red', fontSize: '14'},
              legend: {textStyle: {color: '#f8f0ff'}},
              llegend: {position: 'none'},
              hAxis: {baselineColor: '#f8f0ff', gridlines: {color: '#444444'}, textStyle: {color: '#f8f0ff'}, titleTextStyle: {color: '#ffcc00'},
                      minorGridlines: {count: 2}, viewWindowMode: 'maximized', minValue: 0 },
              vAxis: {baselineColor: '#f8f0ff', gridlines: {color: '#444444'}, textStyle: {color: '#f8f0ff'}, titleTextStyle: {color: '#ffcc00'}},
              bar: {groupWidth: '90%'}
            };
        
            var chart = new google.visualization.BarChart(document.getElementById('chart_state_fight_points'));
            chart.draw(data, options);
          }
    </script>
    <div style="clear:both"></div>
</div>