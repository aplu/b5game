@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/fight-summary">Prehľad útokov</a></div>
@else
<div class="mainTitle">Prehľad útokov</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

    <br>
    <form action="" method="post"><select name="off"><option value="all">-- Všetci útočníci --</option><option value="cenrep">Centaurská Republika</option><option value="drakh">Kolónia Drakhov</option><option value="techno">Kruh Technomágov</option><option value="minbar">Minbarská Federácia</option><option value="narn">Narnský Režim</option><option value="ea">Pozemská Aliancia</option><option value="">Neutrálne územie</option></select> <select name="def"><option value="all">-- Všetci obrancovia --</option><option value="cenrep">Centaurská Republika</option><option value="drakh">Kolónia Drakhov</option><option value="techno">Kruh Technomágov</option><option value="minbar">Minbarská Federácia</option><option value="narn">Narnský Režim</option><option value="ea">Pozemská Aliancia</option><option value="">Neutrálne územie</option></select> <input type="text" name="from" value="24-10-2273"> <input type="text" name="to" value="27-10-2273"> <input type="submit" value="Zobraziť"></form><br>
    <table width="90%" cellspacing="0" class="Tab center">
        <tr>
            <th width="92">Začiatok</th>
            <th width="92">Koniec</th>
            <th width="45">Sektor</th>
            <th>Útočník</th>
            <th>Obranca</th>
            <th>Typ</th>
            <th>Počet kôl</th>
            <th width="55">Výsledok</th>
            <th width="55">Detail</th>
        </tr>
@if (rand(0,1))
        <tr>
            <td>31 Jan - 09:40</td>
            <td>31 Jan - 09:40</td>
            <td><a href="map_sector.php?sector=dav">dav</a></td>
            <td style="color: #dd9090">Centaurská Republika</td>
            <td style="color: #6984f9">Pozemská Aliancia</td>
            <td>prebratie</td>
            <td>0</td>
            <td>
                <font color="lime">úspech</font>
            </td>
            <td><a href="fight_detail.php?idx=219">Zobraziť</a></td>
        </tr>
        <tr>
            <td>31 Jan - 09:13</td>
            <td>31 Jan - 09:18</td>
            <td><a href="map_sector.php?sector=lei">lei</a></td>
            <td style="color: #787811">Kolónia Drakhov</td>
            <td style="color: #808080">Tiene</td>
            <td>na sektor</td>
            <td>1</td>
            <td>
                <font color="red">neúspech</font>
            </td>
            <td><a href="fight_detail.php?idx=218">Zobraziť</a></td>
        </tr>
        <tr>
            <td>31 Jan - 06:51</td>
            <td>31 Jan - 06:51</td>
            <td><a href="map_sector.php?sector=ant">ant</a></td>
            <td style="color: #8a68a5">Minbarská Federácia</td>
            <td style="color: #">Neutrálne územie</td>
            <td>farbičkovanie</td>
            <td>1</td>
            <td>
                <font color="lime">úspech</font>
            </td>
            <td><a href="fight_detail.php?idx=217">Zobraziť</a></td>
        </tr>
        <tr>
            <td>30 Jan - 11:17</td>
            <td>30 Jan - 12:17</td>
            <td><a href="map_sector.php?sector=q73">q73</a></td>
            <td style="color: #787811">Kolónia Drakhov</td>
            <td style="color: #808080">Tiene</td>
            <td>čistenie</td>
            <td>1</td>
            <td>
                <font color="lime">úspech</font>
            </td>
            <td><a href="fight_detail.php?idx=205">Zobraziť</a></td>
        </tr>
        <tr>
            <td>29 Jan - 09:23</td>
            <td>29 Jan - 09:23</td>
            <td><a href="map_sector.php?sector=s38">s38</a></td>
            <td style="color: #a03b37">Narnský Režim</td>
            <td style="color: #6984f9">Pozemská Aliancia</td>
            <td>prebratie</td>
            <td>0</td>
            <td>
                <font color="lime">úspech</font>
            </td>
            <td><a href="fight_detail.php?idx=186">Zobraziť</a></td>
        </tr>        
@endif
    </table><br>
    <div style="clear:both"></div>
</div>