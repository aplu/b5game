@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/sectors">Detail sektora</a></div>
@else
<div class="mainTitle">Detail sektora</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-map')
    @endif

    <b style="color: red">Vyber sektor</b><br><br>
    <form action="{{ route('select-sector') }}" method="POST">
        @csrf
        <select name="sector">
@foreach ($sectors as $sector)
            <option value="{{ $sector->short }}">{{ $sector->name }} [{{ $sector->short }}]{!! $sector->ruler ? ' - <i>'.$sector->ruler->name.'</i>' : '' !!}</option>
@endforeach
        </select>
        <input type="submit" value="Zobraziť">
    </form><br><br>

    <div style="clear:both"></div>
</div>