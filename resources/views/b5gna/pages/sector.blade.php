@php

// $sector = ['ruler' => 'EA', 'short'=>'ear', 'name' => 'Earth', 'points' => '704,109,709,127,709,159,690,159,690,149,666,149,666,126,670,126,670,109'];

$status = 'rest';   // unknown, rest, fight, ???
// $status = 'pokoj';

$protection = true;
$buildings = ['nonempty'];

$hw = 'ea';
$stations = ['nonempty'];
$mining = [];

$me = 'Jeff';
$groups = [
    // subarray for each group (or non-group)
    // [
    //     'name' => '',   // empty or unset name means free ships (not within group)
    //     'ships' => [
    //     ]
    // ]
    'Garibaldi' => [
        [],
    ],
    'Jeff' => [
        ['status' => 'rest', /*'sum' => '12 / 1 / 0 / 0',*/ 'ships' => [
                'S' => ['Starfury' => 12],
                'K' => ['Hermes' => 5]],
        ],
        ['id' => 123, 'name' => 'Scout', 'status' => 'guard', 'guard' => 200, /*'sum' => '99 / 8 / 0 / 0',*/ 'ships' => [
                'S' => ['Starfury' => 99],
                'K' => ['Hermes' => 8]
        ]],
    ],
    'Susan' => [
    ],
];

if (isset($groups[$me])) {
    $myGroups = $groups[$me];
    unset($groups[$me]);
} else {
    $myGroups = [];
}

$scanInProgress = true;


@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/sector/{{ $sector['short'] }}">Detail sektora</a></div>
@else
<div class="mainTitle">Detail sektora</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-map')
    @endif

@if (empty($sector))
    <br><font color="red">Neznámy sektor!!!</font>
@else

    <div class="sideBoxLeft">
        <div class="sidePanel" style="font-size: 11px;">
            <label class="sideLabel">Situácia v sektore</label>
        @if ($status == 'unknown')
            <font color="gray">neznáma</font>
        @elseif ($status == 'rest')
            pokoj
        @endif

        @if ($protection)
            <div style="color:#50aaee;">ochranná lehota</div>
        @endif

            <label class="sideLabel">Indexy</label>
            <div class="resLine" style="display: inline-block; background-image: url('/images/b5gna/icons16/res-ti.png');" title="titánium"><b class="colResLight">75</b></div>
            <div class="resLine" style="display: inline-block; background-image: url('/images/b5gna/icons16/res-q40.png');" title="quantium 40"><b class="colResLight">75</b></div>
            <div class="resLine" style="display: inline-block; background-image: url('/images/b5gna/icons16/res-cr.png');" title="kredity"><b class="colResLight">75</b></div>

            <label class="sideLabel">Zásoba surovín v sektore</label>
            <div class="resLine" style="background-image: url('/images/b5gna/icons16/res-rti.png');" title="ruda titánia"><b class="colResDark">888'000'000'000</b> ton</div>
            <div class="resLine" style="background-image: url('/images/b5gna/icons16/res-rq40.png');" title="ruda quantia 40"><b class="colResDark">777'000'000'000</b> ton</div>
            <div class="resLine" style="background-image: url('/images/b5gna/icons16/res-cr.png');" title="kredity"><b class="colResDark">666'555'444'333'222'111</b></div><label class="sideLabel">Kapacita ťažby</label>
            <div title="voľná / celková"><span style="font-size:18px">1/</span>10000</div>

            <label class="sideLabel">Stavby v sektore</label>
        @if (empty($buildings))
            žiadna stavba
        @else
            <div style="color: #8080ff">obchodná stanica</div>
            <div style="color: #40ff40">ťažobná stanica</div>
            <div style="color: #ff4040">obranná stanica</div>
            <div style="color: #00c0c0">skoková brána</div>
        @endif

            <label class="sideLabel">Zdroj dát</label><span class="hl">aktuálny stav</span>
        </div>
    </div>
    <div class="sideBox">
        <div class="sidePanelTitle">Domovský sektor štátu</div>
        <div class="sidePanel" style="padding-top:5px; font-size: 11px;">
    @if (empty($hw))
            sektor nie je HW štátu
    @else
            <b style="color: #6984f9">Pozemská Aliancia</b><br>
    @endif
        </div>
        <div class="sidePanelTitle">Stanice v sektore <span class="smallText">[8]</span></div>
        <div class="sidePanel" style="padding-top:5px; font-size: 11px;">
    @if (empty($stations))
            v sektore nie sú žiadne stanice
    @else
            <b style="color: #6984f9">Jeffrey Sinclair</b><br>
            <b style="color: #6984f9">Susan Ivanova</b><br>
            <b style="color: #6984f9">John Sheridan</b><br>
            <b style="color: #6984f9">Talia Winters</b><br>
            <b style="color: #6984f9">Michael Garibaldi</b><br>
            <b style="color: #6984f9">Lillien Hopps</b><br>
            <b style="color: #6984f9">Stephen Franklin</b><br>
    @endif
        </div>
        <div class="sidePanelTitle">Ťažba v sektore <span class="smallText">[9]</span></div>
        <div class="sidePanel" style="padding-top:5px; font-size: 11px">
    @if (empty($mining))
            v sektore nie je žiadna ťažba
    @else
            <b style="color: #6984f9">Jeffrey Sinclair</b>
            <span class="smallText" title="ťažobné lode / tankeri / obchodné lode / plošiny">[999, 999, 999, 99]</span><br>
            <b style="color: #6984f9">Susan Ivanova</b>
            <span class="smallText" title="ťažobné lode / tankeri / obchodné lode / plošiny">[999, 999, 999, 99]</span><br>
            <b style="color: #6984f9">John Sheridan</b>
            <span class="smallText" title="ťažobné lode / tankeri / obchodné lode / plošiny">[999, 999, 999, 99]</span><br>
            <b style="color: #6984f9">Talia Winters</b>
            <span class="smallText" title="ťažobné lode / tankeri / obchodné lode / plošiny">[999, 999, 999, 99]</span><br>
            <b style="color: #6984f9">Michael Garibaldi</b>
            <span class="smallText" title="ťažobné lode / tankeri / obchodné lode / plošiny">[999, 999, 999, 99]</span><br>
            <b style="color: #6984f9">Lillien Hopps</b>
            <span class="smallText" title="ťažobné lode / tankeri / obchodné lode / plošiny">[999, 999, 999, 99]</span><br>
            <b style="color: #6984f9">Stephen Franklin</b>
            <span class="smallText" title="ťažobné lode / tankeri / obchodné lode / plošiny">[999, 999, 999, 99]</span><br>
    @endif
        </div>
    </div>

    <div class="mainContentBox2">
        <div class="sectorTitle" style="background-color: {{ $sector->ruler->color }}">
            <b style="color: white">{{ $sector->name }} </b> [{{ $sector->short }}]<b> - {{ $sector->ruler->name }}</b>
        </div>
        <img src="/images/b5gna/sectors/{{ $sector->short }}.jpg" alt="" width="470" height="350" border="0" style="border: 3px solid /*#50aaee*/black;">
    </div>
    <div style="clear: both;"></div><br>

    <table width="80%" cellspacing="0" class="Tab center">
        <tr>
            <td colspan="6">
                <h2>Bojové lode v sektore</h2>
            </td>
        </tr>
        <tr>
            <th>Vzťah</th>
            <th>Vlastník</th>
            <th>Skupina</th>
            <th>Stav</th>
            <th>Počet lodí</th>
            <th>Obsah</th>
        </tr>
    @if (empty($groups))
        <tr>
            <td colspan="6">v sektore nie sú žiadne cudzie lode</td>
        </tr>
    @else
        @foreach ($groups as $owner => $group)
        <tr>
            <td>-/-</td>
            <td style="color: #6984f9">{{ $owner }}</td>
            <td><font color="lime">voľné lode</font></td>
            <td>v pokoji</td>
            <td><b>40</b></td>
            <td style="text-align:left">
                [K] <span style="color: #6984f9">Olympus Corvette</span>: <b>10</b><br>
                [K] <span style="color: #6984f9">Hermes</span>: <b>10</b><br>
                [L] <span style="color: #6984f9">Hyperion Cruiser</span>: <b>10</b><br>
                [D] <span style="color: #6984f9">Nova Dreadnought</span>: <b>10</b><br>
            </td>
        </tr>
        @endforeach

        {{-- <tr>
            <td>-/-</td>
            <td style="color: #6984f9">Jeffrey Sinclair</td>
            <td><font color="lime">voľné lode</font></td>
            <td>v pokoji</td>
            <td><b>40</b></td>
            <td style="text-align:left">
                [K] <span style="color: #6984f9">Olympus Corvette</span>: <b>10</b><br>
                [K] <span style="color: #6984f9">Hermes</span>: <b>10</b><br>
                [L] <span style="color: #6984f9">Hyperion Cruiser</span>: <b>10</b><br>
                [D] <span style="color: #6984f9">Nova Dreadnought</span>: <b>10</b><br>
            </td>
        </tr>
        <tr>
            <td>-/-</td>
            <td style="color: #6984f9">Susan Ivanova</td>
            <td>HW Guard</td>
            <td>v pokoji</td>
            <td><b>60</b></td>
            <td style="text-align:left">
                [K] <span style="color: #6984f9">Hermes</span>: <b>20</b><br>
                [L] <span style="color: #6984f9">Hyperion Cruiser</span>: <b>20</b><br>
                [D] <span style="color: #6984f9">Nova Dreadnought</span>: <b>20</b><br>
            </td>
        </tr> --}}
    @endif
    </table><br><br>

    <table width="80%" cellspacing="0" class="Tab center">
        <tr>
            <td colspan="6">
                <h2>Naše bojové skupiny</h2>
            </td>
        </tr>
        <tr>
            <th width="40">Výber</th>
            <th>Vlastník</th>
            <th>Skupina</th>
            <th>Stav</th>
            <th>Súčet<br>S/K/L/D</th>
            <th>Obsah</th>
        </tr>
    @if (empty($myGroups))
        <tr>
            <td colspan="6">v sektore nie sú žiadne naše lode</td>
        </tr>
    @else
        @foreach ($myGroups as $group)
        <tr>
            <td>{!! !empty($group['id']) ? '<input type="checkbox" name="Grp['.$group['id'].']" value="true">' : '' !!}</td>
            <td style="color: #6984f9">{{ $me }}</td>
            <td>{!! !empty($group['id']) ? $group['name'] : '<font color="lime">voľné</font>' !!}</td>
            <td style="color: #669999">v pokoji</td>
            {{-- <td style="color: #9999ff">príprava skoku</td> --}}
            <td><b>1</b> / <b>0</b> / <b>0</b> / <b>0</b></td>
            <td style="text-align:left">[S] <span style="color: #6984f9">Starfury</span>: <b>1</b><br></td>
        </tr>
        @endforeach

        {{-- <tr>
            <td><input type="checkbox" name="Grp[702]" value="true"></td>
            <td style="color: #6984f9">Jeffrey</td>
            <td>1</td>
            <td style="color: #669999">v pokoji<br><span style="color: #ff8800">bránenie</span></td>
            <td><b>0</b> / <b>1</b> / <b>0</b> / <b>0</b></td>
            <td style="text-align:left">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
        </tr>
        <tr>
            <td><input type="checkbox" name="Grp[702]" value="true"></td>
            <td style="color: #6984f9">Jeffrey</td>
            <td>1</td>
            <td style="color: #669999">v pokoji<br><span style="color: #999900">príprava obrany</span><br><span id="CGuard1524" title="11 Jan - 15:42"></span><script language="JavaScript">CountDown('CGuard1524',32392)</script></td>
            <td><b>0</b> / <b>1</b> / <b>0</b> / <b>0</b></td>
            <td style="text-align:left">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
        </tr>
        <tr>
            <td colspan="6" style="color:white; background: #800000; font-size:12px;"><b>V sektore nie je možné útočiť. Nie je tu nepriateľ.</b></td>
        </tr>
        <tr>
            <th colspan="2">Režim bránenia</th>
            <td colspan="4"><input type="submit" name="SendGuard" value="Prejsť do režimu bránenia"></td>
        </tr>
        <tr>
            <th colspan="2">Skenovanie<br>okolia sektora</th>
        @if ($scanInProgress)
            <td colspan="4" style="vertical-align: middle">prebieha skenovanie okolia: <span id="CountScan" title="24 Nov - 23:53"></span><script language="JavaScript">CountDown('CountScan',28768)</script></td>
        @else
            <td colspan="4" style="vertical-align: middle"><input type="submit" name="SendScan" value="Zahájiť skenovanie okolia"></td>
        @endif
        </tr> --}}
    @endif
    </table>
    <br><br><br>

    <table width="90%" class="Tab2 center">
        <tr>
            <td colspan="7" class="TabH2">
                <h2>História útokov v sektore</h2>
            </td>
        </tr>
        <tr>
            <th>Začiatok</th>
            <th>Koniec</th>
            <th>Útočník</th>
            <th>Obranca</th>
            <th>Typ</th>
            <th width="55">Výsledok</th>
            <th width="55">Detail</th>
        </tr>
        <tr>
            <td>06 Jan - 06:05</td>
            <td>06 Jan - 06:05</td>
            <td style="color: #a03b37">Narnský Režim</td>
            <td style="color: #6984f9">Pozemská Aliancia</td>
            <td>farbičkovanie</td>
            <td>
                <font color="lime">úspech</font>
            </td>
            <td><a href="/fight/1234">Zobraziť</a></td>
        </tr>
        <tr>
            <td>18 Oct - 18:21</td>
            <td>18 Oct - 18:21</td>
            <td style="color: #6984f9">Pozemská Aliancia</td>
            <td style="color: #dd9090">Centaurská Republika</td>
            <td>čistenie</td>
            <td>
                <font color="lime">úspech</font>
            </td>
            <td><a href="/fight/345">Zobraziť</a></td>
        </tr>
        <tr>
            <td>18 Oct - 09:30</td>
            <td>18 Oct - 09:35</td>
            <td style="color: #dd9090">Centaurská Republika</td>
            <td style="color: #6984f9">Pozemská Aliancia</td>
            <td>na sektor</td>
            <td>
                <font color="red">neúspech</font>
            </td>
            <td><a href="/fight/234">Zobraziť</a></td>
        </tr>
        <tr>
            <td>21 Sep - 13:48</td>
            <td>21 Sep - 13:48</td>
            <td style="color: #6984f9">Pozemská Aliancia</td>
            <td style="color: #">Bez občianstva</td>
            <td>farbičkovanie</td>
            <td>
                <font color="lime">úspech</font>
            </td>
            <td><a href="/fight/123">Zobraziť</a></td>
        </tr>
    </table><br><br>

@endif

    <div style="clear:both"></div>
</div>