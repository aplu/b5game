@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/fight/123">Detail útoku</a></div>
@else
<div class="mainTitle">Detail útoku</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

    <br>
    <table width="90%" cellspacing="0" class="Tab center">
        <tr>
            <th>Začiatok</th>
            <th>Koniec</th>
            <th width="45">Sektor</th>
            <th>Útočník</th>
            <th>Obranca</th>
            <th>Typ</th>
            <th width="55">Výsledok</th>
            <th>Počet kôl</th>
        </tr>
        <tr>
            <td>18 Oct - 18:21</td>
            <td>18 Oct - 18:21</td>
            <td><a href="/sector/ear">ear</a></td>
            <td style="color: #6984f9">Pozemská Aliancia</td>
            <td style="color: #dd9090">Centaurská Republika</td>
            <td>čistenie</td>
            <td>
                <font color="lime">úspech</font>
            </td>
            <td>1</td>
        </tr>
    </table><br>
    <table width="60%" cellspacing="0" class="Tab center">
        <tr>
            <td class="TabH1">
                <h2>Flotila útočníkov</h2>
            </td>
        </tr>
        <tr>
            <td><br>Lode obrancu nestihli odoslať informácie o útočníkovi.<br><br></td>
        </tr>
    </table><br>
    <table width="60%" cellspacing="0" class="Tab center">
        <tr>
            <td colspan="5" class="TabH1">
                <h2>Flotila obrancov</h2>
            </td>
        </tr>
        <tr>
            <th>Meno</th>
            <th width="65">Destroyer</th>
            <th width="60">Loď</th>
            <th width="60">Krížnik</th>
            <th width="60">Stíhače</th>
        </tr>
        <tr style="color: #dd9090">
            <td>Londo Mollari</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>8</td>
        </tr>
        <tr class="TabH1">
            <td><b>Celkové HP</b></td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
        </tr>
    </table><br>
    <table width="70%" cellspacing="0" class="Tab center">
        <tr>
            <td colspan="5">
                <h2>Straty v kolách</h2>
            </td>
        </tr>
        <tr>
            <th>Kolo</th>
            <th>Vlastník</th>
            <th>Model</th>
            <th>Meno lode</th>
            <th>Počet</th>
        </tr>
        <tr style="color: #6984f9">
            <td>1</td>
            <td>John Sheridan</td>
            <td style="color: #6984f9">[S] Starfury</td>
            <td>-</td>
            <td>1</td>
        </tr>
        <tr style="color: #6984f9">
            <td>1</td>
            <td>John Sheridan</td>
            <td style="color: #6984f9">[S] Starfury</td>
            <td>-</td>
            <td>1</td>
        </tr>
        <tr style="color: #6984f9">
            <td>1</td>
            <td>John Sheridan</td>
            <td style="color: #6984f9">[S] Starfury</td>
            <td>-</td>
            <td>1</td>
        </tr>
        <tr style="color: #dd9090">
            <td>1</td>
            <td>Londo Mollari</td>
            <td style="color: #dd9090">[S] Sentri</td>
            <td>-</td>
            <td>1</td>
        </tr>
    </table><br>
    <table width="80%" cellspacing="0" class="Tab center">
        <tr>
            <td colspan="9">
                <h2>Celkové straty obrancov</h2>
            </td>
        </tr>
        <tr>
            <th>Vlastník</th>
            <th width="60">Destroyer</th>
            <th width="60">Loď</th>
            <th width="60">Krížnik</th>
            <th width="60">Stíhače</th>
            <th width="60">Ťažobné</th>
            <th width="60">Tankery</th>
            <th width="60">Obchodné</th>
            <th width="60">Plošiny</th>
        </tr>
        <tr style="color: #dd9090">
            <td>Londo Mollari</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr class="TabH1">
            <td><b>Celkovo</b></td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
    </table><br>
    <table width="50%" cellspacing="0" class="Tab center">
        <tr>
            <td colspan="5">
                <h2>Sumár boja</h2>
            </td>
        </tr>
        <tr>
            <th>Strana</th>
            <th width="60">Destroyer</th>
            <th width="60">Loď</th>
            <th width="60">Krížnik</th>
            <th width="60">Stíhače</th>
        </tr>
        <tr style="color: #dd9090">
            <td><b>Obranca - straty</b></td>
            <td>0 %</td>
            <td>0 %</td>
            <td>0 %</td>
            <td>100 %</td>
        </tr>
    </table><br>
    <div style="clear:both"></div>
</div>