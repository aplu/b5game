@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/macroeco/">Ekonomika štátov</a></div>
@else
<div class="mainTitle">Ekonomika štátov</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br><br>
    <table width="90%" cellspacing="0" class="Tab center">
        <tr>
            <th rowspan="2">Štát</th>
            <th rowspan="2">Počet<br>občanov</th>
            <th colspan="3">Ťažba</th>
            <th colspan="3">Ťažba/hráč</th>
            <th colspan="3">Stanica</th>
            <th colspan="3">Stanica/hráč</th>
            <th colspan="3">Efektivita</th>
        </tr>
        <tr>
            <th>rTi</th>
            <th>rQ40</th>
            <th>Cr</th>
            <th>rTi</th>
            <th>rQ40</th>
            <th>Cr</th>
            <th>TP</th>
            <th>Raf</th>
            <th>OC</th>
            <th>TP</th>
            <th>Raf</th>
            <th>OC</th>
            <th>TP</th>
            <th>Raf</th>
            <th>OC</th>
        </tr>
        <tr style="color: #dd9090">
            <td>Centaurská Republika</td>
            <td>9</td>
            <td>4179</td>
            <td>2777</td>
            <td>3055</td>
            <td class="TabH2">464</td>
            <td class="TabH2">309</td>
            <td class="TabH2">339</td>
            <td>22125</td>
            <td>2516</td>
            <td>3149</td>
            <td class="TabH2">2'458</td>
            <td class="TabH2">280</td>
            <td class="TabH2">350</td>
            <td>69</td>
            <td>33</td>
            <td>87</td>
        </tr>
        <tr style="color: #787811">
            <td>Kolónia Drakhov</td>
            <td>8</td>
            <td>414</td>
            <td>697</td>
            <td>876</td>
            <td class="TabH2">52</td>
            <td class="TabH2">87</td>
            <td class="TabH2">110</td>
            <td>16283</td>
            <td>1603</td>
            <td>2004</td>
            <td class="TabH2">2'035</td>
            <td class="TabH2">200</td>
            <td class="TabH2">251</td>
            <td>70</td>
            <td>33</td>
            <td>86</td>
        </tr>
        <tr style="color: #8a68a5">
            <td>Minbarská Federácia</td>
            <td>12</td>
            <td>5275</td>
            <td>2852</td>
            <td>3611</td>
            <td class="TabH2">440</td>
            <td class="TabH2">238</td>
            <td class="TabH2">301</td>
            <td>24514</td>
            <td>2536</td>
            <td>3626</td>
            <td class="TabH2">2'043</td>
            <td class="TabH2">211</td>
            <td class="TabH2">302</td>
            <td>69</td>
            <td>34</td>
            <td>86</td>
        </tr>
        <tr style="color: #a03b37">
            <td>Narnský Režim</td>
            <td>10</td>
            <td>3012</td>
            <td>2392</td>
            <td>2311</td>
            <td class="TabH2">301</td>
            <td class="TabH2">239</td>
            <td class="TabH2">231</td>
            <td>15690</td>
            <td>2122</td>
            <td>2350</td>
            <td class="TabH2">1'569</td>
            <td class="TabH2">212</td>
            <td class="TabH2">235</td>
            <td>69</td>
            <td>34</td>
            <td>86</td>
        </tr>
        <tr style="color: #6984f9">
            <td>Pozemská Aliancia</td>
            <td>9</td>
            <td>2087</td>
            <td>1162</td>
            <td>1700</td>
            <td class="TabH2">232</td>
            <td class="TabH2">129</td>
            <td class="TabH2">189</td>
            <td>10737</td>
            <td>913</td>
            <td>1893</td>
            <td class="TabH2">1'193</td>
            <td class="TabH2">101</td>
            <td class="TabH2">210</td>
            <td>66</td>
            <td>32</td>
            <td>84</td>
        </tr>
        <tr style="color: #60e090">
            <td>Bez občianstva</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td class="TabH2">0</td>
            <td class="TabH2">0</td>
            <td class="TabH2">0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td class="TabH2">0</td>
            <td class="TabH2">0</td>
            <td class="TabH2">0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr class="TabH2">
            <td>Spolu / Priemer</td>
            <td>48</td>
            <td>14967</td>
            <td>9880</td>
            <td>11553</td>
            <td class="TabH2">312</td>
            <td class="TabH2">206</td>
            <td class="TabH2">241</td>
            <td>89349</td>
            <td>9690</td>
            <td>13022</td>
            <td class="TabH2">1'861</td>
            <td class="TabH2">202</td>
            <td class="TabH2">271</td>
            <td>69</td>
            <td>33</td>
            <td>86</td>
        </tr>
        <tr class="TabH1" style="color: #6984f9; font-weight:bold;">
            <td>Moja štatistika</td>
            <td>1</td>
            <td>20</td>
            <td>5</td>
            <td>20</td>
            <td class="TabH2">20</td>
            <td class="TabH2">5</td>
            <td class="TabH2">20</td>
            <td>61</td>
            <td>16</td>
            <td>21</td>
            <td class="TabH2">61</td>
            <td class="TabH2">16</td>
            <td class="TabH2">21</td>
            <td>40</td>
            <td>18</td>
            <td>50</td>
        </tr>
    </table><br><br>

    <div id="chart_gdp" style="width:900px; height:400px" class="center"></div><br>

    <table width="99%" cellspacing="0" class="Tab center">
        <tr>
            <th rowspan="2">Štát</th>
            <th colspan="6">HDP (hrubý domáci produkt)</th>
            <th colspan="6">HDP/hráč</th>
        </tr>
        <tr>
            <th>rTi</th>
            <th>rQ40</th>
            <th>Cr</th>
            <th>Ti</th>
            <th>Q40</th>
            <th>UU</th>
            <th>rTi</th>
            <th>rQ40</th>
            <th>Cr</th>
            <th>Ti</th>
            <th>Q40</th>
            <th>UU</th>
        </tr>
        <tr style="color: #dd9090">
            <td>Centaurská Republika</td>
            <td>25,05 mil</td>
            <td>2,73 mil</td>
            <td>5,38 mld</td>
            <td>15,31 mil</td>
            <td>845'978,78</td>
            <td>1,66 mld</td>
            <td>2'782'999</td>
            <td>303'060</td>
            <td>597,84 mil</td>
            <td>1'700'711</td>
            <td>93'998</td>
            <td>184,71 mil</td>
        </tr>
        <tr style="color: #787811">
            <td>Kolónia Drakhov</td>
            <td>1,86 mil</td>
            <td>482'804,40</td>
            <td>1,44 mld</td>
            <td>1,31 mil</td>
            <td>160'448,5</td>
            <td>278,42 mil</td>
            <td>232'502</td>
            <td>60'351</td>
            <td>179,74 mil</td>
            <td>163'223</td>
            <td>20'056</td>
            <td>34,8 mil</td>
        </tr>
        <tr style="color: #8a68a5">
            <td>Minbarská Federácia</td>
            <td>31,33 mil</td>
            <td>2,82 mil</td>
            <td>6,28 mld</td>
            <td>16,66 mil</td>
            <td>822'578,60</td>
            <td>1,88 mld</td>
            <td>2'610'778</td>
            <td>235'128</td>
            <td>523,11 mil</td>
            <td>1'388'575</td>
            <td>68'548</td>
            <td>156,87 mil</td>
        </tr>
        <tr style="color: #a03b37">
            <td>Narnský Režim</td>
            <td>17,39 mil</td>
            <td>2,19 mil</td>
            <td>3,82 mld</td>
            <td>10,82 mil</td>
            <td>719'785,53</td>
            <td>1,23 mld</td>
            <td>1'739'276</td>
            <td>219'230</td>
            <td>382,16 mil</td>
            <td>1'082'214</td>
            <td>71'979</td>
            <td>122,9 mil</td>
        </tr>
        <tr style="color: #6984f9">
            <td>Pozemská Aliancia</td>
            <td>12,77 mil</td>
            <td>1,15 mil</td>
            <td>2,74 mld</td>
            <td>7,13 mil</td>
            <td>287'857,10</td>
            <td>771,52 mil</td>
            <td>1'418'599</td>
            <td>127'610</td>
            <td>304,36 mil</td>
            <td>792'002</td>
            <td>31'984</td>
            <td>85,72 mil</td>
        </tr>
        <tr style="color: #60e090">
            <td>Bez občianstva</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr class="TabH2">
            <td>Spolu / Priemer</td>
            <td>88,4 mil</td>
            <td>9,37 mil</td>
            <td>19,66 mld</td>
            <td>51,23 mil</td>
            <td>2,84 mil</td>
            <td>5,82 mld</td>
            <td>1'841'594</td>
            <td>195'264</td>
            <td>409,51 mil</td>
            <td>1'067'193</td>
            <td>59'097</td>
            <td>121,33 mil</td>
        </tr>
        <tr class="TabH1" style="color: #6984f9; font-weight:bold;">
            <td>Moja štatistika</td>
            <td>117'000</td>
            <td>4'875</td>
            <td>15 mil</td>
            <td>24'400</td>
            <td>877,5</td>
            <td>3,8 mil</td>
            <td>117'000</td>
            <td>4'875</td>
            <td>15 mil</td>
            <td>24'400</td>
            <td>878</td>
            <td>3,8 mil</td>
        </tr>
    </table><br><br>
    <table width="99%" cellspacing="0" class="Tab center">
        <tr>
            <th rowspan="2">Štát</th>
            <th colspan="6">HPL (hrubý produkt na jednu ťažobnú loď)</th>
            <th colspan="4">Výrobné ceny [Cr]</th>
        </tr>
        <tr>
            <th>rTi</th>
            <th>rQ40</th>
            <th>Cr</th>
            <th>Ti</th>
            <th>Q40</th>
            <th>UU</th>
            <th>rTi</th>
            <th>rQ40</th>
            <th>Ti</th>
            <th>Q40</th>
        </tr>
        <tr style="color: #dd9090">
            <td>Centaurská Republika</td>
            <td>5'994</td>
            <td>982</td>
            <td>1,76 mil</td>
            <td>4'134</td>
            <td>331</td>
            <td>166'056</td>
            <td>294</td>
            <td>1'793</td>
            <td>426</td>
            <td>5'321</td>
        </tr>
        <tr style="color: #787811">
            <td>Kolónia Drakhov</td>
            <td>4'493</td>
            <td>693</td>
            <td>1,64 mil</td>
            <td>3'154</td>
            <td>230</td>
            <td>140'121</td>
            <td>365</td>
            <td>2'370</td>
            <td>520</td>
            <td>7'130</td>
        </tr>
        <tr style="color: #8a68a5">
            <td>Minbarská Federácia</td>
            <td>5'939</td>
            <td>989</td>
            <td>1,74 mil</td>
            <td>4'019</td>
            <td>339</td>
            <td>160'374</td>
            <td>293</td>
            <td>1'757</td>
            <td>433</td>
            <td>5'130</td>
        </tr>
        <tr style="color: #a03b37">
            <td>Narnský Režim</td>
            <td>5'774</td>
            <td>917</td>
            <td>1,65 mil</td>
            <td>4'013</td>
            <td>315</td>
            <td>159'301</td>
            <td>286</td>
            <td>1'804</td>
            <td>412</td>
            <td>5'251</td>
        </tr>
        <tr style="color: #6984f9">
            <td>Pozemská Aliancia</td>
            <td>6'118</td>
            <td>988</td>
            <td>1,61 mil</td>
            <td>4'073</td>
            <td>326</td>
            <td>155'894</td>
            <td>263</td>
            <td>1'630</td>
            <td>396</td>
            <td>4'935</td>
        </tr>
        <tr style="color: #60e090">
            <td>Bez občianstva</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr class="TabH2">
            <td>Spolu / Priemer</td>
            <td>5'906</td>
            <td>949</td>
            <td>1,7 mil</td>
            <td>4'034</td>
            <td>322</td>
            <td>159'995</td>
            <td>288</td>
            <td>1'794</td>
            <td>422</td>
            <td>5'289</td>
        </tr>
        <tr class="TabH1" style="color: #6984f9; font-weight:bold;">
            <td>Moja štatistika</td>
            <td>5'850</td>
            <td>975</td>
            <td>750'000</td>
            <td>2'340</td>
            <td>176</td>
            <td>84'342</td>
            <td>128</td>
            <td>769</td>
            <td>321</td>
            <td>4'274</td>
        </tr>
    </table>
    <script type="text/javascript">

google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChartGdp);
function drawChartGdp() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'štát');
    data.addColumn('number', 'Centaurská Republika');
    data.addColumn('number', 'Kolónia Drakhov');
    data.addColumn('number', 'Minbarská Federácia');
    data.addColumn('number', 'Narnský Režim');
    data.addColumn('number', 'Pozemská Aliancia');
    data.addColumn('number', 'Bez občianstva');
    data.addRow(['HDP', 1662.38, 278.42, 1882.47, 1229.01, 771.52, 0 ]);
    data.addRow(['HDP/hráč', 184.71, 34.8, 156.87, 122.9, 85.72, 0 ]);
    var options = {
        title: 'HDP (hrubý domáci produkt)',
        colors: ['#dd9090','#787811','#8a68a5','#a03b37','#6984f9','#60e090'],
        chartArea: { left: '9%', width: '70%', top: '15%', height: '70%' },
        fontName: 'Tahoma, Verdana, Geneva, Arial, Helvetica, sans-serif',
        fontSize: '13',
        backgroundColor: '#080808',
        focusTarget: 'category',
        titleTextStyle: { color: 'red', fontSize: '14' },
        legend: { textStyle: { color: '#f8f0ff'} },
        hAxis: {
            baselineColor: '#f8f0ff',
            gridlines: { color: '#444444' },
            textStyle: { color: '#f8f0ff' },
            titleTextStyle: { color: '#ffcc00' },
            title: 'UU v mil.',
            minorGridlines: { count: 2 },
            viewWindowMode: 'maximized'
        },
        vAxis: {
            baselineColor: '#f8f0ff',
            gridlines: { color: '#444444' },
            textStyle: { color: '#f8f0ff' },
            titleTextStyle: { color: '#ffcc00' }
        },
        bar: { groupWidth: '90%' }
    };

    var chart = new google.visualization.BarChart(document.getElementById('chart_gdp'));
    chart.draw(data, options);
}

    </script>
    <div style="clear:both"></div>
</div>