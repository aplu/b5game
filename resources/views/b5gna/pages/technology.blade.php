@php
$dock = true;
$lvl = 0;
$lvlGaranted = 4;

$starfury = 0;

$allShips = [
    'S' => [
        ['tid' => 51, 'name' => 'Starfury', 'img' => 'ea-starfury.png', 'source' => ['race'=>'Ľudia'], 'hp' => 4200, 'attack_s' => 6190, 'attack_l' => 3380, 'ti' => 34801, 'q40' => 0, 'cr' => 35500000, 'time' => '6 hod', 'status' => 'done'],
        ['tid' => 56, 'name' => 'Thunderbolt', 'img' => 'ea-thunderbolt.png', 'source' => ['state'=>'Pozemská Aliancia'], 'status' => 'no'],
    ],
    'K' => [
        ['tid' => 52, 'name' => 'Hermes', 'img' => 'ea-hermes.png', 'status' => 'running', 'time' => 6*3600, 'finish' => '10 Nov - 09:39'],
        ['tid' => 55, 'name' => 'Olympus Corvette', 'img' => 'ea-olympus.png', 'status' => 'no'],
    ],
    'L' => [
        ['tid' => 205, 'name' => 'Hyperion Cruiser', 'img' => 'ea-hyperion.png', 'status' => 'no'],
        ['tid' => 210, 'name' => 'Omega Destroyer', 'img' => 'ea-omega.png', 'status' => 'no'],
    ],
    'D' => [
        ['tid' => 53, 'name' => 'Nova Dreadnought', 'img' => 'ea-nova.png', 'status' => 'no'],
        ['tid' => 54, 'name' => 'Warlock Destroyer', 'img' => 'ea-warlock.png', 'status' => 'no'],
    ],
];
@endphp
@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/technology">Centrum technológií</a></div>
@else
<div class="mainTitle">Centrum technológií</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br>
    <br>Tvoj level: <b class="hl">{{ $lvl }}</b>
    <br>Garantovaný level: <b class="hl">{{ $lvlGaranted }}</b><br>
    <br>

    <div class="center" style="text-align: center; width: 70%;">
@foreach ($allShips as $shipType => $ships)
    @foreach ($ships as $ship)
        <form action="" method="post">
            <input type="hidden" name="tid" value="{{ $ship['tid'] }}">
            <img src="/images/b5gna/ships/{{ $ship['img'] }}" alt="" width="120" height="100" border="0" style="float: left;">
            <table width="70%" cellspacing="0" class="Tab center">
                <tr>
                    <th colspan="2">[{{ $shipType }}] {{ $ship['name'] }}</th>
                </tr>
        @if ($ship['source'] ?? false)
                <tr>
                    <td width="100" class="TabH1">Pôvod</td>
            @if (isset($ship['source']['race']))
                    <td style="color: #6984f9">[rasa] {{ $ship['source']['race'] }}</td>
            @elseif (isset($ship['source']['state']))
                    <td style="color: #6984f9">[štát] {{ $ship['source']['state'] }}</td>
            @endif
                </tr>
        @endif
                <tr>
                    <td width="100" class="TabH1">Parametre</td>
                    <td>
                        HP: <b class="hl">{{ $ship['hp'] ?? 0 }}</b><br>
                        Útok S/L: <b class="hl">{{ $ship['attack_s'] ?? 0 }}</b>/<b class="hl">{{ $ship['attack_l'] ?? 0 }}</b>
                    </td>
                </tr>
        @if ($ship['status'] == 'no')
                <tr>
                    <td class="TabH1">Cena zavedenia</td>
                    <td>
                        Titánium: <b class="hl">{{ $ship['ti'] ?? 0 }} ton</b><br>
                        Quantium 40: <b class="hl">{{ $ship['q40'] ?? 0 }} ton</b><br>
                        Kredity: <b class="hl">{{ $ship['cr'] ?? 0 }}</b><br>
                    </td>
                </tr>
                <tr>
                    <td class="TabH1">Doba zavedenia</td>
                    <td><b class="hl">{{ ($ship['time'] ?? 0)/3600 }} hod</b></td>
                </tr>
        @endif
                <tr>
                    <td colspan="2" class="TabH2">
        @if ($ship['status'] == 'no')
                        <input type="submit" name="Send" value="Zaviesť do praxe">
        @elseif ($ship['status'] == 'running')
                    <b>
                        Prebieha zavádzanie technológie:
                        <span id="t{{ $ship['tid'] }}" title="{{ $ship['finish'] }}"></span>
                        <script language="JavaScript">
                        CountDown('t{{ $ship['tid'] }}',{{ $ship['time'] ?? 0 }})
                        </script>
                    </b>
        @elseif ($ship['status'] == 'done')
                        <b style="color:lime">Technológia je zavedená do praxe</b>
        @endif
                </td>
            </tr>
            </table>
        </form>
        <br style="clear: both;"><br><br>
    @endforeach
@endforeach
<!--
        <form action="" method="post"><input type="hidden" name="tid" value="56"><img src="/images/b5gna/ships/ea-thunderbolt.png" alt="" width="120" height="100" border="0" style="float: left;">
            <table width="70%" cellspacing="0" class="Tab center">
                <tr>
                    <th colspan="2">[S] Thunderbolt</th>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Pôvod</td>
                    <td style="color: #6984f9">[štát] Pozemská Aliancia</td>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Parametre</td>
                    <td>HP: <b class="hl">4'800</b><br>Útok S/L: <b class="hl">3'600</b>/<b class="hl">7'430</b></td>
                </tr>
                <tr>
                    <td class="TabH1">Cena zavedenia</td>
                    <td>Titánium: <b class="hl">80'641 ton</b><br>Quantium 40: <b class="hl">9'600 ton</b><br>Kredity: <b class="hl">38'400'000</b><br></td>
                </tr>
                <tr>
                    <td class="TabH1">Doba zavedenia</td>
                    <td><b class="hl">18 hod</b></td>
                </tr>
                <tr>
                    <td colspan="2" class="TabH2">
    @if ($dock && $lvl >= 5)
                        <input type="submit" name="Send" value="Zaviesť do praxe">
    @else
        @if (!$dock)
                        <b style="color:red">Technológia vyžaduje vesmírny dok</b><br>
        @endif
        @if ($lvl < 5)
                        <b style="color:red">Technológia vyžaduje dosiahnutie 5. levelu hráča</b><br>
        @endif
    @endif
                    </td>
                </tr>
            </table>
        </form><br style="clear: both;"><br><br>
        <form action="" method="post"><input type="hidden" name="tid" value="52"><img src="/images/b5gna/ships/ea-hermes.png" alt="" width="120" height="100" border="0" style="float: left;">
            <table width="70%" cellspacing="0" class="Tab center">
                <tr>
                    <th colspan="2">[K] Hermes</th>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Pôvod</td>
                    <td style="color: #6984f9">[rasa] Ľudia</td>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Parametre</td>
                    <td>HP: <b class="hl">18'600</b><br>Útok S/L: <b class="hl">6'500</b>/<b class="hl">9'800</b><br>Kapacita stíhačov: <b class="hl">1</b></td>
                </tr>
                <tr>
                    <td class="TabH1">Cena zavedenia</td>
                    <td>Titánium: <b class="hl">51'841 ton</b><br>Quantium 40: <b class="hl">1'040 ton</b><br>Kredity: <b class="hl">13'200'000</b><br></td>
                </tr>
                <tr>
                    <td class="TabH1">Doba zavedenia</td>
                    <td><b class="hl">6 hod</b></td>
                </tr>
                <tr>
                    <td colspan="2" class="TabH2">
    @if (!$dock)
                        <b style="color:red">Technológia vyžaduje vesmírny dok</b><br>
    @else
                        <input type="submit" name="Send" value="Zaviesť do praxe">
    @endif
                    </td>
                </tr>
            </table>
        </form><br style="clear: both;"><br><br>
        <form action="" method="post"><input type="hidden" name="tid" value="55"><img src="/images/b5gna/ships/ea-olympus.png" alt="" width="120" height="100" border="0" style="float: left;">
            <table width="70%" cellspacing="0" class="Tab center">
                <tr>
                    <th colspan="2">[K] Olympus Corvette</th>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Pôvod</td>
                    <td style="color: #6984f9">[štát] Pozemská Aliancia</td>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Parametre</td>
                    <td>HP: <b class="hl">24'000</b><br>Útok S/L: <b class="hl">22'500</b>/<b class="hl">5'630</b><br>Kapacita stíhačov: <b class="hl">0</b></td>
                </tr>
                <tr>
                    <td class="TabH1">Cena zavedenia</td>
                    <td>Titánium: <b class="hl">76'801 ton</b><br>Quantium 40: <b class="hl">6'400 ton</b><br>Kredity: <b class="hl">16'000'000</b><br></td>
                </tr>
                <tr>
                    <td class="TabH1">Doba zavedenia</td>
                    <td><b class="hl">12 hod</b></td>
                </tr>
                <tr>
                    <td colspan="2" class="TabH2">
    @if (!$dock)
                        <b style="color:red">Technológia vyžaduje vesmírny dok</b><br>
    @else
                        <input type="submit" name="Send" value="Zaviesť do praxe">
    @endif
                    </td>
                </tr>
            </table>
        </form><br style="clear: both;"><br><br>
        <form action="" method="post"><input type="hidden" name="tid" value="205"><img src="/images/b5gna/ships/ea-hyperion.png" alt="" width="120" height="100" border="0" style="float: left;">
            <table width="70%" cellspacing="0" class="Tab center">
                <tr>
                    <th colspan="2">[L] Hyperion Cruiser</th>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Pôvod</td>
                    <td style="color: #6984f9">[rasa] Ľudia</td>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Parametre</td>
                    <td>HP: <b class="hl">60'000</b><br>Útok S/L: <b class="hl">4'500</b>/<b class="hl">38'250</b><br>Kapacita stíhačov: <b class="hl">4</b></td>
                </tr>
                <tr>
                    <td class="TabH1">Cena zavedenia</td>
                    <td>Titánium: <b class="hl">57'601 ton</b><br>Quantium 40: <b class="hl">3'600 ton</b><br>Kredity: <b class="hl">18'000'000</b><br></td>
                </tr>
                <tr>
                    <td class="TabH1">Doba zavedenia</td>
                    <td><b class="hl">12 hod</b></td>
                </tr>
                <tr>
                    <td colspan="2" class="TabH2">
    @if (!$dock)
                        <b style="color:red">Technológia vyžaduje vesmírny dok</b><br>
    @else
                        <input type="submit" name="Send" value="Zaviesť do praxe">
    @endif
                    </td>
                </tr>
            </table>
        </form><br style="clear: both;"><br><br>
        <form action="" method="post"><input type="hidden" name="tid" value="210"><img src="/images/b5gna/ships/ea-omega.png" alt="" width="120" height="100" border="0" style="float: left;">
            <table width="70%" cellspacing="0" class="Tab center">
                <tr>
                    <th colspan="2">[L] Omega Destroyer</th>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Pôvod</td>
                    <td style="color: #6984f9">[štát] Pozemská Aliancia</td>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Parametre</td>
                    <td>HP: <b class="hl">54'000</b><br>Útok S/L: <b class="hl">6'750</b>/<b class="hl">51'750</b><br>Kapacita stíhačov: <b class="hl">12</b></td>
                </tr>
                <tr>
                    <td class="TabH1">Cena zavedenia</td>
                    <td>Titánium: <b class="hl">90'001 ton</b><br>Quantium 40: <b class="hl">16'500 ton</b><br>Kredity: <b class="hl">30'000'000</b><br></td>
                </tr>
                <tr>
                    <td class="TabH1">Doba zavedenia</td>
                    <td><b class="hl">24 hod</b></td>
                </tr>
                <tr>
                    <td colspan="2" class="TabH2">
    @if ($dock && $lvl >= 7)
                        <input type="submit" name="Send" value="Zaviesť do praxe">
    @else
        @if (!$dock)
                        <b style="color:red">Technológia vyžaduje vesmírny dok</b><br>
        @endif
        @if ($lvl < 7)
                        <b style="color:red">Technológia vyžaduje dosiahnutie 7. levelu hráča</b><br>
        @endif
    @endif
                    </td>
                </tr>
            </table>
        </form><br style="clear: both;"><br><br>
        <form action="" method="post"><input type="hidden" name="tid" value="53"><img src="/images/b5gna/ships/ea-nova.png" alt="" width="120" height="100" border="0" style="float: left;">
            <table width="70%" cellspacing="0" class="Tab center">
                <tr>
                    <th colspan="2">[D] Nova Dreadnought</th>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Pôvod</td>
                    <td style="color: #6984f9">[rasa] Ľudia</td>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Parametre</td>
                    <td>HP: <b class="hl">120'000</b><br>Útok S/L: <b class="hl">9'000</b>/<b class="hl">92'250</b><br>Kapacita stíhačov: <b class="hl">12</b></td>
                </tr>
                <tr>
                    <td class="TabH1">Cena zavedenia</td>
                    <td>Titánium: <b class="hl">81'601 ton</b><br>Quantium 40: <b class="hl">4'800 ton</b><br>Kredity: <b class="hl">42'000'000</b><br></td>
                </tr>
                <tr>
                    <td class="TabH1">Doba zavedenia</td>
                    <td><b class="hl">12 hod</b></td>
                </tr>
                <tr>
                    <td colspan="2" class="TabH2">
    @if ($dock)
                        <input type="submit" name="Send" value="Zaviesť do praxe">
    @else
                        <b style="color:red">Technológia vyžaduje vesmírny dok</b><br>
    @endif
                    </td>
                </tr>
            </table>
        </form><br style="clear: both;"><br><br>
        <form action="" method="post"><input type="hidden" name="tid" value="54"><img src="/images/b5gna/ships/ea-warlock.png" alt="" width="120" height="100" border="0" style="float: left;">
            <table width="70%" cellspacing="0" class="Tab center">
                <tr>
                    <th colspan="2">[D] Warlock Destroyer</th>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Pôvod</td>
                    <td style="color: #6984f9">[štát] Pozemská Aliancia</td>
                </tr>
                <tr>
                    <td width="100" class="TabH1">Parametre</td>
                    <td>HP: <b class="hl">112'500</b><br>Útok S/L: <b class="hl">16'880</b>/<b class="hl">118'130</b><br>Kapacita stíhačov: <b class="hl">0</b></td>
                </tr>
                <tr>
                    <td class="TabH1">Cena zavedenia</td>
                    <td>Titánium: <b class="hl">129'602 ton</b><br>Quantium 40: <b class="hl">24'000 ton</b><br>Kredity: <b class="hl">26'000'000</b><br></td>
                </tr>
                <tr>
                    <td class="TabH1">Doba zavedenia</td>
                    <td><b class="hl">24 hod</b></td>
                </tr>
                <tr>
                    <td colspan="2" class="TabH2">
    @if ($dock && $lvl >= 10)
                        <input type="submit" name="Send" value="Zaviesť do praxe">
    @else
        @if (!$dock)
                        <b style="color:red">Technológia vyžaduje vesmírny dok</b><br>
        @endif
        @if ($lvl < 10)
                        <b style="color:red">Technológia vyžaduje dosiahnutie 10. levelu hráča</b><br>
        @endif
    @endif
                    </td>
                </tr>
            </table>
        </form>
-->

    <br style="clear: both;"><br><br></div>
    <div style="clear:both"></div>
</div>