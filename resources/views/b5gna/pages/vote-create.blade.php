@php
$title = 'Anketa';
//$title = 'Vládne hlasovanie - pridať';

$access = rand(0,1);

@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/vote/create">{{ $title }}</a></div>
@else
<div class="mainTitle">{{ $title }}</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br><br>

@if (!$access)
    <font color="red"><b>Nemáš právo pridávať hlasovanie!</b></font>
@else
	<form action="" method="post">
		<span class="popis">Zadajte otázku hlasovania:</span><br>
		<textarea cols="60" rows="5" name="voteq">Otázka</textarea><br><br>

		<span class="popis">Zadajte možnosti (oddelené enterom):</span><br>
		<textarea cols="60" rows="5" name="voteopt">Áno
Nie</textarea><br><br>

		<input type="checkbox" name="votepub" class="checkbox" value="1" checked>
		<span class="popis">Verejné hlasovanie</span><br><br>

		<input type="submit" name="votsub" value="Odoslať" class="but">
	</form><br>
@endif

    <div style="clear:both"></div>
</div>