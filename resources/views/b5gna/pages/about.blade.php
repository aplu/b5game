@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/">Babylon 5 Game: Open Age</a></div>
@else
<div class="mainTitle">Babylon 5 Game: Open Age</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

	<div class="narrowBox">

        <div class="errorBox" style="margin-bottom:4em">
            <div class="colLight" style="margin-bottom:5px;">
                Tato verze hry je stále v ranném vývoji.
            </div>
            Spuštění není zatím naplánováno.<br>
            Sledujte změny na této stránce.
        </div>

		<p>Ahoj! Nachádzaš sa na stránke online webovej hry inšpirovanej televíznym seriálom <a href="http://www.babylon5.com" target="_blank">Babylon 5</a>.
			Hra nevyžaduje žiadnu inštaláciu, jediné, čo potrebuješ je Tvoj internetový prehliadač.
			Pridaj sa k nám a ponor sa do hlbín vesmíru. Preži s nami rozvoj, vojny ale aj pády civilizácii.
			Aj na Tebe závisí osud národov.<br>Ako na to? <a href="/reg">Klikni sem a vytvor si svoje konto.</a></p>

		<img src="/images/b5gna/intro01.jpg" width="650" height="280" alt=""><br>
		<br>

		<p><big>Verze <strong>Open Age</strong> vzniká přepracováním původní hry <a href="http://b5game.icsp.sk" target="_blank"><strong>Babylon 5 Game: New Age</strong></a> (z let 2009-2020), jejíž autor Lucas přispěl kromě implementace rozsáhlých změn i grafikou a XHTML šablonami pro <i>legacy</i> vzhled hry (aktuálně jediný funkční vzhled). Velké díky patří i ostatním technomágům SirChislehurstovi, Bardovi a Shaademu za dlouhá léta vymýšlením herního obsahu a udržováním hry v provozu.</big></p>

		<hr style="border:0; border-top:1px solid #5a135c"><br>

		<p>
			Verzia <strong>New Age</strong> vznikla prepracovaním pôvodnej hry <strong>Babylon 5 Game</strong> z roku 2004 (vďaka UjoviDurovi a jeho tímu za inšpiráciu).
			Mnoho vecí sa zlepšilo a pribudlo, na ďalších sa stále pracuje.
			Dej je posunutý na obdobie tesne po skončení pôvodného seriálu. To umožnilo pridanie nových rás a štátov (Drakh, Interstellar Alliance)
			a uvoľnenie fantázie (nie je obmedzená udalosťami zo seriálu). Tu je zoznam zopár vylepšení oproti pôvodnej verzii:
		</p>
		<ul style="text-align:left; line-height: 18px;" >
			<li>nový systém správy flotily (skupiny, stratégia, letky, opravy, grafické presuny, prehľady)</li>
			<li>nový bojový skript (boje prebiehajú v čase, možnosť stratégie aj počas boja)</li>
			<li>prepracovaná mapa umožňuje jednoducho vykonať zmeny podľa potrieb aktuálneho veku</li>
			<li>prieskumné centrum</li>
			<li>štáty, občianstvo (možnosť zakladať nové štáty, zmeniť štát, hráči rôznych rás v jednom štáte)</li>
			<li>upravená ekonomika (dane, burza, ťažba, ekonomické prehľady)</li>
			<li>možnosť presúvať stanicu hráča</li>
			<li>technológie</li>
			<li>centrálny informačný systém - správy z hry na jednom mieste</li>
			<li>nový systém fór, možnosť vytvárania a spravovania osobných fór</li>
			<li>nový dizajn</li>
		</ul>

		<br>

		<img src="/images/b5gna/intro02.jpg" width="650" height="280" alt=""><br><br>

		<p><em>
		The Universe speaks in many languages, but only one voice. The language is not Narn, or Human, or Centauri, or Gaim, or Minbari.
		It speaks in the language of hope. It speaks in the language of trust. It speaks in the language of strength and the language of compassion.
		It is the language of the heart and the language of the soul. But always, it is the same voice.
		It's the voice of our ancestors speaking through us and the voice of our inheritors waiting to be born. It is the small still voice that says we are one.
		No matter the blood, no matter the skin, no matter the world, no matter the star, we are one.
		No matter the pain, no matter the darkness, no matter the loss, no matter the fear, we are one. Here gathered together in common cause,
		we agree to recognize this singular truth and this singular rule. That we must be kind to one another.
		Because each voice enriches us and ennobles us and each voice loss diminishes us. We are the voice of the universe.
		The soul of creation. The fire that will light the way to a better future. We are one.
		</em><br></p>
		<div align="right">G'Kar: Declaration of Principles</div>
	</div>{{-- /.narrowBox --}}

	<div style="clear:both"></div>
</div>