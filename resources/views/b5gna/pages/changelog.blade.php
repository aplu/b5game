@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/changelog">Change Log</a></div>
@else
<div class="mainTitle">Change Log</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">
	<br>

	<div style="text-align:left;">
	<table>
		<tbody>
			<tr>
				<td width="75">01.02.2020</td>
				<td width="70" style="color:#8888ff">[added]</td>
				<td><strong>b5gna</strong></td>
				<td style="padding: 3px 0px 3px 15px;">přidané šablony původní verze <a href="http://b5game.icsp.sk/" target="_blank">B5Game: New Age</a></td>
			</tr>
		</tbody>
	</table>
	</div>
	<br>

	<div style="clear:both"></div>
</div>
