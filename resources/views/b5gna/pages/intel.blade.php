@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/intel">Prieskumné centrum</a></div>
@else
<div class="mainTitle">Prieskumné centrum</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

	@if (!($debug['all'] ?? false))
		@include('b5gna.menu.dummy-map')
	@endif

	<br>
	{{-- <table width="80%" cellspacing="0" class="Tab center">
		<tr>
			<th>Sektor</th>
			<th>Vzťah</th>
			<th>Štát</th>
			<th>Lode</th>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Earth</b><br><a href="/sector/ear">[ear]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">
				[K] <span style="color: #6984f9">Hermes</span>: <b>9999</b><br>
				[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>8888</b><br>
				[L] <span style="color: #6984f9">Hyperion Cruiser</span>: <b>7777</b><br>
				[D] <span style="color: #6984f9">Nova Dreadnought</span>: <b>6666</b><br></td>
		</tr>
	</table><br> --}}

	<table width="80%" cellspacing="0" class="Tab center">
		<tr>
			<th>Sektor</th>
			<th>Vzťah</th>
			<th>Štát</th>
			<th>Lode</th>
		</tr>
		<tr>
			<td rowspan="2" class="TabH1" style="color: #787811;"><b>Drakh</b><br><a href="../game/map_sector.php?sector=dkh">[dkh]</a></td>
			<td><b style="color: #fbcf00">A</b>/<b style="color: #fbcf00">A</b></td>
			<td style="color: #dd9090">Centaurská Republika</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #dd9090">Secondus</span>: <b>1</b><br>[L] <span style="color: #dd9090">Vorchan Cruiser</span>: <b>4</b><br>[D] <span style="color: #dd9090">Octurion</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #787811">Kolónia Drakhov</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #787811">Drakh Light Cruiser</span>: <b>5</b><br>[K] <span style="color: #888811">Drakh Scout</span>: <b>3</b><br>[L] <span style="color: #888811">Drakh Carrier</span>: <b>3</b><br>[L] <span style="color: #888811">Drakh Heavy Cruiser</span>: <b>2</b><br>[D] <span style="color: #787811">Drakh Advanced</span>: <b>20</b><br>[D] <span style="color: #787811">Mother Ship</span>: <b>5</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Aris Colony</b><br><a href="../game/map_sector.php?sector=aco">[aco]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Altair</b><br><a href="../game/map_sector.php?sector=alt">[alt]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>3</b><br>[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>7</b><br>[L] <span style="color: #6984f9">Omega Destroyer</span>: <b>29</b><br>[D] <span style="color: #6984f9">Warlock Destroyer</span>: <b>33</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" class="TabH1" style="color: #6984f9;"><b>Babylon 5</b><br><a href="../game/map_sector.php?sector=b5">[b5]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>2</b><br>[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>1</b><br>[D] <span style="color: #6984f9">Nova Dreadnought</span>: <b>4</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #8a68a5">Minbarská Federácia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #8a68a5">Whitestar</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" class="TabH1" style="color: #6984f9;"><b>Ceti 4</b><br><a href="../game/map_sector.php?sector=cet">[cet]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #8a68a5">Minbarská Federácia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #8a68a5">Whitestar</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Cooke</b><br><a href="../game/map_sector.php?sector=coo">[coo]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Earth</b><br><a href="../game/map_sector.php?sector=ear">[ear]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>7</b><br>[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>15</b><br>[L] <span style="color: #6984f9">Hyperion Cruiser</span>: <b>11</b><br>[L] <span style="color: #6984f9">Omega Destroyer</span>: <b>4</b><br>[D] <span style="color: #6984f9">Nova Dreadnought</span>: <b>9</b><br>[D] <span style="color: #6984f9">Warlock Destroyer</span>: <b>10</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Gaim</b><br><a href="../game/map_sector.php?sector=gai">[gai]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" class="TabH1" style="color: #6984f9;"><b>Grid Epsilon</b><br><a href="../game/map_sector.php?sector=gep">[gep]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #8a68a5">Minbarská Federácia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #8a68a5">Whitestar</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Io</b><br><a href="../game/map_sector.php?sector=io">[io]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>45</b><br>[L] <span style="color: #6984f9">Hyperion Cruiser</span>: <b>6</b><br>[L] <span style="color: #6984f9">Omega Destroyer</span>: <b>44</b><br>[D] <span style="color: #6984f9">Nova Dreadnought</span>: <b>10</b><br>[D] <span style="color: #6984f9">Warlock Destroyer</span>: <b>41</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Orion</b><br><a href="../game/map_sector.php?sector=ori">[ori]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>8</b><br>[L] <span style="color: #6984f9">Omega Destroyer</span>: <b>3</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Outer Solar System</b><br><a href="../game/map_sector.php?sector=oso">[oso]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br>[D] <span style="color: #6984f9">Nova Dreadnought</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Proxima 3</b><br><a href="../game/map_sector.php?sector=pro">[pro]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" class="TabH1" style="color: #6984f9;"><b>Quadrant 13</b><br><a href="../game/map_sector.php?sector=q13">[q13]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #8a68a5">Minbarská Federácia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #8a68a5">Whitestar</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Quadrant 54</b><br><a href="../game/map_sector.php?sector=q54">[q54]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Regula 4</b><br><a href="../game/map_sector.php?sector=reg">[reg]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Sector 1</b><br><a href="../game/map_sector.php?sector=s1">[s1]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" class="TabH1" style="color: #6984f9;"><b>Sector 11</b><br><a href="../game/map_sector.php?sector=s11">[s11]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #8a68a5">Minbarská Federácia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #8a68a5">Whitestar</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Sector 2</b><br><a href="../game/map_sector.php?sector=s2">[s2]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Sector 3</b><br><a href="../game/map_sector.php?sector=s3">[s3]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>3</b><br>[D] <span style="color: #6984f9">Nova Dreadnought</span>: <b>2</b><br></td>
		</tr>
		<tr>
			<td rowspan="3" class="TabH1" style="color: #6984f9;"><b>Sector 9</b><br><a href="../game/map_sector.php?sector=s9">[s9]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br>[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>10</b><br>[L] <span style="color: #6984f9">Omega Destroyer</span>: <b>23</b><br>[D] <span style="color: #6984f9">Warlock Destroyer</span>: <b>6</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #8a68a5">Minbarská Federácia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #8a68a5">Whitestar</span>: <b>4</b><br>[L] <span style="color: #8a68a5">Morshin Carrier</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #a03b37">Narnský Režim</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #a03b37">Sho'Kos</span>: <b>5</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Sinzar</b><br><a href="../game/map_sector.php?sector=sin">[sin]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Syrius</b><br><a href="../game/map_sector.php?sector=syr">[syr]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Vega</b><br><a href="../game/map_sector.php?sector=veg">[veg]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br>[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>12</b><br>[L] <span style="color: #6984f9">Hyperion Cruiser</span>: <b>2</b><br>[L] <span style="color: #6984f9">Omega Destroyer</span>: <b>3</b><br>[D] <span style="color: #6984f9">Warlock Destroyer</span>: <b>9</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #6984f9;"><b>Wolf</b><br><a href="../game/map_sector.php?sector=wlf">[wlf]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" class="TabH1" style="color: #8a68a5;"><b>Beta Durani</b><br><a href="../game/map_sector.php?sector=bet">[bet]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #8a68a5">Minbarská Federácia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #8a68a5">Torotha Assault</span>: <b>1</b><br>[K] <span style="color: #8a68a5">Whitestar</span>: <b>11</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" class="TabH1" style="color: #8a68a5;"><b>Brakesh</b><br><a href="../game/map_sector.php?sector=bra">[bra]</a></td>
			<td><b style="color: #fbcf00">A</b>/<b style="color: #fbcf00">A</b></td>
			<td style="color: #dd9090">Centaurská Republika</td>
			<td style="text-align: left; vertical-align: top;">[L] <span style="color: #dd9090">Vorchan Cruiser</span>: <b>2</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" class="TabH1" style="color: #8a68a5;"><b>Hilak 7</b><br><a href="../game/map_sector.php?sector=hil">[hil]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[L] <span style="color: #6984f9">Omega Destroyer</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #8a68a5">Minbarská Federácia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #8a68a5">Whitestar</span>: <b>23</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" class="TabH1" style="color: #8a68a5;"><b>Omega 7</b><br><a href="../game/map_sector.php?sector=omg">[omg]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Olympus Corvette</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #8a68a5">Minbarská Federácia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #8a68a5">Whitestar</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="3" class="TabH1" style="color: #a03b37;"><b>Centauri Prime</b><br><a href="../game/map_sector.php?sector=cen">[cen]</a></td>
			<td><b style="color: #fbcf00">A</b>/<b style="color: #fbcf00">A</b></td>
			<td style="color: #dd9090">Centaurská Republika</td>
			<td style="text-align: left; vertical-align: top;">[L] <span style="color: #dd9090">Vorchan Cruiser</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #8a68a5">Minbarská Federácia</td>
			<td style="text-align: left; vertical-align: top;">[L] <span style="color: #8a68a5">Morshin Carrier</span>: <b>13</b><br>[D] <span style="color: #8a68a5">Sharlin</span>: <b>3</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #a03b37">Narnský Režim</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #a03b37">Sho'Kos</span>: <b>23</b><br>[D] <span style="color: #a03b37">Bin'Tak</span>: <b>73</b><br>[D] <span style="color: #a03b37">Sho'Kar</span>: <b>5</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" class="TabH1" style="color: #a03b37;"><b>Quadrant 68</b><br><a href="../game/map_sector.php?sector=q68">[q68]</a></td>
			<td>-/-</td>
			<td style="color: #6984f9">Pozemská Aliancia</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td><b style="color: #00eaff">N</b>/<b style="color: #00eaff">N</b></td>
			<td style="color: #a03b37">Narnský Režim</td>
			<td style="text-align: left; vertical-align: top;">[K] <span style="color: #a03b37">G'Karith</span>: <b>1</b><br></td>
		</tr>
	</table><br>
	<div style="clear:both"></div>
</div>