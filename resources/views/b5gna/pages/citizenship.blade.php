@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/citizenship">Moje občianstvo</a></div>
@else
<div class="mainTitle">Moje občianstvo</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br>
    <table width="50%" cellspacing="0" class="Tab center">
        <tr>
            <th colspan="2">Aktuálny stav</td>
        </tr>
        <tr>
            <td width="180">Rasa</td>
            <td style="color: {{ $player->race->color }}">{{ $player->race->name }}</td>
        </tr>
        <tr>
            <td>Štát</td>
            <td style="color: {{ $player->state->color }}">{{ $player->state->name }}</td>
        </tr>
        <tr>
            <td>Domovský sektor (HW)</td>
            <td><a href="/sector/{{ $player->hw->short }}">{{ $player->hw->name }}</a></td>
        </tr>
        {{-- <tr>
            <td>Strana/rod/kasta/kmeň</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Postavenie</td>
            <td>občan</td>
        </tr> --}}
    </table><br><br><a href="/rules-citizenship">Prejsť na pravidlá občianstva</a><br><br><br>
    <form action="" method="post">
        {{-- <input type="hidden" name="idx" value=""> --}}
        <table width="60%" cellspacing="0" class="Tab center">
            <tr>
                <th>Vzdať sa štátneho občianstva</th>
            </tr>
            <tr>
                <td style="color:red;">Kliknutím na toto tlačidlo prestaneš byť občanom štátu <b style="color: #6984f9">Pozemská Aliancia</b>!!!</td>
            </tr>
            <tr>
                <td class="TabH1"><input type="submit" name="AbandonSend" value="Vzdať sa občianstva"></td>
            </tr>
        </table>
    </form><br><br>
    <div style="clear:both"></div>
</div>