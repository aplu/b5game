@php
if (isset($fail)) {
	$title = 'Chyba pri prihlasovaní';
	$warning = '<span style="color:red">Meno a/alebo heslo nie je správne. Prístup do hry bol odmietnutý.</span><br><br>V prípade nejasností nám <a href="/contact">napíš</a>.';
} else {
	$title = 'Neprihlásen!';
	$warning = '<b style="color:red">Požadovaná stránka je prístupná iba prihláseným uživateľom!</b><br><br>
		Ak si bol prihlásený, tak platnosť tvojho prihlásenia vypršala. Prihlás sa znova!';
}
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/auth-failed">{{ $title }}</a></div>
@else
<div class="mainTitle">{{ $title }}</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

	@if (isset($warning))
	<div class="warningBox">
		{!! $warning !!}
	</div>
	@endif
	<div style="clear:both"></div>
</div>