@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="{{ route('states') }}">Štatistika štátov</a></div>
@else
<div class="mainTitle">Štatistika štátov</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br><br>
    <table width="80%" cellspacing="0" class="Tab center">
        <tr>
            <th>Limit hráčov na štát: {{ $age->players_per_state ?? '?' }}</th>
            <th>Skratka</th>
            <th>Počet<br>sektorov</th>
            <th>Počet<br>občanov</th>
            {{-- <th>Aktívni<br>hráči</th>
            <th>Time<br>out</th>
            <th>Ban</th> --}}
            <th>Verejná<br>registrácia</th>
            <th>Zničený</th>
            <th>Ochranná<br>lehota</th>
            <th>Body</th>
            {{-- <th>Poradie</th> --}}
        </tr>
        @foreach ($states as $state)
        <tr>
            <td style="color: {{ $state->color }}">{{ $state->name }}</td>
            <td style="color: {{ $state->color }}"><b>{{ $state->short }}</b></td>
            <td class="TabH1"><span class="hl">{{ $state->sectors_count }}</span> [{{ round($state->sectors_count / $sectors_count * 100, 1) }}%]</td>
            <td>{{ $state->players_count }}</td>
            {{-- <td>0</td>
            <td>0</td>
            <td>0</td> --}}
            <td>
                @if ($state->entry == 'CLOSED')
                    <font color="red">nie</font>
                @else
                    <font color="lime">áno</font>
                @endif
            </td>
            <td>
                @if ($state->destroyed)
                    <font color="lime">áno</font>
                @else
                    <font color="red">nie</font>
                @endif
            </td>
            <td>
                @if ($state->protected)
                    <font color="lime">áno</font>
                @else
                    <font color="red">nie</font>
                @endif
            </td>
            <td>{{ $state->score }}</td>
            {{-- <td></td> --}}
        </tr>
        @endforeach
    </table><br><br>

    <div class="center">
        <div id="chart_players" style="width:350px; height:280px; float: left"></div>
        <div id="chart_sectors" style="width:560px; height:280px; float: left"></div>
    </div><br>

    <div style="clear:both"></div><br>

    <h1>Vlády</h1><br>

    @foreach ($states as $state)
        <table width="70%" cellspacing="0" class="Tab center">
            <tr>
                <td colspan="2" style="background-color: {{ $state->color }}; color: black;"><b>{{ '[' . $state->short . '] ' . $state->name }}</b></td>
            </tr>
            {{-- <tr>
                <td width="200" style="text-align: left;"><b style="color: {{ $state->color }}">ujoduro</b> (Kiro)</td>
                <td style="text-align: left;">minister zahraničia</td>
            </tr>
            <tr>
                <td width="200" style="text-align: left;"><b style="color: {{ $state->color }}">Jerry</b> (Kiro)</td>
                <td style="text-align: left;">premiér</td>
            </tr>
            <tr>
                <td width="200" style="text-align: left;"><b style="color: {{ $state->color }}">Miso</b> (Despa)</td>
                <td style="text-align: left;">minister financií</td>
            </tr>
            <tr>
                <td width="200" style="text-align: left;"><b style="color: {{ $state->color }}">Anne</b> (Despa)</td>
                <td style="text-align: left;">cisár</td>
            </tr>
            <tr>
                <td width="200" style="text-align: left;"><b style="color: {{ $state->color }}">Caesar</b> (Kiro)</td>
                <td style="text-align: left;">člen Centauria</td>
            </tr> --}}
        </table><br><br>
    @endforeach

    {{-- <table width="70%" cellspacing="0" class="Tab center">
        <tr>
            <td colspan="2" style="background-color: #6984f9; color: black;"><b>[EA] Pozemská Aliancia</b></td>
        </tr>
        <tr>
            <td width="200" style="text-align: left;"><b style="color: #6984f9;">Apokalips</b> (NIE-My budeme vladnut :D)</td>
            <td style="text-align: left;">minister zahraničia</td>
        </tr>
        <tr>
            <td width="200" style="text-align: left;"><b style="color: #6984f9;">aragok</b> (ANO - Budeme vládnout )</td>
            <td style="text-align: left;">minister vnútra</td>
        </tr>
        <tr>
            <td width="200" style="text-align: left;"><b style="color: #6984f9;">jjenda</b> (ANO - Budeme vládnout )</td>
            <td style="text-align: left;">prezident</td>
        </tr>
        <tr>
            <td width="200" style="text-align: left;"><b style="color: #6984f9;">zdroj</b> (ANO - Budeme vládnout )</td>
            <td style="text-align: left;">poslanec parlamentu</td>
        </tr>
        <tr>
            <td width="200" style="text-align: left;"><b style="color: #6984f9;">Rio</b> (NIE-My budeme vladnut :D)</td>
            <td style="text-align: left;">minister financií</td>
        </tr>
    </table><br><br> --}}

    <script type="text/javascript">

google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChartPlayers);
function drawChartPlayers() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'štát');
    {{-- data.addColumn('number', 'Minbarská Federácia');
    data.addColumn('number', 'Narnský Režim');
    data.addColumn('number', 'Centaurská Republika');
    data.addColumn('number', 'Pozemská Aliancia');
    data.addColumn('number', 'Kolónia Drakhov');
    data.addColumn('number', 'Kruh Technomágov');
    data.addRow(['', 12, 10, 9, 9, 8, 3 ]); --}}
    @foreach ($states as $state)
        data.addColumn('number', "{!! $state->name !!}");
    @endforeach
    data.addRow(['', {{ implode(', ', $states->pluck('players_count', 'id')->toArray()) }} ]);
    var options = {
        title: 'Počet občanov',
        // {{-- colors: ['#8a68a5','#a03b37','#dd9090','#6984f9','#787811','#30bba9'], --}}
        colors: ['{!! implode("', '", $states->pluck('color', 'id')->toArray()) !!}'],
        chartArea: { left: '20%', width: '80%', top: '15%', height: '70%' },
        fontName: 'Tahoma, Verdana, Geneva, Arial, Helvetica, sans-serif',
        fontSize: '13',
        backgroundColor: '#080808',
        focusTarget: 'category',
        titleTextStyle: { color: 'red', fontSize: '14' },
        legend: { position: 'none' },
        hAxis: {
            baselineColor: '#f8f0ff',
            gridlines: { color: '#444444' },
            textStyle: { color: '#f8f0ff' },
            titleTextStyle: { color: '#ffcc00' },
            minorGridlines: { count: 2 },
            viewWindowMode: 'maximized',
            minValue: 0
        },
        vAxis: {
            baselineColor: '#f8f0ff',
            gridlines: { color: '#444444' },
            textStyle: { color: '#f8f0ff' },
            titleTextStyle: { color: '#ffcc00' }
        },
        bar: { groupWidth: '95%' }
    };

    var chart = new google.visualization.BarChart(document.getElementById('chart_players'));
    chart.draw(data, options);

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'štát');
    data.addColumn('number', 'počet sektorov');

    @foreach ($states as $state)
        data.addRow(["{!! $state->name !!}", {{ $state->sectors_count }}]);
    @endforeach
    {{-- data.addRow(['Minbarská Federácia',50]);
    data.addRow(['Centaurská Republika',34]);
    data.addRow(['Narnský Režim',20]);
    data.addRow(['Pozemská Aliancia',20]);
    data.addRow(['Kolónia Drakhov',4]);
    data.addRow(['Kruh Technomágov',2]);
    data.addRow(['neutrálne územie',10]); --}}
    var options = {
        title: 'Vlastníctvo sektorov',
        {{-- colors: ['#8a68a5','#dd9090','#a03b37','#6984f9','#787811','#30bba9','222222'], --}}
        colors: ['{!! implode("', '", $states->pluck('bg', 'id')->toArray()) !!}'],
        chartArea: {left: '9%', width: '90%', top: '15%', height: '80%'},
        fontName: 'Tahoma, Verdana, Geneva, Arial, Helvetica, sans-serif',
        fontSize: '13',
        backgroundColor: '#080808',
        titleTextStyle: {color: 'red', fontSize: '14'},
        legend: {textStyle: {color: '#f8f0ff'}}
    };

    var chart = new google.visualization.PieChart(document.getElementById('chart_sectors'));
    chart.draw(data, options);
}

    </script>
    <div style="clear:both"></div>
</div>