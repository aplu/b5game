@php
$user = Auth::user();

$months = ['Jan'=>1,'Feb'=>2,'Mar'=>3,'Apr'=>4,'May'=>5,'Jun'=>6,'Jul'=>7,'Aug'=>8,'Sep'=>9,'Oct'=>10,'Nov'=>11,'Dec'=>12];

/*$player = [
    'id' => 1,
    'email' => 'jeffrey@babylon5.space',
    'showmail' => true,
    'infomail' => true,
    'fname' => 'Jeffrey Sinclair',
    'icq' => 123456789,
    'skype' => 'jeffsinc',
    'tel' => '(+123) 123456789',
    'sex' => 'M',
    'birth' => 2000,
    'avatar' => '1.jpg',
];*/
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/profile">Upraviť profil</a></div>
@else
<div class="mainTitle">Upraviť profil</div>
@endif
<div class="mainTitleFade"></div>
<div class="content" style="text-align: center;">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-profile')
    @endif

    <br>

    @if ($errors->any())
        <br>Chyba:<br>
        <font color="red"><b>
            {{-- Povinné údaje musíš vyplniť!<br>
            Formát emailovej adresy nie je správny!<br>
            Ak nesúhlasíš s pravidlami, nemôžeš sa registrovať!<br> --}}
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </b></font><br>
    @endif

    <form action="{{ route('change-password') }}" method="POST">
        @method('PATCH')
        @csrf
    <div class="window center" style="text-align:justify; font-size:10px; width: 70%;">
        <div class="windowCaption">Zmeniť heslo</div>
        <div style="margin:3px;">
                <table cellspacing="3">
                    <tr>
                        <td width="130" align="right">Súčasné heslo:</td>
                        <td><input type="password" name="oldPass" value=""></td>
                    </tr>
                    <tr>
                        <td align="right">Nové heslo:</td>
                        <td><input type="password" name="newPass" maxlength="15"></td>
                    </tr>
                    <tr>
                        <td align="right">Ešte raz nové heslo:</td>
                        <td><input type="password" name="newPass2" maxlength="15"></td>
                    </tr>
                </table>
                <div align="right"><input type="submit" value="Zmeniť heslo"></div>
        </div>
    </div>
    </form>

    <br>

    <form action="{{ route('update-profile') }}" method="POST">
        @csrf
    <div class="window center" style="text-align:justify; font-size:10px; width: 70%;">
        <div class="windowCaption">Zmeniť údaje</div>
        <div style="margin:3px;">
            <table cellspacing="3">
                <tr>
                    <td width="130" align="right">E-mail:</td>
                    <td><input type="text" name="email" value="{{ old('email', $user->email) }}" maxlength="30" style="width:250;"></td>
                </tr>
                <tr>
                    <td align="right">Zverejniť e-mail:</td>
                    <td><input type="checkbox" name="email_visible" value="true"{{ old('email_visible', $user->email_visible) ? ' checked' : '' }}></td>
                </tr>
                <tr>
                    <td align="right">Upozornenia mailom:</td>
                    <td><input type="checkbox" name="email_notify" value="true"{{ old('email_notify', $user->email_notify) ? ' checked' : '' }}>
                    chcem dostávať mailom upozornenia na začiatok nového veku</td>
                </tr>
                <tr>
                    <td align="right">Celé meno:</td>
                    <td><input type="text" name="name" value="{{ old('name', $user->name) }}" maxlength="30" style="width:250;"></td>
                </tr>
                <tr>
                    <td align="right">ICQ:</td>
                    <td><input type="text" name="icq" value="{{ old('icq', $user->icq) }}" maxlength="12"></td>
                </tr>
                <tr>
                    <td align="right">Skype:</td>
                    <td><input type="text" name="skype" value="{{ old('skype', $user->skype) }}" maxlength="20"></td>
                </tr>
                <tr>
                    <td align="right">Telefón:</td>
                    <td><input type="text" name="tel" value="{{ old('tel', $user->tel) }}" maxlength="20"></td>
                </tr>
                <tr>
                    <td align="right">Pohlavie:</td>
                    <td><select name="sex">
                        @foreach (['M' => 'chlapec', 'F' => 'dievča'] as $key => $value)
                        <option value="{{ $key }}"{{ $key == old('sex', $user->sex) ? ' SELECTED' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select></td>
                </tr>
                <tr>
                    <td align="right">Narodenia:</td>
                    <td><select name="birth_day">
                            <option>-- den --</option>
                            @for ($day = 1; $day <= 31; $day++)
                                <option value="{{ $day }}"{{ (old('day', $user->birth_day) ? ' selected' : '') }}>{{ $day }}</option>
                            @endfor
                        </select>

                        <select name="birth_month">
                            <option>-- měsíc --</option>
                            @foreach ($months as $month => $num)
                                <option value="{{ $num }}"{{ (old('month', $user->birth_month) ? ' selected' : '') }}>{{ $month }}</option>
                            @endforeach
                        </select>

                        <select name="birth_year">
                            <option>-- rok --</option>
                    @for ($year = date('Y')-6; $year >= 1900; $year--)
                        <option value="{{ $year }}"{{ ($year == old('birth', $user->birth_year) ? ' selected' : '') }}>{{ $year }}</option>
                    @endfor
                    </select></td>
                </tr>
                <tr>
                    <td align="right">Menu:</td>
                    <td>
                        <input type="checkbox" name="tablink" value="true" checked>
                        otvoriť predvolenú stránku pri kliknutí na záložku menu
                    </td>
                </tr>
            </table>
            <div align="right"><input type="submit" name="update" value="Upraviť profil"></div>
        </div>
    </div>
    </form>

    <br>

    <form action="/profile" method="post" enctype="multipart/form-data">
    <div class="window center" style="text-align:justify; font-size:10px; width: 70%;">
        <div class="windowCaption">Zmeniť avatara</div>

            @csrf
            {{-- <input type="hidden" name="MAX_FILE_SIZE" value="7168"> --}}
        <div style="margin:3px;">
            <table>
                <tr>
                    @if (isset($user->avatar) && !empty($user->avatar))
                    <td align="center">
                        <br><img src="/avatars/{{ $user->avatar }}" width="50" height="60"><br>
                        <br>
                        {{-- <form action="/profile" method="post"> --}}
                            <div align="right">
                                <input type="submit" name="remove" value="Zmazať avatara">
                            </div>
                        {{-- </form> --}}
                    </td>
                    @endif
                    <td valign="top" style="padding-left:30px;">
                        <h2>Nový avatar</h2><br>
                        <span class="smallText">(max. 7 KiB; formát JPEG alebo GIF; rozmery 50x60 pixelov)</span>:<br>
                        <input type="file" name="avatarfile" style="width: 380px;"><br>
                    </td>
                </tr>
            </table>
            <div align="right"><input type="submit" name="upload" value="Zmeniť"></div>
        </div>
    </div>
    </form>

    <div style="clear:both"></div>
</div>