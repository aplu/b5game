@php
$user = Auth::user();

/*$player = [
    //'id' => 1,
    'login' => 'j.sinclair',
    'nick' => 'Jeffrey',
    'fname' => 'Jeffrey Sinclair',
    'age' => 40,
    'tel' => $user->phone, //'(+123) 123456789',
    'email' => $user->email,
    'skype' => 'jeffsinc',
    'icq' => 123456789,
    'registered' => date('d M Y', time()-1000000),
    'logins' => rand(0, 1000),
    'time_spent' => rand(0, 200) . ' dní ' . rand(0,23) . ' hodín ' . rand(0,59) . ' minút ' . rand(0,59) . ' sekúnd',
    'average_per_day' => rand(0,23) . ' hodín ' . rand(0,59) . ' minút ' . rand(0,59) . ' sekúnd',
    'posts' => rand(0, 10000),
    'avatar' => '1.jpg',
    'crystals' => rand(0, 100),
    'unread_notices' => 1,

    'race' => 'Ľudia',
    'state' => 'Pozemská Aliancia',
    'sector' => 'ear',
    'sector_name' => 'Earth',
    'party' => '-',
    'status' => 'občan',

    'fight_points' => rand(0, 100000),
    'level' => $level = rand(0, 15),
    'points_to_lvlup' => 50 * $level * $level,
];*/
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="{{ route('edit-profile') }}">Moje konto</a></div>
@else
<div class="mainTitle">Moje konto</div>
@endif
<div class="mainTitleFade"></div>
<div class="content">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-profile')
    @endif

    <div class="sideBox">
        <div class="sidePanelTitle">Základné údaje</div>
        <div class="sidePanel">
            {{-- <label class="sideLabel">Login (prihlasovacie meno)</label>
            <b class="hl">{{ $user->login }}</b><br> --}}

            <label class="sideLabel">Celé meno [vek]</label>
            <b>{{ $user->name }}</b>{{ isset($user->age) ? ' ['.$user->age.']' : '' }}<br>

            @if (isset($user->phone) && !empty($user->phone))
            <label class="sideLabel">Telefón</label>
            {{ $user->phone }}<br>
            @endif

            <label class="sideLabel">E-mail</label>
            {{ $user->email }}<br>

            @if (isset($user->discord) && !empty($user->discord))
            <label class="sideLabel">Discord</label>
            {{ $user->discord }}<br>
            @endif

            @if (isset($user->skype) && !empty($user->skype))
            <label class="sideLabel">Skype</label>
            {{ $user->skype }}<br>
            @endif

            @if (isset($user->icq) && !empty($user->icq))
            <label class="sideLabel">ICQ</label>
            {{ $user->icq }}<br>
            @endif

            {{-- <div class="toolbar">
                <a class="smallBTN" href="{{ route('edit-profile') }}" title="Zmeniť moje údaje"><img src="/images/b5gna/icons16/config.png" alt="" width="16" height="16" border="0">
                    <div>Zmeniť moje údaje</div>
                </a>
            </div> --}}
        </div>
        <div class="sidePanelTitle">Štatistika</div>
        <div class="sidePanel">
            <label class="sideLabel">Dátum registrácie</label>
            {{ $user->created_at->format('d M Y') }}<br>

            {{-- <label class="sideLabel">Počet prihlásení</label>
            {{ $user->login_count }}<br> --}}

            {{-- <label class="sideLabel">Čas strávený v hre</label>
            <span class="smallText">{{ $user['time_spent'] }}</span><br> --}}

            {{-- <label class="sideLabel">Priemer za deň</label>
            <span class="smallText">{{ $user['average_per_day'] }}</span><br> --}}

            {{-- <label class="sideLabel">Počet príspevkov vo fórach</label>
            {{ $user['posts'] }}<br> --}}

            <label class="sideLabel">Súčasná IP adresa</label>
            {{ $_SERVER['REMOTE_ADDR'] }}<br></div>
    </div>
    <div class="mainContentBox">

        <div>
            @if ((isset($player->avatar) && !empty($player->avatar)) || (isset($user->avatar) && !empty($user->avatar)))
                <div style="float:left; width:50px; height:60px; margin-right:10px;">
                    <img src="/avatars/{{ $player->avatar ?? $user->avatar }}" width="50" height="60">
                </div>
            @endif
            @if ($player)
            <div class="smallText" style="display:block; height:60px; margin-bottom:30px;">
                <b style="font-size:16px; color: {{ $player->state->color ?? 'white' }}">{{ $player->name ?? '' }}</b><br>
                @if (isset($player->crystals))
                    TM kryštály: <b>{{ $player->crystals }}</b><br>
                @endif
            </div>
            @endif
        </div>

        {{-- <form action="/age-prereg" method="get"><input type="submit" value="Predregistrácia do nového veku je k dispozícii"></form> --}}
        @if ($player->unread_notices ?? 0 > 0)
            <br><br>
            <div class="window center" style="font-size:12px; width:75%; background-color: #100909; border: 1px dotted #502020;">
                <div class="windowCaption" style="color: black; background-color: #d06060; background-image: none;">
                    Máš neprečítané administrátorské oznamy!
                </div>
                <div align="left" style="margin:10px;">
                    Počet nových oznamov: {{ $player->unread_notices }}<br><br>
                    <b style="color: red;">Každý hráč je povinný čítať oznamy administrátorov!</b><br><br>
                    <div align="right"><a href="/forum/admin">prečítať oznamy</a></div>
                </div>
            </div>
        @endif

        <br><br><br>
        @if (isset($player->race_id) && $player->race)
        <table width="60%" cellspacing="0" class="Tab2 center">
            <tr>
                <th colspan="2">Tvoj stav v aktuálnom veku</th>
            </tr>
            <tr>
                <td width="180">Rasa</td>
                <td style="color: {{ $player->race->color }}">{{ $player->race->name }}</td>
            </tr>
            <tr>
                <td>Štát</td>
                <td style="color: {{ $player->state->color }}">{{ $player->state->name }}</td>
            </tr>
            <tr>
                <td>Domovský sektor (HW)</td>
                <td><a href="/sector/{{ $player->hw->short }}">{{ $player->hw->name }}</a></td>
            </tr>
            {{-- <tr>
                <td>Strana/rod/kasta/kmeň</td>
                <td>{{ $player->party }}</td>
            </tr>
            <tr>
                <td>Politické postavenie</td>
                <td>{{ $player->position }}</td>
            </tr> --}}
            @if (in_array('FIGHT_POINTS', $rules))
            <tr>
                <td>Bojové body</td>
                <td><b>{{ $player->fight_points }}</b></td>
            </tr>
            <tr>
                <td>Level</td>
                <td><b class="hl">{{ $player->lvl }}</b></td>
            </tr>
            {{-- <tr>
                <td>Body potrebné pre ďalší level</td>
                <td>{{ $player->fight_points_to_lvlup() }}</td>
            </tr> --}}
            @endif
        </table><br><br>
        @endif

    </div><!-- /.mainContentBox -->

    <div style="clear:right"></div>

    @if (!count($ages))
        <div class="window" style="text-align:center; padding:2em; font-size:150%">
            {{ __('age.no_age') }}
        </div>
    @else
        <div class="window" style="text-align:center; padding:1em; margin:1em 1em 2em; font-size:120%">
            V seznamu věků níže můžeš vytvářet postavy a kliknutím na ně se mezi nimi přepínat.
        </div>

        @foreach ($ages as $age)
            @php
                $players_counter = $age->players()->count();
                $user_players = $age->user_players;
                // $user_players = $age->user_players(Auth::user())->get();

                $speeds = [];
                // Check Age properties with _speed suffix if speed is larger than 1.00
                foreach (['build', 'build_mining', 'mining', 'tech', 'production'] as $speed) {
                    if ($age->{$speed . '_speed'} != 1) {
                        $speeds[] = $age->{$speed . '_speed'} . '× ' . __('age.' . $speed . '_speed');
                    }
                }
            @endphp
            <div style="float:right; width:200px; text-align:center">
                <img src="{{ $age->image }}" style="border-radius:10px; border:1px solid blue">
            </div>
            <table width="60%" class="Tab center" style="margin:2em auto">
                <tr>
                    <th colspan="2" style="font-size:150%">
                        {!! __('Age') . ' #'.$age->id . ' <strong>' . $age->name . '</strong>' !!}
                    </th>
                </tr>
                <tr>
                    <th style="width:20%">{{ __('age.since_until') }}</th>
                    <td>{{ ($age->since ? $age->since->format('j.n.Y H:i') : __('age.unknown_date')) . ' – ' . ($age->until ? $age->until->format('j.n.Y H:i') : __('age.unknown_date')) }}</td>
                </tr>
                {{-- <tr>
                    <th>{{ __('age.type_title') }}</th>
                    <td>{{ __('age.type.' . $age->type) }}</td>
                </tr> --}}
                @if ($age->rules)
                    <tr>
                        <th>{{ __('age.rules_title') }}</th>
                        <td>@php
                            $age_rules = [];
                            foreach ($age->rules as $rule) {
                                $age_rules[] = ltrim(__('age.rules.' . $rule), 'age.rules.');
                            }
                            echo(implode(', ', $age_rules));
                        @endphp</td>
                    </tr>
                @endif

                @if (in_array('RECALC', $age->rules))
                <tr>
                    <th>{{ __('age.recalc_after') }}</th>
                    <td>{{ time_unit($age->recalc, true) }}</td>
                </tr>
                @endif

                @if (!empty($speeds))
                    <tr>
                        <th>{{ __('age.speeds') }}</th>
                        <td>{{ implode(', ', $speeds) }}</td>
                    </tr>
                @endif

                <tr>
                    <th>{{ __('age.author') }}</th>
                    <td>{{ $age->author }}</td>
                </tr>
                <tr>
                    <th>{{ __('age.num_of_players') }}</th>
                    <td>{{ $players_counter . ($age->max_players ? ' / ' . $age->max_players : '') }}</td>
                </tr>

                @if ($user_players)
                    @foreach ($user_players as $player)
                    <tr>
                        <th><big>{{ __('Player') }}</big></th>
                        <td>
                            <big><a href="{{ route('switch-player', ['player' => $player]) }}">
                                <big><b class="hl">{{ $player->name }}</b></big>
                                <span style="color:{{$player->race->color}}">({{ $player->race->name }})</span><br>
                                <span style="color:{{$player->state->color}}">{{ $player->state->name }}</span>
                                {{-- od {{ $player->created_at->format('j.n.Y H:i') }} --}}
                            </a></big>
                        </td>
                    </tr>
                    @endforeach
                @endif

                <tr>
                    <th colspan="2">
                        @if ($age->since && $age->since > now())
                            <strong>Věk ještě nezačal.</strong>
                        @elseif ($age->until && $age->until < now())
                            <strong>Věk již skončil.</strong>
                        @elseif ($age->max_players && $players_counter >= $age->max_players)
                            <strong>Max hráčů {{ $age->max_players }} dosaženo.</strong>
                        @elseif ($age->players_per_user && count($user_players) >= $age->players_per_user)
                            <strong>Máš maximum možných postav ({{ $age->players_per_user }}) v tomto věku.</strong>
                        @else
                            <form action="{{ route('choose-race', ['age' => $age]) }}" method="POST">
                            @csrf

                            @if ($age->type == 'PUBLIC' || Auth::user()->tester)
                                <input type="submit" value="{{ __('profile.create_player') }}">
                                {{ count($user_players) . ' / ' . $age->players_per_user }}
                            @else
                                <strong>Vstup pouze pro testery.</strong>
                            @endif
                            </form>
                        @endif
                    </th>
                </tr>
            </table>
        @endforeach
    @endif

    <div style="clear:both"></div>
</div>