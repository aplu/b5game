@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/move-plan">Letový plán</a></div>
@else
<div class="mainTitle">Letový plán</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

	@if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-move')
    @endif

    Vyber začiatočný sektor letového plánu<br><br>
    <img src="/images/b5gna/map/map.png" alt="" border="0" usemap="#map">
    <map name="map" id="map">
        <area shape="poly" alt="" coords="704,109,709,127,709,159,690,159,690,149,666,149,666,126,670,126,670,109" href="/move-plan?sector=ear" title="Earth">
    </map><br>

    <div style="clear:both"></div>
</div>