@php
$type = 'private'; // private,state

switch ($type) {
    default:
    case 'private':
        $title = 'Denník pohybov na surovinových účtoch';
        $tableWidth = '90%';
        break;

    case 'state':
        $title = 'Denník pohybov na národnom účte';
        $tableWidth = '85%';
        break;
}
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/balance">Denník pohybov na surovinových účtoch</a></div>
@else
<div class="mainTitle">Denník pohybov na surovinových účtoch</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">


@if ($type == 'state')
    <table width="75%" cellspacing="0" class="Tab center">
        <tr class="TabH1" style="font-size: 12px; color:red;">
            <td colspan="5"><b>Národný sklad</b></td>
        </tr>
        <tr>
            <th>Titánium</th>
            <th>Quantium 40</th>
            <th>Kredity</th>
            <th>Ruda Titánium</th>
            <th>Ruda Quantium 40</th>
        </tr>
        <tr>
            <td>1'016'750 ton</td>
            <td>145'780 ton</td>
            <td>1'302'849'573</td>
            <td>930'163 ton</td>
            <td>288'626 ton</td>
        </tr>
    </table>
@endif

    <br><br>
    <form action="" method="post"> <input type="text" name="from" value="17-10-2273"> <input type="text" name="to" value="24-10-2273"> <input type="submit" value="Zobraziť"></form><br>
    <table width="{{ $tableWidth }}" cellspacing="0" class="Tab center">
        <tbody>
            <tr>
                <th>Čas</th>
                <th>Kredity</th>
                <th>Titánium</th>
                <th>Q40</th>
                <th>Ruda Ti</th>
                <th>Ruda Q40</th>
                <th>Pohyb</th>
            </tr>
            <tr>
                <td class="TabH1">24 Oct - 20:58</td>
                <td style="color:red">-1</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td class="TabH1" style="text-align:left;"><b class="hl">prevod:</b> príjemca: <b style="color: #30bba9">Sir_Chislehurst</b></td>
            </tr>
            <tr>
                <td class="TabH1">24 Oct - 20:38</td>
                <td style="color:red">-200'000</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td class="TabH1" style="text-align:left;"><b class="hl">burza:</b> vytvorenie dopytu</td>
            </tr>
            <tr>
                <td class="TabH1">24 Oct - 20:34</td>
                <td style="color:red">0</td>
                <td style="color:red">-1'000</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td class="TabH1" style="text-align:left;"><b class="hl">burza:</b> vytvorenie ponuky</td>
            </tr>
            <tr>
                <td class="TabH1">24 Oct - 20:29</td>
                <td style="color:red">0</td>
                <td style="color:lime">1'000</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td class="TabH1" style="text-align:left;"><b class="hl">burza:</b> stiahnutie ponuky</td>
            </tr>
            <tr>
                <td class="TabH1">24 Oct - 20:00</td>
                <td style="color:red">-14'000'000</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td class="TabH1" style="text-align:left;"><b class="hl">dane</b> </td>
            </tr>
            <tr>
                <td class="TabH1">24 Oct - 20:00</td>
                <td style="color:red">0</td>
                <td style="color:lime">23'400</td>
                <td style="color:lime">438</td>
                <td style="color:red">-58'500</td>
                <td style="color:red">-2'437</td>
                <td class="TabH1" style="text-align:left;"><b class="hl">prepočet</b> </td>
            </tr>
            <tr class="TabH2">
                <th>Spolu za obdobie:</th>
                <td style="color:red">-149'214'290</td>
                <td style="color:red">-168'075</td>
                <td style="color:red">-2'048</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <th></th>
        </tbody>
    </table>
    <br>
    <div style="clear:both"></div>
</div>