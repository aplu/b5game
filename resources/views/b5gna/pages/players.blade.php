@php

$states = [
    'none' => ['color' => '#e0e0e0', 'bg' => '#202020', 'name' => 'Bez občianstva'],
    'CR'   => ['color' => '#dd9090', 'bg' => '#2A0000', 'name' => 'Centaurská Republika'],
    'DE'   => ['color' => '#787811', 'bg' => '#1e1e04', 'name' => 'Kolónia Drakhov'],
    'TM'   => ['color' => '#30bba9', 'bg' => '#0c2e2a', 'name' => 'Kruh Technomágov'],
    'MF'   => ['color' => '#8a68a5', 'bg' => '#221a29', 'name' => 'Minbarská Federácia'],
    'NR'   => ['color' => '#a03b37', 'bg' => '#280e0d', 'name' => 'Narnský Režim'],
    'EA'   => ['color' => '#6984f9', 'bg' => '#03081C', 'name' => 'Pozemská Aliancia'],
    'VE'   => ['color' => '#88aa88', 'bg' => '#112211', 'name' => 'Vorlon Empire'],
];

$allPlayers = ['Jeffrey', 'Delenn', 'John', 'Susan'];

/*if (!empty($_POST)) {
	$players = [
		['name'=>'Jeffrey', 'sex'=>'m', 'state'=>'EA', 'age'=>'35', 'created_at'=>'03 Dec 2239'],
		['name'=>'Delenn', 'sex'=>null, 'state'=>'MF', 'age'=>'74', 'created_at'=>'21 Aug 2200'],
		['name'=>'Susan', 'sex'=>'f', 'state'=>'EA', 'age'=>'31', 'created_at'=>'11 Jun 2243'],
	];
}*/
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/players">Zoznam hráčov</a></div>
@else
<div class="mainTitle">Zoznam hráčov</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

	<br>
	<table class="center"><tbody>
		<tr>
			<td>
				<h2>Vyber hráča</h2>
				<br>
				<form action="/players/" method="post">
					@csrf
					<select name="idx" style="width:220px;">
						<option value="all">--- všetci aktívni ---</option>
						@foreach ($players as $player)
							<option value="{{ $player->id }}">{{ $player->name }}</option>
						@endforeach
					</select>
					<br>
					<br>
					<input type="submit" value="Zobraziť">
				</form>
			</td>
			<td>&nbsp;</td>
			<td>
				<h2>Online hráči</h2>
				<br>
				<form action="/players/" method="post">
					@csrf
					<select name="typ_online" style="width:220px;">
						<option value="all">--- všetci ---</option>
						<option value="cenrep">Centaurská Republika</option>
						<option value="drakh">Kolónia Drakhov</option>
						<option value="techno">Kruh Technomágov</option>
						<option value="minbar">Minbarská Federácia</option>
						<option value="narn">Narnský Režim</option>
						<option value="ea">Pozemská Aliancia</option>
						<option value="">Bez občianstva</option>
					</select>
					<br>
					<br>
					<input type="submit" value="Zobraziť">
				</form>
			</td>
			<td>&nbsp;</td>
			<td>
				<h2>Vyber štát</h2>
				<br>
				<form action="/players/" method="post">
					@csrf
					<select name="typ_vypis" style="width:220px;">
						<option value="cenrep">Centaurská Republika</option>
						<option value="drakh">Kolónia Drakhov</option>
						<option value="techno">Kruh Technomágov</option>
						<option value="minbar">Minbarská Federácia</option>
						<option value="narn">Narnský Režim</option>
						<option value="ea">Pozemská Aliancia</option>
						<option value="" selected="">Bez občianstva</option>
						<option value="ban">Banánoví kluci a holčičky</option>
					</select>
					<br>
					<br>
					<input type="submit" value="Zobraziť">
				</form>
			</td>
		</tr>
	</tbody></table>
	<br><br>

@if (!empty($filtered_players))
	<table width="98%" cellspacing="0" class="Tab center">
		<tbody>
			<tr>
				<th width="23"></th>
				<th>Nick</th>
				<th width="30">Vek</th>
				<th width="40">Stav<br>konta</th>
				<th>Rasa</th>
				<th>Štátne<br>postavenie</th>
				<th>Strana/rod<br>kasta/kmeň</th>
				<th>Telefón</th>
				<th>Skype</th>
				<th width="78">Dátum<br>registrácie</th>
			</tr>
	@foreach ($players as $id => $p)
			<tr style="color: {{ $states[$p['state']]['color'] }}">
		@if ($p['sex'] == 'f')
				<td><img src="/images/b5gna/female.gif" width="18" height="18" alt=""></td>
		@elseif ($p['sex'] == 'm')
				<td><img src="/images/b5gna/male.gif" width="18" height="18" alt=""></td>
		@else
				<td><img src="/images/b5gna/nosex.gif" width="18" height="18" alt=""></td>
		@endif
				<td><a href="/player/{{ $id }}" class="name" style="color: {{ $states[$p['state']]['color'] }}">{{ $p['name'] }}</a></td>
				<td>{{ $p['age'] ? $p['age']/*$p->born->diffInYears()*/ : '' }}</td>
				<td>ok</td>
				<td style="color: {{ $states[$p['state']]['color'] }}">{{ $states[$p['state']]['name'] }}</td>
				<td>občan</td>
				<td>-</td>
				<td></td>
				<td></td>
				<td>{{ $p['created_at']/*->format('d M Y')*/ }}</td>
			</tr>
	@endforeach
		</tbody>
	</table>

@elseif (!empty($selected_player))

	<br><br>
	<table width="50%" cellspacing="0" class="Tab center"><tbody>
		<tr>
			<th colspan="2">Základné údaje</th>
		</tr>
		<tr>
			<td>Avatar</td>
			<td><img src="/avatars/1.jpg" width="50" height="60"></td>
		</tr>
		<tr>
			<td width="180">Nick</td>
			<td style="color: #{{ $states[$player['state']]['color'] }}"><b>{{ $player['name'] }}</b></td>
		</tr>
		{{-- <tr>
			<td>Celé meno</td>
			<td>-</td>
		</tr> --}}
		<tr>
			<td>Vek</td>
			<td>{{ $player['age'] ? $player['age']/*$p->born->diffInYears()*/ : '-' }}</td>
		</tr>
		{{-- <tr>
			<td>E-mail</td>
			<td>nezverejnený</td>
		</tr> --}}
		{{-- <tr>
			<td>Telefón</td>
			<td>0910582876</td>
		</tr> --}}
		{{-- <tr>
			<td>Skype</td>
			<td>-</td>
		</tr> --}}
		{{-- <tr>
			<td>ICQ</td>
			<td>-</td>
		</tr> --}}
	</tbody></table>

	<br><br>

	<table width="50%" cellspacing="0" class="Tab center"><tbody>
		<tr>
			<th colspan="2">Postavenie v hre</th>
		</tr>
		<tr>
			<td width="180">Rasa</td>
			<td style="color: {{ $states[$player['state']]['color'] }}">{{ $states[$player['state']]['name'] }}</td>
		</tr>
		{{-- <tr>
			<td>Štát</td>
			<td style="color: #{{ $player->nation->color }}">{{ $player->nation->name }}</td>
		</tr> --}}
		<tr>
			<td>Strana/rod/kasta/kmeň</td>
			<td>-</td>
		</tr>
		<tr>
			<td>Postavenie</td>
			<td>-</td>
		</tr>
	</tbody></table>

	<br><br>

	<table width="50%" cellspacing="0" class="Tab center"><tbody>
		<tr>
			<th colspan="2">Štatistika</th>
		</tr>
		<tr>
			<td>Dátum registrácie</td>
			<td>{{ /*$player->created_at->format('d M Y')*/ $player['created_at'] }}</td>
		</tr>
		<tr>
			<td>Posledné prihlásenie</td>
			<td>{{ /*$player->lastLogin->format('d M Y - H:i:s')*/ $player['last_login'] }}</td>
		</tr>
		{{-- <tr>
			<td>Počet prihlásení</td>
			<td>3252</td>
		</tr>
		<tr>
			<td>Čas strávený v hre</td>
			<td>61 dní 15 hodín 15 minút 57 sekúnd</td>
		</tr>
		<tr>
			<td>Priemer za deň</td>
			<td>1 hodín 33 minút 41 sekúnd</td>
		</tr>
		<tr>
			<td>Počet príspevkov vo fórach</td>
			<td>149</td>
		</tr>
		<tr>
			<td>SPAM index</td>
			<td>0,157</td>
		</tr> --}}
	</tbody></table>

@endif

	<br>
	<div style="clear:both"></div>
</div>
