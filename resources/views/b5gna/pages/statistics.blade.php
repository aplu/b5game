@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/statistics">Štatistika hráčov</a></div>
@else
<div class="mainTitle">Štatistika hráčov</div>
@endif
<div class="mainTitleFade"></div>
<div class="content" style="text-align: center;">

    ekonomický a vojenský rozvoj<br><br><br>

    <table width="50%" cellspacing="0" class="Tab center">
        <tr>
            <th style="width: 50px;">Poradie</th>
            <th>Meno</th>
            <th>Percento z maxima</th>
        </tr>
        <tr>
            <td>1</td>
            <td style="color: #dd9090 ">Jerry</td>
            <td>100 %</td></tr>
        <tr>
            <td>2</td>
            <td style="color: #8a68a5">Asgard12</td>
            <td>91 %</td>
        </tr>
        <tr>
            <td>3</td>
            <td style="color: #dd9090 ">Dark One</td>
            <td>87 %</td></tr>
        <tr>
            <td>4</td>
            <td style="color: #dd9090">ujoduro</td>
            <td>86 %</td>
        </tr>
        <tr>
            <td>5</td>
            <td style="color: #8a68a5 ">Richard B. Riddick</td>
            <td>84 %</td></tr>
        <tr>
            <td>6</td>
            <td style="color: #8a68a5">Maverick</td>
            <td>77 %</td>
        </tr>
        <tr>
            <td>7</td>
            <td style="color: #dd9090 ">Anne</td>
            <td>71 %</td></tr>
        <tr>
            <td>8</td>
            <td style="color: #8a68a5">Ulkesh</td>
            <td>70 %</td>
        </tr>
        <tr><td>9</td><td style="color: #dd9090 ">navinav</td><td>68 %</td></tr>
        <tr>
            <td>10</td>
            <td style="color: #8a68a5">DusKE</td>
            <td>68 %</td>
        </tr>
        <tr><td>11</td><td style="color: #a03b37 ">Kakarrot</td><td>64 %</td></tr>
        <tr>
            <td>12</td>
            <td style="color: #8a68a5">ShalTok</td>
            <td>62 %</td>
        </tr>
        <tr><td>13</td><td style="color: #dd9090 ">Magog</td><td>62 %</td></tr>
        <tr>
            <td>14</td>
            <td style="color: #8a68a5">Shafter</td>
            <td>61 %</td>
        </tr>
        <tr><td>15</td><td style="color: #8a68a5 ">voloda1024</td><td>59 %</td></tr>
        <tr>
            <td>16</td>
            <td style="color: #dd9090">Miso</td>
            <td>58 %</td>
        </tr>
        <tr><td>17</td><td style="color: #a03b37 ">ANALfabet_Hubakuk</td><td>55 %</td></tr>
        <tr>
            <td>18</td>
            <td style="color: #dd9090">ven0m</td>
            <td>54 %</td>
        </tr>
        <tr><td>19</td><td style="color: #dd9090 ">Caesar</td><td>53 %</td></tr>
        <tr>
            <td>20</td>
            <td style="color: #a03b37">popocatepetI</td>
            <td>53 %</td>
        </tr>
        <tr><td>21</td><td style="color: #6984f9 ">Rio</td><td>49 %</td></tr>
        <tr>
            <td>22</td>
            <td style="color: #6984f9">jjenda</td>
            <td>45 %</td>
        </tr>
        <tr><td>23</td><td style="color: #a03b37 ">klapacius</td><td>44 %</td></tr>
        <tr>
            <td>24</td>
            <td style="color: #a03b37">Turgon III.</td>
            <td>43 %</td>
        </tr>
        <tr><td>25</td><td style="color: #787811 ">Kosh VI</td><td>43 %</td></tr>
        <tr>
            <td>26</td>
            <td style="color: #787811">Velký teplý Al</td>
            <td>43 %</td>
        </tr>
        <tr><td>27</td><td style="color: #8a68a5 ">volvo23</td><td>43 %</td></tr>
        <tr>
            <td>28</td>
            <td style="color: #787811">aquarius</td>
            <td>42 %</td>
        </tr>
        <tr><td>29</td><td style="color: #a03b37 ">Škvrnitý Démon</td><td>40 %</td></tr>
        <tr>
            <td>30</td>
            <td style="color: #a03b37">haho</td>
            <td>39 %</td>
        </tr>
        <tr><td>31</td><td style="color: #787811 ">yntums</td><td>38 %</td></tr>
        <tr>
            <td>32</td>
            <td style="color: #787811">Dobyvatel</td>
            <td>34 %</td>
        </tr>
        <tr><td>33</td><td style="color: #6984f9 ">Apokalips</td><td>32 %</td></tr>
        <tr>
            <td>34</td>
            <td style="color: #787811">Ragnarök</td>
            <td>32 %</td>
        </tr>
        <tr><td>35</td><td style="color: #787811 ">froggy</td><td>30 %</td></tr>
        <tr>
            <td>36</td>
            <td style="color: #6984f9">Gaterinka</td>
            <td>30 %</td>
        </tr>
        <tr><td>37</td><td style="color: #6984f9 ">zdroj</td><td>30 %</td></tr>
        <tr>
            <td>38</td>
            <td style="color: #787811">Mikiti</td>
            <td>25 %</td>
        </tr>
        <tr><td>39</td><td style="color: #a03b37 ">Emperor XXII</td><td>23 %</td></tr>
        <tr>
            <td>40</td>
            <td style="color: #6984f9">GimliCZ</td>
            <td>21 %</td>
        </tr>
        <tr><td>41</td><td style="color: #a03b37 ">tlčhuba</td><td>20 %</td></tr>
        <tr>
            <td>42</td>
            <td style="color: #8a68a5">Nusku1</td>
            <td>15 %</td>
        </tr>
        <tr><td>43</td><td style="color: #6984f9 ">aragok</td><td>12 %</td></tr>
        <tr>
            <td>44</td>
            <td style="color: #8a68a5">hraničář</td>
            <td>10 %</td>
        </tr>
        <tr><td>45</td><td style="color: #8a68a5 ">makx</td><td>4 %</td></tr><tr class="TabH2 ">
            <td>46</td>
            <td style="color: #6984f9">nobody</td>
            <td>1 %</td>
        </tr>
        <tr><td>47</td><td style="color: #6984f9 ">Itachi</td><td>0 %</td></tr>
        <tr>
            <td>48</td>
            <td style="color: #6984f9">TomiYalkz</td>
            <td>0 %</td>
        </tr>
        <tr><td>49</td><td style="color: #a03b37 ">sybok</td><td>0 %</td></tr></table>    <div style="clear:both "></div>
  </div>