@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/fights-mine">Prehľad mojich prebiehajúcich útokov</a></div>
@else
<div class="mainTitle">Prehľad mojich prebiehajúcich útokov</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

    <br>
    <table width="90%" cellspacing="0" class="Tab center">
        <tr>
            <th>Začiatok</th>
            <th width="45">Sektor</th>
            <th>Útočník</th>
            <th>Obranca</th>
            <th>Typ</th>
            <th>Ďalšie kolo</th>
            <th width="55">Počet kôl</th>
            <th></th>
        </tr>
        <tr>
            <td colspan="8"><br>naše lode nie sú zapojené v žiadnom útoku<br><br></td>
        </tr>
    </table><br>
    <div style="clear:both"></div>
</div>