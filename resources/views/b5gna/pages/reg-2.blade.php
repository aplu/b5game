@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/reg-2">Registrácia</a></div>
@else
<div class="mainTitle">Registrácia</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

	<div class="narrowBox">
		<p>Pre prístup do hry je potrebné si vytvoriť užívateľské konto. Získaš ho registráciou. Vyplň svoje registračné údaje a klikni na "Odoslať registráciu". Skontroluj správnosť zadanej e-mailovej adresy a taktiež, či ju nemaž zaplnenú, inak sa nám nepodarí Ti zaslať prístupové heslo. V prípade komplikácii nám <a href="/contact">napíš</a>.</p>
		<br>
		<div class="window" style="font-size:12px;">
			<div class="windowCaption">Registrácia užívateľa</div>
			<div align="left" style="margin:10px;">

				@if ($errors->any())
					<br>Chyba pri registrácii:<br>
					<font color="red"><b>
						{{-- Povinné údaje musíš vyplniť!<br>
						Formát emailovej adresy nie je správny!<br>
						Ak nesúhlasíš s pravidlami, nemôžeš sa registrovať!<br> --}}
			            @foreach ($errors->all() as $error)
			                {{ $error }}<br>
			            @endforeach
					</b></font><br>
				@endif

				<form action="{{ route('register') }}" method="post">
				@csrf
					<table width="100%" cellspacing="2">
						<tr>
							<td colspan="2"><h1>Povinné údaje</h1></td>
						</tr>
						{{-- <tr>
							<td width="110" align="right"><label for="nick">Nick</label></td>
							<td>
								<input id="nick" type="text" name="nick" value="{{ old('nick') }}" required maxlength="20" style="width:150px;">
								&nbsp;&nbsp; prezývka, pod ktorou budeš zobrazovaný v hre --}}
                                {{-- @error('name') <br><font color="red"><strong>{{ $message }}</strong></font><br><br> @enderror --}}
							{{-- </td>
						</tr> --}}
						<tr>
							<td align="right"><label for="email">E-mail</label></td>
							<td>
								<input id="email" type="text" name="email" value="{{ old('email') }}" required style="width:150px;">
                                @error('email') <font color="red"><strong>{{ $message }}</strong></font> @enderror
							</td>
						</tr>
						<tr>
							<td align="right"><label for="password">Heslo</label></td>
							<td><input id="password" type="password" name="password" required style="width:150px;"></td>
						</tr>
						<tr>
						<tr>
							<td align="right"><label for="password_confirmation">Potvrdit heslo</label></td>
							<td><input id="password_confirmation" type="password" name="password_confirmation" required style="width:150px;"></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input id="rules_confirmation" type="checkbox" name="rules_confirmation" required class="checkbox" {{ old('rules_confirmation') ? 'checked' : '' }}> <label for="rules_confirmation">Súhlasím s <a href="/rules" target="_blank">pravidlami hry</a></label></td>
						</tr>

						<tr>
							<td colspan="2">
								<br>
								<h1>Voliteľné údaje</h1>
								Ak máš záujem o sebe povedať viac<br>
								<br>
							</td>
						</tr>
						<tr>
							<td align="right"><label>Meno</label></td>
							<td><input type="text" name="name" value="{{ old('name') }}" maxlength="64" style="width:150px;"></td>
						</tr>
						<tr>
							<td align="right"><label>Pohlavie</label></td>
							<td><select name="sex" style="width:158px;">
								<option value="">-- neurčené --</option>
								<option value="m"{{ old('sex') == 'm' ? ' selected' : '' }}>mužské</option>
								<option value="f"{{ old('sex') == 'f' ? ' selected' : '' }}>ženské</option>
							</select></td>
						</tr>
						<tr>
							<td align="right"><label>Rok narodenia</label></td>
							<td><select name="birth_year" style="width:158px;">
								<option value="">- neurčené -</option>
								@for ($year = date('Y')-6; $year >= 1900; $year--)
									<option value="{{ $year }}"{{ $year == old('birth_year') ? ' selected' : '' }}>{{ $year }}</option>
								@endfor
							</select></td>
						</tr>

						{{-- <tr>
							<td align="right"><label>Telefon</label></td>
							<td><input type="text" name="phone" value="" maxlength="32" style="width:150px;"></td>
						</tr> --}}
						{{-- <tr>
							<td align="right"><label><a href="http://discord.com">Discord</a></label></td>
							<td><input type="text" name="discord" value="{{ old('discord') }}" maxlength="32" style="width:150px;"></td>
						</tr> --}}
						{{-- <tr>
							<td align="right"><label><a href="https://matrix.org/" target="_blank">Matrix</a></label></td>
							<td><input type="text" name="matrix" value="" maxlength="32" style="width:150px;"></td>
						</tr>
						<tr>
							<td align="right"><label>Messenger</label></td>
							<td><input type="text" name="messenger" value="" maxlength="64" style="width:150px;"></td>
						</tr>
						<tr>
							<td align="right"><label>Skype</label></td>
							<td><input type="text" name="skype" value="" maxlength="32" style="width:150px;"></td>
						</tr>
						<tr>
							<td align="right"><label>Telegram</label></td>
							<td><input type="text" name="telegram" value="" maxlength="32" style="width:150px;"></td>
						</tr>
						<tr>
							<td align="right"><label>XMPP/Jabber</label></td>
							<td><input type="text" name="telegram" value="" maxlength="32" style="width:150px;"></td>
						</tr>
						<tr>
							<td align="right"><label>ICQ</label></td>
							<td><input type="text" name="icq" value="" maxlength="16" style="width:150px;"></td>
						</tr> --}}
						<tr>
							<td colspan="2" align="right"><br>
								<input type="submit" value="Odoslať registráciu">
							</td>
						</tr>
					</table>
				</form>

			</div>
		</div>
	</div>

	<div style="clear:both"></div>
</div>