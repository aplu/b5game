@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/market-log">Moje transakcie</a></div>
@else
<div class="mainTitle">Moje transakcie</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-market')
    @endif

    <br>
    <form action="" method="post"> <input type="text" name="from" value="20-10-2273"> <input type="text" name="to" value="27-10-2273"> <input type="submit" value="Zobraziť"></form><br>
    <table width="90%" cellspacing="0" class="Tab center">
        <tr>
            <td colspan="7" class="TabH1">
                <h2>Predaný tovar</h2>
            </td>
        </tr>
        <tr>
            <th>Čas</th>
            <th>Burza</th>
            <th>Kupujúci</th>
            <th>Komodita</th>
            <th>Množstvo</th>
            <th>Cena</th>
            <th>Daň</th>
        </tr>
        <tr>
            <td>25 Oct - 23:21</td>
            <td style="color: #60e090">Galaktická burza</td>
            <td style="color: #787811">Kosh VI</td>
            <td style="color:lime">Titánium</td>
            <td>1'000</td>
            <td>200</td>
            <td>0</td>
        </tr>
    </table><br><br>
    <table width="90%" cellspacing="0" class="Tab center">
        <tr>
            <td colspan="7" class="TabH1">
                <h2>Kúpený tovar</h2>
            </td>
        </tr>
        <tr>
            <th>Čas</th>
            <th>Burza</th>
            <th>Predávajúci</th>
            <th>Komodita</th>
            <th>Množstvo</th>
            <th>Cena</th>
            <th>CLO</th>
        </tr>
        <tr>
            <td>24 Oct - 22:51</td>
            <td style="color: #60e090">Galaktická burza</td>
            <td style="color: #8a68a5">Maverick</td>
            <td style="color:lime">Ruda Titánium</td>
            <td>1'000</td>
            <td>135</td>
            <td>0</td>
        </tr>
    </table><br><br>
    <div style="clear:both"></div>
</div>