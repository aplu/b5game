@php
/*$players = [
	1 => 'Jeffrey Sinclair',
	2 => 'Delenn',
	3 => 'Londo Mollari',
	4 => 'G\'Kar'
];*/
$players = [
     1 => ['nick' => 'Delenn',    'state' => 'MF', 'race' => 'Minbari',    'avatar' => '1.jpg'],
     2 => ['nick' => 'Lennier',   'state' => 'MF', 'race' => 'Minbari',    'avatar' => '1.jpg'],
     3 => ['nick' => 'Elric',     'state' => 'TM', 'race' => 'Technomage', 'avatar' => '1.jpg'],
     4 => ['nick' => 'Galen',     'state' => 'TM', 'race' => 'Technomage', 'avatar' => '1.jpg'],
     5 => ['nick' => 'GKar',      'state' => 'NR', 'race' => 'Narn',       'avatar' => '1.jpg'],
     6 => ['nick' => 'NaToth',    'state' => 'NR', 'race' => 'Narn',       'avatar' => '1.jpg'],
     7 => ['nick' => 'Elisabeth', 'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
     8 => ['nick' => 'Jeffrey',   'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
     9 => ['nick' => 'John',      'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    10 => ['nick' => 'Lilli',     'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    11 => ['nick' => 'Lyta',      'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    12 => ['nick' => 'Michael',   'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    13 => ['nick' => 'Susan',     'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    14 => ['nick' => 'Stephen',   'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    15 => ['nick' => 'Talia',     'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    16 => ['nick' => 'Theresa',   'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    17 => ['nick' => 'Kosh',      'state' => 'VE', 'race' => 'Vorlon',     'avatar' => '1.jpg'],
    18 => ['nick' => 'Ulkesh',    'state' => 'VE', 'race' => 'Vorlon',     'avatar' => '1.jpg'],
    19 => ['nick' => 'Londo',     'state' => 'CR', 'race' => 'Centauri',   'avatar' => '1.jpg'],
    20 => ['nick' => 'Vir',       'state' => 'CR', 'race' => 'Centauri',   'avatar' => '1.jpg'],
];
// $players = \App\Http\Controllers\B5GNA\PlayersController::$players;

$msgSet = [
	'news' => [
		124 => ['fromId' => 1, 'from' => 'Jeffrey Sinclair', 'time' => 9587629800, 'text' => 'hi'],
	],
	'pm' => [
		123 => ['toId' => 1, 'to' => 'Jeffrey Sinclair', 'time' => 9587629766, 'text' => 'hi ;-)'],
		124 => ['fromId' => 1, 'from' => 'Jeffrey Sinclair', 'time' => 9587629800, 'text' => 'hi'],
	],
	'sys' => [
	],
	'arch' => [
	],
];

@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/msg">Súkromná pošta</a></div>
@else
<div class="mainTitle">Súkromná pošta</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content msg" style="text-align: center;">

	<script language="JavaScript">
		function PutText(text){
			if (document.postform.posttext){
				document.postform.posttext.focus();
				document.postform.posttext.value+=text;
			}
		}
		function ReplyMsg(id){
			if (document.postform.adress){
				document.postform.posttext.focus();
				document.postform.adress.value=id;
			}
		}
		function ShowMsg(id){
			obj=document.getElementById('news');  obj.style.display="none";
			obj=document.getElementById('pm');  obj.style.display="none";
			obj=document.getElementById('sys');  obj.style.display="none";
			obj=document.getElementById('arch');  obj.style.display="none";
			obj=document.getElementById(id);  obj.style.display="";
			$("#anews").css('color','#8282d9');
			$("#apm").css('color','#8282d9');
			$("#asys").css('color','#8282d9');
			$("#aarch").css('color','#8282d9');
			$("#a"+id).css('color','#ffcc00');
		}
	</script>
	<br>
	<form action="" method="post" name="postform">
		<textarea cols="80" rows="8" name="posttext"></textarea>
		<br>
		<br>

		Adresát: &nbsp; <select name="adress" style="width:150">
			<option value="--">-- vyberte hráča --</option>
	@foreach ($players as $id => $player)
			<option value="{{ $id }}">{{ $player['nick'] }}</option>
	@endforeach
		</select> &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="postsend" value="Odoslať správu">
	</form>
	<div style="margin: 8 0 8 0">
		<a href="javascript:PutText(':-D')"><img src="/images/b5gna/smilies/biggrin.gif"></a>
		<a href="javascript:PutText(':-)')"><img src="/images/b5gna/smilies/smile.gif"></a>
		<a href="javascript:PutText(':-(')"><img src="/images/b5gna/smilies/sad.gif"></a>
		<a href="javascript:PutText(':-o')"><img src="/images/b5gna/smilies/surprised.gif"></a>
		<a href="javascript:PutText(':shock:')"><img src="/images/b5gna/smilies/shock.gif"></a>
		<a href="javascript:PutText(':-?')"><img src="/images/b5gna/smilies/confused.gif"></a>
		<a href="javascript:PutText('8-)')"><img src="/images/b5gna/smilies/cool.gif"></a>
		<a href="javascript:PutText(':lol:')"><img src="/images/b5gna/smilies/lol.gif"></a>
		<a href="javascript:PutText(':-p')"><img src="/images/b5gna/smilies/razz.gif"></a>
		<a href="javascript:PutText(':oops:')"><img src="/images/b5gna/smilies/redface.gif"></a>
		<a href="javascript:PutText(':cry:')"><img src="/images/b5gna/smilies/cry.gif"></a>
		<a href="javascript:PutText(':evil:')"><img src="/images/b5gna/smilies/evil.gif"></a>
		<a href="javascript:PutText(':badgrin:')"><img src="/images/b5gna/smilies/badgrin.gif"></a>
		<a href="javascript:PutText(':roll:')"><img src="/images/b5gna/smilies/rolleyes.gif"></a>
		<a href="javascript:PutText(';-)')"><img src="/images/b5gna/smilies/wink.gif"></a>
		<a href="javascript:PutText(':!:')"><img src="/images/b5gna/smilies/exclaim.gif"></a>
		<a href="javascript:PutText(':?:')"><img src="/images/b5gna/smilies/question.gif"></a>
		<a href="javascript:PutText(':idea:')"><img src="/images/b5gna/smilies/idea.gif"></a>
		<a href="javascript:PutText(':-|')"><img src="/images/b5gna/smilies/neutral.gif"></a>
		<a href="javascript:PutText(':doubt:')"><img src="/images/b5gna/smilies/doubt.gif"></a>
		<a href="javascript:PutText(':(')"><img src="/images/b5gna/smilies/sad2.gif"></a>
		<a href="javascript:PutText(':love:')"><img src="/images/b5gna/smilies/icon_love.gif"></a>
		<a href="javascript:PutText(':beer:')"><img src="/images/b5gna/smilies/beer.png"></a>
		<a href="javascript:PutText(':)')"><img src="/images/b5gna/smilies/smile2.gif"></a>
		<a href="javascript:PutText(':keel:')"><img src="/images/b5gna/smilies/icon_keel.gif"></a>
		<a href="javascript:PutText(':eyes:')"><img src="/images/b5gna/smilies/rolleyes2.gif"></a>
		<a href="javascript:PutText('8)')"><img src="/images/b5gna/smilies/glasses.gif"></a>
		<a href="javascript:PutText(':x')"><img src="/images/b5gna/smilies/shutup.gif"></a>
		<a href="javascript:PutText(':square:')"><img src="/images/b5gna/smilies/square.gif"></a>
		<a href="javascript:PutText(':sleep:')"><img src="/images/b5gna/smilies/sleep.gif"></a>
		<a href="javascript:PutText(':up:')"><img src="/images/b5gna/smilies/thumbsup.gif"></a>
		<a href="javascript:PutText(':down:')"><img src="/images/b5gna/smilies/thumbsdown.gif"></a>
		<a href="javascript:PutText(':toilet:')"><img src="/images/b5gna/smilies/icon_toilet.gif"></a>
		<a href="javascript:PutText(':wall:')"><img src="/images/b5gna/smilies/wall.gif"></a>
		<a href="javascript:PutText(':peace:')"><img src="/images/b5gna/smilies/peace.gif"></a>
	</div><br>
	
	<div>
		&laquo; <a href="javascript:ShowMsg('news')" id="anews" style="color:#ffcc00">Nové správy</a> &raquo;
		&laquo; <a href="javascript:ShowMsg('pm')" id="apm">Osobné správy</a> &raquo;
		&laquo; <a href="javascript:ShowMsg('sys')" id="asys">Systémové správy</a> &raquo;
		&laquo; <a href="javascript:ShowMsg('arch')" id="aarch">Archivované správy</a> &raquo;
	</div><br>


	<form action="" method="post">
	@foreach ($msgSet as $type => $msgs)
		<div id="{{ $type }}"@if($type!='news') style="display:none"@endif>
			@if (empty($msgs))
			Nie sú prítomné žiadne správy<br><br>
			@else
			@foreach ($msgs as $id => $msg)
			<table width="80%" cellspacing="0" class="TabPost">
				<tr>
				@if (isset($msg['from']))
					<td>Od: <a href="javascript:ReplyMsg('{{ $msg['fromId'] }}')" class="name" style="color: #">{{ $msg['from'] }}</a></td>
				@else
					<td>Pre: <a href="javascript:ReplyMsg('{{ $msg['toId'] }}')" class="name" style="color: #">{{ $msg['to'] }}</a></td>
				@endif
					<td style="text-align:right;">{{ date('d M Y - H:i:s', $msg['time']) }}</td>
				</tr>
				<tr>
					<td colspan="2" style="border-top: 1px solid #B2EAB3">
						<table width="100%" cellspacing="0" class="TabPost" style="padding-right: 5; padding-left: 5; padding-top: 1; border: 0;">
							<tr>
								<td width="90"><input type="checkbox" name="del_id[]" value="991386"><span class="smallText"> zmazať</span><br><input type="checkbox" name="arch_id[]" value="991386"><span class="smallText"> archivovať</span></td>
								<td valign="top" class="postText">{{ $msg['text'] }}</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
			@endforeach
			@endif
		</div>
	@endforeach

		<div class="msgSelectionBox center">
			<div align="left" style="width:200px; font-size: 10px;" class="center">
				<input type="checkbox" name="del_all" value="true" class="checkbox"> zmazať všetky správy<br>
				<input type="checkbox" name="del_sys" value="true" class="checkbox"> zmazať systémové správy<br>
				<input type="checkbox" name="del_send" value="true" class="checkbox"> zmazať odoslané správy<br>
				<input type="checkbox" name="del_rec" value="true" class="checkbox"> zmazať prijaté správy<br>
			</div><br>
			<input type="submit" name="endosk" value="Vykonať">
		</div>
	</form>

	<div style="clear:both"></div>
</div>