<div class="mainTitle">Úvod do hry B5Game</div>
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

<div class="narrowBox" style="text-align: justify;">
  
  <h2 class="manual">Vitaj v online webovej hre Babylon 5 Game: New Age</h2>

  <h2>Táto stránka je zastaralá!</h2><br>
  
  <div class="manualSub">
    <p>
    Už z názvu možno vieš, že hra je inšpirovaná televíznym sci-fi seriálom Babylon 5 (B5), ktorého autorom je J.Michael Straczynský.
    Príbehovo je voľným pokračovaním seriálu, kedy staršie rasy odišli za okraj a galaxia sa spamätáva z vojny, ktorú rozpútala jedna zo starších rás - Tiene.
    V hre si vytvárajú ďalšie pokračovanie príbehu sami hráči. Príbeh sa môže aj čiastočne alebo úplne odkláňať od B5 univerza resp. od príbehovej línie stanovenej v seriály.
    Poznanie seriálu nie je pre hru nutné.
    </p>

    <p>
    <b>Babylon 5 Game: New Age</b> je viac-menej kolektívna hra, v ktorej závisí víťazstvo od spolupráce medzi hráčmi. Je neustále vo vývoji, na ktorom sa môžu spolupodieľať všetci. Poskytuje plnú variabilitu pre rôzne štýly vekov (jedno herné obdobie - od vynulovania hry po dosiahnutie cieľa),
    či vo veku hrajúce štáty a jednotlivé rasy. V hre rozlišujeme rasy a štáty analogicky ako v skutočnom svete. Hráč si na začiatku veku vyberie rasu (Narni, Pozemšťania...), ktorá je celý vek nemenná.
    Od toho závisia napr. niektoré technológie, ktoré možno využívať. Štát je skupina hráčov, ktorí hrajú spoločne (Narnský režim, Pozemská aliancia...). Príslušnosť k štátu sa nazýva občianstvo.
    Primárne majú občania jedného štátu jednu rasu. Avšak nie je to nutné. Hráč môže počas veku opustiť jeden štát a pridať sa k inému bez ohľadu na rasu. Skupina hráčov si môže vytvoriť aj nový štát.
    Štáty môžu tiež vlastniť technológie a teda technológie dostupné konkrétnemu hráčovi sú závislé od kombinácie rasy a štátu.
    Každý štát má svoj vlastný politický systém, na základe ktorého sa niektorým občanom prideľujú práva konať za štát (napr. voľbami) - stávajú vládou štátu.
    </p>
    
    <p>
    Základnými štátmi sú Kolónia Drakhov (DKH staršia rasa - bývalí pomocníci Tieňov), Minbarská federácia (MF), Pozemská aliancia (EA), Samospráva Marsu (SM), Narnský režim (NR), Drazi Freehold (DFH) a Centaurská republika (CR).
    Do deja a samotnej hry môžu zasiahnuť aj iné zoskupenia dané seriálom, napr. Interstellar Alliance (IA, Medzihviedzna aliancia) ale aj samotnými hráčmi vytvorené nové štáty.
    V hre je možné aj oddeliť sa od pôvodného štátu a zostať bez občianstva (jeden z mála prvkov nekolektívnosti v hre). Vtedy hráč hrá výlučne sám za seba.
    <br><br>
    Dozor nad hrou, pravidlami a vývojom vykonávajú administrátori, ktorí sú príslušníci špeciálnej rasy a špeciálneho štátu - Kruh Technomágov (TM).
    <br><br>

    Veky trvajú rôzne dlho, závisí od cieľa v danom prebiehajúcom veku. Všeobecne ich môžeme rozdeliť na tri najzákladnejšie typy:
    <ul>
      <li><b>Deathmatch vek (DM)</b>, kde môže byť množstvo štátov vopred stanovené a víťazom je jeden štát alebo aliancia, ktorá dobyje Domovský svet (HW, Homeworld) iného štátu.</li>
      <li><b>Klasický vek</b>, kde cieľ môže byť ekonomický, územný, iný vojenský ako DM a podobne. V tomto veku sa do hry zapájajú všetky štáty.</li>
      <li><b>Zmiešaný vek</b>, v ktorom sa do veku zapájajú len niektoré rasy ale hra nekončí dobytím HW štátu.</li>
    </ul>

    Hra tak ako aj ostatné obsahujú určité základné piliere - prvky, ktorých tou správnou kombináciou a vyváženosťou možno byť v danom veku úspešný. Sú to:
    <ul>
      <li><b>Ekonomika</b></li>
      <li><b>Flotila</b></li>
      <li><b>Komunikácia medzi hráčmi, organizácia a vláda</b></li>
      <li><b>Diplomacia</b></li>
    </ul>
    </p>

    <p>
    Samotný dizajn hry je rozdelený na niekoľko menu a jeho ďalších zložiek. Ako prvá pomoc pre hráča slúži tlačidlo "? Pomoc", ktoré je umiestnené v pravom hornom rohu pod panelom s menu. Po kliknutí sa zobrazí pomoc k aktuálnej stránke.
    Detailnejšiu pomoc možno nájsť v manuály hry. V prípade nejasností je dobré žiadať vysvetlenia na herných fórach od svojich spoluhráčov, ideálne na fóre "Pre nováčikov".    
    </p>    
  </div>
  <br>

 
  <h2 class="manual">Mapa</h2>

  <div class="manualSub">
    <p>
    Herný svet predstavuje mapa, ktorá je rozdelená do sektorov. Každý štát má svoj domovský sektor (homeworld - skratka HW).
    Každý hráč má (jednu) stanicu, ktorá sa na začiatku nachádza v HW jeho štátu. Avšak hráč si stanicu môže presunúť. V sektore stanice prebieha napr. výroba flotily, ktorá sa môže ďalej presúvať po mape.
    </p>
  </div>  
  <br>


  
  <h2 class="manual">Ekonomika</h2>

  <div class="manualSub">

    <p>
    Ekonomika je jedným zo štartovacích prvkov, ktorý priamo ovplyvňuje iné prvky v hre, predovšetkým flotilu. Sila každého štátu nezávisí len od počtu hráčov v danom štáte, lebo aj tie menej početné môžu mať navrch vďaka rýchlejšie rozvinutej ekonomike. 
    </p>
    <p>
    Hlavnými zložkami ekonomiky sú dva druhy surovín: Titánium (Ti), Quantium 40 (Q40) a Kredity (Cr), ktoré sú všeobecným platidlom. Niektoré rasy k hre kredity nepotrebujú (tzv. staršie rasy), prípadne potrebú len v menšom množstve.
    Titánium sa získava tavením rudy Titánia (rTi) v Taviacich peciach (TP).
    Rudu Titánia ťažia Ťažobné lode. Q40 sa získava spracovaním rudy Q40 (rQ40) v rafinériách. Rudu Q40 ťažia Tankery. Kredity sú výsledkom výnosov z obchodovania Obchodných lodí. 
    <br><br>
    Budovy, ktoré môže hráč stavať:
    <ul>
      <li><b>Taviace pece</b> - vyrábajú Titánium z rudy</li>
      <li><b>Rafinérie</b> - vyrábajú Q40 z rudy</li>
      <li><b>Obchodné centrá</b> - umožňujú stavbu obchodných lodí</li>
      <li><b>Továrne</b> - urýchľujú výrobu stíhačov, lodí a plavidiel pre ťažbu</li>
      <li><b>Hangár</b> - slúži ako miesto pre ťažobné a obchodné lode, pre tankery a pre stíhače</li>
      <li><b>Vesmírny dok</b> - môže byť len jeden, umožňuje stavbu krížnikov a lodí, umožňuje stavbu ťažobných plošín (vysvetlíme nižšie)</li>
    </ul>
    
    Každý deň o 20:00 nastáva tzv. prepočet. V tomto momente sa vypočítava výroba Rafinérií a Taviacich pecí. Zároveň sa cez prepočet hráčom odpočítajú dane pre ich štát.
    <br><br>
    Množstvo rafinérií a pecí, množstvo ťažobných lodi, tankerov a obchodných lodí, ako aj kvalitu spracovania možno zvyšovať upgradami v ekonomickej časti hry:
    <ul>
      <li><b>Efektivita taviacich pecí</b> - zvyšuje efektivitu spracovania rudy Titánia na Titánium</li>
      <li><b>Efektivita rafinerií</b> - zvyšuje efektivitu spracovania rudy Quantia 40 na Quantium 40</li>
      <li><b>Efektivita obchodovania</b> - zvyšuje výnosnosť obchodným lodiam</li>
      <li><b>Miesto na stanici</b> - zvyšuje miesto pre stavbu budov</li>
    </ul>
    </p>

    <p>
    Na ďalšie možné zhodnocovanie a navyšovanie surovín a kreditov, prípadne doplňovanie slúži Burza a Dotácie medzi hráčmi. Každý štát má aj svoj národný sklad surovín, do ktorého môže získať suroviny a kredity len prostredníctvom daní.
    S ekonomikou ale aj s inými prvkami v hre súvisí mapa galaxie, poloha stanice hráča ako aj možnosť jej presunu a poloha iných sektorov.
    Každý sektor má indexy, ktoré určujú výnosnosť ťažby a obchodovania. V sektore môže vláda štátu postaviť (a aj zbúrať): 
    <ul>
      <li><b>Skokovú bránu</b> - umožňuje ťažbu v sektore a zrýchľuje pohyb plavidiel flotily.</li>
      <li><b>Obchodnú stanicu</b> - zvyšuje výnosnosť obchodovania a teda Kreditov.</li>
      <li><b>Ťažobnú stanicu</b> - zvyšuje výnosnosť ťažby rúd (rTi a rQ40).</li>
      <li><b>Obrannú stanicu</b> - súvisí s vojenskými činnosťami. Zvyšuje silu flotily pri obrane sektora.</li>
    </ul>
    </p>

    <p>
    Ťažba ako aj obchody sa dejú nielen v domovskom svete ale aj v sektoroch mimo HW. Ak chceme ťažiť v domovskom svete, stačí vyrobiť ťažobné lode a obchodné lode a tankery, zaradiť do skupiny a vyslať ťažiť do domovského sektora.
    Ak však chceme ťažiť mimo domovského sveta potrebujeme ťažobnú plošinu, ktorá má nosnosť 50 lodiek a už vyššie spomenutú skokovú bránu.
    Avšak nie všetky rasy potrebujú na ťažbu v mimo sektoroch skokovú bránu (sú to staršie rasy, napr. Drakhovia, Tiene).
    Ťažobnú plošinu však môžeme vyrobiť až keď máme určitý stupeň rozvoja ekonomiky - je nutné postaviť vesmírny dok.
    V domovskom sektore môžu ťažiť všetci hráči štátu (ale iba s maximálne 200 loďami), no v mimodomovskom sektore je ťažobných lodí všetkých hráčov obmedzený iba kapacitou sektora.
    V domovskom sektore trvá jeden ťažobný cyklus 12 hodín. V mimodomovskom je základná dĺžka 16 hodín a k nej sa pripočítavajú podľa vzdialenosti domovského za každý sektor 2 hodiny. Na ťažbu je v hre možné útočiť.
    Brániť ťažbu si môžeme nielen samotnou flotilou, ale aj ochrannými letkami stíhačov, ktoré sú pridelené priamo k danej ťažbe.
    Ako bolo spomenuté vyššie pri stavbách - hangár je tou stavbou, ktorá určuje kapacitu pre množstvo stíhačov, ktoré môžem použiť na ochranu ťažby.
    </p>  
  </div>
  <br>
  
  
  <h2 class="manual">Flotila</h2>

  <div class="manualSub">
    <p>
    Klasický prvok v hre, ktorý je priamo napojený na ekonomickú časť. Každá rasa a štát vlastní určité technológie, ktoré si konkrétny hráč môže zaviesť do praxe. Na základe toho je možné stavať 4 základné typy plavidiel:
    <ul>
      <li><b>stíhače</b> (S)</li>
      <li><b>krížniky</b> (K)</li>
      <li><b>lode</b> (L)</li>
      <li><b>destroyer</b> (D)</li>
      </ul>
    </p>
  
    <p>
    Technológie dávajú jednotlivým štátom a ich hráčom možnosť stavať a budovať niekoľko druhov plavidiel v rámci uvedených základných typov. Inak povedané hráč konkrétneho štátu môže v závislosti od veku vyrobiť viacero druhov stíhačov, krížnikov, či lodí.
    Ďalšími premennými flotily sú pohyb, sila útoku, obrany. Každý typ plavidla má svoje špecifiká a svoje parametre a nie všetky plavidlá sú schopné akéhokoľvek druhu pohybu. Pohyb vychádza zo seriálu a teda z existencie hyperpriestoru. Plavidlá teda v rámci svojho pohybu vstupujú do hyperpriestoru
    a späť do normálneho vesmíru pomocou skokových motorov alebo skokových brán prípadne iných možných technológií. Pohyb je možný iba medzi sektormi, ktoré spolu susedia. Všetky útoky - na ťažbu,
    na sektor ako taký alebo len na konkrétny štát sa vedú priamo v sektoroch. V hyperpriestore nie je možné útočiť. Boje prebiehajú v kolách, každé kolo trvá určitý čas. Skupinám plavidiel
    ako aj letkám pri stíhačoch je možné nastaviť stratégiu a to aj počas boja a medzi jednotlivými kolami. Boj trvá až do úplného zničenia jednej zo strán, avšak každý má možnosť z boja ustúpiť.
    </p>
  </div>
  <br>

    
  <h2 class="manual">Komunikácia medzi hráčmi, organizácia, vláda a diplomacia</h2>

  <div class="manualSub">
    <p> 
    Ide rovnako o veľmi dôležité prvky v hre, ktoré dávajú hre azda najväčšiu variabilitu a možnosti. Každý štát má svoj vlastný systém volieb a vládu (kompetencie), prípadne iný orgán, ktorý vystupuje v mene štátu. Komunikáciu v hre zabezpečujú rôzne druhy fór a pošta. Základné fóra:
    <ul>
      <li><b>Galaktické fórum</b> (GF) - pre všetkých hráčov.</li>
      <li><b>Politické fórum</b> (PF) - vidia všetci hráči, ale právo písať majú len členovia vlády zodpovední za diplomaciu.</li>
      <li><b>Kronika veku</b> (KV) - slúži na dotváranie príbehu.</li>
      <li><b>Pre nováčikov</b> - určené na otázky predovšetkým pre nováčikov ale aj pre všetkých ostatných hráčov.</li>
      <li><b>Admin oznamy</b> - povinné čítanie pre všetkých hráčov.</li>
      <li><b>Bug fórum</b> - miesto, kde je možné oznámiť chybu v hre.</li>
      <li><b>Vývojové fórum</b> - nápady, návrhy, stručne a jasne všetko, čo by sme chceli zmeniť.</li>
      <li><b>Národné fórum</b> - pre všetkých občanov štátu.</li>
      <li><b>Kastové/rodové/fórum politickej strany</b> - určené pre občanov podľa ich príslušnosti v štáte.</li>
      <li><b>Vládne fórum</b> - len pre členov vlády daného štátu.</li>
    </ul>
    </p>
  
    <p>
    Každý hráč má možnosť zakladať (aj rušiť) svoje vlastné osobné fóra, kde si môže určiť, kto tam bude mať prístup. Každý štát má tak vytvorenú možnosť organizovať sa a využívať ešte lepšie možnosti ekonomiky a flotily tak, aby sa v danom veku
    stal aj so svojimi občanmi víťazom. Na to mu slúži aj diplomacia a nastavovanie vzťahov aliancie, vojny či neutrality, anketa a výveska. Ankety sú v hre trojaké - administrátorská, národná a vládna. Neodmysliteľnou
    pomôckou pre hráča je nastaviteľný Centrálny informačný systém (CIS), ktorý hráčov upozorňuje na rôzne udalosti vo všetkých oblastiach hry (napr. výsledok ťažby, prepočtu, zmeny v politike štátu a pod.).
    </p>
    <br><br>


    <p>
    Veríme, že či už seriál Babylon 5 poznáš alebo nie, hra a jej prvky Ťa zaujmú a staneš sa platným členom štátu, prípadne jeho vodcom na ceste za výhrou.
    </p>
    <br>



    <div align="center"><form action="http://b5manual.icsp.sk/doku.php?id=jak_zacit" method="post"><input type="submit" value="Návod k prvým krokom v hre"></form></div><br>

  </div>
  
</div>


    <div style="clear:both"></div>
  </div>