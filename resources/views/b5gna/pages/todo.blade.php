@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/todo">ToDo List</a></div>
@else
<div class="mainTitle">ToDo List</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">
	<br>
	<div style="text-align:left;">
		<table>
			<tr><th>Dátum</th><th>Typ</th><th>Priorita</th><th>Popis</th></tr>
			<tr><td width="75">01.02.2020</td><td width="60" style="color: #84FFC3">[feature]</td><td width="110" style="color: #22ff22">[stredná priorita]</td><td style="padding-bottom:5px;">přesunout <a href="https://trello.com/b/56ILmnc0/b5game-open-age" target="_blank">ToDo do Trella</a></td></tr>
		</table>
	</div>
	<div style="clear:both"></div>
</div>
