@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/acclog">Denník zmien na hráčskych kontách</a></div>
@else
<div class="mainTitle">Denník zmien na hráčskych kontách</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br><br>
    <table width="75%" cellspacing="0" class="Tab center">
        <tr>
            <th width="150">Čas</th>
            <th>Účet</th>
            <th>Pohyb</th>
            <th>Operátor</th>
        </tr>
        <tr>
            <td>22 Oct 2019 - 19:24:38</td>
            <td style="color:#">nickbenjack</td>
            <td>zamietnutie aktivácie účtu</td>
            <td style="color:#30bba9">Sir_Chislehurst</td>
        </tr>
        <tr>
            <td>22 Oct 2019 - 19:24:37</td>
            <td style="color:#">benjack</td>
            <td>aktivácia účtu</td>
            <td style="color:#30bba9">Sir_Chislehurst</td>
        </tr>
        <tr>
            <td>22 Oct 2019 - 13:20:32</td>
            <td style="color:#">benjack</td>
            <td>registrácia</td>
            <td style="color:#ff0000">systém</td>
        </tr>
        <tr>
            <td>22 Oct 2019 - 13:17:55</td>
            <td style="color:#">nickbenjack</td>
            <td>registrácia</td>
            <td style="color:#ff0000">systém</td>
        </tr>
        <tr>
            <td>22 Oct 2019 - 08:03:01</td>
            <td style="color:#">sido</td>
            <td>odblokovanie účtu</td>
            <td style="color:#ff0000">systém</td>
        </tr>
        <tr>
            <td>19 Oct 2019 - 08:24:57</td>
            <td style="color:#">psitron</td>
            <td>odblokovanie účtu</td>
            <td style="color:#ff0000">systém</td>
        </tr>
        <tr>
            <td>18 Oct 2019 - 02:36:36</td>
            <td style="color:#a03b37">sybok</td>
            <td>zapojenie do veku</td>
            <td style="color:#ff0000">systém</td>
        </tr>
        <tr>
            <td>18 Oct 2019 - 02:36:12</td>
            <td style="color:#a03b37">sybok</td>
            <td>odblokovanie účtu</td>
            <td style="color:#ff0000">systém</td>
        </tr>
        <tr>
            <td>16 Oct 2019 - 13:53:11</td>
            <td style="color:#6984f9">Zathras</td>
            <td>zapojenie do veku</td>
            <td style="color:#ff0000">systém</td>
        </tr>
        <tr>
            <td>16 Oct 2019 - 13:48:43</td>
            <td style="color:#6984f9">Zathras</td>
            <td>odblokovanie účtu</td>
            <td style="color:#ff0000">systém</td>
        </tr>
        <tr>
            <td>15 Oct 2019 - 16:17:37</td>
            <td style="color:#8a68a5">makx</td>
            <td>zapojenie do veku</td>
            <td style="color:#ff0000">systém</td>
        </tr>
        <tr>
            <td>15 Oct 2019 - 16:16:51</td>
            <td style="color:#8a68a5">makx</td>
            <td>odblokovanie účtu</td>
            <td style="color:#ff0000">systém</td>
        </tr>
        <tr>
            <td>13 Oct 2019 - 20:00:04</td>
            <td style="color:#">20Markus</td>
            <td>deaktivácia (Time-Out Limit)</td>
            <td style="color:#ff0000">systém</td>
        </tr>
    </table><br><br>
    <div style="clear:both"></div>
</div>