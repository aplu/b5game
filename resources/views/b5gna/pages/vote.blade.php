@php
$title = $title ?? 'Administrátorská anketa'; // 'Anketa', 'Štátna anketa'
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/vote">{{ $title }}</a></div>
@else
<div class="mainTitle">{{ $title }}</div>
@endif
<div class="mainTitleFade"></div>

<div class="content vote" style="text-align: center;">

	@if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-vote')
    @endif

	<br>
	<table width="80%" border="0" cellspacing="0" class="TabPost">
	    <tr>
	        <td>Založil: <span style="color: #30bba9"><b>Sir_Chislehurst</b></span><br>Tvoja voľba: ešte si nehlasoval/a!</td>
	        <td style="text-align:right">09 Mar 2273 - 12:09:38&nbsp;<br>Hlasovali:&nbsp;53&nbsp;</td>
	    </tr>
	    <tr>
	        <td colspan="2" class="postText" style="text-align:center; border-top: 1px solid #B2EAB3;">Aký je Váš záujem hrať túto hru?</td>
	    </tr>
	    <tr>
	        <td colspan="2">
	            <ul style="margin: 5 5 3 40;" class="postText">
	                <li><a href="?idx=1258&opt=0&typ=1" class="ABox">1. Chcem hrať bez ohľadu na nastavenie veku/hry:&nbsp; <span class="stdText">40</span></a></li>
	                <li><a href="?idx=1258&opt=1&typ=1" class="ABox">2. Chcem hrať, ale len uzatvorený "VIP" vek, iný nie:&nbsp; <span class="stdText">5</span></a></li>
	                <li><a href="?idx=1258&opt=2&typ=1" class="ABox">3. Dám si pauzu, uvidím časom:&nbsp; <span class="stdText">8</span></a></li>
	                <li><a href="?idx=1258&opt=3&typ=1" class="ABox">4. Už nechcem hrať vôbec:&nbsp; <span class="stdText">0</span></a></li>
	            </ul>
	        </td>
	    </tr>
	    <tr>
	        <td colspan="2" style="border-top: 1px solid #B2EAB3;">
	            <table width="100%" cellspacing="0" class="TabPost" style="border:0; font-size:12px;">
	                <tr>
	                    <td width="20" style="vertical-align: top;"> 1:</td>
	                    <td valign="bottom" style="padding-left: 10;"><b><span style="color: #6984f9">zdroj</span></b>; <b><span style="color: #">Storman</span></b>; <b><span style="color: #dd9090">Magog</span></b>; <b><span style="color: #6984f9">Apokalips</span></b>; <b><span style="color: #a03b37">Kakarrot</span></b>; <b><span style="color: #787811">froggy</span></b>; <b><span style="color: #6984f9">Rio</span></b>; <b><span style="color: #dd9090">Caesar</span></b>; <b><span style="color: #a03b37">popocatepetI</span></b>; <b><span style="color: #787811">aquarius</span></b>; <b><span style="color: #a03b37">Emperor XXII</span></b>; <b><span style="color: #6984f9">GimliCZ</span></b>; <b><span style="color: #6984f9">jjenda</span></b>; <b><span style="color: #">lukassssss</span></b>; <b><span style="color: #8a68a5">Shafter</span></b>; <b><span style="color: #dd9090">ujoduro</span></b>; <b><span style="color: #">20Markus</span></b>; <b><span style="color: #6984f9">aragok</span></b>; <b><span style="color: #">malus</span></b>; <b><span style="color: #a03b37">haho</span></b>; <b><span style="color: #">bukator</span></b>; <b><span style="color: #">MORFEUS</span></b>; <b><span style="color: #a03b37">Turgon III.</span></b>; <b><span style="color: #dd9090">Jerry</span></b>; <b><span style="color: #">bedo26</span></b>; <b><span style="color: #787811">Ragnarök</span></b>; <b><span style="color: #8a68a5">ShalTok</span></b>; <b><span style="color: #8a68a5">Richard B. Riddick</span></b>; <b><span style="color: #dd9090">Miso</span></b>; <b><span style="color: #">tomas1502</span></b>; <b><span style="color: #787811">Dobyvatel</span></b>; <b><span style="color: #787811">Kosh VI</span></b>; <b><span style="color: #8a68a5">voloda1024</span></b>; <b><span style="color: #">Cleve</span></b>; <b><span style="color: #dd9090">Dark One</span></b>; <b><span style="color: #a03b37">tlčhuba</span></b>; <b><span style="color: #787811">Velký teplý Al</span></b>; <b><span style="color: #dd9090">Anne</span></b>; <b><span style="color: #6984f9">Itachi</span></b>; <b><span style="color: #8a68a5">Maverick</span></b>; </td>
	                </tr>
	                <tr>
	                    <td colspan="2" style="text-align:center">* * *</td>
	                </tr>
	                <tr>
	                    <td width="20" style="vertical-align: top;"> 2:</td>
	                    <td valign="bottom" style="padding-left: 10;"><b><span style="color: #30bba9">Bard</span></b>; <b><span style="color: #8a68a5">Nusku1</span></b>; <b><span style="color: #30bba9">Shaade</span></b>; <b><span style="color: #">Defender</span></b>; <b><span style="color: #a03b37">ANALfabet_Hubakuk</span></b>; </td>
	                </tr>
	                <tr>
	                    <td colspan="2" style="text-align:center">* * *</td>
	                </tr>
	                <tr>
	                    <td width="20" style="vertical-align: top;"> 3:</td>
	                    <td valign="bottom" style="padding-left: 10;"><b><span style="color: #8a68a5">Ulkesh</span></b>; <b><span style="color: #">sisli</span></b>; <b><span style="color: #">scorpion99</span></b>; <b><span style="color: #">TheNihilist</span></b>; <b><span style="color: #">Tomin</span></b>; <b><span style="color: #dd9090">ven0m</span></b>; <b><span style="color: #">jaro78</span></b>; <b><span style="color: #a03b37">klapacius</span></b>; </td>
	                </tr>
	                <tr>
	                    <td colspan="2" style="text-align:center">* * *</td>
	                </tr>
	                <tr>
	                    <td width="20" style="vertical-align: top;"> 4:</td>
	                    <td valign="bottom" style="padding-left: 10;"></td>
	                </tr>
	            </table>
	        </td>
	    </tr>
	</table><br><br>

	<table width="80%" border="0" cellspacing="0" class="TabPost">
	    <tr>
	        <td>Založil: <span style="color: #30bba9"><b>Sir_Chislehurst</b></span><br>Tvoja voľba: ešte si nehlasoval/a!</td>
	        <td style="text-align:right">05 Mar 2273 - 12:52:26&nbsp;<br>Hlasovali:&nbsp;38&nbsp;</td>
	    </tr>
	    <tr>
	        <td colspan="2" class="postText" style="text-align:center; border-top: 1px solid #B2EAB3;">Vo všeobecnosti ste spokojní s nastavením cien plavidiel a indexov sektorov? (výsledok je menej plavidiel).</td>
	    </tr>
	    <tr>
	        <td colspan="2">
	            <ul style="margin: 5 5 3 40;" class="postText">
	                <li>1. Áno:&nbsp; <span class="stdText">23</span></li>
	                <li>2. Nie:&nbsp; <span class="stdText">15</span></li>
	            </ul>
	        </td>
	    </tr>
	    <tr>
	        <td colspan="2" style="border-top: 1px solid #B2EAB3;">
	            <table width="100%" cellspacing="0" class="TabPost" style="border:0; font-size:12px;">
	                <tr>
	                    <td width="20" style="vertical-align: top;"> 1:</td>
	                    <td valign="bottom" style="padding-left: 10;"><b><span style="color: #">Storman</span></b>; <b><span style="color: #8a68a5">Ulkesh</span></b>; <b><span style="color: #dd9090">Magog</span></b>; <b><span style="color: #6984f9">Apokalips</span></b>; <b><span style="color: #787811">froggy</span></b>; <b><span style="color: #6984f9">Rio</span></b>; <b><span style="color: #dd9090">Caesar</span></b>; <b><span style="color: #6984f9">jjenda</span></b>; <b><span style="color: #">TheNihilist</span></b>; <b><span style="color: #dd9090">ujoduro</span></b>; <b><span style="color: #dd9090">navinav</span></b>; <b><span style="color: #a03b37">haho</span></b>; <b><span style="color: #dd9090">Jerry</span></b>; <b><span style="color: #787811">Ragnarök</span></b>; <b><span style="color: #a03b37">klapacius</span></b>; <b><span style="color: #8a68a5">ShalTok</span></b>; <b><span style="color: #8a68a5">Richard B. Riddick</span></b>; <b><span style="color: #dd9090">Miso</span></b>; <b><span style="color: #">Defender</span></b>; <b><span style="color: #787811">Dobyvatel</span></b>; <b><span style="color: #">Cleve</span></b>; <b><span style="color: #dd9090">Dark One</span></b>; <b><span style="color: #787811">Velký teplý Al</span></b>; </td>
	                </tr>
	                <tr>
	                    <td colspan="2" style="text-align:center">* * *</td>
	                </tr>
	                <tr>
	                    <td width="20" style="vertical-align: top;"> 2:</td>
	                    <td valign="bottom" style="padding-left: 10;"><b><span style="color: #6984f9">zdroj</span></b>; <b><span style="color: #8a68a5">DusKE</span></b>; <b><span style="color: #787811">aquarius</span></b>; <b><span style="color: #6984f9">GimliCZ</span></b>; <b><span style="color: #6984f9">aragok</span></b>; <b><span style="color: #">Tomin</span></b>; <b><span style="color: #a03b37">Turgon III.</span></b>; <b><span style="color: #dd9090">ven0m</span></b>; <b><span style="color: #">jaro78</span></b>; <b><span style="color: #">ivo15</span></b>; <b><span style="color: #8a68a5">Nusku1</span></b>; <b><span style="color: #8a68a5">voloda1024</span></b>; <b><span style="color: #a03b37">tlčhuba</span></b>; <b><span style="color: #dd9090">Anne</span></b>; <b><span style="color: #6984f9">Itachi</span></b>; </td>
	                </tr>
	            </table>
	        </td>
	    </tr>
	</table><br><br>

    <div style="clear:both"></div>
  </div>