@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/rules-citizenship">Pravidlá súvisiace s občianstvom a štátmi</a></div>
@else
<div class="mainTitle">Pravidlá súvisiace s občianstvom a štátmi</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

    <div class="narrowBox" align="justify">

        <p>
            Pri zapojení do veku si vyberáš (alebo je ti určený) štát, za ktorý budeš hrať. Tým automaticky získaš občianstvo tohto štátu. O občianstvo je možné prísť alebo sa ho vzdať. Takisto sa da požiadať o občianstvo v inom štáte alebo si vyhlásiť vlastný štát. Na tejto stránke nájdeš pravidlá, ktorými sa pri týchto situáciach treba riadiť.
        </p><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">I. Strata občianstva</div>
            <div align="left" style="margin:10px;">
                <ol>
                    <li>Hráč môže o občianstvo prísť:
                        <ol type="A">
                            <li>z vlastnej vôle,</li>
                            <li>rozhodnutím vlády štátu (v prípade Kolónie Drakhov rozhodnutím všetkých občanov štátu) formou ankety. Člen vlády štátu (splnomocnenec u Drakhov) zašle o tomto rozhodnutí poštu TM. TM majú moderačné právo - môžu žiadosť zamietnuť s uvedením dôvodu zamietnutia,</li>
                            <li>zničením štátu v zmysle pravidiel veku (zničením štátu prechádzajú všetci občania k neobčanom).</li>
                        </ol>
                    </li><br><br>

                    <li>
                        Po strate občianstva podľa bodu 1. písmena A) a B) má hráč:<br> - povinnosť do 48 hodín presunúť svoju stanicu mimo HW svojho bývalého štátu. Tejto povinnosti ho môže zbaviť vláda dotyčného štátu,<br> - zákaz po dobu 48 hodín od straty občianstva útočiť na akékoľvek plavidlá a na ťažbu a to aj v prípade, že sa v tejto lehote stane občanom iného štátu.
                    </li><br><br>

                    <li>Ak si hráč v zmysle bodu 2 svoju stanicu nepresunie, môže vláda dotyčného štátu požiadať TM o presun stanice hráča mimo HW. V takom prípade nové umiestnenie stanice určí TM.</li><br><br>

                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">II. Získanie občianstva</div>
            <div align="left" style="margin:10px;">
                <ol>
                    <li>Získať nové občianstvo je možné v prípade, ak to nezakazujú pravidlá veku pri niektorých cieľoch, alebo pravidlá veku dané v špecifickom veku. Hráč sa dohodne s vládou štátu, či mu občianstvo udelí a za akých podmienok.</li><br><br>
                    <li>Žiadateľ o občianstvo:<br> - musí byť bez občianstva,<br> - musí mať potrebné množstvo TM kryštálov,<br> - nemôže mať svoju stanicu v sektore HW iného štátu s výnimkou toho, v ktorom žiada o občianstvo,<br> - musí mať plavidlo (nie stíhač) v sektore, kde sa nachádza aspoň jedno plavidlo (nie stíhač) akéhokoľvek občana štátu, do ktorého chce vstúpiť.
                    </li><br><br>

                    <li>Hráč napíše žiadosť o zmenu občianstva do pošty TM, pričom uvedie: štát, ktorého občianstvo chce získať a sektor, v ktorom sa nachádza jeho plavidlo v zmysle bodu 2.</li><br><br>

                    <li>Pokiaľ vláda súhlasí s udelením občianstva, jej najvyšší predstaviteľ pošle poštu o tejto skutočnosti TM, ktorý žiadosť hráča vybavuje.</li><br><br>

                    <li>Hráč môže získať občiastvo zvoleného štátu len vtedy, ak nebude prekročený limit hráčov na zvolený štát. Ak nebude táto podmienka splnená, žiadosť bude zamietnutá.</li><br><br>

                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">III. Založenie nového štátu</div>
            <div align="left" style="margin:10px;">
                <ol>

                    <li>Nový štát môžu založiť len hráči bez občianstva, najmenej však traja. Musia byť splnené tieto podmienky:<br> - v sektore, ktorý sa má stať domovským (HW), musia mať už vopred presunutú svoju stanicu minimálne 3 zakladajúci hráči,<br> - žiadny zakladajúci hráč nesmie mať stanicu v HW iného štátu,<br> - v sektore, ktorý sa má stať domovským (HW), musí byť aspoň 1 plavidlo (nie stíhač) niektorého zo zakladajúcich hráčov,<br> - zakladajúci hráči musia mať k dispozícii aspoň 1 TM kryštál
                    </li><br><br>

                    <li>Žiadosť o založenie nového štátu podáva za všetkých zakladajúcich hráčov len jeden z nich (nimi poverený) po vzájomnej dohode týchto hráčov (odporúča sa vytvorenie na to určeného osobného fóra), pričom uvedie:<br> - mená ostatných zakladajúcich hráčov<br> - sektor, ktorý sa stane ich oficiálnym domovským sektorom (HW)<br> - nazov novovznikajúceho štátu<br> - návrh farby nového štátu (nie je pre TM záväzná)<br> - politický systém novovznikajúceho štátu (výber podľa politických systémov klasických štátov)<br> - dôvod založenia štátu<br> - 2 štátne technológie - obe v rámci rovnakého vybraného štátu</li><br><br>

                    <li>Kruh TM má právo žiadosť zamietnúť, ak uzná dôvody založenia za neopodstatnené (aby nevznikalo veľké množstvo štátov).</li><br><br>

                    <li>Za výber HW nového štátu, ako aj za všetky následky spojené so založením nového štátu zodpovedajú hráči sami. Nový štát nemá nárok na žiadnu ochranu od TM.</li><br><br>

                    <li>O založení nového štátu informuje ostatných hráčov TM a to na Politickom fóre. Štát môže a nemusí informovať o umiestnení jeho HW.</li><br><br>

                    <li>Zakladanie nových štátov môže byť v niektorých vekoch zakázané. Pozri pravidlá aktuálneho veku.</li><br><br>
                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">IV. Zánik štátu</div>
            <div align="left" style="margin:10px;">
                <ol>

                    <li>Štát, ktorého počet občanov klesne pod 3, sa považuje za zaniknutý. Pre účely pravidiel veku sa štát považuje za zaniknutý až po uplynutí lehoty 2 dní od vyhlásenia TM o zániku štátu na Politickom fóre.</li><br><br>

                    <li>Zvyšní občania prídu o občianstvo.</li><br><br>
                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">V. Zmena HW štátu</div>
            <div align="left" style="margin:10px;">
                <ol>

                    <li>Vláda štátu môže požiadať TM (zašle poštu) o zmenu HW sektora za splnenia týchto podmienok:<br> - vo zvolenom sektore sa nachádza aspoň polovica staníc všetkých aktívnych hráčov štátu<br> - zvolený sektor je vo vlastníctve štátu a neprebieha tu vojna<br> - štát, ktorý žiada o zmenu, je vlastníkom svojho aktuálneho HW sektora a v sektore neprebieha boj
                    </li><br><br>

                    <li>O podaní žiadosti o zmenu HW musí vláda informovať na PF. Zmena HW je platná potvrdením zo strany TM. Štát môže aj nemusí informovať aj o sektore, ktorý sa stal novým HW.</li><br><br>

                </ol>
            </div>
        </div><br>

    </div>
    <div style="clear:both"></div>
</div>