@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/rules-crystals">Pravidlá TM kryštálov</a></div>
@else
<div class="mainTitle">Pravidlá TM kryštálov</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

    <div class="narrowBox" align="justify">

        <p>
            TM kryštály (TMK) sú ukazovateľom kvalít hráča ako aj jeho prínosu k hre a pre hráčov. Zároveň majú špeciálnu hodnotu, za ktorú je možné získať niekoľko výhod v danom veku, ale len obmedzene. Získavanie, mínanie a podmienky, za ktorých prichádza k strate TM kryštálov, špecifikujú tieto pravidlá:
        </p><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">I. Všeobecne o TM kryštáloch</div>
            <div align="left" style="margin:10px;">
                <ol>

                    <li>Získať a stratiť ich môže len hráč (nie štát).</li><br><br>
                    <li>Ich hodnota môže byť záporná len v prípade straty bodov (porušením pravidiel).</li><br><br>
                    <li>Minúť ich je možné v prospech seba a v prospech štátu.</li><br><br>
                    <li>Prenášajú sa do ďalších vekov.</li><br><br>
                    <li>Evidencia TMK je v menu Štatistika - TM kryštály.</li><br><br>
                    <li>Pre pridelenie (ak o ňom rozhoduje hráč) a mínanie TM kryštálov sa zasiela pošta TM (určený podľa admin oznamu).</li><br>

                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">II. Získavanie TM kryštálov</div>
            <div align="left" style="margin:10px;">
                <ol type="A">
                    <li>Automatické pridelenie systémom:<br><br>
                        <ul type="square">
                            <li>novoregistrovaný hráč - 1 TMK</li>
                            <li>zapojenie sa do veku - 1 TMK</li>
                        </ul>
                    </li><br><br>

                    <li>Pridelenie prostredníctvom TM:<br><br>
                        <ul type="square">
                            <li>
                                za víťazstvo vo veku: najvyšší predstavitelia víťazného štátu (u KD obaja splnomocnenci) rozdelia hráčom 30 TMK. TM môžu na začiatku veku stanoviť iný spôsob rozdeľovania. Maximálne možno prideliť 3 TMK jednému hráčovi, pričom hráč nemusí byť občan daného štátu, ale akýkoľvek hráč.</li>
                            <li>za najvyšší level získaný ku koncu veku - 1 TMK (v prípade viacerých hráčov s najvyšším levelom, TMK získa každý z nich)</li>
                            <li>za príspevky do Kroniky podľa záverečného vyhodnotenia - 1 TMK</li>
                            <li>za najlepšieho gestora pre nováčikov - 2 TMK</li>
                            <li>za výhru v súťaži - podľa vopred oznámeného hodnotenia TM</li>
                            <li>iná odmena podľa uváženia TM</li>
                        </ul>
                    </li><br>

                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">III. Strata TM kryštálov</div>
            <div align="left" style="margin:10px;">
                <ul type="square">
                    <li>za porušenie pravidiel - počet TMK podľa uváženia TM</li>
                </ul>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">IV. Mínanie TM kryštálov</div>
            <div align="left" style="margin:10px;">
                <ol type="A">

                    <li>V prospech seba:<br><br>
                        <ul type="square">
                            <li>zmena občianstva. resp. nové občianstvo (vrátane založenia nového štátu) - 1 TMK</li>
                            <li>preplatenie presunu stanice do iného sektora svojho štátu - 1 TMK</li>
                            <li>nová administrátorská anketa - 1 TMK (podlieha konečnému schváleniu TM)</li>
                            <li>nadobudnutie 1 plavidla v sektore Babylon 5 len 1 x počas veku (ľahký krížnik podľa výberu TM) - len pre hráča bez občianstva, po 15-tom dni trvania veku a po nadobudnutí nemôže získať občianstvo po dobu 10 dní - 1 TMK</li>
                        </ul>
                    </li><br><br>

                    <li>V prospech štátu - TM kryštály sa odčítajú hráčom podľa ich vôle, ktorú hráči prejavia na vytvorenom fóre (na tento účel si fórum vytvoria hráči sami). Jeden hráč môže minúť v prospech štátu najviac 2 TMK za vek:
                        <ul type="square">
                            <li>zvýšenie 1 indexu vo vybranom sektore o hodnotu 20, maximálne však na hodnotu 80 - 4 TMK</li>
                            <li>zvýšenie zásob vo vybranom sektore pre jednu položku o 40% z aktuálneho množstva - 3 TMK</li>
                            <li>zvýšenie kapacity vo vybranom sektore o hodnotu 350 - 2 TMK</li>
                            <li>zvýšenie všetkých indexov na úroveň 75 a zásob na úroveň 160 mil. Ti / 30 mil. Q40 / 70 mld. Cr vo vybranom sektore - 7 TMK</li>
                        </ul>
                    </li><br>

                </ol>
            </div>
        </div><br>

    </div>
    <div style="clear:both"></div>
</div>