@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/stations">Stanice</a></div>
@else
<div class="mainTitle">Stanice</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br>
    <form action="" method="post">
        <table width="50%" class="Tab center">
            <tr style="font-size: 12px; color:red;">
                <td colspan="3" class="TabH1"><b>Ťažobná stanica</b></td>
            </tr>
            <tr>
                <th width="120">Titánium</th>
                <td colspan="2">300'000 Ti</td>
            </tr>
            <tr>
                <th>Quantium 40</th>
                <td colspan="2">0 Q40</td>
            </tr>
            <tr>
                <th>Kredity</th>
                <td colspan="2">1,6 mld Cr</td>
            </tr>
            <tr>
                <th>Postavené</th>
                <td colspan="2" style="text-align:left">
                    <a href="/sector/ear">Earth</a><br>
                    <a href="/sector/iso">Inner Solar System</a><br>
                </td>
            </tr>
    </form>
    </table><br><br>
    <form action="" method="post">
        <table width="50%" class="Tab center">
            <tr style="font-size: 12px; color:red;">
                <td colspan="3" class="TabH1"><b>Obchodná stanica</b></td>
            </tr>
            <tr>
                <th width="120">Titánium</th>
                <td colspan="2">250'000 Ti</td>
            </tr>
            <tr>
                <th>Quantium 40</th>
                <td colspan="2">0 Q40</td>
            </tr>
            <tr>
                <th>Kredity</th>
                <td colspan="2">1,2 mld Cr</td>
            </tr>
    </form>
    </table><br><br>
    <form action="" method="post">
        <table width="50%" class="Tab center">
            <tr style="font-size: 12px; color:red;">
                <td colspan="3" class="TabH1"><b>Obranná stanica</b></td>
            </tr>
            <tr>
                <th width="120">Titánium</th>
                <td colspan="2">500'000 Ti</td>
            </tr>
            <tr>
                <th>Quantium 40</th>
                <td colspan="2">15'000 Q40</td>
            </tr>
            <tr>
                <th>Kredity</th>
                <td colspan="2">400 mil Cr</td>
            </tr>
    </form>
    </table><br><br>
    <form action="" method="post">
        <table width="50%" class="Tab center">
            <tr style="font-size: 12px; color:red;">
                <td colspan="3" class="TabH1"><b>Skoková brána</b></td>
            </tr>
            <tr>
                <th width="120">Titánium</th>
                <td colspan="2">100'000 Ti</td>
            </tr>
            <tr>
                <th>Quantium 40</th>
                <td colspan="2">8'000 Q40</td>
            </tr>
            <tr>
                <th>Kredity</th>
                <td colspan="2">600 mil Cr</td>
            </tr>
            <tr>
                <th>Postavené</th>
                <td colspan="2" style="text-align:left">
                    <a href="/sector/ear">Earth</a><br>
                    <a href="/sector/iso">Inner Solar System</a><br>
                </td>
            </tr>
            <tr>
                <th>Vo výstavbe</th>
                <td colspan="2" style="text-align:left"><a href="map_sector.php?sector=io">Io</a> -
                    <font color="red"><span id="s4io" title="23 Jan - 21:10"></span>
                        <script language="JavaScript">
                            CountDown('s4io',12345)
                        </script>
                    </font><br>
                </td>
            </tr>
        </table>
    </form>
    </table><br><br>
    <div style="clear:both"></div>
</div>