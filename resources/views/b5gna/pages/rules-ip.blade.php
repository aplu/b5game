@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/rules-ip">Pravidlá hry z rovnakej IP adresy</a></div>
@else
<div class="mainTitle">Pravidlá hry z rovnakej IP adresy</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

    <div class="narrowBox" align="justify">

        <p>
            Tieto pravidlá sa týkajú užívateľov, ktorí hrajú z rovnakej IP adresy. Pre účely týchto pravidieľ pod IP adresou rozumieme jedinečné označenie počítača/sieťového zariadenia v sieti internet (verejná IP adresa). Dvaja hráči majú rovnakú IP adresu, ak hrajú z rovnakého počítača alebo zdieľajú navzájom internetové pripojenie (jedna domácnosť, internát a pod).
        </p><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">&nbsp;</div>
            <div align="left" style="margin:10px;">
                <ol>
                    <li>Kazdý hráč, ktorý bude odohrávať z rovnakého PC/rovnakej IP čo i len dočasne musí túto skutočnosť nahlásiť (napr. cez internú poštu) niektorému z členov kruhu TM (administrátori) - primárne Sir_Chislehurst.</li><br><br>

                    <li>Nahlásnie bude obsahovať:<br> - dôvod, pre ktorý hráč bude odohrávať z rovnakého PC/rovnakej IP adresy<br> - označenie hráča, s ktorým bude odohrávať z rovnakého PC/rovnakej IP adresy<br> - prípadne čas, ak pôjde o dočasné odohrávanie</li><br><br>

                    <li>Hráči, ktorí budú odohrávať z rovnakého PC/rovnakej IP dlhodobo, musia hrať za rovnaký štát. V prípade internátov môže TM udeliť výnimku.</li><br><br>

                    <li>Medzi hráčmi, ktorí hrajú z rovnakého PC/ rovnakej IP dlhodobo, je zakázaná akákoľvek materiálna podpora (vrátane prevodov surovín). TM môže udeliť výnimku, ak hráči nie sú v rodinnom vzťahu a nebývajú v jednom objekte. Napr.: ak je rovnaká IP z dôvodu WLAN pripojenia.</li><br><br>

                    <li>Hráči odohrávajúci len dočasne/ad hoc môžu byť z iného štátu, avšak musia žiadať povolenie, ktoré im môže byť udelené ako výnimka (nebude sa udeľovať mnohonásobne).</li><br><br>

                    <li>Pri zistení neohláseného odohrávania z rovnakého PC/IP bude TM postupovať v zmysle pravidiel (hráč dostane podľa závažnosti najskôr priestor na vyjadrenie).</li><br><br>

                </ol>
            </div>
        </div>
    </div>
    <div style="clear:both"></div>
</div>