@php
$id = 1;
$title = 'Fórum'; // 'Galaktické fórum';
$description = '';
$private = request('private') ?? false;

$states = [
    'none' => ['color' => '#e0e0e0', 'bg' => '#202020', 'name' => 'Bez občianstva'],
    'CR'   => ['color' => '#dd9090', 'bg' => '#2A0000', 'name' => 'Centaurská Republika'],
    'DE'   => ['color' => '#787811', 'bg' => '#1e1e04', 'name' => 'Kolónia Drakhov'],
    'TM'   => ['color' => '#30bba9', 'bg' => '#0c2e2a', 'name' => 'Kruh Technomágov'],
    'MF'   => ['color' => '#8a68a5', 'bg' => '#221a29', 'name' => 'Minbarská Federácia'],
    'NR'   => ['color' => '#a03b37', 'bg' => '#280e0d', 'name' => 'Narnský Režim'],
    'EA'   => ['color' => '#6984f9', 'bg' => '#03081C', 'name' => 'Pozemská Aliancia'],
    'VE'   => ['color' => '#88aa88', 'bg' => '#112211', 'name' => 'Vorlon Empire'],
];

$players = [
     1 => ['nick' => 'Delenn',    'state' => 'MF', 'race' => 'Minbari',    'avatar' => '1.jpg'],
     2 => ['nick' => 'Lennier',   'state' => 'MF', 'race' => 'Minbari',    'avatar' => '1.jpg'],
     3 => ['nick' => 'Elric',     'state' => 'TM', 'race' => 'Technomage', 'avatar' => '1.jpg'],
     4 => ['nick' => 'Galen',     'state' => 'TM', 'race' => 'Technomage', 'avatar' => '1.jpg'],
     5 => ['nick' => 'GKar',      'state' => 'NR', 'race' => 'Narn',       'avatar' => '1.jpg'],
     6 => ['nick' => 'NaToth',    'state' => 'NR', 'race' => 'Narn',       'avatar' => '1.jpg'],
     7 => ['nick' => 'Elisabeth', 'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
     8 => ['nick' => 'Jeffrey',   'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
     9 => ['nick' => 'John',      'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    10 => ['nick' => 'Lilli',     'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    11 => ['nick' => 'Lyta',      'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    12 => ['nick' => 'Michael',   'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    13 => ['nick' => 'Susan',     'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    14 => ['nick' => 'Stephen',   'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    15 => ['nick' => 'Talia',     'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    16 => ['nick' => 'Theresa',   'state' => 'EA', 'race' => 'Human',      'avatar' => '1.jpg'],
    17 => ['nick' => 'Kosh',      'state' => 'VE', 'race' => 'Vorlon',     'avatar' => '1.jpg'],
    18 => ['nick' => 'Ulkesh',    'state' => 'VE', 'race' => 'Vorlon',     'avatar' => '1.jpg'],
    19 => ['nick' => 'Londo',     'state' => 'CR', 'race' => 'Centauri',   'avatar' => '1.jpg'],
    20 => ['nick' => 'Vir',       'state' => 'CR', 'race' => 'Centauri',   'avatar' => '1.jpg'],
];

$posts = [
	['user' => 1, 'msg' => "Ráda tě tu opět vidím."],
	['user' => 9, 'msg' => 'I já tebe :love:'],
	['user' => 5, 'msg' => ':roll:'],
];

$pages = count($posts) ? ceil(count($posts) / 40)-1 : 0;
$page = request('page');
if (!is_numeric($page))
	$page = 0;
$firstPage = max(0, $page-10);
$lastPage = min($pages, $page+10);


@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/forum/{{ $id }}">{{ $title }}</a></div>
@else
<div class="mainTitle">{{ $title }}</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')
<div class="content forum" style="text-align: center;">

	@if (!($debug['all'] ?? false))
		@include('b5gna.menu.dummy-forum')
	@endif	

@push('head')
<script language="JavaScript">

function PutText(text){
	if (document.postform.postText) {
		document.postform.postText.focus();
		document.postform.postText.value+=text;
	}
}

</script>
@endpush

{{-- <script language="JavaScript"> $(".mainTitle").text("{{ $title }}"); </script> --}}

@if (!empty($description))
<div class="smallText center" style="padding: 0px 180px;">
	{!! $description !!}
</div>
<br>
@endif

@if ($private)
<form action="/forum/edit?idx={{ $id }}" method="post">
	<input type="submit" name="nil" value="Spravovať osobné fórum">
</form>
<br>
@endif

<form action="/forum" method="post" name="postform">
	<textarea cols="100" rows="8" name="postText"></textarea>
	<br><br>
	<input type="hidden" name="idx" value="{{ $id }}">
	<input type="hidden" name="typ" value="0">
	<input type="submit" name="refresh" value="Obnoviť">&nbsp;&nbsp;&nbsp;<input type="submit" name="postSend" value="Vložiť príspevok">
	<input type="hidden" name="pageID" value="scnOsd4v">
</form>

<div style="margin: 8 0 8 0">
@php
$smilies = [
	':-D'			=> '<img src="/images/b5gna/smilies/biggrin.gif" border="0">',
	':-)'			=> '<img src="/images/b5gna/smilies/smile.gif" border="0">',
	':-('			=> '<img src="/images/b5gna/smilies/sad.gif" border="0">',
	':-o'			=> '<img src="/images/b5gna/smilies/surprised.gif" border="0">',
	':shock:'		=> '<img src="/images/b5gna/smilies/shock.gif" border="0">',
	':-?'			=> '<img src="/images/b5gna/smilies/confused.gif" border="0">',
	'8-)'			=> '<img src="/images/b5gna/smilies/cool.gif" border="0">',
	':lol:'			=> '<img src="/images/b5gna/smilies/lol.gif" border="0">',
	':-p'			=> '<img src="/images/b5gna/smilies/razz.gif" border="0">',
	':oops:'		=> '<img src="/images/b5gna/smilies/redface.gif" border="0">',
	':cry:'			=> '<img src="/images/b5gna/smilies/cry.gif" border="0">',
	':evil:'		=> '<img src="/images/b5gna/smilies/evil.gif" border="0">',
	':badgrin:'		=> '<img src="/images/b5gna/smilies/badgrin.gif" border="0">',
	':roll:'		=> '<img src="/images/b5gna/smilies/rolleyes.gif" border="0">',
	';-)'			=> '<img src="/images/b5gna/smilies/wink.gif" border="0">',
	':!:'			=> '<img src="/images/b5gna/smilies/exclaim.gif" border="0">',
	':?:'			=> '<img src="/images/b5gna/smilies/question.gif" border="0">',
	':idea:'		=> '<img src="/images/b5gna/smilies/idea.gif" border="0">',
	':-|'			=> '<img src="/images/b5gna/smilies/neutral.gif" border="0">',
	':doubt:'		=> '<img src="/images/b5gna/smilies/doubt.gif" border="0">',
	':('			=> '<img src="/images/b5gna/smilies/sad2.gif" border="0">',
	':love:'		=> '<img src="/images/b5gna/smilies/icon_love.gif" border="0">',
	':beer:'		=> '<img src="/images/b5gna/smilies/beer.png" border="0">',
	':)'			=> '<img src="/images/b5gna/smilies/smile2.gif" border="0">',
	':keel:'		=> '<img src="/images/b5gna/smilies/icon_keel.gif" border="0">',
	':eyes:'		=> '<img src="/images/b5gna/smilies/rolleyes2.gif" border="0">',
	'8)'			=> '<img src="/images/b5gna/smilies/glasses.gif" border="0">',
	':x'			=> '<img src="/images/b5gna/smilies/shutup.gif" border="0">',
	':square:'		=> '<img src="/images/b5gna/smilies/square.gif" border="0">',
	':sleep:'		=> '<img src="/images/b5gna/smilies/sleep.gif" border="0">',
	':up:'			=> '<img src="/images/b5gna/smilies/thumbsup.gif" border="0">',
	':down:'		=> '<img src="/images/b5gna/smilies/thumbsdown.gif" border="0">',
	':toilet:'		=> '<img src="/images/b5gna/smilies/icon_toilet.gif" border="0">',
	':wall:'		=> '<img src="/images/b5gna/smilies/wall.gif" border="0">',
	':peace:'		=> '<img src="/images/b5gna/smilies/peace.gif" border="0">',
];
@endphp
@foreach ($smilies as $smile => $img)
	<a href="javascript:PutText('{{ $smile }}')">{!! $img !!}</a>
@endforeach
</div>
<br>

@section('pagination')
<div>
@if ($firstPage > 0)
	...
@endif
@for ($i = $firstPage; $i <= $lastPage; $i++)
	@if ($i == $page)
	<b>{{ $i }}</b>
	@else
	<a href="?page={{ $i }}&idx={{ $id }}&typ=0">{{ $i }}</a>
	@endif
@endfor
@if ($lastPage < $pages)
	...
@endif
	<br><br>
</div>
@endsection

@yield('pagination')

@if (empty($posts))
	Fórum neobsahuje žiadny príspevok.<br><br>
@else

	@foreach ($posts as $id => $post)
	@php
	$time = $posts[$id]['time'] = (!isset($time) ? time()-rand(3600,360000) : $time+rand(60,600));
	$player = $players[$post['user']];
	$color = $states[$player['state']]['color'];
	$bgColor = $states[$player['state']]['bg'];
	$avatar = '/avatars/' . ($post['avatar'] ?? $player['avatar']);

	$msg = strtr($post['msg'], array_merge($smilies, [
		"\n" => '<br>',
		'+b+' => '<b>',
		'-b-' => '</b>',
		'+i+' => '<i>',
		'-i-' => '</i>',
		'+u+' => '<u>',
		'-u-' => '</u>',
		'+hl+' => '<span class="hl"><b>',
		'-hl-' => '</b></span>',
		'+s+' => '<small>',
		'-s-' => '</small>',
		'+big+' => '<big>',
		'-big-' => '</big>',
		//'+link+' => '<a href="',
		//'-link-' => '" target="_blank"><b>link</b></a>',
	]));
	// +link+<url>-link-
	$msg = preg_replace('/\+link\+(.+)-link-/', '<a href="$1" target="_blank">$1</a>', $msg);
	// \n -> <br>

	// Markdown link
	$msg = preg_replace('/\[([^\]]+)\]\((.+)\)/', '<a href="$2" target="_blank">$1</a>', $msg);
	/*foreach ($smilies as $smile => $img)
		$msg = strtr($msg, [])*/
	@endphp
	<table width="85%" cellspacing="0" class="TabPost center" style="border: 1px solid {{ $color }}">
	    <tr>
	        <th colspan="2" style="border-bottom: 1px solid {{ $color }}; background-color:{{ $bgColor }}">
	        	<b class="hl">Nový</b> |
	        	<a href="/players/{{ $post['user'] }}" class="name" style="color:{{ $color }}">{{ $player['nick'] }}</a>
	            <a href="/mail/{{ $post['user'] }}"><img src="/images/b5gna/mail_small.gif" width="11" height="8" alt=""></a>
	        	| {{ date('d M - H:i', $time) }}
	        </th>
	    <tr>
	        <td width="56" valign="top" style="width: 56px; padding: 2px 0px 2px 3px;"><img src="{{ $avatar }}" width="50" height="60"></td>
	        <td width="91%" valign="top" class="postText" style="padding: 2px 2px 2px 0px;">{!! $msg !!}
	@if (false)
	        	<br>
	            <div align="right" style="font-size:10px; margin-top:3px;"><a href="javascript:PutText('+b+{{ $post['user'] }}-b- +s+[{{ date('d M Y - H:i:s', $time) }}]-s-: ')"><b>... odpovedať</b></a></div>
	@endif
	        </td>
	    </tr>
	</table>
	<br>
	@endforeach

@endif

@yield('pagination')

@if ($private)
	<table width="70%" cellspacing="0" class="Tab center">
	    <tr>
	        <td colspan="3" class="TabH1">
	            <h2>Prístupové práva fóra</h2>
	        </td>
	    </tr>
	    <tr>
	        <th>Typ</th>
	        <th>Subjekt</th>
	        <th>Právo</th>
	    </tr>
	    <tr>
	        <td>všetci</td>
	        <td><strong class="hl">všetci hráči</strong></td>
	        <td>čítať a písať</td>
	    </tr>
	</table><br>
@endif

	<div style="clear:both"></div>
</div>