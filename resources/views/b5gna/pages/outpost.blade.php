@php
/*$error = [
    // 'title' => 'Efektivita rafinérií',
    // 'msg' => 'Nedostatok surovín na upgrade stanice!',

    //'title' => 'Obchodné centrum [200ks]',
    //'msg' => 'Nedostatok surovín na stavbu požadovaného počtu budov!',

    'title' => 'Továreň [10ks]',
    'msg' => 'Maximálny počet budov je 50!<br>Nedostatok miesta na stanici! Potrebné miesto: 200',
];*/
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="{{ route('outpost') }}">Stanica</a></div>
@else
<div class="mainTitle">Stanica</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')
<div class="content" >

    <div class="sideBox">
        @if (in_array('SECTORS', $rules))
            <div class="sidePanelTitle">Detail stanice</div>
            <div class="sidePanel">
                <label class="sideLabel">Umiestnenie stanice</label>
                <a href="/sector/{{ $player->hw->short }}">{{ $player->hw->name . ' [' . $player->hw->short . ']' }}</a>
                @if (in_array('OUTPOST_RELOCATE', $rules))
                    <div class="toolbar">
                        <a class="smallBTN" href="/relocate" title="Presun stanice">
                            <img src="/images/b5gna/icons16/transfer_left_right.png" alt="" width="16" height="16" border="0">
                            <div>Presun stanice</div>
                        </a>
                    </div>
                @endif
            </div>
        @endif

        <div class="sidePanelTitle">Hangáre</div>
        <div class="sidePanel smallText">
            <label class="sideLabel">Miesto v hangároch</label>
            <b class="hl" style="font-size:16px" title="voľné/celkovo">{{ $player->free_ships() . '/' . ($player->hangar*10) }}</b><br>

            <label class="sideLabel">Ťažobné a obchodné lode</label>

            <span class="colResDark">Ťažobné lode:</span>
            <b title="ťažia">{{ $player->ships_ti }}</b>
            @if ($player->ships_ti_free)
            + <b style="color: red" title="neťažia">{{ $player->ships_ti_free }}</b>
            @endif
            @if ($player->ships_ti_queue)
            + <b class="colResDark" title="vo výstavbe">{{ $player->ships_ti_queue }}</b>
            @endif
            <br>

            <span class="colResDark">Tankery:</span>
            <b title="ťažia">{{ $player->ships_q40 }}</b>
            @if ($player->ships_q40_free)
            + <b style="color: red" title="neťažia">{{ $player->ships_q40_queue }}</b>
            @endif
            @if ($player->ships_q40_queue)
            + <b class="colResDark" title="vo výstavbe">{{ $player->ships_q40_queue }}</b>
            @endif
            <br>

            <span class="colResDark">Obchodné lode:</span>
            <b title="ťažia">{{ $player->ships_cr }}</b>
            @if ($player->ships_cr_free)
            + <b style="color: red" title="neťažia">{{ $player->ships_cr_queue }}</b>
            @endif
            @if ($player->ships_cr_queue)
            + <b class="colResDark" title="vo výstavbe">{{ $player->ships_cr_queue }}</b>
            @endif
            <br>

            <span class="colResDark">Ťažba spolu:</span>
            <b>{{ $player->ships_ti + $player->ships_ti_free + $player->ships_ti_queue + $player->ships_q40 + $player->ships_q40_free + $player->ships_q40_queue + $player->ships_cr + $player->ships_cr_free + $player->ships_cr_queue }}</b><br>

            <label class="sideLabel">Stíhače</label>
            <span class="colResDark">Ochrana ťažby:</span>
            <b>999</b><br>

            <span class="colResDark">Voľné v HW:</span>
            <b>999</b><br>
        </div>

        @if (in_array('MINING', $rules))
        <div class="sidePanelTitle">Produkcia</div>
        <div class="sidePanel smallText">
            <label class="sideLabel">Štátny limit ťažby</label>
            <b class="hl" style="font-size:16px" title="voľný/celkový">{{ ($player->max_miners - $player->ships_ti - $player->ships_q40 - $player->ships_cr) . '/' . $player->max_miners }}</b><br>

            <label class="sideLabel">Limit obchodných lodí</label>
            <b class="hl" style="font-size:16px" title="voľný/celkový">{{ ($player->hangar * 10 - $player->ships_cr) . '/' . ($player->hangar * 10) }}</b><br><br>

            <div class="toolbar">
                <a class="smallBTN" href="/miners" title="Výroba ťažby">
                    <img src="/images/b5gna/icons16/cube_yellow_add.png" alt="" width="16" height="16" border="0">
                    <div>Výroba ťažby</div>
                </a>
                <a class="smallBTN" href="/mining" title="Správa ťažby">
                    <img src="/images/b5gna/icons16/gold-mine.png" alt="" width="16" height="16" border="0">
                    <div>Správa ťažby</div>
                </a>
            </div>
        </div>
        @endif
    </div>

    <div class="mainContentBox">
        @if (in_array('UPGRADES', $rules))
            @foreach ($upgrades as $upgrade)
                <div name="{{ $upgrade->code }}" id="{{ $upgrade->code }}" class="levelBox">
                    {{ $upgrade->name }}
                    <div class="levelPerc">{{ 32 + $player->{$upgrade->code} * 4 }}<span style="font-size:12px">%</span></div>
                    <div class="levelLevel">level {{ $player->{$upgrade->code} }}</div>
                </div>
            @endforeach

            {{-- <div name="lvl_ti" id="lvl_ti" class="levelBox">
                Efektivita taviacich pecí
                <div class="levelPerc">{{ 32 + $player->lvl_ti * 4 }}<span style="font-size:12px">%</span></div>
                <div class="levelLevel">level {{ $player->lvl_ti }}</div>
            </div>

            <div name="lvl_q40" id="lvl_q40" class="levelBox">
                Efektivita rafinérií
                <div class="levelPerc">{{ 16 + $player->lvl_q40 * 2 }}<span style="font-size:12px">%</span></div>
                <div class="levelLevel">level {{ $player->lvl_q40 }}</div>
            </div>

            <div name="lvl_cr" id="lvl_cr" class="levelBox">
                Efektivita obchodovania
                <div class="levelPerc">{{ 40 + $player->lvl_cr * 5 }}<span style="font-size:12px">%</span></div>
                <div class="levelLevel">level {{ $player->lvl_cr }}</div>
            </div>

            <div name="lvl_space" id="lvl_space" class="levelBox" style="width: 250px; float: right; margin-right:0px;">
                Miesto na stanici
                <div class="levelPerc" title="voľné/celkové">{{ amount($player->space) . '/' . amount($player->max_space) }}</div>
                <div class="levelLevel">level {{ $player->lvl_space }}</div>
            </div> --}}

            {{-- <div class="stInfoBox" id="info_lvl_ti" style="display:none">
                <b class="hl">Efektivita taviacich pecí</b><br>
                <span style="color: red">Upgrade je na maximálnej úrovni! Ďalšie zvyšovanie nie je možné!</span>
            </div> --}}
            @foreach ($upgrades as $upgrade)
                @php
                    // $lvl is needed for eval()
                    $lvl = $player->{$upgrade->code};
                @endphp
                <div class="stInfoBox" id="info_{{ $upgrade->code }}" style="display:none">
                    <b class="hl">{{ $upgrade->name }}</b>
                    @if ($upgrade->max && $player->{$upgrade->code} >= $upgrade->max)
                        <br><span style="color: red">{{ __('outpost.upgrade_is_maxed') }}</span>
                    @else
                        - zvýšiť o {{ ''/*$upgrade->increase*/ }} na <b class="colResDark">level {{ $player->{$upgrade->code}+1 }}</b><br>
                        <div class="stItemRes">
                            <label class="resLabel">Ti</label>
                            <span class="colTi">{{ amount(eval('return (' . $upgrade->ti . ');')) }}</span>
                            <br>
                            <label class="resLabel">Q40</label>
                            <span class="colQ40">{{ amount(eval('return (' . $upgrade->q40 . ');')) }}</span>
                            <br>
                            <label class="resLabel">Cr</label>
                            <span class="colCr">{{ amount(eval('return (' . $upgrade->cr . ');')) }}</span>
                            <br>
                            <label class="resLabel">Čas</label>
                            {{ time_unit($upgrade->time / ($player->age->build_speed ?? 1), true) }}
                        </div>
                        <form action="{{ route('upgrade', $upgrade) }}" method="post" style="float: right; margin-top: 36px;">
                            @csrf
                            <input type="submit" value="Upgradovať" style="width:100px">
                        </form>
                    @endif
                </div>
            @endforeach

            {{-- <div class="stInfoBox" id="info_lvl_ti" style="display:none">

                <b class="hl">Efektivita taviacich pecí</b> - zvýšiť o 4% na <b class="colResDark">level {{ $player->lvl_ti+1 }}</b><br>
                <div class="stItemRes">
                    <label class="resLabel">Ti</label><span class="colTi">0'111</span><br>
                    <label class="resLabel">Q40</label><span class="colQ40">111'111'111</span><br>
                    <label class="resLabel">Cr</label><span class="colCr">111'111'111'111'111</span><br>
                    <label class="resLabel">Čas</label>6 hod
                </div>
                <form action="{{ route('upgrade', 1) }}" method="post" style="float: right; margin-top: 36px;">
                    @csrf
                    <input type="submit" value="Upgradovať" style="width:100px">
                </form>
            </div> --}}

            {{-- <div class="stInfoBox" id="info_lvl_q40" style="display:none"> --}}
                {{-- <b class="hl">Efektivita rafinérií</b><br>
                <span style="color: red">Upgrade je na maximálnej úrovni! Ďalšie zvyšovanie nie je možné!</span> --}}

                {{-- <b class="hl">Efektivita rafinérií</b> - zvýšiť o 2% na <b class="colResDark">level {{ $player->lvl_q40+1 }}</b><br>
                <div class="stItemRes">
                    <label class="resLabel">Ti</label><span class="colTi">222'222'222'</span><br>
                    <label class="resLabel">Q40</label><span class="colQ40">222'222'222</span><br>
                    <label class="resLabel">Cr</label><span class="colCr">222'222'222'222'222</span><br>
                    <label class="resLabel">Čas</label>6 hod
                </div>
                <form action="{{ route('upgrade', 2) }}" method="post" style="float: right; margin-top: 36px;">
                    @csrf
                    <input type="submit" value="Upgradovať" style="width:100px">
                </form>
            </div> --}}

            {{-- <div class="stInfoBox" id="info_lvl_cr" style="display:none"> --}}
                {{-- <b class="hl">Efektivita obchodovania</b><br>
                <span style="color: red">Upgrade je na maximálnej úrovni! Ďalšie zvyšovanie nie je možné!</span> --}}

                {{-- <b class="hl">Efektivita obchodovania</b> - zvýšiť o 5% na <b class="colResDark">level {{ $player->lvl_cr+1 }}</b><br>
                <div class="stItemRes">
                    <label class="resLabel">Ti</label><span class="colTi">14'344</span><br>
                    <label class="resLabel">Q40</label><span class="colQ40">718</span><br>
                    <label class="resLabel">Cr</label><span class="colCr">14'343'683</span><br>
                    <label class="resLabel">Čas</label>6 hod
                </div>
                <form action="{{ route('upgrade', 3) }}" method="post" style="float: right; margin-top: 36px;">
                    @csrf
                    <input type="submit" value="Upgradovať" style="width:100px">
                </form>
            </div> --}}

            {{-- <div class="stInfoBox" id="info_space" style="display:none">
                <b class="hl">Miesto na stanici</b> - zvýšiť o 100 na <b class="colResDark">level {{ $player->lvl_space+1 }}</b><br>
                <div class="stItemRes">
                    <label class="resLabel">Ti</label><span class="colTi">18'773</span><br>
                    <label class="resLabel">Q40</label><span class="colQ40">0</span><br>
                    <label class="resLabel">Cr</label><span class="colCr">2'607'335</span><br>
                    <label class="resLabel">Čas</label>2 hod
                </div>
                <form action="{{ route('upgrade', 4) }}" method="post" style="float: right; margin-top: 36px;">
                    @csrf
                    <input type="submit" value="Upgradovať" style="width:100px">
                </form>
            </div> --}}

            <script language="JavaScript">
                $('#lvl_space').click(function() {
                    $('#info_lvl_ti').hide();
                    $('#info_lvl_q40').hide();
                    $('#info_lvl_cr').hide();
                    $('#info_lvl_space').show();
                });
                $('#lvl_ti').click(function() {
                    $('#info_lvl_q40').hide();
                    $('#info_lvl_cr').hide();
                    $('#info_lvl_space').hide();
                    $('#info_lvl_ti').show();
                });
                $('#lvl_q40').click(function() {
                    $('#info_lvl_ti').hide();
                    $('#info_lvl_cr').hide();
                    $('#info_lvl_space').hide();
                    $('#info_lvl_q40').show();
                });
                $('#lvl_cr').click(function() {
                    $('#info_lvl_ti').hide();
                    $('#info_lvl_q40').hide();
                    $('#info_lvl_space').hide();
                    $('#info_lvl_cr').show();
                });
            </script>

            <div style="clear:left;">&nbsp;</div>
        @endif

        @if (session('outpost.error'))
            <div class="errorBox">
            @if (session('outpost.error')['title'])
                <div class="colLight" style="margin-bottom:5px;">
                    {!! session('outpost.error')['title'] !!}
                </div>
            @endif
            @if (session('outpost.error')['msg'])
                {!! session('outpost.error')['msg'] !!}
            @endif
            </div>
        @endif

        {{-- <img src="/images/b5gna/b5outline.png" width="600" height="203" alt="" class="imgStation"> --}}

        @if (in_array('BUILDINGS', $rules))
            <h2>Budovy na stanici</h2>
            @php
                $left = true;
            @endphp

            @foreach ($buildings as $building)
                <div class="stItem" style="float:{{ $left ? 'left' : 'right' }}">
                    <img src="{{ $building->image }}" alt="" border="0">
                    <div class="stItemTitle">
                        {{ $building->name }}
                        <div style="float:right; color:white; vertical-align:bottom">
                            <label class="resLabel">počet</label>
                            <span title="Počet postavených budov">{{ $player->{$building->code} }}</span>
                            @if (false)
                            <span class="colResDark" style="font-size:10px" title="Počet budov vo výstavbe">+20</span>
                            @endif
                        </div>
                    </div>
                    <div class="stItemRes">
                    @if ($building->space)
                        <label class="resLabel">Miesto</label><b>{{ amount($building->space) }}</b><br>
                    @endif
                    @if ($building->pop)
                        <label class="resLabel">Lidí</label><span style="color:#ffaa88">{{ amount($building->pop) }}</span><br>
                    @endif
                    @if ($building->mil)
                        <label class="resLabel">Vojáků</label><span style="color:#884400">{{ amount($building->mil) }}</span><br>
                    @endif
                    @if ($building->tele)
                        <label class="resLabel">Telepatů</label><span style="color:#880088">{{ amount($building->tele) }}</span><br>
                    @endif
                    @if ($building->rti)
                        <label class="resLabel">rTi</label><span style="color:#b0b0b0">{{ amount($building->rti) }}</span><br>
                    @endif
                    @if ($building->ti)
                        <label class="resLabel">Ti</label><span style="color:#b0b0b0">{{ amount($building->ti) }}</span><br>
                    @endif
                    @if ($building->rq40)
                        <label class="resLabel">rQ40</label><span style="color:#70e070">{{ amount($building->rq40) }}</span><br>
                    @endif
                    @if ($building->q40)
                        <label class="resLabel">Q40</label><span style="color:#70e070">{{ amount($building->q40) }}</span><br>
                    @endif
                    @if ($building->cr)
                        <label class="resLabel">Cr</label><span style="color:#30c0ff">{{ amount($building->cr) }}</span>
                    @endif
                    </div>
                    <div class="stItemBuild">
                        @if ($building->demolishable || $building->max > $player->{$building->code})
                        <form action="{{ route('build', $building) }}" method="post">
                            @csrf
                            <input type="hidden" name="type" value="{{ $building->code }}">
                            <input type="button" name="m" value="-" style="padding-left:2px; padding-right:2px; width:18px" onClick="spinButtonDown('{{ $building->code }}');"><input type="text" name="amount" id="{{ $building->code }}" value="0" {{-- maxlength="3"  --}}class="spinButton"><input type="button" name="p" value="+" style="padding-left:2px; padding-right:2px; width:18px" onClick="spinButtonUp('{{ $building->code }}');">
                            <br><input type="submit" value="Postaviť" style="width:73px">
                        </form>
                        @endif
                    </div>
                </div>
                @if (!$left)
                    <div style="clear:left"></div>
                @endif
                @php
                    $left = !$left; // invert $left
                @endphp
            @endforeach

            <div style="clear:left">&nbsp;</div>
            <a class="stSwitch" onClick="slideDIV('#stDetail')">Parametre budov &raquo;</a>

            <div id="stDetail" style="display: none"><br>
                <table class="Tab2" style="width: 100%">
                    <tr>
                        <td colspan="6" class="TabH2">
                            <h2>Parametre budov</h2>
                        </td>
                    </tr>
                    <tr>
                        <th width="120">Budova</th>
                        <th>Minimálny počet</th>
                        <th>Maximálny počet</th>
                        <th>Zaberá miesto</th>
                        <th>Súčasná výstavba</th>
                        <th>Čas výstavby</th>
                    </tr>
                @foreach ($buildings as $building)
                    <tr>
                        <td>{{ $building->name }}</td>
                        <td>{{ $building->min }}</td>
                        <td>{{ $building->max }}</td>
                        <td>{{ $building->space }}</td>
                        <td>{{ $building->at_once }}</td>
                        <td>{{ time_unit($building->time / ($player->age->build_speed ?? 1), true) }}</td>
                    </tr>
                @endforeach

                </table><br>
            </div>

            <br><br>
        @endif

        @if (in_array('BUILDINGS', $rules) || in_array('UPGRADES', $rules))
            <h2>Vo výstavbe</h2><br>
            <table class="Tab2" style="width: 100%">
                <tr>
                    <th>Typ</th>
                    <th>Názov</th>
                    <th>Počet</th>
                    <th>Stav</th>
                    <th>Zostávajúci čas</th>
                    <th></th>
                </tr>
                @php
                $jobCounter = 0;
                @endphp
                @foreach ($jobs as $id => $job)
                    @php
                    $jobCounter++;
                    @endphp
                <tr>
                    <td style="color:gray">
                        @if ($job['type'] == 'building')
                            Budova
                        @elseif ($job['type'] == 'upgrade')
                            Upgrade
                        @else
                            Neznámý
                        @endif
                    </td>
                    <td>{{ $job['name'] }}</td>
                    <td><b class="hl">{{ $job['amount'] }}</b></td>
                    <td>
                        <span id="jobState{{ $id }}"><span class="colResDark">čaká na výstavbu</span></span>
                        <script language="JavaScript">
                            setTimeout("$('#jobState{{ $id }}').html('<span style=color:yellow>vo výstavbe</span>')", {{ $job['start']*1000 }});
                            setTimeout("$('#jobState{{ $id }}').html('<span style=color:lime>hotovo</span>')", {{ $job['finish']*1000 }});
                        </script>
                    </td>
                    <td><span id="jobTimer{{ $id }}" title="17 Nov - 02:25"></span>
                        <script language="JavaScript">
                            CountDown('jobTimer{{ $id }}', {{ $job['finish'] }})
                        </script>
                    </td>
                    <td style="padding:0px;">
                        @if ($jobCounter > 1)
                        <a id="stopJob{{ $id }}" href="{{ route('stop-job', $id) }}" title="Zrušiť výstavbu"><img src="/images/b5gna/buttons/delete2.png" alt=""></a>
                        <script language="JavaScript">
                            setTimeout("$('#stopJob{{ $id }}').hide()", {{ $job['finish']*1000 }})
                        </script>
                        @endif
                    </td>
                </tr>
                @endforeach

                {{-- <tr>
                    <td style="color:gray">budova</td>
                    <td>Hangár</td>
                    <td><b class="hl">2</b></td>
                    <td>
                        <span id="TextTimer5dd09c585c241"><span class="colResDark">čaká na výstavbu</span></span>
                        <script language="JavaScript">
                            setTimeout("$('#TextTimer5dd09c585c241').html('<span style=color:yellow>vo výstavbe</span>')",0);
                            setTimeout("$('#TextTimer5dd09c585c241').html('<span style=color:lime>hotovo</span>')",1328000);
                        </script>
                    </td>
                    <td><span id="Count23447" title="17 Nov - 02:25"></span>
                        <script language="JavaScript">
                            CountDown('Count23447',1328)
                        </script>
                    </td>
                    <td style="padding:0px;">
                        <a id="delBTN23447" href="?action=stop&idx=23447" title="Zrušiť výstavbu"><img src="/images/b5gna/buttons/delete2.png" alt=""></a>
                        <script language="JavaScript">
                            setTimeout("$('#delBTN23447').hide(100)",-5872000)
                        </script>
                    </td>
                </tr>
                <tr>
                    <td style="color:gray">budova</td>
                    <td>Taviaca pec</td>
                    <td><b class="hl">20</b></td>
                    <td>
                        <span id="TextTimer5dd09c585c3e1"><span class="colResDark">čaká na výstavbu</span></span>
                        <script language="JavaScript">
                            setTimeout("$('#TextTimer5dd09c585c3e1').html('<span style=color:yellow>vo výstavbe</span>')",22928000);
                            setTimeout("$('#TextTimer5dd09c585c3e1').html('<span style=color:lime>hotovo</span>')",30128000);
                        </script>
                    </td>
                    <td><span id="Count23451" title="17 Nov - 10:25"></span>
                        <script language="JavaScript">
                            CountDown('Count23451',30128)
                        </script>
                    </td>
                    <td style="padding:0px;">
                        <a id="delBTN23451" href="?action=stop&idx=23451" title="Zrušiť výstavbu"><img src="/images/b5gna/buttons/delete2.png" alt=""></a>
                        <script language="JavaScript">
                            setTimeout("$('#delBTN23451').hide(100)",22928000)
                        </script>
                    </td>
                </tr>
                <tr>
                    <td style="color:gray">upgrade</td>
                    <td>Efektivita rafinérií</td>
                    <td><b class="hl">1</b></td>
                    <td>
                        <span id="TextTimer5dd09c585c7f4"><span class="colResDark">čaká na výstavbu</span></span>
                        <script language="JavaScript">
                            setTimeout("$('#TextTimer5dd09c585c7f4').html('<span style=color:yellow>vo výstavbe</span>')",94928000);
                            setTimeout("$('#TextTimer5dd09c585c7f4').html('<span style=color:lime>hotovo</span>')",116528000);
                        </script>
                    </td>
                    <td><span id="Count23463" title="18 Nov - 10:25"></span>
                        <script language="JavaScript">
                            CountDown('Count23463',116528)
                        </script>
                    </td>
                    <td style="padding:0px;">
                        <a id="delBTN23463" href="?action=stop&idx=23463" title="Zrušiť výstavbu"><img src="/images/b5gna/buttons/delete2.png" alt=""></a>
                        <script language="JavaScript">
                            setTimeout("$('#delBTN23463').hide(100)",94928000)
                        </script>
                    </td>
                </tr> --}}
            </table>
        @endif
    </div>
    <div style="clear:both"></div>
</div>
