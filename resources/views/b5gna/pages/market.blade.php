@php
$time = request('time') ?? date('Hi');
$closed = $time < 800;
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/market">Burza</a></div>
@else
<div class="mainTitle">Burza</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-market')
    @endif

    <font color="#60e090">
        <span style="font-size: 22px; font-family: Arial, Helvetica, sans-serif;">
            <b>Galaktická burza</b>
        </span>
    </font><br><br><br>

    @if ($closed)

    <br><b style="color: red">Burza je od 0:00 do 8:00 zatvorená.</b><br><br><br>

    @else

        @if (isset($error))
        <font color="red">Cena je nižšia ako minimálna povolená cena! Pozri "? Pomoc" vpravo hore.<br></font><br><br>
        @elseif (isset($error2))
        <font color="red">Cena musí byť väčšia ako 0.<br>Minimálne množstvo je 100 ton.<br>Cena je nižšia ako minimálna povolená cena! Pozri "? Pomoc" vpravo hore.<br></font><br><br>
        @elseif (isset($error3))
        <font color="red">Minimálne množstvo je 100 ton.<br>Bola prekročená maximálna povolená cena! Pozri "? Pomoc" vpravo hore.<br></font><br><br>
        @elseif (isset($transfer))
        <font color="lime">Ponuka bola úspešne vložená na burzu.<br>Zaplatil si CLO zo suroviny Titánium vo výške 0 ton.</font><br><br>
        @elseif (isset($success))
        <font color="lime">Dopyt bol úspešne vložený na burzu.</font><br><br>
        @endif

        <table width="60%" cellspacing="0" class="Tab center">
            <tbody>
                <tr>
                    <td colspan="4" class="TabH1">
                        <h2>Ponuky na burze</h2>
                    </td>
                </tr>
                <tr>
                    <th>Komodita</th>
                    <th>Predajca</th>
                    <th>Množstvo</th>
                    <th>Cena za 1 tonu</th>
                </tr>
                <tr>
                    <td>
                        <font color="#8888FF">Quantium 40</font>
                    </td>
                    <td>neznámy</td>
                    <td>200'000</td>
                    <td style="color: #00FF00">15'000</td>
                </tr>
                <tr>
                    <td>
                        <font color="#8888FF">Ruda Titánium</font>
                    </td>
                    <td>neznámy</td>
                    <td>500'000</td>
                    <td style="color: #00FF00">135</td>
                </tr>
                <tr>
                    <td>
                        <font color="#8888FF">Ruda Titánium</font>
                    </td>
                    <td>neznámy</td>
                    <td>702'781</td>
                    <td style="color: #00FF00">144</td>
                </tr>
                <tr>
                    <td>
                        <font color="#8888FF">Ruda Titánium</font>
                    </td>
                    <td>neznámy</td>
                    <td>589'651</td>
                    <td style="color: #00FF00">150</td>
                </tr>
                <tr>
                    <td>
                        <font color="#8888FF">Titánium</font>
                    </td>
                    <td>neznámy</td>
                    <td>200'000</td>
                    <td style="color: #00FF00">570</td>
                </tr>
                <tr>
                    <td>
                        <font color="#8888FF">Titánium</font>
                    </td>
                    <td>neznámy</td>
                    <td>250'000</td>
                    <td style="color: #00FF00">575</td>
                </tr>
                <tr>
                    <td>
                        <font color="#8888FF">Titánium</font>
                    </td>
                    <td>neznámy</td>
                    <td>400'000</td>
                    <td style="color: #00FF00">585</td>
                </tr>
                <tr>
                    <td>
                        <font color="#8888FF">Titánium</font>
                    </td>
                    <td>neznámy</td>
                    <td>123'986</td>
                    <td style="color: #00FF00">600</td>
                </tr>
            </tbody>
        </table><br><br>
        <table width="60%" cellspacing="0" class="Tab center">
            <tbody>
                <tr>
                    <td colspan="4" class="TabH1">
                        <h2>Dopyty na burze</h2>
                    </td>
                </tr>
                <tr>
                    <th>Komodita</th>
                    <th>Kupujúci</th>
                    <th>Množstvo</th>
                    <th>Cena za 1 tonu</th>
                </tr>
                <tr>
                    <td>
                        <font color="#8888FF">Quantium 40</font>
                    </td>
                    <td>neznámy</td>
                    <td>25'000</td>
                    <td style="color: #00FF00">5'000</td>
                </tr>
                <tr>
                    <td>
                        <font color="#8888FF">Quantium 40</font>
                    </td>
                    <td>neznámy</td>
                    <td>25'000</td>
                    <td style="color: #00FF00">5'000</td>
                </tr>
            </tbody>
        </table><br><br>
        <form action="/market" method="post"><input type="hidden" name="pageID" value="f3p8wOlS">
            <table width="60%" cellspacing="0" class="Tab center">
                <tbody>
                    <tr class="TabH2">
                        <td></td>
                        <td>
                            <h2>Nová ponuka</h2>
                        </td>
                        <td>
                            <h2>Nový dopyt</h2>
                        </td>
                    </tr>
                    <tr>
                        <th>Komodita</th>
                        <td><select name="SCommodity"><option value="ti">Titánium</option>
<option value="q40">Quantium 40</option>
<option value="rti">Ruda Titánium</option>
<option value="rq40">Ruda Quantium 40</option>
</select></td>
                        <td><select name="DCommodity"><option value="ti">Titánium</option>
<option value="q40">Quantium 40</option>
<option value="rti">Ruda Titánium</option>
<option value="rq40">Ruda Quantium 40</option>
</select></td>
                    </tr>
                    <tr>
                        <th>Množstvo</th>
                        <td><input type="text" name="SQuantity" maxlength="12"></td>
                        <td><input type="text" name="DQuantity" maxlength="12"></td>
                    </tr>
                    <tr>
                        <th>Jednotková cena<br>za 1 tonu</th>
                        <td><input type="text" name="SBid" maxlength="12"></td>
                        <td><input type="text" name="DBid" maxlength="12"></td>
                    </tr>
                    <tr class="TabH2">
                        <td></td>
                        <td><input type="submit" name="NewSBid" value="Odoslať ponuku"></td>
                        <td><input type="submit" name="NewDBid" value="Odoslať dopyt"></td>
                    </tr>
                    <tr>
                        <td colspan="3"><span style="color:red;">POZOR: Pri umiestnení tovaru na túto burzu bude z ponúkaného množstva strhnuté CLO!<br>Tak isto aj pri nákupe na tejto burze bude strhnuté CLO!</span></td>
                    </tr>
                </tbody>
            </table>
        </form><br><br>
        <table width="80%" cellspacing="0" class="Tab center">
            <tbody>
                <tr>
                    <td colspan="6" class="TabH1">
                        <h2>Moje ponuky a dopyty na burze</h2>
                    </td>
                </tr>
                <tr>
                    <th>Typ</th>
                    <th>Komodita</th>
                    <th>Množstvo</th>
                    <th>Cena za 1 tonu</th>
                    <th>Čas vloženia na burzu</th>
                    <th>Príkaz</th>
                </tr>
                <tr>
                    <td class="TabH2">ponuka</td>
                    <td>
                        <font color="#8888FF">Titánium</font>
                    </td>
                    <td>1'000</td>
                    <td style="color: #00FF00">200</td>
                    <td>
                        <font color="red">čaká na vloženie</font>
                    </td>
                    <td><a href="?delete=577">Zrušiť ponuku</a></td>
                </tr>
                <tr>
                    <td class="TabH2">dopyt</td>
                    <td>
                        <font color="#8888FF">Ruda Titánium</font>
                    </td>
                    <td>1'000</td>
                    <td style="color: #00FF00">200</td>
                    <td>
                        <font color="red">čaká na vloženie</font>
                    </td>
                    <td><a href="?delete=578">Zrušiť dopyt</a></td>
                </tr>
                <tr>
                    <td class="TabH2">ponuka</td>
                    <td>
                        <font color="#8888FF">Titánium</font>
                    </td>
                    <td>1'000</td>
                    <td style="color: #00FF00">200</td>
                    <td>25 Oct 2273 - 19:08:26</td>
                    <td><a href="?delete=577">Zrušiť ponuku</a></td>
                </tr>
            </tbody>
        </table><br>

    @endif
        <div style="clear:both"></div>
    </font>
</div>