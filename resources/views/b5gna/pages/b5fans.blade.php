@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/b5fans">Združenie fanúšikov Babylonu 5</a></div>
@else
<div class="mainTitle">Združenie fanúšikov Babylonu 5</div>
@endif
<div class="mainTitleFade"></div>

<div class="content">

	<div class="narrowBox">

		<p>Nachádzaš sa v časti venovanej <strong>"Združeniu fanúšikov Babylonu 5"</strong>, ktoré vzniklo 11. marca 2011 na počesť seriálnu Babylon 5. Sme fanúšikovia seriálu Babylonu 5 a všetkého, čo s ním súvisí, napr.:
		</p>
		<ul style="text-align:left; line-height: 18px;" >
		<li>seriál a filmy</li>
		<li>CON-y a prednášky</li>
		<li>karty B5CCG - turnaje a voľné partičky</li>
		<li>online prehliadačová hra <a href="http://b5game.icsp.sk">b5game.icsp.sk</a></li>
		<li>a ďalšie</li>
		</ul>

		<br>
		<h2>Dôležité informácie</h2><br>

		<a href="/docs/ozb5game_stanovy.rtf">Stiahnuť stanovy združenia</a><br><br>


		<p>
			<strong>Členstvo</strong> - ak sa chceš stať členom združenia v zmysle stanov združenia, pošli <a href="/docs/ozb5game_ziadost.rtf">žiadosť</a> na e-mail babylon5.fans@gmail.com. 
			Budeme veľmi radi, ak sa staneš členom nášho združenia.<br><br>

			Viac informácií o aktivitách fanúšikov sa dozvieš na týchto stránkach:

			<ul style="text-align:left; line-height: 18px;" >
			<li><a href="https://sites.google.com/site/babylon5naslovensku/" target="_blank">Babylon 5 na Slovensku</a></li>
			<li><a href="http://www.facebook.com/#!/home.php?sk=group_108001759281252" target="_blank">Skupina na facebooku</a></li>
			</ul>
		</p>
		<br>

		<h2>ČLENOVIA</h2>
		<p>Zoznam členov vedie zakladajúci tím. O členoch sa viac dočítaš na stránke <a href="https://sites.google.com/site/babylon5naslovensku/zdruzenie/zakladne-informacie/zoznam-clenov-zdruzenia" target="_blank">Babylon 5 na Slovensku</a></p>

	</div>
</div>
