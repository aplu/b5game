@php

$states = [
    'none' => ['color' => '#e0e0e0', 'bg' => '#202020', 'name' => 'Bez občianstva'],
    'CR'   => ['color' => '#dd9090', 'bg' => '#2A0000', 'name' => 'Centaurská Republika'],
    'DE'   => ['color' => '#787811', 'bg' => '#1e1e04', 'name' => 'Kolónia Drakhov'],
    'TM'   => ['color' => '#30bba9', 'bg' => '#0c2e2a', 'name' => 'Kruh Technomágov'],
    'MF'   => ['color' => '#8a68a5', 'bg' => '#221a29', 'name' => 'Minbarská Federácia'],
    'NR'   => ['color' => '#a03b37', 'bg' => '#280e0d', 'name' => 'Narnský Režim'],
    'EA'   => ['color' => '#6984f9', 'bg' => '#03081C', 'name' => 'Pozemská Aliancia'],
    'VE'   => ['color' => '#88aa88', 'bg' => '#112211', 'name' => 'Vorlon Empire'],
];

$forumGroupColumns = [
    // 1st column
    [
        // group => [ id => [ forum ], ... ]
        'basic' => [
            '__title' => 'Základné fóra',
            1 =>  ['count' => 9999, 'unread' => 1234, 'name' => 'Galaktické fórum'],
            2 =>  ['count' => 8888, 'unread' => 2345, 'name' => 'Politické fórum'],
            3 =>  ['count' => 7777, 'unread' => 345, 'name' => 'Kronika'],
            7 =>  ['count' => 6666, 'unread' => 456, 'name' => 'Pre nováčikov'],
            9 =>  ['count' => 5555, 'unread' => 567, 'name' => 'Admin oznamy'],
            20 => ['count' => 4444, 'unread' => 678, 'name' => 'Výklad pravidiel'],
            25 => ['count' => 3333, 'unread' => 789, 'name' => 'Komunita hráčov'],
            30 => ['count' => 2222, 'unread' => 890, 'name' => 'Víťazi v jednotlivých vekoch'],
            90 => [/*'count' => 1111, 'unread' => 901,*/ 'new' => true, 'name' => 'Bug fórum'],
            'nation'  => ['count' => 1010, /*'unread' => 321,*/ 'type' => 1, 'name' => 'Národné fórum', 'user' => 'Pozemská Aliancia', 'state' => 'EA'],
        ],

        'offtopic' => [
            '__title' => 'Off-topic fóra',
            '__favorable' => true,
            '__hidable' => true,
            390 =>  ['count' => 4380, 'unread' => 408, 'name' => 'Filmy-Seriály', 'user' => '20Markus', 'state' => 'none'],
            149 =>  ['count' => 4380, 'unread' => 408, 'name' => 'Games', 'user' => 'swiss', 'state' => 'none'],
            2664 => [/*'count' => 4380, 'unread' => 408,*/ 'new' => true, 'name' => 'KONSPIRACIE vs PRAVDA', 'user' => 'zdroj', 'state' => 'EA'],
            1209 => [/*'count' => 4380, 'unread' => 408,*/ 'new' => true, 'name' => 'Muzicka', 'user' => 'Shaade', 'state' => 'TM'],
            204 =>  ['count' => 4380, 'unread' => 408, 'name' => 'Vtipy - haluze', 'user' => 'Hades', 'state' => 'none'],
            1161 => [/*'count' => 4380, 'unread' => 408,*/ 'new' => true, 'name' => 'WoT', 'user' => 'Jeliman', 'state' => 'none'],
            1239 => ['count' => 1234, 'unread' => 0, 'name' => 'WoT nicks', 'user' => 'Caesar', 'state' => 'CR'],
        ],

        'hidden' => [
            '__title' => 'Skryté fóra',
            '__favorable' => true,
            '__hidable' => true,
            '__hidden' => true,
            1470 => ['new' => true, 'name' => 'Slang B5', 'user' => 'mul', 'state' => 'none'],
            1471 => ['new' => true, 'name' => 'Slang B5 - navrhy a prispevky', 'user' => 'mul', 'state' => 'none'],
        ],
    ],

    // 2nd column
    [
        'hilighted' => [
            '__title' => 'Zvýraznené fóra',
            2276 => ['count' => 123, 'unread' => 1, 'name' => 'B5_Chaty_Stretka', 'user' => 'Sir_Chislehurst', 'state' => 'TM'],
            1751 => ['count' => 1, 'name' => 'Eventy/udalosti vo veku', 'user' => 'Sir_Chislehurst', 'state' => 'TM'],
            296  => ['count' => 234, 'unread' => 21, 'name' => 'Porušenia pravidiel a pokuty', 'user' => 'Sir_Chislehurst', 'state' => 'TM'],
            2819 => ['new' => true, 'name' => 'Zoznam uloh pre C7', 'user' => 'Sir_Chislehurst', 'state' => 'TM'],
        ],

        'favourite' => [
            '__title' => 'Obľúbené fóra',
            '__favorable' => true,
            //'__hidable' => true,
        ],

        'personal' => [
            '__title' => 'Osobné fóra',
            '__favorable' => true,
            '__hidable' => true,
            2914 => ['new' => true, 'name' => 'Smlouvy a dohody', 'user' => 'Apokalips', 'state' => 'EA'],
            2927 => ['new' => true, 'name' => 'Vyvoj nove B5G', 'user' => 'Zathras', 'state' => 'EA'],
        ],
    ],
];

@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/forums">Výber fóra</a></div>
@else
<div class="mainTitle">Výber fóra</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

	@if (!($debug['all'] ?? false))
		@include('b5gna.menu.dummy-forum')
	@endif
	
	@foreach ($forumGroupColumns as $col => $forumGroups)
	<div class="forumCol">
		@foreach ($forumGroups as $forumGroup)
		<div class="group">
			@if (isset($forumGroup['__title']))
			<div class="groupTitle">{{ $forumGroup['__title'] }}</div>
			@endif

			@if (isset($forumGroup['__hidden']))
			<a class="btn" id="fHiddenBtn" onClick="slideDIV('#fHidden'); ToggleDIV('fHiddenBtn');">Zobraziť skryté fóra &raquo;</a>
			<div id="fHidden" style="display:none">
			@endif

			@foreach ($forumGroup as $id => $forum)
			<?php
				// skip forum indexes starting with __ (used as forum group attributes)
				if (is_string($id) && substr($id, 0, 2) == '__')
					continue;

				$isNew = isset($forum['new']);
				$unread = isset($forum['unread']) && is_numeric($forum['unread']) ? $forum['unread'] : 0;

				$style = ($unread || $isNew ? 'background-color:#eecc00;' : '')
					. ($unread ? ' color:black;' : '');
			?>
			<div class="forumItem<?= ($unread || $isNew) ? ' forumItemSel' : '' ?>">
				<div class="forumCount"<?= (!empty($style) ? ' style="' . $style . '"' : '') . ($isNew ? '  title="nové fórum"' : '') ?>>
					<?php if ($unread) { ?><b title="počet neprečítaných príspevkov">{{ $unread }}</b><?php } ?><br>
					@if ($isNew)
					<img src="/images/b5gna/buttons/flag-new-red.png" alt="" style="position:relative; top:-6px; left:6px;">
					@else
					<span title="počet príspevkov"{{ $unread ? ' style="color:#ccaa00"' : '' }}>{{ $forum['count'] }}</span>
					@endif
				</div>
				@if (isset($forumGroup['__favorable']) && $forumGroup['__favorable'])
				<img src="/images/b5gna/buttons/star_grey.png" alt="" id="fav{{ $id }}" width="24" height="24" border="0" class="forumFav" title="obľúbené" onClick="favClick({{ $id }});">
				@endif
				@if (isset($forumGroup['__hidable']) && $forumGroup['__hidable'])
				<img src="/images/b5gna/buttons/masks-off.png" alt="" id="hide{{ $id }}" width="24" height="24" border="0" class="forumFav" title="skryté" onClick="hideClick({{ $id }});">
				@endif
				<a href="/forum/{{ $id }}">
					<div class="forumName">{{ $forum['name'] }}</div>
					@if (isset($forum['user']))
					<div class="forumNick"<?php if (isset($forum['state'])) { ?> style="background-color: {{ $states[$forum['state']]['color'] }}"<?php } ?>>
						{{ $forum['user'] }}
					</div>
					@endif
				</a>
			</div>
			@endforeach

			@if (isset($forumGroup['__hidden']))
			</div>
			@endif

		</div>
		@endforeach
	</div>
	@endforeach

	<div style="clear:both"></div>
</div>