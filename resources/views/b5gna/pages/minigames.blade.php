@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/">Minihry</a></div>
@else
<div class="mainTitle">Minihry</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

	<div class="narrowBox">

		<div style="text-align:left">
			<a href="/invaders/">
			<img src="/images/b5goa/minigames/invaders--134x200.jpg" align="left" style="width:134px; height:200px; margin-right:2em">
			<big><strong>Babylon 5: Invaders</strong></big><br>
			vesmírná střílečka<br>
			<br>
			<span class="btn bigBTN">Hrát</span>
			</a><br>
			<br>
			<p><i>Malá skupina Starfury se krátce po skoku ztrácí v hyperprostoru... váš osud se zdá být zpečetěn. Vysíláte SOS, zkoušíte udržovat maximální bezpečný rozestup a snažíte se uklidnit, abyste šetřili kyslík. Jakobyste se však ocitli v pekle, když se před vámi zjevuje armáda z nejhorší noční můry. Jste jejich první překážka před invazí na Zemi. A nemůžete se ukrýt ani nikoho varovat, jelikož nepřítel o vás ví a připravuje se k útoku. Kolik jich zvládnete vzít sebou?</i></p>
			Více info na <a href="https://trello.com/c/lRsabiu8/1-invaders-shootemup">
				<img src="/images/icons/trello.svg" alt="Trello" width="16" height="16" border="0" style="vertical-align:middle">
				Trello nástěnce Minihry</a>
			a <a href="https://discord.gg/GuNuc8y" target="_blank">
				<img src="/images/icons/discord.svg" alt="Discord" width="16" height="16" border="0" style="width:20px; height:20px; padding:2px; vertical-align:middle">
				Discordu B5Game&gt; kanál #invaders
			</a><br>
		</div>
		<div style="clear:both"></div>

	</div>{{-- /.narrowBox --}}

	<div style="clear:both"></div>
</div>