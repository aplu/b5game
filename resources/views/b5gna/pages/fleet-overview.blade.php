@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/fleet-overview">Prehľad lodí a letiek</a></div>
@else
<div class="mainTitle">Prehľad lodí a letiek</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

	<br>
	<table width="85%" cellspacing="0" class="Tab center">
		<tr>
			<th width="40" rowspan="2">Sektor</th>
			<th rowspan="2">Vlastník</th>
			<th rowspan="2">Skupina<br>bojová/ťažobná</th>
			<th rowspan="2">Stav</th>
			<th colspan="4">Súčet</th>
			<th rowspan="2">Obsah</th>
		</tr>
		<tr>
			<th>S</th>
			<th>K</th>
			<th>L</th>
			<th>D</th>
		</tr>
@if (rand(0,1))
		<tr>
			<td colspan="9"><br>žiadne lode<br><br></td>
		</tr>
		<tr class="TabH1">
			<td colspan="3"></td>
			<th>Súčet</th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<td></td>
		</tr>
@else
		<tr>
			<td rowspan="9" style="background-color: #; vertical-align:middle;"><a href="../game/map_sector.php?sector=HYP" style="color:white"><b>HYP</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>W II</td>
			<td><span style="color: #ff99ff">v hyperpriestore</span><br>cieľ: <a href="../game/map_sector.php?sector=s8">s8</a> -> <a href="../game/map_sector.php?sector=cet">cet</a><br><span id="CTransfer196" title="01 Feb - 12:02"></span>
				<script language="JavaScript">
	CountDown('CTransfer196', 4943)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td style="text-align:left">[D] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Nova Dreadnought</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XXVII</td>
			<td><span style="color: #ff99ff">v hyperpriestore</span><br>cieľ: <a href="../game/map_sector.php?sector=gai">gai</a> -> <a href="../game/map_sector.php?sector=ocs">ocs</a><br><span id="CTransfer239" title="01 Feb - 10:40"></span>
				<script language="JavaScript">
	CountDown('CTransfer239', 3)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XXVI</td>
			<td><span style="color: #ff99ff">v hyperpriestore</span><br>cieľ: <a href="../game/map_sector.php?sector=ocs">ocs</a> -> <a href="../game/map_sector.php?sector=ocs">ocs</a><br><span id="CTransfer188" title="01 Feb - 10:46"></span>
				<script language="JavaScript">
	CountDown('CTransfer188', 415)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XXI</td>
			<td><span style="color: #ff99ff">v hyperpriestore</span><br>cieľ: <a href="../game/map_sector.php?sector=ics">ics</a> -> <a href="../game/map_sector.php?sector=odz">odz</a><br><span id="CTransfer172" title="01 Feb - 16:48"></span>
				<script language="JavaScript">
	CountDown('CTransfer172', 22126)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XX</td>
			<td><span style="color: #ff99ff">v hyperpriestore</span><br>cieľ: <a href="../game/map_sector.php?sector=al2">al2</a> -> <a href="../game/map_sector.php?sector=s84">s84</a><br><span id="CTransfer140" title="01 Feb - 16:31"></span>
				<script language="JavaScript">
	CountDown('CTransfer140', 21079)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XIII</td>
			<td><span style="color: #ff99ff">v hyperpriestore</span><br>cieľ: <a href="../game/map_sector.php?sector=s14">s14</a> -> <a href="../game/map_sector.php?sector=ocs">ocs</a><br><span id="CTransfer131" title="01 Feb - 10:46"></span>
				<script language="JavaScript">
	CountDown('CTransfer131', 359)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XII</td>
			<td><span style="color: #ff99ff">v hyperpriestore</span><br>cieľ: <a href="../game/map_sector.php?sector=rag">rag</a> -> <a href="../game/map_sector.php?sector=al2">al2</a><br><span id="CTransfer130" title="01 Feb - 13:05"></span>
				<script language="JavaScript">
	CountDown('CTransfer130', 8738)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout VI</td>
			<td><span style="color: #ff99ff">v hyperpriestore</span><br>cieľ: <a href="../game/map_sector.php?sector=q13">q13</a> -> <a href="../game/map_sector.php?sector=q13">q13</a><br><span id="CTransfer113" title="01 Feb - 16:42"></span>
				<script language="JavaScript">
	CountDown('CTransfer113', 21773)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout II</td>
			<td><span style="color: #ff99ff">v hyperpriestore</span><br>cieľ: <a href="../game/map_sector.php?sector=s8">s8</a> -> <a href="../game/map_sector.php?sector=ocs">ocs</a><br><span id="CTransfer109" title="01 Feb - 10:45"></span>
				<script language="JavaScript">
	CountDown('CTransfer109', 322)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #; vertical-align:middle;"><a href="../game/map_sector.php?sector=syr" style="color:white"><b>syr</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XVII</td>
			<td><span style="color: #669999">v pokoji</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" style="background-color: #dd9090; vertical-align:middle;"><a href="../game/map_sector.php?sector=rag" style="color:white"><b>rag</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XXV</td>
			<td><span style="color: #9999ff">príprava skoku</span><br>cieľ: <a href="../game/map_sector.php?sector=al2">al2</a> -> <a href="../game/map_sector.php?sector=idz">idz</a><br><span id="CTransfer187" title="01 Feb - 12:48"></span>
				<script language="JavaScript">
	CountDown('CTransfer187', 7691)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XV</td>
			<td><span style="color: #9999ff">príprava skoku</span><br>cieľ: <a href="../game/map_sector.php?sector=al2">al2</a> -> <a href="../game/map_sector.php?sector=tok">tok</a><br><span id="CTransfer133" title="01 Feb - 12:51"></span>
				<script language="JavaScript">
	CountDown('CTransfer133', 7884)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" style="background-color: #787811; vertical-align:middle;"><a href="../game/map_sector.php?sector=pak" style="color:white"><b>pak</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XXII</td>
			<td><span style="color: #9999ff">príprava skoku</span><br>cieľ: <a href="../game/map_sector.php?sector=ocs">ocs</a> -> <a href="../game/map_sector.php?sector=ocs">ocs</a><br><span id="CTransfer173" title="01 Feb - 12:33"></span>
				<script language="JavaScript">
	CountDown('CTransfer173', 6822)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XIX</td>
			<td><span style="color: #9999ff">príprava skoku</span><br>cieľ: <a href="../game/map_sector.php?sector=ocs">ocs</a> -> <a href="../game/map_sector.php?sector=ocs">ocs</a><br><span id="CTransfer139" title="01 Feb - 12:35"></span>
				<script language="JavaScript">
	CountDown('CTransfer139', 6940)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #6984f9; vertical-align:middle;"><a href="../game/map_sector.php?sector=ear" style="color:white"><b>ear</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>
				<font color="lime">voľné</font>
			</td>
			<td><span style="color: #669999">v pokoji</span></td>
			<td class="TabH2">12</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[S] <span style="color: #6984f9">Thunderbolt</span>: <b>12</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #8a68a5; vertical-align:middle;"><a href="../game/map_sector.php?sector=aco" style="color:white"><b>aco</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout VII</td>
			<td><span style="color: #669999">v pokoji</span><br><span style="color: #ff8800">bránenie</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #8a68a5; vertical-align:middle;"><a href="../game/map_sector.php?sector=al1" style="color:white"><b>al1</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XXIV</td>
			<td><span style="color: #669999">v pokoji</span><br><span style="color: #ff8800">bránenie</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #8a68a5; vertical-align:middle;"><a href="../game/map_sector.php?sector=al2" style="color:white"><b>al2</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout X</td>
			<td><span style="color: #669999">v pokoji</span><br><span style="color: #ff8800">bránenie</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #8a68a5; vertical-align:middle;"><a href="../game/map_sector.php?sector=b5" style="color:white"><b>b5</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>W IV</td>
			<td><span style="color: #669999">v pokoji</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td style="text-align:left">[D] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Warlock Destroyer</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #8a68a5; vertical-align:middle;"><a href="../game/map_sector.php?sector=bet" style="color:white"><b>bet</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout VIII</td>
			<td><span style="color: #669999">v pokoji</span><br><span style="color: #ff8800">bránenie</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="3" style="background-color: #8a68a5; vertical-align:middle;"><a href="../game/map_sector.php?sector=cet" style="color:white"><b>cet</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>W III</td>
			<td><span style="color: #669999">v pokoji</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td style="text-align:left">[D] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Warlock Destroyer</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>W I</td>
			<td><span style="color: #669999">v pokoji</span><br><span style="color: #999900">príprava obrany</span><br><span id="CGuard192" title="01 Feb - 13:15"></span>
				<script language="JavaScript">
	CountDown('CGuard192', 9349)
				</script>
			</td>
			<td class="TabH2">24</td>
			<td class="TabH2">1</td>
			<td class="TabH2">2</td>
			<td class="TabH2">12</td>
			<td style="text-align:left">[S] <span style="color: #6984f9">Thunderbolt</span>: <b>24</b><br>[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Olympus Corvette</span>: <b>1</b><br>[L] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Omega Destroyer</span>: <b>2</b><br>[D] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Warlock Destroyer</span>: <b>12</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>New HW</td>
			<td><span style="color: #669999">v pokoji</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Olympus Corvette</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #8a68a5; vertical-align:middle;"><a href="../game/map_sector.php?sector=gor" style="color:white"><b>gor</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout IV</td>
			<td><span style="color: #669999">v pokoji</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #8a68a5; vertical-align:middle;"><a href="../game/map_sector.php?sector=q14" style="color:white"><b>q14</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XI</td>
			<td><span style="color: #669999">v pokoji</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #8a68a5; vertical-align:middle;"><a href="../game/map_sector.php?sector=q55" style="color:white"><b>q55</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout IX</td>
			<td><span style="color: #669999">v pokoji</span><br><span style="color: #ff8800">bránenie</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="2" style="background-color: #8a68a5; vertical-align:middle;"><a href="../game/map_sector.php?sector=reg" style="color:white"><b>reg</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XVIII</td>
			<td><span style="color: #669999">v pokoji</span><br><span style="color: #ff8800">bránenie</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout V</td>
			<td><span style="color: #669999">v pokoji</span><br><span style="color: #ff8800">bránenie</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="4" style="background-color: #8a68a5; vertical-align:middle;"><a href="../game/map_sector.php?sector=s8" style="color:white"><b>s8</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>W V</td>
			<td><span style="color: #9999ff">príprava skoku</span><br>cieľ: <a href="../game/map_sector.php?sector=s9">s9</a> -> <a href="../game/map_sector.php?sector=b5">b5</a><br><span id="CTransfer279" title="01 Feb - 11:15"></span>
				<script language="JavaScript">
	CountDown('CTransfer279', 2136)
				</script>
			</td>
			<td class="TabH2">24</td>
			<td class="TabH2">0</td>
			<td class="TabH2">2</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[S] <span style="color: #6984f9">Thunderbolt</span>: <b>24</b><br>[L] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Omega Destroyer</span>: <b>2</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XVI</td>
			<td><span style="color: #9999ff">príprava skoku</span><br>cieľ: <a href="../game/map_sector.php?sector=s9">s9</a> -> <a href="../game/map_sector.php?sector=ocs">ocs</a><br><span id="CTransfer134" title="01 Feb - 12:37"></span>
				<script language="JavaScript">
	CountDown('CTransfer134', 7031)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout III</td>
			<td><span style="color: #9999ff">príprava skoku</span><br>cieľ: <a href="../game/map_sector.php?sector=s9">s9</a> -> <a href="../game/map_sector.php?sector=ocs">ocs</a><br><span id="CTransfer110" title="01 Feb - 12:36"></span>
				<script language="JavaScript">
	CountDown('CTransfer110', 6981)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout I</td>
			<td><span style="color: #9999ff">príprava skoku</span><br>cieľ: <a href="../game/map_sector.php?sector=s9">s9</a> -> <a href="../game/map_sector.php?sector=ocs">ocs</a><br><span id="CTransfer108" title="01 Feb - 12:38"></span>
				<script language="JavaScript">
	CountDown('CTransfer108', 7134)
				</script>
			</td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #a03b37; vertical-align:middle;"><a href="../game/map_sector.php?sector=ari" style="color:white"><b>ari</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XXIII</td>
			<td><span style="color: #669999">v pokoji</span><br><span style="color: #ff8800">bránenie</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr>
			<td rowspan="1" style="background-color: #a03b37; vertical-align:middle;"><a href="../game/map_sector.php?sector=mab" style="color:white"><b>mab</b></a></td>
			<td style="color: #8a68a5">Marcus</td>
			<td>Scout XIV</td>
			<td><span style="color: #669999">v pokoji</span><br><span style="color: #ff8800">bránenie</span></td>
			<td class="TabH2">0</td>
			<td class="TabH2">1</td>
			<td class="TabH2">0</td>
			<td class="TabH2">0</td>
			<td style="text-align:left">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
		</tr>
		<tr class="TabH1">
			<td colspan="3"></td>
			<th>Súčet</th>
			<th>60</th>
			<th>29</th>
			<th>4</th>
			<th>15</th>
			<td></td>
		</tr>
@endif
	</table><br><br>
	<div style="clear:both"></div>
</div>