@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/rules-age">Pravidlá veku</a></div>
@else
<div class="mainTitle">Pravidlá veku</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

    <div class="narrowBox" align="justify">

        <p>
            Pravidlá veku popisujú fungovanie a organizáciu hry počas veku. Sú tu určené podmienky pre víťazstvo a spôsoby, ktorými sa možno k víťazstvu dopracovať.
        </p><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">I. Základné pojmy</div>
            <div align="left" style="margin:10px;">
                <ol>

                    <li><strong>Vek</strong> - časový úsek od začiatku po koniec jednej hry. Na začiatku veku sa herný systém vynuluje.</li><br><br>
                    <li><strong>Štát</strong> - spoločenstvo hráčov, ktoré vystupuje v hre spoločne. Je prezentované vládou, má svoj názov a farbu.</li><br><br>
                    <li><strong>Občianstvo</strong> - určuje príslušnosť hráča k štátu. Súčasne je možné byť občanom iba v jednom štáte. Občianstvo je možné získať, stratiť alebo sa ho vzdať. Hráč bez občianstva sa nazýva <strong>neobčan</strong>.</li><br><br>
                    <li><strong>Sektor</strong> - časť herného plánu (mapy), ktorá je ďalej nedeliteľná. Môže byť buď vo vlastníctve štátu alebo neutrálny (bez vlastníka). Vlastník sektora je identifikovaný v hernom systéme (detail sektora).</li><br><br>
                    <li><strong>HW</strong> - domovský svet (home world) je označenie základného sektora hráča resp. štátu. HW je vlastnosť, nie vlastníctvo!</li><br><br>
                    <li><strong>HW hráča</strong> - je sektor, v ktorom sa nachádza stanica hráča.</li><br><br>
                    <li><strong>HW štátu</strong> - označuje sektor, ktorý je z hľadiska pravidiel pre štát podstatný. Hráč po zapojení do veku má HW podľa HW štátu do ktorého sa prihlásil. V jeden sektor môže byť HW max. pre jeden štát.</li><br><br>
                    <li><strong>Útok na HW</strong> - je útok na sektor, ktorý je HW určitého štátu za podmienky, že tento sektor vlastní štát, ktorému je HW.</li><br><br>
                    <li><strong>OL</strong> - ochranná lehota je časový úsek, počas ktorého má štát v OL ochranu na základe pravidiel.</li><br><br>

                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">II. Ciele veku a víťazstvo</div>
            <div align="left" style="margin:10px;">
                <ol>

                    <li>Ciele veku a víťazstvo závisia na type veku. Typy veku sú nasledovné:<br><br>
                        <ol type="A">

                            <li><strong class="hl">Štandardný - klasický vek</strong><br> Pre tento vek platia všeobecne nasledovné pravidlá:<br><br>
                                <ul type="disc">

                                    <!--
<li>Cieľ, ktorý platí pre všetky štáty, vyberú a určia TM zo zoznamu cieľov a zároveň určia dĺžku trvania veku, ak sa v rámci cieľa nepíše inak (väčšinou 2 mesiace).</li><br>
<li>Vek končí v momente, kedy cieľ splní prvý štát (viacero štátov), alebo uplynutím stanoveného času.</li><br>
<li>Vek vyhrá štát, ktorý ako prvý splní pevne stanovený cieľ veku alebo splní podmienku cieľa veku v prípade uplynutia lehoty trvania veku.</li><br>
<li>Vždy sa vyhodnocuje kompletné poradie štátov.</li><br>
<li>Zničené štáty sa zapisujú na najnižšie voľné miesto v poradí, v akom boli zničené.</li><br>
<li>Pokiaľ splní cieľ veku naraz viacero štátov, alebo medzi štátmi nastane rovnosť, poradie sa určí podľa počtu získaných bojových bodov.</li><br>
<li>Štáty si nastavujú vzťahy podľa vlastného uváženia.</li><br>
<li>Po úspešnom útoku na HW štátu si štáty vyberajú jednu z možností podľa č. III bod 4.</li><br>
<li>Počas veku je možné získať občianstvo iného štátu a zakladať štáty, ak nie je v rámci cieľov uvedené inak.</li><br>
<li>Pri cieľoch, ktoré si vyžadujú plniť danú podmienku určitý čas (napr.: udržať územie 2 dni), štát vyhlási začiatok pokusu o splnenie podmienky na PF. V prípade, že pokus bude neúspešný, môže vyhlásiť ďalší začiatok na PF až po potvrdení o neúspešnom pokuse zo strany TM.</li><br>
-->

                                    <li>Ciele veku si vyberajú štáty ľubovolne bez oznamovania, pričom za splnenie cieľov získajú daný počet bodov.</li><br>
                                    <li>Dĺžka trvania veku, ak Kruh TM neurčí inak, je 2 mesiace.</li><br>
                                    <li>Vek končí uplynutím stanoveného času.</li><br>
                                    <li>Vek vyhrá štát, ktorý má na konci veku najviac bodov. V prípade rovnosti bodov rozhoduje vyšší počet dosiahnutých bojových bodov.</li><br>
                                    <li>Nevyhodnocuje sa kompletné poradie štátov. Vek má len víťaza a ostatní sú porazení.</li><br>
                                    <li>Štáty nie je možné zničiť.</li><br>
                                    <li>Štáty si nastavujú vzťahy podľa vlastného uváženia.</li><br>
                                    <li>Počas veku je možné získať občianstvo iného štátu a zakladať štáty, ak Kruh TM neurčí inak.</li><br>
                                    <li>Pri cieľoch, ktoré si vyžadujú plniť danú podmienku určitý čas (napr.: udržať územie 2 dni), štát vyhlási začiatok pokusu o splnenie podmienky na Politickom fóre. Daná podmienka musí byť naplnená na začiatku a na konci danej lehoty, nevyhodnocuje sa počas lehoty (počas lehoty teda nemusí byť naplnená).</li><br>

                                </ul>
                            </li><br>

                            <li><strong class="hl">Špecifický vek</strong><br>
                                <br>
                                <ul type="disc">
                                    <li>Cieľ určia TM špecificky a to pre každý štát a zároveň určia dĺžku veku vrátane iných špeciálnych pravidiel a podmienok, ktoré budú pre vek platiť.</li><br>
                                </ul>
                            </li><br>

                        </ol>
                    </li>

                    <!--
<li>Zoznam cieľov klasického veku:<br><br>
<ul type="disc">
<li><strong class="hl">C1. Diktatúra</strong> - vyhráva iba jeden štát, ktorý v stanovenej lehote trvania veku dobyje najvyšší počet HW štátov bez ohľadu na výber možností podľa časti III. bod 4 (úspešný útok na HW štátu). Počíta sa aj opakované dobytie HW štátu - najviac však 3 dobytia HW jedného štátu, ktoré sa uskutočnia v rozmedzí najmenej 10 dní medzi jednotlivými dobytiami.</li><br>
<li><strong class="hl">C2. Dominancia sily</strong> - vyhráva jeden štát v aliancii, ktorá zničí všetky ostatné štáty. Poradie resp. víťaza v aliancii určí vyšší počet dosiahnutých bojových bodov.</li><br>
<li><strong class="hl">C3. Dominancia územia</strong> - vyhráva iba jeden štát, ak po dobu dvoch dní udrží aspoň 45% sektorov, v prípade štátu s politickým systémom Drakhov 47%. V prípade, že v stanovenej lehote trvania veku nesplní podmienku žiaden štát, víťazom sa stane štát s najvyšším počtom sektorov ku koncu veku.</li><br>
<li><strong class="hl">C4. Dominancia bojovej aktivity</strong> - vyhráva iba jeden štát, ktorý v stanovenej lehote trvania veku dosiahne najvyšší počet bojových bodov. V prípade rovnakého počtu bodov ku koncu veku, vek prebieha ďalej a víťazstvo sa kontroluje vždy na prepočet. Počas veku nie je možné získať občianstvo iného štátu ani zakladať štáty.</li><br>
</ul>
</li><br>
-->

                    <li>Zoznam cieľov a ich podmienky, zisk bodov:<br><br>
                        <ul type="disc">

                            <li><strong class="hl">C1. Diktatúra</strong><br> - štát dobyje HW iného štátu pričom splní dobrovoľnú podmienku pri útoku na HW počas zahájenia útoku: 3 body<br> - štát dobyje HW iného štátu bez splnenia dobrovoľnej podmienky: 2 body<br> - štát môže získať body najviac z dvoch dobytí HW toho istého štátu<br> - dobyť HW jedného štátu so ziskom bodov je možné maximálne trikrát (za ďalšie dobytia HW štátu sa body už nezískavajú)<br>
                            </li><br>
                            <li><strong class="hl">C2. Dominancia sily</strong><br> - štát dobyje HW 3 rôznych štátov bez ohľadu na spôsob dobytia: 5 bodov<br> - ide o jednorázový zisk bodov<br>
                            </li><br>
                            <li><strong class="hl">C3. Dominancia územia</strong><br> - štát udrží svoje územie - daný počet sektorov počas 2 dní<br> 25% sektorov: 1 bod<br> 35% sektorov: 2 body<br> 45% sektorov: 3 body<br> 55% sektorov: 5 bodov<br> - ide o jednorazový zisk bodov v rámci daného počtu sektorov - štát môže splniť daný cieľ s daným územím iba raz za vek (raz 35%, raz 45% atď.)<br>
                            </li><br>
                            <li><strong class="hl">C4. Dominancia bojovej aktivity</strong><br> - štat získa za každých 30.000 bojových bodov 2 body<br> - štát získa body vždy v rámci daného míľnika - 30 tis., 60. tis. atď. Ak štát už body získal a body stratí odchodom hráča k neobčanom, zisk platí, avšak ďalšie body získa štandardne - dosiahnutím daného míľnika
                            </li><br>
                            <li><strong class="hl">C5. Dominancia ekonomiky</strong><br> - štát udrží 2 dni najvyššie UU HDP na hráča: 2 body<br> - ide o jednorazový zisk bodov
                            </li><br>
                            <li><strong class="hl">C6. Individuálna dominancia</strong><br> - občan štátu ma najlepší ekonomický rozvoj podľa štatistík, štát získa: 1 bod<br> - ide o automatický zisk bodov, pričom daný ekonomický rozvoj sa kontroluje a zisk bodov sa udeľuje vždy k 1. a 15. dňu mesiaca (štatistiku len v ekonomickej časti vidí iba Kruh TM)<br> - jeden štát môže spolu získať maximálne 2 body
                            </li><br>
                            <li><strong class="hl">C7. Dominancia aktivity</strong><br> - každý štát dostane od TM rovnaký zoznam úloh s podmienkami. Štáty si zo zoznamu vyberú jednu úlohu do päť dní od začiatku veku (lehota končí na piaty deň o polnoci a výber nahlási zodpovednému TM)<br> - za splnenie úlohy štát získa: 3 body<br> - ide o jednorazovú úlohu (nie je možné žiadať opakovane)
                            </li><br>

                        </ul>
                    </li><br>

                    <li>Nahlasovanie splnenia cieľov<br><br> Štáty sú povinné nahlásiť splnenie cieľa Kruhu TM v prípade cieľov C1, C2 a podľa podmienok aj C7.
                    </li><br>

                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">III. Útok na HW</div>
            <div align="left" style="margin:10px;">
                <ol>

                    <!--
<li>Útočiť na HW štátu je možné iba vtedy, ak útočník (zadávateľ útoku) vlastní počas zahájenia útoku aspoň dva sektory v okruhu jedného skoku od HW obrancu. Hráč bez občianstva má zakázané zadávať útok v HW štátu.</li><br><br>
<li>Za útočníka pre účely vyhlásenia rozhodnutia v zmysle bodu 4 tohto článku vrátane plnenia súvisiacich práv sa považuje iba štát, ktorý začal útok.</li><br><br>
<li>Zúčastniť sa útoku na akejkoľvek strane môžu všetky štáty a aj hráči bez občianstva ľubovoľne.</li><br><br>

<li>Po úspešnom útoku na HW nastane jedna z možností:<br><br>
<ol type="A">

<li>Útočník do 24 hodín od skončenia útoku opustí HW a obranca dostane 6 dní ochrannú lehotu.</li><br>
<li>Útočník do 24 hodín od skončenia útoku neopustí HW  a nastane okupácia HW štátu s cieľom jeho zničenia. Obranca má 4 dni na to, aby získal späť vlastníctvo HW sektora, inak štát zaniká, občania prejdú k neobčanom. Po úspešnom získaní vlastníctva obrancom začne od tohto momentu plynúť pre obrancu ochranná lehota 10 dní.</li><br>
<li>Útočník do 24 hodín od skončenia útoku neopustí HW  a nastane okupácia HW štátu s cieľom krádeže technológie. Obranca má 2 dni na to, aby získal späť vlastníctvo HW sektora, inak útočník získa jednu štátnu technológiu obrancu podľa vlastného výberu. Ak obranca získa počas 2 dní svoje HW, začne plynúť pre obrancu ochranná lehota 5 dní, ak nie, získa ochrannú lehotu 7 dní.</li><br>

</ol>
</li><br>

<li>Útočník je povinný na PF prehlásiť, ktorú možnosť si vyberá a to najneskôr do 12 hodín od ukončenia útoku. Výber technológie sa oznamuje na PF.</li><br>
<li>Po neúspešnom útoku na HW štátu (útočník nezíska sektor), môže útočník ako aj štáty zúčastnené na strane útočníka začať ďalší útok na HW štátu až po uplynutí 3 dní od tohto neúspešného útoku.</li><br>
-->

                    <li>Útočiť na HW štátu je možné iba vtedy, ak daný štát nie je v ochrannej lehote. Štát - zadávateľ útoku pri zadávaní útoku nemusí splniť žiadnu podmienku, avšak pri splnení dobrovoľnej podmienky získa viac bodov v prípade úspešného útoku. Dobrovoľná podmienka: štát vlastní počas zahájenia útoku aspoň dva sektory v okruhu jedného skoku od HW obrancu.</li><br><br>
                    <li>Hráč bez občianstva má zakázané zadávať útok v HW štátu.</li><br><br>
                    <li>Za útočníka sa považuje iba štát, ktorý začal útok.</li><br><br>
                    <li>Zúčastniť sa útoku na akejkoľvek strane môžu všetky štáty a aj hráči bez občianstva ľubovoľne.</li><br><br>
                    <li>Po úspešnom útoku na HW útočník, ako aj všetci hráči zapojení na strane útočníka, do 24 hodín od skončenia útoku opustia HW.</li><br><br>
                    <li>Po neúspešnom útoku na HW štátu (útočník nezíska sektor), môže útočník ako aj štáty zúčastnené na strane útočníka začať ďalší útok na HW štátu až po uplynutí 3 dní od tohto neúspešného útoku.</li><br><br>

                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">IV. Ochranná lehota (OL)</div>
            <div align="left" style="margin:10px;">
                <ol>
                    <!--
<li>OL začína od momentu jej potvrdenia na PF zo strany TM. TM OL potvrdzuje na základe vyhlásenia útočníka o spôsobe správania sa po úspešnom útoku na HW,
od uplynutia lehoty na takéto vyhlásenie útočníkom, alebo na základe iných podmienok (uplynutie lehoty v rámci okupácie, prevzatie kontroly nad HW a podobne).</li><br><br>

<li>Sektory, ktoré spadajú pod OL sú HW štátu a sektory vzdialené 1 skok od HW tohto štátu. Počas OL je systémovo nastavená možnosť útočiť v týchto sektoroch
len pre štát, ktorý je v OL. Ostatné štáty/hráči majú možnosť útoku zablokovanú - a to od momentu potvrdenia OL zo strany TM až do uplynutia posledného dňa OL (o polnoci).</li><br><br>

<li>Od momentu potvrdenia OL zo strany TM je zakázané zo strany iných hráčov/štátov premiestňovať stanicu hráča alebo posielať ťažbu do sektorov spadajúcich pod OL,
búrať stanice alebo skokové brány v týchto sektoroch. V prípade, že títo hráči majú stanicu alebo ťažbu v sektoroch OL, sú povinní ju do 12 hodín od začiatku OL
premiestniť.</li><br><br>

<li>Ak sa v momente začiatku OL nachádzajú v HW štátu (alebo tam letia a let už nie je možné zastaviť) plavidlá hráča, ktorého štát nespadá pod danú OL,
je hráč povinný v lehote 12 hodín od začiatku OL HW opustiť. V iných sektoroch, ako je HW štátu v OL, je pohyb plavidiel dovolený.</li><br><br>

<li>V prípade nedodržania povinností podľa bodu 3. a 4. môže štát v OL požiadať TM o presun/premiestnenie stanice/ťažby/plavidiel.</li><br><br>

<li>Pokiaľ sa v sektoroch spadajúcich pod OL nachádza HW iného štátu, tak v tomto sektore sa OL neaplikuje. Ostatné povinnosti platia štandardne.</li><br><br>

<li>Ak sektor s HW štátu, ktorý je v OL, vlastní niekto iný, tak vláda štátu môže požiadať TM o prepísanie vlastníctva sektora.</li><br><br>
-->

                    <li>OL začína plynúť od momentu jej potvrdenia na PF zo strany TM. TM OL potvrdzuje po ukončení úspešného útoku na HW štátu.</li><br><br>
                    <li>Štát, ktorý má OL získať, môže túto skutočnosť vo vlastnom záujme oznámiť TM (aj počas útoku alebo po útoku).</li><br><br>
                    <li>V prípade, že po úspešnom útoku na HW štátu nebola zničená ťažba, štát získa OL v trvaní 7 dní. V prípade zničenej ťažby OL trvá 10 dní.</li><br><br>
                    <li>Sektory, ktoré spadajú pod OL sú HW štátu a sektory vzdialené 1 skok od HW tohto štátu. Počas OL je systémovo nastavená možnosť útočiť v týchto sektoroch len pre štát, ktorý je v OL. Ostatné štáty/hráči majú možnosť útoku zablokovanú - a to od momentu potvrdenia OL zo strany TM až do uplynutia posledného dňa OL (o polnoci).</li><br><br>
                    <li>Od momentu potvrdenia OL zo strany TM je zakázané zo strany iných hráčov/štátov premiestňovať stanicu hráča alebo posielať ťažbu do sektorov spadajúcich pod OL, búrať stanice alebo skokové brány v týchto sektoroch. V prípade, že títo hráči majú stanicu alebo ťažbu v sektoroch OL, sú povinní ju do 12 hodín od potvrdenia OL premiestniť.</li><br><br>
                    <li>Ak sa v momente začiatku OL nachádzajú v HW štátu (alebo tam letia a let už nie je možné zastaviť) plavidlá hráča, ktorého štát nespadá pod danú OL, je hráč povinný v lehote 12 hodín od začiatku OL HW opustiť. V iných sektoroch, ako je HW štátu v OL, je pohyb plavidiel dovolený.</li><br><br>
                    <li>V prípade nedodržania povinností môže štát v OL požiadať TM o presun/premiestnenie stanice/ťažby/plavidiel.</li><br><br>
                    <li>Pokiaľ sa v sektoroch spadajúcich pod OL nachádza HW iného štátu, tak v tomto sektore sa OL neaplikuje. Ostatné povinnosti platia štandardne.</li><br><br>
                    <li>Ak sektory v OL v čase potvrdenia zo strany TM vlastní niekto iný, tak TM prepíše vlastníctvo sektora v lehote 24 hodín od potvrdenia na PF.</li><br><br>
                    <li>V prípade, ak v HW štátu prebieha v čase potvrdenia OL útok, ktorý začal pred potvrdením OL iný štát ako je vlastník HW, do útoku sa po potvrdení OL nemôžu pridávať žiadne ďalšie plavidlá zo strany útočníka.</li><br><br>

                </ol>
            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">V. Sektor Babylon 5 (B5)</div>
            <div align="left" style="margin:10px;">
                <ol>

                    <li>Sektor b5 je neutrálny sektor. Všetci môžu cez sektor voľne prelietať a zdržovať sa v ňom, nikdy nemôže spadať pod OL akéhokoľvek štátu.</li><br><br>
                    <li>Žiaden hráč v sektore b5 nemôže mať svoju stanicu a ani ťažbu s výnimkou hráčov bez občianstva. Kapacita sektora a indexy sú prispôsobené možnosti ťažby neobčanov.</li><br><br>
                    <li>V sektore je zablokovaná možnosť útočiť a to približne prvý mesiac prebiehajúceho veku (+-5 dní). Odblokovanie a umožnenie útoku vykonajú TM v danom rozmedzí bez oznámenia.</li><br><br>
                    <li>Pravidlá sektora b5 platia všeobecne pre každý vek, ak TM neurčia pre daný vek inak.</li><br><br>

                </ol>

            </div>
        </div><br>

        <div class="window" style="font-size:12px;">
            <div class="windowCaption">VI. Záver</div>
            <div align="left" style="margin:10px;">
                <ol>

                    <li>Je zakázané pri útokoch umelo získavať výhody alebo možnosti dané systémom hry (napr. umelo navodzovať podmienky naplnenia OL, alebo umelo - fingovanými útokmi - získavať bojové body a podobne).</li><br><br>
                    <li>Je zakázané, aby si alianční partneri preberali sektor HW za akýmkoľvek účelom, ak je pôvodným vlastníkom štát, ktorý tam má HW.</li><br><br>
                    <li>Ťažba v sektore s HW iného štátu je zakázana pokiaľ tento štát existuje.</li><br><br>
                    <li>Výklad pravidiel poskytuje kruh TM na vytvorenom fóre alebo admin fóre. Takýto výklad je záväzný.</li><br><br>
                    <li>Sankcie určuje kruh TM v závislosti od závažnosti. Individuálne sankcie sú predovšetkým pokuty pre hráčov vo forme straty TMK alebo surovín, prípadne podľa uváženia TM. Sankcie pre štáty môžu byť predovšetkým v rovine zákazu.</li><br><br>
                    <li>Vyhlásenia na PF musia byť jednoznačné. Odporúčame uviesť označenie aj znenie bodu pravidiel, na ktorý sa odvolávate.</li><br><br>

                </ol>
            </div>
        </div><br>

    </div>
    <div style="clear:both"></div>
</div>