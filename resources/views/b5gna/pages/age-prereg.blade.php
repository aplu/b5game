@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/age-prereg">Predregistrácia do nového veku</a></div>
@else
<div class="mainTitle">Predregistrácia do nového veku</div>
@endif
<div class="mainTitleFade"></div>

<div class="content">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-profile')
    @endif

    <div class="narrowBox">
        <p>
            Účelom predregistrácie je vyvážené rozdelenie hráčov do štátov v najbližšom veku. Platia tieto pravidlá predregistrácie:<br> 1) Výber štátu v predregistrácii je záväzný (v novom veku sa nebude dať zapojiť za iný).<br> 2) Hráči, ktorí si nevyberú štát v predregistrácii, sa budú môcť zapojiť do veku až v 5. deň veku.<br> 3) Body 1 a 2 neplatia pre hráčov, ktorí sa registrovali do hry pred menej ako 120 dňami.
        </p><br>
        <table cellspacing="0" class="Tab2 center" style="width:80%">
            <tr>
                <td colspan="4" class="TabH2">
                    <h2>Výsledok predregistrácie do aktuálneho veku</h2>
                </td>
            </tr>
            <tr>
                <th>Štát</th>
                <th>Počet</th>
                <th>Maximum</th>
                <th>Hráči</th>
            </tr>
            <tr style="color:#dd9090">
                <td style="width:30%;"><b>Centaurská Republika</b></td>
                <td style="width:10%" class="hl"><b>10</b></td>
                <td style="width:10%" class="hl">10</td>
                <td>Magog, Caesar, ujoduro, navinav, MORFEUS, ven0m, Jerry, Miso, Dark One, Anne</td>
            </tr>
            <tr style="color:#787811">
                <td style="width:30%;"><b>Kolónia Drakhov</b></td>
                <td style="width:10%" class="hl"><b>8</b></td>
                <td style="width:10%" class="hl">10</td>
                <td>froggy, yntums, aquarius, Ragnarök, Mikiti, Dobyvatel, Kosh VI, Velký teplý Al</td>
            </tr>
            <tr style="color:#30bba9">
                <td style="width:30%;"><b>Kruh Technomágov</b></td>
                <td style="width:10%" class="hl"><b>4</b></td>
                <td style="width:10%" class="hl">4</td>
                <td>Lucas, Sir_Chislehurst, Bard, Shaade</td>
            </tr>
            <tr style="color:#8a68a5">
                <td style="width:30%;"><b>Minbarská Federácia</b></td>
                <td style="width:10%" class="hl"><b>10</b></td>
                <td style="width:10%" class="hl">10</td>
                <td>Ulkesh, DusKE, Shafter, Asgard12, Nusku1, ShalTok, Richard B. Riddick, volvo23, voloda1024, Maverick</td>
            </tr>
            <tr style="color:#a03b37">
                <td style="width:30%;"><b>Narnský Režim</b></td>
                <td style="width:10%" class="hl"><b>10</b></td>
                <td style="width:10%" class="hl">10</td>
                <td>Kakarrot, popocatepetI, TheNihilist, Škvrnitý Démon, haho, Turgon III., klapacius, tomas1502, ANALfabet_Hubakuk, tlčhuba</td>
            </tr>
            <tr style="color:#6984f9">
                <td style="width:30%;"><b>Pozemská Aliancia</b></td>
                <td style="width:10%" class="hl"><b>7</b></td>
                <td style="width:10%" class="hl">10</td>
                <td>zdroj, Apokalips, Rio, GimliCZ, jjenda, aragok, Itachi</td>
            </tr>
        </table><br><br>
        <div class="window center" style="font-size:12px; width:50%; background-color: #100909; border: 1px dotted #502020;">
            <div align="left" style="margin:10px;"><b style="color: red;">Momentálne neprebieha predregistrácia do nového veku.</b></div>
        </div>
    </div>
    <div style="clear:both"></div>
</div>