@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/reg">Registrácia</a></div>
@else
<div class="mainTitle">Registrácia</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

	<div class="narrowBox">
		<p>Pre prístup do hry je potrebné si vytvoriť užívateľské konto. Získaš ho registráciou. Vyplň svoje registračné údaje a klikni na "Odoslať registráciu".
		  Skontroluj správnosť zadanej e-mailovej adresy a taktiež, či ju nemaž zaplnenú, inak sa nám nepodarí Ti zaslať prístupové heslo. V prípade komplikácii nám <a href="/contact">napíš</a>.
		</p>
		<br>
		<div class="window" style="font-size:12px;">
			<div class="windowCaption">Registrácia užívateľa</div>
			<div align="left" style="margin:10px;">
				<font color="red"><br>
					<b>Upozornenie:</b><br>
					<br>
					<p>V tejto hre sa užívateľské konto použité v minulých vekoch automaticky presúva do nasledujúcich vekov. Nie je povolené vytvoriť nové konto osobe, ktorá v minulosti už v tejto hre bola registrovaná. Ak už máš svoje konto, tak sa prihlás do hry. Po prihlásení v novom veku budeš mať možnosť si vybrať nový štát a zmeniť svoj nick. Užívatelia, ktorí v minulosti mali zakázaný prístup do hry, môžu požiadať o zrušenie trestu cez kontaktný formulár.</p>
					<a href="/reg-2">Nikdy som nebol registrovaný, klikni sem pre vytvorenie nového konta</a><br>
					<br>
					<a href="/new-pass">Zabudol som svoje heslo, potrebujem nové</a><br>
					<br>
					<a href="/contact">Bol som registrovaný, ale neviem sa prihlásiť, kontaktuj nás!</a><br>
				</font><br>
				<br>
			</div>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
