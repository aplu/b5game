@php
$destinations = [
	'b5' => 'Babylon 5',
	'cen' => 'Centauri Prime',
	'ear' => 'Earth',
	'min' => 'Minbar',
	'nar' => 'Narn HW',
];

$selected = isset($_GET['selected']) && isset($destinations[$_GET['selected']]) ? $_GET['selected'] : '';

@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/relocate">Presunúť stanicu do iného sektora</a></div>
@else
<div class="mainTitle">Presunúť stanicu do iného sektora</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

	<br>Stanicu je možné presunúť do sektora vo vlastníctve tvojho štátu (u neobčanov do neutrálneho sektora).<br><br><br>

@if (empty($destinations))
	<font color="red">Žiadny vyhovujúci sektor sa nenašiel.</font>
@else
	<form action="" method="post">
		<h2>Cieľový sektor</h2><br>
		<select name="to" style="width:180;">
	@foreach ($destinations as $idx => $name)
			<option value="{{ $idx }}"{!! $selected == $idx ? ' selected' : '' !!}>{{ $name }}</option>
	@endforeach
		</select>
		<input type="submit" value="Vybrať">
	</form><br><br>

	@if (!empty($selected))
	<form action="" method="post"><input type="hidden" name="to" value="s3">
		<table width="50%" cellspacing="0" class="Tab center">
			<tbody>
				<tr>
					<th>Podmienky presunu stanice</th>
					<th width="90">Stav</th>
				</tr>
				<tr>
					<td>Cieľový sektor musí patriť tvojmu štátu</td>
					<td><font color="lime">- splnená -</font></td>
				</tr>
				<tr>
					<td>Tvoje lode nemôžu byť zapojené v boji</td>
					<td><font color="lime">- splnená -</font></td>
				</tr>
				<tr>
					<td>Nemôžeš mať vo výrobe ťažbu</td>
					<td><font color="red">- nesplnená -</font></td>
				</tr>
				<tr>
					<td>Nemôžeš mať vo výrobe žiadne lode ani stíhače</td>
					<td><font color="lime">- splnená -</font></td>
				</tr>
				<tr>
					<td>Všetky tvoje ťažobné skupiny musia byt stiahnuté</td>
					<td><font color="red">- nesplnená -</font></td>
				</tr>
				<tr>
					<td>
						Dostatok surovín na zaplatenie presunu:<br>
						Titánium: <b class="hl">75'000 ton</b><br>
						Quantium 40: <b class="hl">1'500 ton</b><br>
						Kredity: <b class="hl">56,25 mil</b><br>
						<br>
						alebo <b class="hl">jeden TM kryštál</b>
					</td>
					<td><font color="lime">- splnená -</font></td>
				</tr>
				<tr>
					<td colspan="2" style="color:white; background:#800000; font-size:12px;">
						<b>Nie sú splnené všetky podmienky na presun!</b>
					</td>
				</tr>
			</tbody>
		</table>
	</form><br><br>
	@endif

@endif

	<div style="clear:both"></div>
</div>
