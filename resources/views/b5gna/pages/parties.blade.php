@php
$parties = [
    1 => ['name' => 'Terran Party', 'chairman' => 'Luis Santiago', 'members' => 3],
    2 => ['name' => 'Loosers', 'chairman' => 'Marie Crane', 'members' => 2],
];
$status = request('status') ?? 'requested'; // none,member,chairman,requested,canceled
$party = 1;
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/parties">Politické strany</a></div>
@else
<div class="mainTitle">Politické strany</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    @if ($status == 'requested')
    Žiadosť bola odoslaná.<br><br>
    @elseif ($status == 'canceled')
    Strana bola zrušená.<br><br>
    @endif

    <table width="40%" cellspacing="0" class="Tab center">
        <tr>
            <td class="TabH1">
        @if ($status == 'member' || $status == 'chairmen')
                Si členom strany:
                <font color="red">{{ $parties[$party]['name'] }}</font>
        @else
                Nie si členom strany.
        @endif
            </td>
        </tr>
    </table><br>

    <table width="80%" cellspacing="0" class="Tab center">
        <tr style="font-size: 12px; color:red;">
            <td colspan="3"><b>Zoznam politických strán</b></td>
        </tr>
        <tr>
            <th>Názov</th>
            <th>Predseda</th>
            <th>Počet členov</th>
        </tr>
        @foreach ($parties as $id => $party)
        <tr>
            <td>{{ $party['name'] }}</td>
            <td>{{ $party['chairman'] }}</td>
            <td>{{ $party['members'] }}</td>
        </tr>
        @endforeach
    </table><br>

    @if ($status == 'chairmen')
    <table width="45%" cellspacing="0" class="Tab center">
        <tr style="font-size: 12px; color:red;">
            <td><b>Zrušiť politickú stranu.</b></td>
        </tr>
        <tr>
            <td><a href="?cancel=true">Zrušiť</a></td>
        </tr>
    </table>

    @elseif ($status == 'none' || $status == 'requested' || $status == 'canceled')

    <form action="" method="post">
        <table width="45%" cellspacing="0" class="Tab center">
            <tr style="font-size: 12px; color:red;">
                <td colspan="2"><b>Založiť novú stranu</b></td>
            </tr>
            <tr>
                <td><input type="text" name="newBranch" size="30" maxlength="30"></td>
                <td><input type="submit" value="Založiť"></td>
            </tr>
        </table>
    </form><br>
    <form action="" method="post">
        <table width="45%" cellspacing="0" class="Tab center">
            <tr style="font-size: 12px; color:red;">
                <td colspan="2"><b>Žiadosť o vstup do strany</b></td>
            </tr>
            <tr>
                <td><select name="request">
                @foreach ($parties as $id => $party)
                    <option value="{{ $id }}">{{ $party['name'] }}</option>
                @endforeach
                </select></td>
                <td><input type="submit" value="Poslať žiadosť"></td>
            </tr>
        </table>
    </form><br>

    @endif

    <div style="clear:both"></div>
</div>