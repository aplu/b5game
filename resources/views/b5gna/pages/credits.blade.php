@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/credits">Credits</a></div>
@else
<div class="mainTitle">Credits</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')
<div class="content" >

    <div class="mainContentBox">

    	<p>Babylon 5, characters, names, and all related indicia are trademarks of Time Warner Entertainment Co. and Babylonian Productions, LP.<br>
© 1993-2020 Time Warner Entertainment Co. and Babylonian Productions, LP. All Rights Reserved.</p>

	    <a target="_blank" href="https://icons8.com/icons/set/tomato">Tomato icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>

    </div>
    <div style="clear:both"></div>
</div>
