@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/transfer">Prevod surovín</a></div>
@else
<div class="mainTitle">Prevod surovín</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

@if (isset($success))
    Prevod bol úspešný! Príjemca: <b style="color: #30bba9">Sir_Chislehurst</b><br>
@elseif (isset($error))
    <font color="red">Kredity: množstvo bolo znížené na limit!</font><br>
@endif
    <br>
    <table width="85%" cellspacing="0" class="Tab center">
        <tr>
            <td class="TabH1"></td>
            <th>Kredity</th>
            <th>Titánium</th>
            <th>Q40</th>
            <th>Ruda Ti</th>
            <th>Ruda Q40</th>
        </tr>
        </tr>
        <tr>
            <th>Tvoje suroviny</th>
            <td>36'785'711</td>
            <td>31'925</td>
            <td>452</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <th>Celkový limit na 3 dni</th>
            <td>300'000'000</td>
            <td>450'000</td>
            <td>30'000</td>
            <td>600'000</td>
            <td>90'000</td>
        </tr>
        <tr>
            <th>Tvoj celkový zvyšný limit</th>
            <td>300'000'000</td>
            <td>450'000</td>
            <td>30'000</td>
            <td>600'000</td>
            <td>90'000</td>
        </tr>
    @if (isset($chosen))
        <tr class="TabH1">
            <td>Limit na 3 dni pre <b style="color: #30bba9">Sir_Chislehurst</b></td>
            <td>96'508'011</td>
            <td>144'762</td>
            <td>9'651</td>
            <td>193'016</td>
            <td>28'952</td>
        </tr>
        <tr class="TabH1">
            <td>Zvyšný limit pre <b style="color: #30bba9">Sir_Chislehurst</b></td>
            <td>96'508'010</td>
            <td>144'762</td>
            <td>9'651</td>
            <td>193'016</td>
            <td>28'952</td>
        </tr>
    @endif
        <tr class="TabH2">
    @if (isset($chosen))
            <th>Maximálny prevod pre <b style="color: #30bba9">Sir_Chislehurst</b></th>
    @else
            <th>Maximálny prevod</th>
    @endif
            <td style="color:lime">36'785'710</td>
            <td style="color:lime">31'925</td>
            <td style="color:lime">452</td>
            <td style="color:red">0</td>
            <td style="color:red">0</td>
        </tr>
    </table><br><br>
    <form action="" method="post">
        <table width="90%" cellspacing="0" class="Tab center">
            <tr>
                <td colspan="6" class="TabH1">
                    <h2>Previesť suroviny</h2>
                </td>
            </tr>
            <tr>
                <th>Príjemca</th>
                <th>Kredity</th>
                <th>Titánium</th>
                <th>Q40</th>
                <th>Ruda Ti</th>
                <th>Ruda Q40</th>
            </tr>
            <tr>
                <td><select name="player" style="width:200px">
                    <option value="0">--- vyber príjemcu ---</option>
                    <option value="1807" >ANALfabet_Hubakuk</option>
                    <option value="1997" >Anne</option>
                    <option value="29" >Apokalips</option>
                    <option value="160" >aquarius</option>
                    <option value="401" >aragok</option>
                    <option value="819" >Asgard12</option>
                    <option value="162" >Bard</option>
                    <option value="99" >Caesar</option>
                    <option value="1927" >Dark One</option>
                </select></td>
                <td><input type="text" name="trans[cr]" value="0" style="width: 100px"></td>
                <td><input type="text" name="trans[ti]" value="0" style="width: 100px"></td>
                <td><input type="text" name="trans[q40]" value="0" style="width: 100px"></td>
                <td><input type="text" name="trans[rti]" value="0" style="width: 100px"></td>
                <td><input type="text" name="trans[rq40]" value="0" style="width: 100px"></td>
            </tr>
            <tr>
                <td colspan="6" class="TabH1"><input type="submit" name="Send" value="Odoslať suroviny"></td>
            </tr>
        </table>
    </form><br><br>

    <h2>Prevody za posledné 3 dni</h2><br>
    <table width="90%" cellspacing="0" class="Tab center">
        <tbody>
            <tr>
                <th>Čas</th>
                <th>Kredity</th>
                <th>Titánium</th>
                <th>Q40</th>
                <th>Ruda Ti</th>
                <th>Ruda Q40</th>
                <th>Pohyb</th>
            </tr>
            <tr>
                <td class="TabH1">24 Oct - 20:58</td>
                <td style="color:red">-1</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td class="TabH1" style="text-align:left;"><b class="hl">prevod:</b> príjemca: <b style="color: #30bba9">Sir_Chislehurst</b></td>
            </tr>
            <tr class="TabH2">
                <th>Spolu za obdobie:</th>
                <td style="color:red">-1</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <td style="color:red">0</td>
                <th></th>
            </tr>
        </tbody>
    </table>
    <div style="clear:both"></div>
</div>