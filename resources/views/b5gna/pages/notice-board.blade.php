@php
$notices = [
	['author' => 'Sheridan', 'datetime' => '12 Nov 2273 - 12:34:56', 'content' => 'Attention please...'],
];
@endphp
@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/notice-board">Národná výveska</a></div>
@else
<div class="mainTitle">Národná výveska</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content notice-board" style="text-align: center;">

	@foreach ($notices as $notice)
	<br><br>
	<table width="90%" border="0" cellspacing="0" cellpadding="3" class="TabPost center">
		<tbody>
			<tr>
				<td>&nbsp;<span class="name">{{ $notice['author'] }}</span></td>
				<td style="text-align:right">{{ $notice['datetime'] }}&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" style="border-top: 1px solid #552780;">{{ $notice['content'] }}</td>
			</tr>
		</tbody>
	</table>
	@endforeach

	<br>
	<div style="clear:both"></div>
</div>