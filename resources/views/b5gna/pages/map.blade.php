@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/map">Mapa vesmíru</a></div>
@else
<div class="mainTitle">Mapa vesmíru</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-map')
    @endif

<script language="JavaScript">

var sector='';
var timerDetail=0;

function timerExec(){
  if (sector!='') $('#sectDetail').load('map-info.php?sector='+sector);
}

function showInfo(sect){
  sector=sect;
  if (timerDetail!=0) clearTimeout(timerDetail);  
  timerDetail=setTimeout("timerExec()", 100);
}

</script>

    <br>
    <div id="sectDetail" class="center">
        <div style="padding-top:25px;">Umiestnením kurzora myši nad sektor sa zobrazia základné informácie o sektore.</div>
    </div>
    <br>
    <img src="/images/b5gna/map/map.png" usemap="#map">
    <map name="map" id="map">
        {{-- <area alt="" shape="poly" coords="704,109,709,127,709,159,690,159,690,149,666,149,666,126,670,126,670,109" href="/sector/ear" onMouseOver="showInfo('ear')"> --}}
        <area alt="" shape="poly" coords="350,150,450,150,450,250,350,250" href="/sector/ear" onMouseOver="showInfo('ear')">
    </map><br>

    <div style="clear:both"></div>
</div>