@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/rules">Pravidlá hry</a></div>
@else
<div class="mainTitle">Pravidlá hry</div>
@endif
<div class="mainTitleFade"></div>

<div class="content" style="text-align: center;">

	<div class="narrowBox" align="justify">

		<p>
			Vítaj v hre Babylon 5 Game: New Age. Pri hre nie je dôležité vyhrať. Oveľa dôležitejšie je
			sa zabaviť, prežiť trošku napätia, radosti, pozitívneho súperenia, potrápiť svoju hlavu, zdokonaliť sa,
			ale aj spoznať nových ľudí. Z hry by mal človek odchádzať vnútorne uvoľnený (bez ohľadu na výsledok).
			Aby sme mohli dosiahnúť práve spomenuté ciele, musíme si určiť pravidlá. Pravidlá by mali byť pre
			hráčov a nie proti nim. Preto Ťa prosíme o ich rešpektovanie. Avšak pravidlá nie sú všetko.
			Zalezí na každom jednom učastníkovi, čo so sebou do hry prinesie a či sa rozhodne druhých
			svojou prítomnosťou obohatiť alebo im uškodiť.
		</p><br>

		<div class="window" style="font-size:12px;">
			<div class="windowCaption">Základné pravidlá</div>
			<div align="left" style="margin:10px;">
				<ol>
					<li>Rešpektuj svojho spoluhráča aj protihráča (bez neho by si nemal s kým hrať). Správaj sa k nim slušne, neurážaj ich.</li><br><br>
					<li>Za vzor svojho správania k ostatným si ber predstavu o tom, ako by si chcel, aby sa ostatní správali k Tebe.</li><br><br>
					<li>V hre môžeš byť registrovaný len jeden krát. Nie je možné vytvárať viac účtov.</li><br><br>
					<li>Vytváranie účtov akýmkoľvek spôsobom a kýmkoľvek za rovnakým účelom a využitím ako je vytvorenie si vlastného multiúčtu sa považuje tiež za multiúčet, a preto je zakázané.</li><br><br>
					<li>Každý užívateľ môže hrať len sám za seba. Preto neposkytuj svoje heslo nikomu inému. Hrať cez Tvoje konto nemôže iná osoba ani s Tvojim súhlasom.</li><br><br>
					<li>Buď čestný! Nikto nie je dokonalý, ani tvorcovia tejto hry. Hra určite obsahuje chyby. Ak na niektorú natrafíš, ohlás ju administrátorovi. Zneužitím kazíš hru ostatným!</li><br><br>
					<li>Úmyselne nepoškodzuj hru ani jej hráčov.</li><br><br>
				</ol>
			</div>
		</div><br>

		<div class="window" style="font-size:12px;">
			<div class="windowCaption">Pravidlá komunikácie</div>
			<div align="left" style="margin:10px;">
				<p>
					Hra umožňuje na mnohých miestach vkladať textovú informáciu. Rešpektuj slobodu názoru. Avšak každá sloboda končí tam, kde začína sloboda druhej osoby.
					Snaž sa svoje názory prezentovať v priateľskej atmosfére. Tu je zoznam toho, čo by Tvoje vyjadrenia nemali obsahovať:
				</p>
				<ol>
					<li>Vulgarizmy, urážky, ponižovanie iného človeka.</li><br><br>
					<li>Povzbudzovanie k násiliu, agresivite a trestným činom.</li><br><br>
					<li>Netolerantné a hanlivé vyjadrenia k inej národnosti, kultúre a náboženskému vyznaniu.</li><br><br>
					<li>Šírenie nelegálneho softvéru, autorských diel a pornografie, vrátane odkazov na takýto obsah.</li><br><br>
					<li>Reklamu bez povolenia administrátora.</li><br><br>
				</ol>
			</div>
		</div>
		<br>

		<div class="window" style="font-size:12px;">
			<div class="windowCaption">Pravidlá administrátorov</div>
			<div align="left" style="margin:10px;">
				<ol>
					<li>Administrátori sú tu pre hráčov. Zabezpečujú chod hry. Sú to takí istí ľudia ako Ty. Majú svoje chyby, preto sa môžu mýliť. Prosíme o trpezlivosť.</li><br><br>
					<li>Administrátor musí byť nestranný, nesmie sa zapájať do hry ako hráč.</li><br><br>
					<li>Administrátor musí byť spravodlivý voči všetkým.</li><br><br>
					<li>Porušenie pravidiel sa v prvom rade snaží riešiť dohovorom. Ak stroskotá alebo sa porušenie opakuje, môže podľa uváženia udeliť trest. V najhoršom prípade zakázať prístup do hry.</li><br><br>
					<li>Voči rozhodnutiu administrátora je možne vznieť námietku. Avšak ak administrátor svoje rozhodnutie napriek tomu nezmení, stáva sa konečným a nemenným. Je nutné sa podriadiť.</li><br><br>
					<li>Administrátor vykonáva svoju službu s radosťou a tiež sa chce určitým spôsobom zabaviť ako každý účastník hry. Avšak nemôže svoje právomoci zneužiť.</li><br><br>
				</ol>
			</div>
		</div>
		<br>


		<div class="window" style="font-size:12px;">
			<div class="windowCaption">Informácie o právach a vlastníctve</div>
			<div align="left" style="margin:10px;">
				<ol>
					<li>Registrácia do hry a jej používanie je bezplatné. V hre nie je možné zakúpiť žiadne platené bonusy.</li><br><br>
					<li>Registráciou užívateľovi nevznikajú žiadne práva ani nároky.</li><br><br>
					<li>Hra a jej obsah (vrátane obsahu databázy) je vlastníctvom prevádzkovateľa.</li><br><br>
					<li>Všetky informácie vložené užívateľom alebo získané jeho prístupom na stránku hry môžu byť použité na kontrolu dodržiavania pravidiel hry.</li><br><br>
					<li>Akékoľvek informácie o užívateľovi osobného charakteru nebudú použité mimo hru.</li><br><br>
				</ol>
			</div>
		</div>
		<br>


		<div class="window" style="font-size:12px;">
			<div class="windowCaption">Záver</div>
			<div align="left" style="margin:10px;">
				<ol>
					<li>Administrátor môže podľa svojho uváženia udeliť výnimku z pravidiel.</li><br><br>
					<li>Tieto pravidlá sa môžu kedykoľvek meniť a dopĺnať. Zmena bude oznámená vo fóre hry a/alebo v sekcii changelog.</li><br><br>
					<li>Ak s pravidlami nesúhlasíš, nemôžeš za do hry zapojiť. Ak už maš vytvorené užívateľské konto, požiadaj administrátora o jeho zrušenie.</li><br><br>
					<li>Tak a si na konci! :) Vďaka, že si sa dočítal až sem. Prajeme veľa úspechov v hre.</li><br><br>
				</ol>
			</div>
		</div>
		<br>


		<div class="window" style="font-size:12px;">
			<div class="windowCaption">Rozšírené pravidlá</div>
			<div align="left" style="margin:10px;">

				Na tomto mieste nájdeš odkazy na rozšírené pravidlá, ktoré popisujú špeciálne situácie a postupy pri nich:<br><br>

				<a href="/rules-ip">Pravidlá hry z rovnakej IP adresy alebo počítača</a><br><br>
				<a href="/rules-citizenship">Pravidlá súvisiace s občianstvom a štátmi</a><br><br>
				<a href="/rules-age">Pravidlá organizácie veku a podmienky víťazstva</a><br><br>
				<a href="/rules-ambassador">Pravidlá hráčskeho ambasádora</a><br><br>
				<a href="/rules-crystals">Pravidlá TM kryštálov</a><br><br>

			</div>
		</div>

	</div>
	<div style="clear:both"></div>
</div>
