@php
$groups = [
    1 => ['name' => 'Space Force One'],
    2 => ['name' => 'Space Force Two'],
];

$strategies = [
    '1;2;3;4' => 'S, K, L, D',
    '1;2;4;3' => 'S, K, D, L',
    '1;3;2;4' => 'S, L, K, D',
    '1;3;4;2' => 'S, L, D, K',
    '1;4;2;3' => 'S, D, K, L',
    '1;4;3;2' => 'S, D, L, K',
    '2;1;3;4' => 'K, S, L, D',
    '2;1;4;3' => 'K, S, D, L',
    '2;3;1;4' => 'K, L, S, D',
    '2;3;4;1' => 'K, L, D, S',
    '2;4;1;3' => 'K, D, S, L',
    '2;4;3;1' => 'K, D, L, S',
    '3;1;2;4' => 'L, S, K, D',
    '3;1;4;2' => 'L, S, D, K',
    '3;2;1;4' => 'L, K, S, D',
    '3;2;4;1' => 'L, K, D, S',
    '3;4;1;2' => 'L, D, S, K',
    '3;4;2;1' => 'L, D, K, S',
    '4;1;2;3' => 'D, S, K, L',
    '4;1;3;2' => 'D, S, L, K',
    '4;2;1;3' => 'D, K, S, L',
    '4;2;3;1' => 'D, K, L, S',
    '4;3;1;2' => 'D, L, S, K',
    '4;3;2;1' => 'D, L, K, S',
];

@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/groups">Správa bojových skupín</a></div>
@else
<div class="mainTitle">Správa bojových skupín</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-move')
    @endif

@if (!empty($groups))
    <br><br>
    <form action="" method="post" id="grp702">
        <table width="90%" cellspacing="0" class="Tab center"><tbody>
            <tr>
                <th>Sektor</th>
                <th colspan="2">Skupina</th>
                <th>Stav</th>
                <th>Obsah</th>
            </tr>
            <tr>
                <td style="background-color: #6984f9; vertical-align:middle;"><a href="../game/map_sector.php?sector=ear" style="color:white"><b>ear</b></a></td>
                <td colspan="2" class="name" style="color: #6984f9"><b>1</b></td>
                <td style="color: #669999">v pokoji</td>
                <td rowspan="5" style="text-align:left;">[K] [<span style="color: #00ff00">100%</span>] <span style="color: #6984f9">Hermes</span>: <b>1</b><br></td>
            </tr>
            <tr>
                <th width="110">Stíhač</th>
                <th width="110">Krížnik</th>
                <th width="110">Loď</th>
                <th width="110">Destroyer</th>
            </tr>
            <tr class="TabH1" style="color:lime;">
                <td><b>0/1</b></td>
                <td><b>1</b></td>
                <td><b>0</b></td>
                <td><b>0</b></td>
            </tr>
            <tr>
                <th>Stíhač - cieľ</th>
                <th>Krížnik - cieľ</th>
                <th>Loď - cieľ</th>
                <th>Destroyer - cieľ</th>
            </tr>
            <tr>
                <td><select name="targetG[702][1]"><option value=""></option>
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach
                </select></td>
                <td><select name="targetG[702][2]"><option value=""></option>
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach
                </select></td>
                <td><select name="targetG[702][3]"><option value=""></option>
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach
                </select></td>
                <td><select name="targetG[702][4]"><option value=""></option>
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach
                </select></td>
            </tr>
            <tr class="TabH2">
                <td colspan="4"><input type="submit" name="SendT" value="Odoslať zmeny"></td>
                <td><input type="button" value="Zobraziť lode" onclick="ToggleDIV('gtable702')"></td>
            </tr>
        </tbody></table>

        <table width="90%" cellspacing="0" class="Tab center" id="gtable702" style="border-top: 5px solid #ffcc00; display: none;">
            <tr>
                <th width="140">Model<br>[trieda]</th>
                <th>Počet</th>
                <th>Názov</th>
                <th width="50">HP</th>
                <th>Príprava<br>skoku</th>
                <th width="90">Nosnosť<br>stíhačov</th>
                <th width="70">Presnosť</th>
                <th width="90">Stratégia</th>
                <th width="50">Vyradiť</th>
            </tr>
            <tr>
                <td style="color: #6984f9;">Hermes<br><span style="color: white">[Krížnik]</span></td>
                <td style="color:gray;">1</td>
                <td style="color: #6984f9">1</td>
                <td style="color: #00ff00">100%</td>
                <td><span id="ship4036" title="24 Nov - 04:12"></span><script language="JavaScript">CountDown('ship4036',5522)</script></td>
                <td>1</td>
                <td>50%</td>
                <td><select name="target[4036]">
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach                    
                </select></td>
                <td><input type="checkbox" name="remove[4036]" value="true"></td>
            </tr>
            <tr>
                <td colspan="9" class="TabH2">
                    <input type="button" value="Označiť všetky" onClick="SetAll('grp702')">
                    <input type="submit" name="SendRemove" value="Vyradiť">
                    <input type="submit" name="SendS" value="Zmeniť stratégiu">
                </td>
            </tr>
        </table>
    </form>
@endif

    <br><br><br>
    <form action="" method="post" id="free">
        <table width="85%" cellspacing="0" class="Tab center">
            <tr>
                <td colspan="10" class="TabH2">
                    <h2>Nezaradené lode a letky</h2>
                </td>
            </tr>
            <tr>
                <th>Sektor</th>
                <th>Model<br>[trieda]</th>
                <th>Počet</th>
                <th>Názov</th>
                <th>HP</th>
                <th>Príprava<br>skoku</th>
                <th>Nosnosť<br>stíhačov</th>
                <th>Presnosť</th>
                <th>Stratégia</th>
                <th>Zaradiť</th>
            </tr>
            <tr>
                <td style="background-color: #6984f9; vertical-align:middle;"><a href="../game/map_sector.php?sector=ear" style="color:white"><b>ear</b></a></td>
                <td style="color: #6984f9;">Starfury<br><span style="color: white">[Stíhač]</span></td>
                <td><b>1</b></td>
                <td style="color: #6984f9">Letka 960</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>50%</td>
                <td><select name="targetF[960]">
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach
                </select></td>
                <td><input type="checkbox" name="insertF[960]" value="true"></td>
            </tr>
            <tr>
                <td style="background-color: #6984f9; vertical-align:middle;"><a href="../game/map_sector.php?sector=ear" style="color:white"><b>ear</b></a></td>
                <td style="color:#6984f9;">Hermes<br><span style="color: white">[Krížnik]</span></td>
                <td style="color:gray;">1</td>
                <td style="color: #6984f9">1</td>
                <td style="color: #00ff00">100%</td>
                <td><span id="ship4036" title="24 Nov - 04:12"><font color="red">1<span style="font-size:80%">h </span>44<span style="font-size:80%">m </span>56<span style="font-size:80%">s</span></font></span><script language="JavaScript">CountDown('ship4036',6356)</script></td>
                <td>1</td>
                <td>50%</td>
                <td><select name="target[4036]">
                    @foreach ($strategies as $value => $strategy)
                    <option value="{{ $value }}">{{ $strategy }}</option>
                    @endforeach
                </select></td>
                <td><input type="checkbox" name="insert[4036]" value="true" class="checkbox"></td>
            </tr>
            <tr class="TabH2">
                <td colspan="10">Zaradiť do:
                    <select name="insgrp" style="width:230">
    @foreach ($groups as $id => $group)
                        <option value="{{ $id }}">{{ $group['name'] }} ()</option>
    @endforeach
                    </select>
                    <input type="button" value="Označiť všetky" onClick="SetAll('free')">
                    <input type="submit" name="SendInsert" value="Zaradiť">
                    <input type="submit" name="SendS" value="Zmeniť stratégiu">
                </td>
            </tr>
    </form>
    </table><br><br>


    <form action="" method="post">
        <table width="45%" cellspacing="0" class="Tab center">
            <tr>
                <td colspan="2" class="TabH2">
                    <h2>Vytvoriť novú bojovú skupinu</h2>
                </td>
            </tr>
            <td><input type="text" name="New" size="35"></td>
            <td><input type="submit" value="Vytvoriť"></td>
            </tr>
        </table>
    </form><br>


@if (!empty($groups))
    <form action="" method="post">
        <table width="45%" cellspacing="0" class="Tab center">
            <tr>
                <td colspan="2" class="TabH2">
                    <h2>Zrušiť skupinu</h2>
                </td>
            </tr>
            <tr>
                <td><select name="dismiss" style="width:230">
    @foreach ($groups as $id => $group)
                    <option value="{{ $id }}">{{ $group['name'] }}</option>
    @endforeach
                </select></td>
                <td><input type="submit" value="Odoslať"></td>
            </tr>
    </form>
    </table><br>
@endif

    <div style="clear:both"></div>
</div>