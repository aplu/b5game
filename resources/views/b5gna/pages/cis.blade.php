@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/cis">Centrálny informačný systém</a></div>
@else
<div class="mainTitle">Centrálny informačný systém</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

	<br>
	&laquo; <a href="?kateg=" style="color:white">Nové informácie</a> &raquo;
	&laquo; <a href="?kateg=admin" style="color:#ff8080">Admin</a> &raquo;
	&laquo; <a href="?kateg=recalc" style="color:#40ff70">Prepočet</a> &raquo;
	&laquo; <a href="?kateg=mining" style="color:#ddee40">Ťažba</a> &raquo;
	&laquo; <a href="?kateg=military" style="color:red">Flotila</a> &raquo;
	&laquo; <a href="?kateg=politic" style="color:#70baff">Politika</a> &raquo;
	&laquo; <a href="?kateg=gov" style="color:#9955FF">Vláda</a> &raquo;
	<br><br><br>

	<form action="" method="post" id="DelForm">
		<table width="85%" cellspacing="0" class="Tab center">
			<tr>
				<th width="18"></th>
				<th width="90">Čas</th>
				<th width="90">Kategória</th>
				<th>Správa</th>
				<th width="16"></th>
			</tr>

			<tr>
			    <td></td>
			    <td class="TabH1">02 Jan - 08:59</td>
			    <td style="color:red">flotila</td>
			    <td style="text-align:justify;"><b style="font-size:12px; color:red;">Našej letke došli zásoby kyslíka</b><br>Naša letka <b class="hl">Letka 123</b> sa stratila v sektore <b class="hl">b5</b> po zlyhaní životných funkcií pilotov.<br><br>Straty: 2x Starfury<br><br></td>
			    <td>
			        <a href="?kateg=military&amp;del=179327"><img src="../images/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[179327]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>
			<tr>
			    <td></td>
			    <td class="TabH1">06 Jan - 05:02</td>
			    <td style="color:red">flotila</td>
			    <td style="text-align:justify;"><b style="font-size:12px; color:red;">Našim stíhačom dochádza kyslík</b><br>V sektore <b class="hl">b5</b> je nedostatok miesta pre stíhače.<br><br><a href="../game/fleet_wings.php">Prejsť na správu letiek »</a><br><br></td>
			    <td>
			        <a href="?kateg=military&amp;del=184135"><img src="../images/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[184135]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>
			<tr>
			    <td><a href="?read=56789"><img src="/images/b5gna/mail_new.gif" width="16" height="12" alt=""></a></td>
			    <td class="TabH1">01 Jan - 01:23</td>
			    <td style="color:red">flotila</td>
			    <td style="text-align:justify;"><b style="font-size:12px; color:red;">Boli sme napadnutí</b><br>Na naše lode v sektore
			        <font color="red"><b>Earth</b></font> zaútočil/i štát/y <b><span style="color: #8a68a5">Minbarská Federácia</span></b><br><br><a href="/fight?id=1234">Bližšie informácie »</a><br><br></td>
			    <td>
			        <a href="?kateg=military&amp;del=184240"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[184240]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>			
			<tr>
				<td><a href="?read=45678"><img src="/images/b5gna/mail_new.gif" width="16" height="12" alt=""></a></td>
				<td class="TabH1">20 Dec - 22:15</td>
				<td style="color:red">flotila</td>
				<td style="text-align:justify;"><b style="font-size:12px; color:red;">Narušenie nášho územia</b><br>Sektor <b class="hl">b5</b> narušil hráč <b style="color:8a68a5">Alfred Bester</b>.<br><br><a href="/map?sector=s9">Zobraziť sektor &raquo;</a><br><br></td>
				<td>
					<a href="?kateg=&del=164228"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br>
					<input type="checkbox" name="Delete[164228]" value="true" class="checkbox" style="width:14px;">
				</td>
			</tr>
			<tr>
			    <td></td>
			    <td class="TabH1">16 Dec - 23:36</td>
			    <td style="color:red">flotila</td>
			    <td style="text-align:justify;"><b style="font-size:12px;">Skenovanie okolia sektora <font color="red">b5</font> bolo úspešné.</b><br><br><a href="/map?sector=b5">Otvoriť detail sektora »</a><br><br></td>
			    <td>
			        <a href="?kateg=military&amp;del=159172"><img src="../images/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[159172]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>			

			<tr>
			    <td></td>
			    <td class="TabH1">01 Jan - 06:06</td>
			    <td style="color:#70baff">politika</td>
			    <td style="text-align:justify;"><b style="font-size:12px; color:red;">Zbúranie štátnej stavby</b><br><b class="hl">Ťažobná stanica</b> v sektore <b class="hl">eps</b> bola zbúraná hráčom <b>Draal</b>.</td>
			    <td>
			        <a href="?kateg=politic&amp;del=184460"><img src="../images/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[184460]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>
			<tr>
			    <td><a href="?read=55555"><img src="/images/b5gna/mail_new.gif" width="16" height="12" alt=""></a></td>
			    <td class="TabH1">01 Jan - 05:55</td>
			    <td style="color:#70baff">politika</td>
			    <td style="text-align:justify;"><b style="font-size:12px; color:red;">Zbúranie štátnej stavby</b><br><b class="hl">Skoková brána</b> v sektore <b class="hl">b5</b> bola zbúraná hráčom <b>Johny</b>.</td>
			    <td>
			        <a href="?kateg=politic&amp;del=185528"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[185528]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>

			{{-- EA --}}
			<tr>
				<td><a href="?read=34567"><img src="/images/b5gna/mail_new.gif" width="16" height="12" alt=""></a></td>
				<td class="TabH1">20 Dec - 06:32</td>
				<td style="color:#70baff">politika</td>
				<td style="text-align:justify;"><b style="font-size:12px;">Bol si zvolený do funkcie:</b><br><br><b class="hl">poslanec parlamentu</b><br><br></td>
				<td>
					<a href="?kateg=&del=163373"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br>
					<input type="checkbox" name="Delete[163373]" value="true" class="checkbox" style="width:14px;">
				</td>
			</tr>

			{{-- EA --}}
			<tr>
			    <td><a href="?read=77777"><img src="/images/b5gna/mail_new.gif" width="16" height="12" alt=""></a></td>
			    <td class="TabH1">17 Dec - 09:38</td>
			    <td style="color:#70baff">politika</td>
			    <td style="text-align:justify;"><b style="font-size:12px;">Bol si zvolený za nového predsedu strany "<b class="hl">Sheridans</b>".</b>
			    </td>
			    <td>
			        <a href="?kateg=politic&amp;del=159624"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[159624]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>

			{{-- EA --}}
			<tr>
				<td><a href="?read=23456"><img src="/images/b5gna/mail_new.gif" width="16" height="12" alt=""></a></td>
				<td class="TabH1">27 Oct - 10:09</td>
				<td style="color:#70baff">politika</td>
				<td style="text-align:justify;"><b style="font-size:12px;">Tvoja žiadosť o vstup do strany "<b class="hl">Sheridans</b>" bola akceptovaná.</b>
				</td>
				<td>
					<a href="?kateg=&amp;del=70889"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br>
					<input type="checkbox" name="Delete[70889]" value="true" class="checkbox" style="width:14px;">
				</td>
			</tr>

			{{-- MF --}}
			<tr>
				<td><a href="?read=166034"><img src="../images/mail_new.gif" width="16" height="12" alt=""></a></td>
				<td class="TabH1">22 Dec - 15:34</td><td style="color:#70baff">politika</td>
				<td style="text-align:justify;"><b class="hl" style="font-size:12px;">Prvý rozpustil Šedý koncil.</b><br></td>
				<td>
					<a href="?kateg=&amp;del=166034"><img src="../images/delete.gif" width="12" height="12" alt=""></a><br><br>
					<input type="checkbox" name="Delete[166034]" id="cb" value="true" class="checkbox" style="width:14px;">
				</td>
			</tr>

			{{-- EA, MF... ? --}}
			<tr>
				<td><a href="?read=12345"><img src="/images/b5gna/mail_new.gif" width="16" height="12" alt=""></a></td>
				<td class="TabH1">05 Sep - 22:42</td>
				<td style="color:#70baff">politika</td>
				<td style="text-align:justify;"><b class="hl" style="font-size:12px;">Začali sa nové voľby.</b><br></td>
				<td>
					<a href="?kateg=politic&amp;del=392"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br>
					<input type="checkbox" name="Delete[392]" value="true" class="checkbox" style="width:14px;">
				</td>
			</tr>

			<tr>
				<td></td>
				<td class="TabH1">26 Oct - 20:00</td>
				<td style="color:#40ff70">prepočet</td>
				<td style="text-align:justify;"><b style="font-size:12px;">Výsledok prepočtu</b><br><br>Rafinéria [<b class="hl">666 ks</b>]:
					<font color="red">55'555</font> Q40<br>Taviaca pec [<b class="hl">123 ks</b>]:
					<font color="red">1'111'000</font> Ti<br><br></td>
				<td>
					<a href="?kateg=&del=69744"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br>
					<input type="checkbox" name="Delete[69744]" value="true" class="checkbox" style="width:14px;">
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="TabH1">26 Oct - 20:00</td>
				<td style="color:#40ff70">prepočet</td>
				<td style="text-align:justify;"><b style="font-size:12px;">Zaplatené dane</b><br><br>Kredity:
					<font color="red">888'888'888'888'888</font><br>Titánium:
					<font color="red">777'777'777</font><br>Quantium 40:
					<font color="red">666'666'666</font><br><br></td>
				<td>
					<a href="?kateg=&del=69791"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br>
					<input type="checkbox" name="Delete[69791]" value="true" class="checkbox" style="width:14px;">
				</td>
			</tr>

			<tr>
			    <td></td>
			    <td class="TabH1">02 Jan - 08:30</td>
			    <td style="color:#ddee40">ťažba</td>
			    <td style="text-align:justify;"><b style="font-size:12px; color:red;">V sektore b5 došli zásoby surovín!</b><br>Naša ťažba nemá čo ťažiť.<br><br><a href="../game/map_sector.php?sector=alt">Otvoriť detail sektora »</a><br><br></td>
			    <td>
			        <a href="?kateg=mining&amp;del=179288"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[179288]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>
			<tr>
			    <td></td>
			    <td class="TabH1">02 Jan - 08:30</td>
			    <td style="color:#ddee40">ťažba</td>
			    <td style="text-align:justify;"><b style="font-size:12px;">Výsledok ťažby skupiny <font color="red">Business Intelligence</font> v sektore <font color="red">b5</font></b><br>Čas doťaženia: <b class="hl">02 Jan 2274 - 08:30:45</b><br><br><br></td>
			    <td>
			        <a href="?kateg=mining&amp;del=179287"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[179287]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>

			<tr>
			    <td></td>
			    <td class="TabH1">08 Jan - 00:59</td>
			    <td style="color:#ddee40">ťažba</td>
			    <td style="text-align:justify;"><b style="font-size:12px;">Výsledok ťažby skupiny <font color="red">Titans</font> v sektore <font color="red">b5</font></b><br>Čas doťaženia: <b class="hl">01 Jan 2224 - 14:00:00</b><br><br>Ťažobná loď [<b class="hl">50 ks</b>]:
			        <font color="red">107'467</font> rTi<br><br>
			        <font color="red">20% vyťažených surovín zhabal štát, ktorý je vlastníkom sektora!<br>30% vyťažených surovín sa stratilo pri pokuse utiecť kontrole!</font><br>
			        <font color="red">Ťažba bola stiahnutá predčasne!</font><br></td>
			    <td>
			        <a href="?kateg=mining&amp;del=186274"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[186274]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>
			<tr>
			    <td></td>
			    <td class="TabH1">07 Jan - 15:37</td>
			    <td style="color:#ddee40">ťažba</td>
			    <td style="text-align:justify;"><b style="font-size:12px;">Výsledok ťažby skupiny <font color="red">Quantums</font> v sektore <font color="red">b5</font></b><br>Čas doťaženia: <b class="hl">08 Jan 2274 - 04:37:29</b><br><br>Tanker [<b class="hl">34 ks</b>]:
			        <font color="red">20'995</font> rQ40<br><br>
			        <font color="red">20% vyťažených surovín zhabal štát, ktorý je vlastníkom sektora!<br>30% vyťažených surovín sa stratilo pri pokuse utiecť kontrole!</font><br></td>
			    <td>
			        <a href="?kateg=mining&amp;del=185790"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[185790]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>
			<tr>
			    <td></td>
			    <td class="TabH1">08 Jan - 00:59</td>
			    <td style="color:#ddee40">ťažba</td>
			    <td style="text-align:justify;"><b style="font-size:12px;">Výsledok ťažby skupiny <font color="red">Minecrafters III</font> v sektore <font color="red">b5</font></b><br>Čas doťaženia: <b class="hl">01 Jan 2224 - 13:00:00</b><br><br>Obchodná loď [<b class="hl">26 ks</b>]:
			        <font color="red">8,464 mil</font> Cr<br><br>
			        <font color="red">Ťažba bola stiahnutá predčasne!</font><br></td>
			    <td>
			        <a href="?kateg=mining&amp;del=186278"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[186278]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>

			<tr>
			    <td></td>
			    <td class="TabH1">12 Oct - 20:00</td>
			    <td style="color:#ddee40">ťažba</td>
			    <td style="text-align:justify;"><b style="font-size:12px; color:red;">Počet ťažobných lodí presiahol štátny limit</b><br>Limit bol presiahnutý o 123ks lodí, ktoré boli vyradené z ťažby!<br><br><a href="../game/tazba_skupiny.php">Prejsť na správu ťažby »</a><br><br></td>
			    <td>
			        <a href="?kateg=mining&amp;del=189361"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[189361]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>

			<tr>
				<td></td>
				<td class="TabH1">11 Oct - 11:11</td>
				<td style="color:#ddee40">ťažba</td>
				<td style="text-align:justify;"><b style="font-size:12px;">Výsledok ťažby skupiny <font color="red">Minecrafters II</font > v sektore <font color="red">b5</font ></b><br>Čas doťaženia: <b class="hl">11 Oct 2273 - 11:11:11</b><br><br>Ťažobná loď [<b class="hl">333 ks</b>]:
					<font color="red">555'500</font> rTi<br>Tanker [<b class="hl">888 ks</b>]:
					<font color="red">1'111'438</font> rQ40<br>Obchodná loď [<b class="hl">444 ks</b>]:
					<font color="red">555,75 mil</font> Cr<br><br></td>
				<td>
					<a href="?kateg=&del=70267"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br>
					<input type="checkbox" name="Delete[70267]" value="true" class="checkbox" style="width:14px;">
				</td>
			</tr>
			<tr>
			    <td></td>
			    <td class="TabH1">11 Oct - 10:10</td>
			    <td style="color:#ddee40">ťažba</td>
			    <td style="text-align:justify;"><b style="font-size:12px;">Výsledok ťažby skupiny <font color="red">Minecrafters</font> v sektore <font color="red">b5</font></b><br>Čas doťaženia: <b class="hl">11 Oct 2273 - 10:10:10</b><br><br>Obchodná loď [<b class="hl">50 ks</b>]:
			        <font color="red">41,438 mil</font> Cr<br><br></td>
			    <td>
			        <a href="?kateg=mining&amp;del=189756"><img src="/images/b5gna/delete.gif" width="12" height="12" alt=""></a><br><br><input type="checkbox" name="Delete[189756]" id="cb" value="true" class="checkbox" style="width:14px;"></td>
			</tr>

		</table>
		<table width="85%" cellspacing="0" class="Tab center">
			<tr>
				<td class="TabH1"><input type="button" value="Označiť všetky" onClick="SetAll('DelForm')"> <input type="submit" name="SendDel" value="Zmazať označené správy"></td>
			</tr>
		</table>
	</form><br><br>


	<div class="window center" style="text-align:justify; font-size:10px; width:50%;">
		<div class="windowCaption">Nastavenie označovania prečítaných správ podľa kategórie</div>
		<div style="margin:3px;">
			<form action="" method="post">
				<table cellspacing="3">
					<tr>
						<td width="100" align="right" style="color:#ff8080">admin</td>
						<td>&nbsp;</td>
						<td><select name="Cadmin" style="width:270px;"><option value="0">označiť ručne</option><option value="1" >označiť po prvom zobrazení</option><option value="2" >bez upozornenia na novú správu</option></select></td>
					</tr>
					<tr>
						<td width="100" align="right" style="color:#40ff70">prepočet</td>
						<td>&nbsp;</td>
						<td><select name="Crecalc" style="width:270px;"><option value="0">označiť ručne</option><option value="1" selected>označiť po prvom zobrazení</option><option value="2" >bez upozornenia na novú správu</option></select></td>
					</tr>
					<tr>
						<td width="100" align="right" style="color:#ddee40">ťažba</td>
						<td>&nbsp;</td>
						<td><select name="Cmining" style="width:270px;"><option value="0">označiť ručne</option><option value="1" selected>označiť po prvom zobrazení</option><option value="2" >bez upozornenia na novú správu</option></select></td>
					</tr>
					<tr>
						<td width="100" align="right" style="color:red">flotila</td>
						<td>&nbsp;</td>
						<td><select name="Cmilitary" style="width:270px;"><option value="0">označiť ručne</option><option value="1" >označiť po prvom zobrazení</option><option value="2" >bez upozornenia na novú správu</option></select></td>
					</tr>
					<tr>
						<td width="100" align="right" style="color:#70baff">politika</td>
						<td>&nbsp;</td>
						<td><select name="Cpolitic" style="width:270px;"><option value="0">označiť ručne</option><option value="1" >označiť po prvom zobrazení</option><option value="2" >bez upozornenia na novú správu</option></select></td>
					</tr>
					<tr>
						<td width="100" align="right" style="color:#9955FF">vláda</td>
						<td>&nbsp;</td>
						<td><select name="Cgov" style="width:270px;"><option value="0">označiť ručne</option><option value="1" >označiť po prvom zobrazení</option><option value="2" >bez upozornenia na novú správu</option></select></td>
					</tr>
				</table>
				<div align="right"><input type="submit" name="SCfg" value="Uložiť nastavenia"></div>
			</form>
		</div>
	</div>
	<div style="clear:both"></div>
</div>