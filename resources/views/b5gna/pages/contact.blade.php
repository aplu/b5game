@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/contact">Kontakt</a></div>
@else
<div class="mainTitle">Kontakt</div>
@endif
<div class="mainTitleFade"></div>

<div class="content">

    <div class="narrowBox"><br>
        <div class="window">
            <div class="windowCaption">Poslať správu</div>
            <div style="margin:10px;">Máš problém s registráciou alebo s prihlásením do hry? Napíš nám! Svoj problém sa snaž popísať čo najpodrobnejšie.<br><br><br>
                <form action="" method="post">
                    @csrf
                    E-mail, kde Ťa môžeme kontaktovať:<br>
                    <input type="text" name="email" value="" maxlength="30" style="width: 300px"><br><br> Tvoj problém:<br>
                    <textarea rows="15" name="sendText" style="width:799px;"></textarea><br><br>
                    <input type="hidden" name="accessid" id="accessid" value="">
                    <div align="right"><input type="submit" name="Send" value="Odoslať problém"></div>
                </form>
            </div>
        </div>

    </div>

    <script language="JavaScript">
        $('#accessid').val('1');
    </script>

    <div style="clear:both"></div>
</div>