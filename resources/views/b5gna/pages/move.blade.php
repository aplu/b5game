@php
$me = 'Jeff';

if (rand(0, 1)) {
    $error = 'Nie je možné zadať presun skupiny s nedostatočnou nosnosťou stíhačov<br>cez sektor bez skokovej brány: Ragesh 3';
}

$groups = [
    123 => ['name' => 'Space Killers', 'sector' => 'min', 's' => 1, 'k' => 2, 'l' => 3, 'd' => 4, 'state' => 'v pokoji', 'plan' => null],
    124 => ['name' => 'Space Killers 2', 'sector' => 'min', 's' => 2, 'k' => 3, 'l' => 4, 'd' => 5, 'state' => 'v pokoji', 'plan' => 345],
];

$plans = [
    345 => ['name' => 'Letový plán č.345', 'sector' => 'ear', 'routes' => ['s1', 'pro']],
];
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/move">Presun bojových skupín</a></div>
@else
<div class="mainTitle">Presun bojových skupín</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    @if (!($debug['all'] ?? false))
        @include('b5gna.menu.dummy-move')
    @endif

    @if ($error ?? false)
    <br><b style="color:red;">{!! $error !!}</b><br>
    @endif

    <br>
    <form action="/move" method="POST"><br>
        <table width="80%" cellspacing="0" class="Tab center">
            <tr>
                <td colspan="7">
                    <h2>Bojové skupiny</h2>
                </td>
            </tr>
            <tr>
                <th>Skupina</th>
                <th>Vlastník</th>
                <th>Sektor</th>
                <th>Obsah<br>S/K/L/D</th>
                <th>Stav</th>
                <th>Príprava<br>na skok</th>
                <th>Letový plán</th>
            </tr>
        @foreach ($groups as $g)
            <tr>
                <td style="color: #6984f9"><b>{{ $g['name'] }}</b></td>
                <td style="color: #6984f9">{{ $me }}</td>
                <td><a href="/sector/{{ $g['sector'] }}">{{ $g['sector'] }}</a></td>
                <td><b>{{ $g['s'] }}</b> / <b>{{ $g['k'] }}</b> / <b>{{ $g['l'] }}</b> / <b>{{ $g['d'] }}</b></td>
                <td style="color: #669999">{{ $g['state'] }}</td>
                <td><span id="grp702" title="24 Nov - 04:12"></span><script language="JavaScript">CountDown('grp702',-81152)</script></td>
                <td><select name="SetPlan[702]" style="width:200px;">
                    <option value="">--- vyber letový plán ---</option>
                </select></td>
            </tr>
        @endforeach
            <tr>
                <td colspan="7" class="TabH1"><input type="submit" value="Priradiť plán"></td>
            </tr>
    </form>
    </table><br><br><br>


    <h1>Letové plány</h1><br>
    <form action="/move/plan" method="POST">
        @csrf
        <input type="submit" value="Vytvoriť letový plán">
    </form><br><br>

    <form action="/game/fleet_transfer.php?planID=2048" method="post"><table width="80%" cellspacing="0" class="Tab center"><tbody><tr><th>Meno</th><th>Štartovací sektor</th><th width="120">Stav</th><th width="230">Príkaz</th></tr><tr><td><b><font color="lime">Letový plán č.2048</font></b></td><td><a href="../game/map_sector.php?sector=ear">ear</a></td><td style="color: #ffd050;">voľný</td><td><a href="plan.php?edit=2048">Zmeniť</a> *** <a href="/game/fleet_transfer.php?delete=2048">Zmazať</a></td></tr><tr><th colspan="2">Cesta</th><th colspan="2">Skupiny</th></tr><tr><td colspan="2">ear -&gt; <a href="../game/map_sector.php?sector=mar">mar</a></td><td colspan="2"><span style="color: #6984f9"><b>Group 1</b></span> - <a href="/game/fleet_transfer.php?cancel=721">Vyradiť</a><br></td></tr><tr><th colspan="4">Odštartovať let</th></tr><tr><td colspan="4"><input type="radio" name="Day" value="0" checked=""> dnes <input type="radio" name="Day" value="1"> zajtra &nbsp;&nbsp;&nbsp;&nbsp;čas <input type="text" name="When" value="10:58" maxlength="5" style="width: 60px;"> &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Start" value="Odštartovať"></td></tr></tbody></table></form>

    <br><br>
    <div style="clear:both"></div>
</div>