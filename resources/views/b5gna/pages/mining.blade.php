@php
$freeMiningGroups = [
    123 => ['name' => 'SpaceMiners', 'tiShips' => 50, 'q40Ships' => 30, 'crShips' => 20, 'platforms' => 2],
];

$miningSectors = [
    'cen' => ['tiQ' => '75+', 'q40Q' => '50+', 'crQ' => '35',  'free' => 1515],
    'ear' => ['tiQ' => '75+', 'q40Q' => '75+', 'crQ' => '75+', 'free' => 2345],
    'min' => ['tiQ' => '75',  'q40Q' => '78',  'crQ' => '78+', 'free' => 3456],
    'nar' => ['tiQ' => '75',  'q40Q' => '85',  'crQ' => '85+', 'free' => 543],
];


$error = request('error') ?? 'Ťažobná skupina SpaceMiners: v domovskom sektore štátu nemôže ťažiť viac ako 200 lodí!';
@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/mining">Správa ťažby</a></div>
@else
<div class="mainTitle">Správa ťažby</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content">

    <div style="text-align:center;"><img src="/images/b5gna/mining_title.jpg" width="910" height="203" alt=""></div>
    <div style="margin: 10px 10px 10px 10px;">
        <form action="" method="post">
            <table class="Tab2" style="width: 100%">
                <tr>
                    <td colspan="8" class="TabH2">
                        <h2>Ťažiace skupiny</h2>
                    </td>
                </tr>
                <tr>
                    <th>Sektor<br>[ti/q40/cr]</th>
                    <th>Vzdialenosť<br>Dĺžka cyklu</th>
                    <th>Názov skupiny</th>
                    <th>Ochranná letka<br>Stratégia</th>
                    <th>Ťažba</th>
                    <th>Očakávaný<br>výnos</th>
                    <th>Doťaží za</th>
                    <th>Prerušiť</th>
                </tr>
                <tr style="border-bottom: 3px solid Black;">
                    <td rowspan="2" style="background-color: #6984f9;">
                        <a href="/sector/ear" style="color:white"><b>ear</b></a><br>
                        [99+/99+/99]
                    </td>
                    <td rowspan="2">
                        <b>0</b> <span class="smallText">skokov</span><br>
                        <b class="hl" style="font-size: 12px;">12</b>
                        <span class="smallText"> hod.</span>
                    </td>
                    <td>Minecrafters</td>
                    <td><font color="red">bez letky</font></td>
                    <td style="font-size: 10px;">
                        <div class="resLabel" style="text-align: right; float: left; padding-right:5px;">ťažobné lode<br>tankery<br>obchodné lode<br>plošiny</div>
                        <div style="text-align: left; float: left; padding-right:5px;">
                            <b class="hl">999</b> ks<br>
                            <b class="hl">999</b> ks<br>
                            <b class="hl">999</b> ks<br><b>0</b> ks
                        </div>
                    </td>
                    <td style="font-size: 10px;">
                        <div style="text-align: left; float:right;">rTi<br>rQ40<br>Cr<br>&nbsp;</div>
                        <div style="text-align: right; float:right; padding-right:5px;">
                            999'999'999<br>
                            999'999,9<br>
                            999'999'999<br>
                            &nbsp;
                        </div>
                    </td>
                    <td><span id="t1486" title="25 Oct - 01:47"></span>
                        <script language="JavaScript">
                            CountDown('t1486',34038)
                        </script>
                    </td>
                    <td><input type="checkbox" name="cancel[1486]" value="true"></td>
                </tr>
                <tr style="border-bottom: 3px solid Black;">
                    <td>MineCrafters2</td>
                    <td><font color="red">bez letky</font></td>
                    <td style="font-size: 10px;">
                        <div class="resLabel" style="text-align: right; float: left; padding-right:5px;">ťažobné lode<br>tankery<br>obchodné lode<br>plošiny</div>
                        <div style="text-align: left; float: left; padding-right:5px;">
                            <b>0</b> ks<br>
                            <b>0</b> ks<br>
                            <b class="hl">3</b> ks<br>
                            <b>0</b> ks
                        </div>
                    </td>
                    <td style="font-size: 10px;">
                        <div style="text-align: left; float:right;">rTi<br>rQ40<br>Cr<br>&nbsp;</div>
                        <div style="text-align: right; float:right; padding-right:5px;">0<br>0<br>1'462'500<br>&nbsp;</div>
                    </td>
                    <td><span id="t1582" title="17 Nov - 13:09">
                        <script language="JavaScript">
                            CountDown('t1582',25709)
                        </script>
                    </td>
                    <td><input type="checkbox" name="cancel[1582]" value="true"></td>
                </tr>
                <tr style="border-bottom: 3px solid Black;">
                    <td rowspan="1" style="background-color: #6984f9;">
                        <a href="../game/map_sector.php?sector=s3" style="color:white"><b>xxx</b></a><br>[80+/50+/40]
                    </td>
                    <td rowspan="1">
                        <b>3</b> <span class="smallText">skoky</span><br>
                        <b class="hl" style="font-size: 12px;">13,5</b><span class="smallText"> hod.</span>
                    </td>
                    <td>SpaceSurvey</td>
                    <td><font color="red">bez letky</font></td>
                    <td style="font-size: 10px;">
                        <div class="resLabel" style="text-align: right; float: left; padding-right:5px;">ťažobné lode<br>tankery<br>obchodné lode<br>plošiny</div>
                        <div style="text-align: left; float: left; padding-right:5px;">
                            <b class="hl">50</b> ks<br>
                            <b>0</b> ks<br>
                            <b>0</b> ks<br>
                            <b class="hl">1</b> ks
                        </div>
                    </td>
                    <td style="font-size: 10px;">
                        <div style="text-align: left; float:right;">rTi<br>rQ40<br>Cr<br>&nbsp;</div>
                        <div style="text-align: right; float:right; padding-right:5px;">
                            185'250<br>
                            0<br>
                            0<br>&nbsp;
                        </div>
                    </td>
                    <td><span id="t1795" title="17 Nov - 19:30">
                        <script language="JavaScript">
                            CountDown('t1795',48588)
                        </script>
                    </td>
                    <td><input type="checkbox" name="cancel[1795]" value="true"></td>
                </tr>
                <tr style="border-bottom: 3px solid Black;"></tr>
                <tr class="TabH1">
                    <td colspan="8" style="text-align: right;"><input type="submit" name="cancelSend" value="Prerušiť označenú ťažbu"></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="sideBox">
        <div class="sidePanelTitle">Voľná ťažba</div>
        <div class="sidePanel smallText">
            <label class="sideLabel">Ťažba nezaradená v skupinách</label>
            <span class="colResDark">Ťažobné lode:</span> <b>0</b><br>
            <span class="colResDark">Tankery:</span> <b>0</b><br>
            <span class="colResDark">Obchodné lode:</span> <b>0</b><br>
            <span class="colResDark">Ťažobné plošiny:</span> <b>0</b>
        </div>
        <div class="sidePanelTitle">Hangáre</div>
        <div class="sidePanel smallText">
            <label class="sideLabel">Miesto v hangároch</label>
            <b class="hl" style="font-size:16px" title="voľné/celkovo">999/999</b><br>
            <label class="sideLabel">Ťažobné a obchodné lode</label>
            <span class="colResDark">Ťažobné lode:</span> <b title="ťažia">999</b><br>
            <span class="colResDark">Tankery:</span> <b title="ťažia">5</b><br>
            <span class="colResDark">Obchodné lode:</span> <b title="ťažia">999</b><br>
            <span class="colResDark">Ťažba spolu: </span><b>999</b><br>
            <label class="sideLabel">Stíhače</label>
            <span class="colResDark">Ochrana ťažby: </span><b>0</b><br>
            <span class="colResDark">Voľné v HW: </span><b>0</b><br>
        </div>
        <div class="sidePanelTitle">Produkcia</div>
        <div class="sidePanel smallText">
            <label class="sideLabel">Štátny limit ťažby</label>
            <b class="hl" style="font-size:16px" title="voľný/celkový">99999/99999</b><br>
            <label class="sideLabel">Limit obchodných lodí</label>
            <b class="hl" style="font-size:16px" title="voľný/celkový">999/999</b><br>
        </div>
    </div>
    <div class="mainContentBox">

        @if (!empty($error))
                <div class="errorBox">
                    <br>{{ $error }}
                </div>
        @endif

        <br>
        <form action="" method="post">
            <table class="Tab2" style="width: 100%">
                <tr>
                    <td colspan="6" class="TabH2">
                        <h2>Voľné skupiny</h2>
                    </td>
                </tr>
                <tr>
                    <th>Názov skupiny</th>
                    <th width="45">Ťažobné<br>lode</th>
                    <th width="45">Tankery</th>
                    <th width="45">Obchod.<br>lode</th>
                    <th width="45">Plošiny</th>
                    <th>Výber</th>
                </tr>
                <tr class="TabH1">
                    <td><input type="text" name="New" size="30"></td>
                    <td colspan="5" style="text-align: right;"><input type="submit" name="newSend" value="Vytvoriť novú skupinu"></td>
                </tr>

        @if (!empty($freeMiningGroups))
            @foreach ($freeMiningGroups as $id => $group)
                <tr>
                    <td>{{ $group['name'] }}</td>
                    <td><input type="text" name="poc[{{ $id }}][0]" value="{{ $group['tiShips'] }}" size="3"></td>
                    <td><input type="text" name="poc[{{ $id }}][2]" value="{{ $group['q40Ships'] }}" size="3"></td>
                    <td><input type="text" name="poc[{{ $id }}][1]" value="{{ $group['crShips'] }}" size="3"></td>
                    <td><input type="text" name="poc[{{ $id }}][3]" value="{{ $group['platforms'] }}" size="3"></td>
                    <td><input type="checkbox" name="select[{{ $id }}]" value="true"></td>
                </tr>
            @endforeach
                <tr class="TabH1">
                    <td colspan="6" style="text-align: right;">
                        <input type="submit" name="updateSend" value="Upraviť počet lodí v skupinách">
                        <input type="submit" name="dismissSend" value="Rozpustiť označené skupiny">
                    </td>
                </tr>
                <tr>
                    <th>Vyslať do sektora<br>[ti/q04/cr]: voľné miesto</th>
                    <th colspan="2">Priradiť ochrannú letku<br>prvej skupine</th>
                    <th colspan="3"></th>
                </tr>
                <tr class="TabH1">
                    <td>
                        <select name="sect" style="width:220px;">
                @foreach ($miningSectors as $idx => $s)
                            <option value="{{$idx}}">{{ "$idx [{$s['tiQ']}/{$s['q40Q']}/{$s['crQ']}]: {$s['free']}" }}</option>
                @endforeach
                        </select>
                    </td>
                    <td colspan="2">
                        <select name="wing">
                            <option value="NO" selected="">-- bez letky --</option>
                        </select>
                    </td>
                    <td colspan="3" style="text-align: right;">
                        <input type="submit" name="startSend" value="Vyslať označenú ťažbu">
                    </td>
                </tr>
        @endif
            </table>
        </form>
    </div>
    <div style="clear:both"></div>
</div>