@php
$src_types = [
    'race' => 'rasa',
    'state' => 'štát'
];

$show_details = isset($_GET['details']);

$ti_value = 580.5958;
$q40_value = 6967.1504;

@endphp

@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="{{ route('vehicle-types') }}">Parametre bojových lodí</a></div>
@else
<div class="mainTitle">Parametre bojových lodí</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

    <br>

    @foreach ($vehicle_classes as $class)
        <table width="{{ $show_details ? '175%' : '95%' }}" cellspacing="0" class="Tab2 center">
            <tr>
                <th>Trieda</th>
                <th>Level</th>
                <th>Meno</th>
                <th>Pôvod</th>
                <th>Titánium</th>
                <th>Q40</th>
                <th>Kredity</th>
                @if ($show_details)
                    <th>Cena lodi</th>
                @endif
                <th>Doba<br>výroby</th>
                @if ($show_details)
                    <th>za 24h<br>s 50 tov</th>
                    <th>Cena 24h</th>
                @endif
                <th>Kapacita<br>stíhačov</th>
                <th>HP</th>
                <th>Útok S</th>
                <th>Útok L</th>
                @if ($show_details)
                    <th>US+UL</th>
                    <th>HP+US+UL</th>
                    <th>Cena 24h<br>/ HP</th>
                    <th>Cena 24h<br>/ US</th>
                    <th>Cena 24h<br>/ UL</th>
                    <th>Cena 24h<br>/ US+UL</th>
                    <th>Cena 24h<br>/ HP+US+UL</th>
                @endif
            </tr>
            @foreach ($class->types as $type)
                @php
                    $amount_per_day = 2 * ($class->is_fighter ? 12 : 1) * 86400 / $type->time;
                    /** @todo hardcoded price per Ti & Q40 is based on mining indexes (all 110 & mining in hw only) */
                    $vehicle_price = ($type->ti * $ti_value) + ($type->q40 * $q40_value) + $type->cr;
                    $price_per_day = $amount_per_day * $vehicle_price;
                    $us_ul = $type->us + $type->ul;
                    $us_ul_hp = $us_ul + $type->max_hp;
                @endphp
                <tr style="color: {{ $type->src->color ?? 'white' }};">
                    <td><b>{{ $class->short }}</b></td>
                    <td class="TabH1"><b>{{ $type->lvl }}</b></td>
                    <td>{{ $type->name }}</td>
                    <td style="font-size:9px; text-align:left;">[{{ $src_types[$type->src_type] ?? '' }}] {{ $type->src->name ?? '' }} {{-- [rasa] Centauri --}}</td>
                    <td style="text-align:right;">{{ $type->ti > 0 ? amount($type->ti) : '–' }}</td>
                    <td style="text-align:right;">{{ $type->q40 > 0 ? amount($type->q40) : '–' }}</td>
                    <td style="text-align:right;">{{ $type->cr > 0 ? num($type->cr / 1e6, 2) . ' m' : '–' }}</td>
                    @if ($show_details)
                        <td style="text-align:right;">{{ amount($vehicle_price / 1e6) }} m</td>
                    @endif
                    <td style="text-align:right;">{{ num($type->time / 3600, 1) }} h</td>
                    @if ($show_details)
                        <td style="text-align:right;">{{ num($amount_per_day, 1) }}×</td>
                        <td style="text-align:right;">{{ amount($price_per_day / 1e6) }} m</td>
                    @endif
                    <td>{{ $class->is_fighter ? '–' : $type->slots }}</td>
                    <td style="text-align:right;">{{ amount($type->max_hp) }}</td>
                    <td style="text-align:right;">{{ amount($type->us) }}</td>
                    <td style="text-align:right;">{{ amount($type->ul) }}</td>
                    @if ($show_details)
                        <td style="text-align:right;">{{ amount($amount_per_day * $us_ul) }}</td>
                        <td style="text-align:right;">{{ amount($amount_per_day * $us_ul_hp) }}</td>
                        <td style="text-align:right;">{{ amount($price_per_day / $type->max_hp) }}</td>
                        <td style="text-align:right;">{{ $type->us > 0 ? amount($price_per_day / $type->us) : '∞'  }}</td>
                        <td style="text-align:right;">{{ $type->ul > 0 ? amount($price_per_day / $type->ul) : '∞'  }}</td>
                        <td style="text-align:right;">{{ $us_ul > 0 ? amount($price_per_day / $us_ul) : '∞' }}</td>
                        <td style="text-align:right;">{{ amount($price_per_day / $us_ul_hp) }}</td>
                    @endif
                </tr>
            @endforeach
        </table><br><br>
    @endforeach

    {{-- <table width="95%" cellspacing="0" class="Tab2 center">
        <tr>
            <th>Trieda</th>
            <th>Level</th>
            <th>Meno</th>
            <th>Pôvod</th>
            <th>Titánium</th>
            <th>Q40</th>
            <th>Kredity</th>
            <th>Doba<br>výroby</th>
            <th>Kapacita<br>stíhačov</th>
            <th>HP</th>
            <th>Útok S</th>
            <th>Útok L</th>
        </tr>
        <tr style="color: #dd9090;">
            <td><b>S</b></td>
            <td class="TabH1"><b>0</b></td>
            <td>Sentri</td>
            <td style="font-size:9px; text-align:left;">[rasa] Centauri</td>
            <td style="text-align:right;">53'565</td>
            <td style="text-align:right;">992</td>
            <td style="text-align:right;">22,32 mil</td>
            <td style="text-align:right;">4,3 hod</td>
            <td>0</td>
            <td style="text-align:right;">3'900</td>
            <td style="text-align:right;">5'630</td>
            <td style="text-align:right;">2'820</td>
        </tr>
        <tr style="color: #888811;">
            <td><b>S</b></td>
            <td class="TabH1"><b>0</b></td>
            <td>Drakh Raider</td>
            <td style="font-size:9px; text-align:left;">[rasa] Drakhovia</td>
            <td style="text-align:right;">83'992</td>
            <td style="text-align:right;">1'555</td>
            <td style="text-align:right;">35 mil</td>
            <td style="text-align:right;">6,8 hod</td>
            <td>0</td>
            <td style="text-align:right;">6'000</td>
            <td style="text-align:right;">9'000</td>
            <td style="text-align:right;">4'500</td>
        </tr>
        <tr style="color: #6984f9;">
            <td><b>S</b></td>
            <td class="TabH1"><b>0</b></td>
            <td>Starfury</td>
            <td style="font-size:9px; text-align:left;">[rasa] Ľudia</td>
            <td style="text-align:right;">58'661</td>
            <td style="text-align:right;">1'086</td>
            <td style="text-align:right;">24,44 mil</td>
            <td style="text-align:right;">4,7 hod</td>
            <td>0</td>
            <td style="text-align:right;">4'200</td>
            <td style="text-align:right;">6'190</td>
            <td style="text-align:right;">3'380</td>
        </tr>
        <tr style="color: #8a68a5;">
            <td><b>S</b></td>
            <td class="TabH1"><b>0</b></td>
            <td>Tishat</td>
            <td style="font-size:9px; text-align:left;">[rasa] Minbari</td>
            <td style="text-align:right;">63'611</td>
            <td style="text-align:right;">1'178</td>
            <td style="text-align:right;">26,5 mil</td>
            <td style="text-align:right;">5,1 hod</td>
            <td>0</td>
            <td style="text-align:right;">4'500</td>
            <td style="text-align:right;">6'980</td>
            <td style="text-align:right;">3'150</td>
        </tr>
        <tr style="color: #a03b37;">
            <td><b>S</b></td>
            <td class="TabH1"><b>0</b></td>
            <td>Gorith</td>
            <td style="font-size:9px; text-align:left;">[rasa] Narni</td>
            <td style="text-align:right;">54'160</td>
            <td style="text-align:right;">1'003</td>
            <td style="text-align:right;">22,57 mil</td>
            <td style="text-align:right;">4,4 hod</td>
            <td>0</td>
            <td style="text-align:right;">3'900</td>
            <td style="text-align:right;">5'740</td>
            <td style="text-align:right;">2'930</td>
        </tr>
        <tr style="color: #dd9090;">
            <td><b>S</b></td>
            <td class="TabH1"><b>5</b></td>
            <td>Rutharian Strike</td>
            <td style="font-size:9px; text-align:left;">[štát] Centaurská Republika</td>
            <td style="text-align:right;">37'164</td>
            <td style="text-align:right;">1'721</td>
            <td style="text-align:right;">10,32 mil</td>
            <td style="text-align:right;">5 hod</td>
            <td>0</td>
            <td style="text-align:right;">4'350</td>
            <td style="text-align:right;">3'380</td>
            <td style="text-align:right;">6'750</td>
        </tr>
        <tr style="color: #787811;">
            <td><b>S</b></td>
            <td class="TabH1"><b>5</b></td>
            <td>Drakh Heavy Raider</td>
            <td style="font-size:9px; text-align:left;">[štát] Kolónia Drakhov</td>
            <td style="text-align:right;">68'028</td>
            <td style="text-align:right;">3'149</td>
            <td style="text-align:right;">18,9 mil</td>
            <td style="text-align:right;">9,1 hod</td>
            <td>0</td>
            <td style="text-align:right;">8'000</td>
            <td style="text-align:right;">6'000</td>
            <td style="text-align:right;">12'350</td>
        </tr>
        <tr style="color: #6984f9;">
            <td><b>S</b></td>
            <td class="TabH1"><b>5</b></td>
            <td>Thunderbolt</td>
            <td style="font-size:9px; text-align:left;">[štát] Pozemská Aliancia</td>
            <td style="text-align:right;">40'865</td>
            <td style="text-align:right;">1'892</td>
            <td style="text-align:right;">11,35 mil</td>
            <td style="text-align:right;">5,5 hod</td>
            <td>0</td>
            <td style="text-align:right;">4'800</td>
            <td style="text-align:right;">3'600</td>
            <td style="text-align:right;">7'430</td>
        </tr>
        <tr style="color: #8a68a5;">
            <td><b>S</b></td>
            <td class="TabH1"><b>5</b></td>
            <td>Nial</td>
            <td style="font-size:9px; text-align:left;">[štát] Minbarská Federácia</td>
            <td style="text-align:right;">44'721</td>
            <td style="text-align:right;">2'070</td>
            <td style="text-align:right;">12,42 mil</td>
            <td style="text-align:right;">6 hod</td>
            <td>0</td>
            <td style="text-align:right;">5'250</td>
            <td style="text-align:right;">4'050</td>
            <td style="text-align:right;">8'100</td>
        </tr>
        <tr style="color: #a03b37;">
            <td><b>S</b></td>
            <td class="TabH1"><b>5</b></td>
            <td>Frazi</td>
            <td style="font-size:9px; text-align:left;">[štát] Narnský Režim</td>
            <td style="text-align:right;">34'119</td>
            <td style="text-align:right;">1'580</td>
            <td style="text-align:right;">9,48 mil</td>
            <td style="text-align:right;">4,6 hod</td>
            <td>0</td>
            <td style="text-align:right;">4'050</td>
            <td style="text-align:right;">3'150</td>
            <td style="text-align:right;">6'080</td>
        </tr>
    </table><br><br>
    <table width="95%" cellspacing="0" class="Tab2 center">
        <tr>
            <th>Trieda</th>
            <th>Level</th>
            <th>Meno</th>
            <th>Pôvod</th>
            <th>Titánium</th>
            <th>Q40</th>
            <th>Kredity</th>
            <th>Doba<br>výroby</th>
            <th>Kapacita<br>stíhačov</th>
            <th>HP</th>
            <th>Útok S</th>
            <th>Útok L</th>
        </tr>
        <tr style="color: #dd9090;">
            <td><b>K</b></td>
            <td class="TabH1"><b>0</b></td>
            <td>Covran Scout</td>
            <td style="font-size:9px; text-align:left;">[rasa] Centauri</td>
            <td style="text-align:right;">186'453</td>
            <td style="text-align:right;">6'215</td>
            <td style="text-align:right;">46,61 mil</td>
            <td style="text-align:right;">1,6 hod</td>
            <td>2</td>
            <td style="text-align:right;">15'000</td>
            <td style="text-align:right;">2'250</td>
            <td style="text-align:right;">7'430</td>
        </tr>
        <tr style="color: #888811;">
            <td><b>K</b></td>
            <td class="TabH1"><b>0</b></td>
            <td>Drakh Scout</td>
            <td style="font-size:9px; text-align:left;">[rasa] Drakhovia</td>
            <td style="text-align:right;">295'117</td>
            <td style="text-align:right;">9'837</td>
            <td style="text-align:right;">73,78 mil</td>
            <td style="text-align:right;">2,8 hod</td>
            <td>0</td>
            <td style="text-align:right;">24'000</td>
            <td style="text-align:right;">11'250</td>
            <td style="text-align:right;">13'050</td>
        </tr>
        <tr style="color: #6984f9;">
            <td><b>K</b></td>
            <td class="TabH1"><b>0</b></td>
            <td>Hermes</td>
            <td style="font-size:9px; text-align:left;">[rasa] Ľudia</td>
            <td style="text-align:right;">232'439</td>
            <td style="text-align:right;">7'748</td>
            <td style="text-align:right;">58,11 mil</td>
            <td style="text-align:right;">2,1 hod</td>
            <td>1</td>
            <td style="text-align:right;">18'600</td>
            <td style="text-align:right;">6'500</td>
            <td style="text-align:right;">9'800</td>
        </tr>
        <tr style="color: #8a68a5;">
            <td><b>K</b></td>
            <td class="TabH1"><b>0</b></td>
            <td>Torotha Assault</td>
            <td style="font-size:9px; text-align:left;">[rasa] Minbari</td>
            <td style="text-align:right;">225'695</td>
            <td style="text-align:right;">7'523</td>
            <td style="text-align:right;">56,42 mil</td>
            <td style="text-align:right;">2 hod</td>
            <td>1</td>
            <td style="text-align:right;">18'000</td>
            <td style="text-align:right;">6'080</td>
            <td style="text-align:right;">9'450</td>
        </tr>
        <tr style="color: #a03b37;">
            <td><b>K</b></td>
            <td class="TabH1"><b>0</b></td>
            <td>G'Karith</td>
            <td style="font-size:9px; text-align:left;">[rasa] Narni</td>
            <td style="text-align:right;">203'281</td>
            <td style="text-align:right;">6'776</td>
            <td style="text-align:right;">50,82 mil</td>
            <td style="text-align:right;">1,7 hod</td>
            <td>2</td>
            <td style="text-align:right;">16'500</td>
            <td style="text-align:right;">2'700</td>
            <td style="text-align:right;">8'100</td>
        </tr>
        <tr style="color: #;">
            <td><b>K</b></td>
            <td class="TabH1"><b>0</b></td>
            <td>Civil Transport</td>
            <td style="font-size:9px; text-align:left;">[štát] </td>
            <td style="text-align:right;">39'907</td>
            <td style="text-align:right;">2'494</td>
            <td style="text-align:right;">12,47 mil</td>
            <td style="text-align:right;">0,5 hod</td>
            <td>0</td>
            <td style="text-align:right;">6'000</td>
            <td style="text-align:right;">10</td>
            <td style="text-align:right;">10</td>
        </tr>
        <tr style="color: #dd9090;">
            <td><b>K</b></td>
            <td class="TabH1"><b>2</b></td>
            <td>Secondus</td>
            <td style="font-size:9px; text-align:left;">[štát] Centaurská Republika</td>
            <td style="text-align:right;">233'748</td>
            <td style="text-align:right;">14'609</td>
            <td style="text-align:right;">73,05 mil</td>
            <td style="text-align:right;">3,3 hod</td>
            <td>2</td>
            <td style="text-align:right;">22'500</td>
            <td style="text-align:right;">21'380</td>
            <td style="text-align:right;">7'880</td>
        </tr>
        <tr style="color: #787811;">
            <td><b>K</b></td>
            <td class="TabH1"><b>2</b></td>
            <td>Drakh Light Cruiser</td>
            <td style="font-size:9px; text-align:left;">[štát] Kolónia Drakhov</td>
            <td style="text-align:right;">366'570</td>
            <td style="text-align:right;">22'910</td>
            <td style="text-align:right;">114,55 mil</td>
            <td style="text-align:right;">5,5 hod</td>
            <td>0</td>
            <td style="text-align:right;">37'500</td>
            <td style="text-align:right;">38'250</td>
            <td style="text-align:right;">6'750</td>
        </tr>
        <tr style="color: #6984f9;">
            <td><b>K</b></td>
            <td class="TabH1"><b>2</b></td>
            <td>Olympus Corvette</td>
            <td style="font-size:9px; text-align:left;">[štát] Pozemská Aliancia</td>
            <td style="text-align:right;">227'513</td>
            <td style="text-align:right;">14'219</td>
            <td style="text-align:right;">71,1 mil</td>
            <td style="text-align:right;">3,4 hod</td>
            <td>0</td>
            <td style="text-align:right;">24'000</td>
            <td style="text-align:right;">22'500</td>
            <td style="text-align:right;">5'630</td>
        </tr>
        <tr style="color: #a03b37;">
            <td><b>K</b></td>
            <td class="TabH1"><b>2</b></td>
            <td>Sho'Kos</td>
            <td style="font-size:9px; text-align:left;">[štát] Narnský Režim</td>
            <td style="text-align:right;">311'435</td>
            <td style="text-align:right;">19'464</td>
            <td style="text-align:right;">97,32 mil</td>
            <td style="text-align:right;">4,5 hod</td>
            <td>2</td>
            <td style="text-align:right;">30'000</td>
            <td style="text-align:right;">29'250</td>
            <td style="text-align:right;">11'250</td>
        </tr>
        <tr style="color: #8a68a5;">
            <td><b>K</b></td>
            <td class="TabH1"><b>10</b></td>
            <td>Whitestar</td>
            <td style="font-size:9px; text-align:left;">[štát] Minbarská Federácia</td>
            <td style="text-align:right;">155'378</td>
            <td style="text-align:right;">19'422</td>
            <td style="text-align:right;">53,95 mil</td>
            <td style="text-align:right;">6,2 hod</td>
            <td>0</td>
            <td style="text-align:right;">42'000</td>
            <td style="text-align:right;">6'300</td>
            <td style="text-align:right;">44'100</td>
        </tr>
    </table><br><br>
    <table width="95%" cellspacing="0" class="Tab2 center">
        <tr>
            <th>Trieda</th>
            <th>Level</th>
            <th>Meno</th>
            <th>Pôvod</th>
            <th>Titánium</th>
            <th>Q40</th>
            <th>Kredity</th>
            <th>Doba<br>výroby</th>
            <th>Kapacita<br>stíhačov</th>
            <th>HP</th>
            <th>Útok S</th>
            <th>Útok L</th>
        </tr>
        <tr style="color: #dd9090;">
            <td><b>L</b></td>
            <td class="TabH1"><b>1</b></td>
            <td>Kutai Gunship</td>
            <td style="font-size:9px; text-align:left;">[rasa] Centauri</td>
            <td style="text-align:right;">578'205</td>
            <td style="text-align:right;">26'768</td>
            <td style="text-align:right;">160,61 mil</td>
            <td style="text-align:right;">6,4 hod</td>
            <td>4</td>
            <td style="text-align:right;">56'250</td>
            <td style="text-align:right;">8'440</td>
            <td style="text-align:right;">33'750</td>
        </tr>
        <tr style="color: #888811;">
            <td><b>L</b></td>
            <td class="TabH1"><b>1</b></td>
            <td>Drakh Heavy Cruiser</td>
            <td style="font-size:9px; text-align:left;">[rasa] Drakhovia</td>
            <td style="text-align:right;">595'309</td>
            <td style="text-align:right;">27'560</td>
            <td style="text-align:right;">165,36 mil</td>
            <td style="text-align:right;">6,7 hod</td>
            <td>2</td>
            <td style="text-align:right;">60'000</td>
            <td style="text-align:right;">11'250</td>
            <td style="text-align:right;">33'750</td>
        </tr>
        <tr style="color: #6984f9;">
            <td><b>L</b></td>
            <td class="TabH1"><b>1</b></td>
            <td>Hyperion Cruiser</td>
            <td style="font-size:9px; text-align:left;">[rasa] Ľudia</td>
            <td style="text-align:right;">624'056</td>
            <td style="text-align:right;">28'891</td>
            <td style="text-align:right;">173,35 mil</td>
            <td style="text-align:right;">6,9 hod</td>
            <td>4</td>
            <td style="text-align:right;">60'000</td>
            <td style="text-align:right;">4'500</td>
            <td style="text-align:right;">38'250</td>
        </tr>
        <tr style="color: #8a68a5;">
            <td><b>L</b></td>
            <td class="TabH1"><b>1</b></td>
            <td>Tinashi Frigate</td>
            <td style="font-size:9px; text-align:left;">[rasa] Minbari</td>
            <td style="text-align:right;">518'750</td>
            <td style="text-align:right;">24'016</td>
            <td style="text-align:right;">144,09 mil</td>
            <td style="text-align:right;">6,2 hod</td>
            <td>0</td>
            <td style="text-align:right;">50'630</td>
            <td style="text-align:right;">19'690</td>
            <td style="text-align:right;">30'940</td>
        </tr>
        <tr style="color: #8a68a5;">
            <td><b>L</b></td>
            <td class="TabH1"><b>2</b></td>
            <td>Morshin Carrier</td>
            <td style="font-size:9px; text-align:left;">[štát] Minbarská Federácia</td>
            <td style="text-align:right;">636'771</td>
            <td style="text-align:right;">39'798</td>
            <td style="text-align:right;">198,99 mil</td>
            <td style="text-align:right;">7,4 hod</td>
            <td>16</td>
            <td style="text-align:right;">75'000</td>
            <td style="text-align:right;">33'750</td>
            <td style="text-align:right;">6'750</td>
        </tr>
        <tr style="color: #888811;">
            <td><b>L</b></td>
            <td class="TabH1"><b>3</b></td>
            <td>Drakh Carrier</td>
            <td style="font-size:9px; text-align:left;">[rasa] Drakhovia</td>
            <td style="text-align:right;">513'682</td>
            <td style="text-align:right;">42'806</td>
            <td style="text-align:right;">183,46 mil</td>
            <td style="text-align:right;">8,7 hod</td>
            <td>8</td>
            <td style="text-align:right;">72'000</td>
            <td style="text-align:right;">6'750</td>
            <td style="text-align:right;">49'500</td>
        </tr>
        <tr style="color: #a03b37;">
            <td><b>L</b></td>
            <td class="TabH1"><b>3</b></td>
            <td>Th'Nor</td>
            <td style="font-size:9px; text-align:left;">[rasa] Narni</td>
            <td style="text-align:right;">370'806</td>
            <td style="text-align:right;">30'900</td>
            <td style="text-align:right;">132,43 mil</td>
            <td style="text-align:right;">6,8 hod</td>
            <td>4</td>
            <td style="text-align:right;">52'500</td>
            <td style="text-align:right;">4'500</td>
            <td style="text-align:right;">42'750</td>
        </tr>
        <tr style="color: #6984f9;">
            <td><b>L</b></td>
            <td class="TabH1"><b>7</b></td>
            <td>Omega Destroyer</td>
            <td style="font-size:9px; text-align:left;">[štát] Pozemská Aliancia</td>
            <td style="text-align:right;">333'091</td>
            <td style="text-align:right;">31'722</td>
            <td style="text-align:right;">99,13 mil</td>
            <td style="text-align:right;">7,7 hod</td>
            <td>12</td>
            <td style="text-align:right;">54'000</td>
            <td style="text-align:right;">6'750</td>
            <td style="text-align:right;">51'750</td>
        </tr>
        <tr style="color: #a03b37;">
            <td><b>L</b></td>
            <td class="TabH1"><b>7</b></td>
            <td>G'Quan</td>
            <td style="font-size:9px; text-align:left;">[štát] Narnský Režim</td>
            <td style="text-align:right;">550'387</td>
            <td style="text-align:right;">52'417</td>
            <td style="text-align:right;">163,8 mil</td>
            <td style="text-align:right;">13,8 hod</td>
            <td>12</td>
            <td style="text-align:right;">97'500</td>
            <td style="text-align:right;">13'500</td>
            <td style="text-align:right;">92'250</td>
        </tr>
        <tr style="color: #dd9090;">
            <td><b>L</b></td>
            <td class="TabH1"><b>10</b></td>
            <td>Vorchan Cruiser</td>
            <td style="font-size:9px; text-align:left;">[štát] Centaurská Republika</td>
            <td style="text-align:right;">221'969</td>
            <td style="text-align:right;">27'746</td>
            <td style="text-align:right;">77,07 mil</td>
            <td style="text-align:right;">8,9 hod</td>
            <td>0</td>
            <td style="text-align:right;">60'000</td>
            <td style="text-align:right;">9'000</td>
            <td style="text-align:right;">63'000</td>
        </tr>
    </table><br><br>
    <table width="95%" cellspacing="0" class="Tab2 center">
        <tr>
            <th>Trieda</th>
            <th>Level</th>
            <th>Meno</th>
            <th>Pôvod</th>
            <th>Titánium</th>
            <th>Q40</th>
            <th>Kredity</th>
            <th>Doba<br>výroby</th>
            <th>Kapacita<br>stíhačov</th>
            <th>HP</th>
            <th>Útok S</th>
            <th>Útok L</th>
        </tr>
        <tr style="color: #a03b37;">
            <td><b>D</b></td>
            <td class="TabH1"><b>1</b></td>
            <td>Sho'Kar</td>
            <td style="font-size:9px; text-align:left;">[rasa] Narni</td>
            <td style="text-align:right;">1'289'735</td>
            <td style="text-align:right;">59'709</td>
            <td style="text-align:right;">358,25 mil</td>
            <td style="text-align:right;">12,6 hod</td>
            <td>24</td>
            <td style="text-align:right;">112'500</td>
            <td style="text-align:right;">16'890</td>
            <td style="text-align:right;">64'140</td>
        </tr>
        <tr style="color: #dd9090;">
            <td><b>D</b></td>
            <td class="TabH1"><b>3</b></td>
            <td>Primus Cruiser</td>
            <td style="font-size:9px; text-align:left;">[rasa] Centauri</td>
            <td style="text-align:right;">897'044</td>
            <td style="text-align:right;">74'753</td>
            <td style="text-align:right;">320,37 mil</td>
            <td style="text-align:right;">15,8 hod</td>
            <td>16</td>
            <td style="text-align:right;">126'000</td>
            <td style="text-align:right;">13'500</td>
            <td style="text-align:right;">94'500</td>
        </tr>
        <tr style="color: #6984f9;">
            <td><b>D</b></td>
            <td class="TabH1"><b>3</b></td>
            <td>Nova Dreadnought</td>
            <td style="font-size:9px; text-align:left;">[rasa] Ľudia</td>
            <td style="text-align:right;">847'374</td>
            <td style="text-align:right;">70'613</td>
            <td style="text-align:right;">302,63 mil</td>
            <td style="text-align:right;">15,2 hod</td>
            <td>12</td>
            <td style="text-align:right;">120'000</td>
            <td style="text-align:right;">9'000</td>
            <td style="text-align:right;">92'250</td>
        </tr>
        <tr style="color: #8a68a5;">
            <td><b>D</b></td>
            <td class="TabH1"><b>3</b></td>
            <td>Sharlin</td>
            <td style="font-size:9px; text-align:left;">[rasa] Minbari</td>
            <td style="text-align:right;">735'412</td>
            <td style="text-align:right;">61'283</td>
            <td style="text-align:right;">262,64 mil</td>
            <td style="text-align:right;">13,4 hod</td>
            <td>8</td>
            <td style="text-align:right;">103'000</td>
            <td style="text-align:right;">14'050</td>
            <td style="text-align:right;">81'560</td>
        </tr>
        <tr style="color: #dd9090;">
            <td><b>D</b></td>
            <td class="TabH1"><b>7</b></td>
            <td>Octurion</td>
            <td style="font-size:9px; text-align:left;">[štát] Centaurská Republika</td>
            <td style="text-align:right;">721'492</td>
            <td style="text-align:right;">68'712</td>
            <td style="text-align:right;">214,73 mil</td>
            <td style="text-align:right;">17,5 hod</td>
            <td>21</td>
            <td style="text-align:right;">121'500</td>
            <td style="text-align:right;">18'000</td>
            <td style="text-align:right;">119'250</td>
        </tr>
        <tr style="color: #787811;">
            <td><b>D</b></td>
            <td class="TabH1"><b>7</b></td>
            <td>Mother Ship</td>
            <td style="font-size:9px; text-align:left;">[štát] Kolónia Drakhov</td>
            <td style="text-align:right;">2'643'181</td>
            <td style="text-align:right;">251'728</td>
            <td style="text-align:right;">786,65 mil</td>
            <td style="text-align:right;">61,9 hod</td>
            <td>60</td>
            <td style="text-align:right;">450'000</td>
            <td style="text-align:right;">56'250</td>
            <td style="text-align:right;">405'000</td>
        </tr>
        <tr style="color: #8a68a5;">
            <td><b>D</b></td>
            <td class="TabH1"><b>7</b></td>
            <td>Shargoti</td>
            <td style="font-size:9px; text-align:left;">[štát] Minbarská Federácia</td>
            <td style="text-align:right;">725'200</td>
            <td style="text-align:right;">69'066</td>
            <td style="text-align:right;">215,83 mil</td>
            <td style="text-align:right;">18,9 hod</td>
            <td>8</td>
            <td style="text-align:right;">135'000</td>
            <td style="text-align:right;">22'500</td>
            <td style="text-align:right;">123'750</td>
        </tr>
        <tr style="color: #787811;">
            <td><b>D</b></td>
            <td class="TabH1"><b>10</b></td>
            <td>Drakh Advanced</td>
            <td style="font-size:9px; text-align:left;">[štát] Kolónia Drakhov</td>
            <td style="text-align:right;">554'923</td>
            <td style="text-align:right;">69'364</td>
            <td style="text-align:right;">192,68 mil</td>
            <td style="text-align:right;">22,3 hod</td>
            <td>0</td>
            <td style="text-align:right;">150'000</td>
            <td style="text-align:right;">22'500</td>
            <td style="text-align:right;">157'500</td>
        </tr>
        <tr style="color: #6984f9;">
            <td><b>D</b></td>
            <td class="TabH1"><b>10</b></td>
            <td>Warlock Destroyer</td>
            <td style="font-size:9px; text-align:left;">[štát] Pozemská Aliancia</td>
            <td style="text-align:right;">416'203</td>
            <td style="text-align:right;">52'025</td>
            <td style="text-align:right;">144,51 mil</td>
            <td style="text-align:right;">16,7 hod</td>
            <td>0</td>
            <td style="text-align:right;">112'500</td>
            <td style="text-align:right;">16'880</td>
            <td style="text-align:right;">118'130</td>
        </tr>
        <tr style="color: #a03b37;">
            <td><b>D</b></td>
            <td class="TabH1"><b>10</b></td>
            <td>Bin'Tak</td>
            <td style="font-size:9px; text-align:left;">[štát] Narnský Režim</td>
            <td style="text-align:right;">498'909</td>
            <td style="text-align:right;">62'363</td>
            <td style="text-align:right;">173,23 mil</td>
            <td style="text-align:right;">20 hod</td>
            <td>0</td>
            <td style="text-align:right;">135'000</td>
            <td style="text-align:right;">19'350</td>
            <td style="text-align:right;">141'740</td>
        </tr>
    </table><br><br> --}}



































{{-- <table width="95%" cellspacing="0" class="Tab2 center">
    <tr>
        <th>Trieda</th>
        <th>Level</th>
        <th>Meno</th>
        <th>Pôvod</th>
        <th>Titánium</th>
        <th>Q40</th>
        <th>Kredity</th>
        <th>Doba<br>výroby</th>
        <th>Kapacita<br>stíhačov</th>
        <th>HP</th>
        <th>Útok S</th>
        <th>Útok L</th>
    </tr>
    <tr style="color: #dd9090;">
        <td><b>S</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Sentri Fighter</td>
        <td style="font-size:9px; text-align:left;">[rasa] Centauri</td>
        <td style="text-align:right;">30'457</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">11,71 mil</td>
        <td style="text-align:right;">2,3 hod</td>
        <td>0</td>
        <td style="text-align:right;">2'600</td>
        <td style="text-align:right;">2'100</td>
        <td style="text-align:right;">2'200</td>
    </tr>
    <tr style="color: #888811;">
        <td><b>S</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Drakh Raider</td>
        <td style="font-size:9px; text-align:left;">[rasa] Drakhovia</td>
        <td style="text-align:right;">50'997</td>
        <td style="text-align:right;">2'452</td>
        <td style="text-align:right;">7,36 mil</td>
        <td style="text-align:right;">3,9 hod</td>
        <td>0</td>
        <td style="text-align:right;">4'200</td>
        <td style="text-align:right;">4'000</td>
        <td style="text-align:right;">3'100</td>
    </tr>
    <tr style="color: #6984f9;">
        <td><b>S</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Starfury</td>
        <td style="font-size:9px; text-align:left;">[rasa] Ľudia</td>
        <td style="text-align:right;">28'165</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">10 mil</td>
        <td style="text-align:right;">2,1 hod</td>
        <td>0</td>
        <td style="text-align:right;">2'300</td>
        <td style="text-align:right;">2'000</td>
        <td style="text-align:right;">1'800</td>
    </tr>
    <tr style="color: #8a68a5;">
        <td><b>S</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Nial Fighter</td>
        <td style="font-size:9px; text-align:left;">[rasa] Minbari</td>
        <td style="text-align:right;">44'948</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">14,71 mil</td>
        <td style="text-align:right;">3,2 hod</td>
        <td>0</td>
        <td style="text-align:right;">3'600</td>
        <td style="text-align:right;">2'500</td>
        <td style="text-align:right;">3'100</td>
    </tr>
    <tr style="color: #a03b37;">
        <td><b>S</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Frazi</td>
        <td style="font-size:9px; text-align:left;">[rasa] Narni</td>
        <td style="text-align:right;">25'521</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">9,43 mil</td>
        <td style="text-align:right;">1,9 hod</td>
        <td>0</td>
        <td style="text-align:right;">2'200</td>
        <td style="text-align:right;">1'700</td>
        <td style="text-align:right;">1'750</td>
    </tr>
    <tr style="color: #787878;">
        <td><b>S</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Spitfire</td>
        <td style="font-size:9px; text-align:left;">[rasa] Tiene</td>
        <td style="text-align:right;">62'810</td>
        <td style="text-align:right;">2'818</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">4,1 hod</td>
        <td>0</td>
        <td style="text-align:right;">4'650</td>
        <td style="text-align:right;">3'200</td>
        <td style="text-align:right;">4'000</td>
    </tr>
    <tr style="color: #c0cc2c;">
        <td><b>S</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Lightning</td>
        <td style="font-size:9px; text-align:left;">[rasa] Vorloni</td>
        <td style="text-align:right;">62'091</td>
        <td style="text-align:right;">2'786</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">4,1 hod</td>
        <td>0</td>
        <td style="text-align:right;">4'500</td>
        <td style="text-align:right;">3'350</td>
        <td style="text-align:right;">4'000</td>
    </tr>
    <tr style="color: #dd9090;">
        <td><b>S</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Rutharian Strike</td>
        <td style="font-size:9px; text-align:left;">[štát] Centaurská Republika</td>
        <td style="text-align:right;">20'759</td>
        <td style="text-align:right;">807</td>
        <td style="text-align:right;">6,54 mil</td>
        <td style="text-align:right;">2,1 hod</td>
        <td>0</td>
        <td style="text-align:right;">1'600</td>
        <td style="text-align:right;">3'500</td>
        <td style="text-align:right;">600</td>
    </tr>
    <tr style="color: #a03b37;">
        <td><b>S</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Gorith Fighter</td>
        <td style="font-size:9px; text-align:left;">[štát] Narnský Režim</td>
        <td style="text-align:right;">21'371</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">7,29 mil</td>
        <td style="text-align:right;">1,7 hod</td>
        <td>0</td>
        <td style="text-align:right;">1'200</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">3'500</td>
    </tr>
</table><br><br>
<table width="95%" cellspacing="0" class="Tab2 center">
    <tr>
        <th>Trieda</th>
        <th>Level</th>
        <th>Meno</th>
        <th>Pôvod</th>
        <th>Titánium</th>
        <th>Q40</th>
        <th>Kredity</th>
        <th>Doba<br>výroby</th>
        <th>Kapacita<br>stíhačov</th>
        <th>HP</th>
        <th>Útok S</th>
        <th>Útok L</th>
    </tr>
    <tr style="color: #dd9090;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Vorchan Cruiser</td>
        <td style="font-size:9px; text-align:left;">[rasa] Centauri</td>
        <td style="text-align:right;">192'721</td>
        <td style="text-align:right;">8'975</td>
        <td style="text-align:right;">111 mil</td>
        <td style="text-align:right;">4,2 hod</td>
        <td>2</td>
        <td style="text-align:right;">31'000</td>
        <td style="text-align:right;">25'000</td>
        <td style="text-align:right;">8'350</td>
    </tr>
    <tr style="color: #888811;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Drakh Cruiser</td>
        <td style="font-size:9px; text-align:left;">[rasa] Drakhovia</td>
        <td style="text-align:right;">392'483</td>
        <td style="text-align:right;">25'893</td>
        <td style="text-align:right;">47,7 mil</td>
        <td style="text-align:right;">6,1 hod</td>
        <td>0</td>
        <td style="text-align:right;">45'000</td>
        <td style="text-align:right;">38'000</td>
        <td style="text-align:right;">10'000</td>
    </tr>
    <tr style="color: #6984f9;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Hyperion Cruiser</td>
        <td style="font-size:9px; text-align:left;">[rasa] Ľudia</td>
        <td style="text-align:right;">166'390</td>
        <td style="text-align:right;">7'748</td>
        <td style="text-align:right;">95,84 mil</td>
        <td style="text-align:right;">3,6 hod</td>
        <td>3</td>
        <td style="text-align:right;">26'500</td>
        <td style="text-align:right;">22'000</td>
        <td style="text-align:right;">5'200</td>
    </tr>
    <tr style="color: #8a68a5;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Tinashi Frigate</td>
        <td style="font-size:9px; text-align:left;">[rasa] Minbari</td>
        <td style="text-align:right;">262'435</td>
        <td style="text-align:right;">10'935</td>
        <td style="text-align:right;">123,73 mil</td>
        <td style="text-align:right;">5,1 hod</td>
        <td>1</td>
        <td style="text-align:right;">36'000</td>
        <td style="text-align:right;">33'000</td>
        <td style="text-align:right;">9'300</td>
    </tr>
    <tr style="color: #a03b37;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Toreth</td>
        <td style="font-size:9px; text-align:left;">[rasa] Narni</td>
        <td style="text-align:right;">144'183</td>
        <td style="text-align:right;">6'918</td>
        <td style="text-align:right;">87,38 mil</td>
        <td style="text-align:right;">3,2 hod</td>
        <td>2</td>
        <td style="text-align:right;">26'000</td>
        <td style="text-align:right;">18'000</td>
        <td style="text-align:right;">4'900</td>
    </tr>
    <tr style="color: #787878;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Vampire</td>
        <td style="font-size:9px; text-align:left;">[rasa] Tiene</td>
        <td style="text-align:right;">373'217</td>
        <td style="text-align:right;">11'503</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">4 hod</td>
        <td>2</td>
        <td style="text-align:right;">31'000</td>
        <td style="text-align:right;">25'000</td>
        <td style="text-align:right;">2'800</td>
    </tr>
    <tr style="color: #c0cc2c;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Ambassador</td>
        <td style="font-size:9px; text-align:left;">[rasa] Vorloni</td>
        <td style="text-align:right;">349'838</td>
        <td style="text-align:right;">10'783</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">3,8 hod</td>
        <td>0</td>
        <td style="text-align:right;">29'000</td>
        <td style="text-align:right;">24'000</td>
        <td style="text-align:right;">3'200</td>
    </tr>
    <tr style="color: #dd9090;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Covran Scout</td>
        <td style="font-size:9px; text-align:left;">[štát] Centaurská Republika</td>
        <td style="text-align:right;">125'773</td>
        <td style="text-align:right;">276</td>
        <td style="text-align:right;">84,12 mil</td>
        <td style="text-align:right;">2,2 hod</td>
        <td>0</td>
        <td style="text-align:right;">12'000</td>
        <td style="text-align:right;">20'000</td>
        <td style="text-align:right;">200</td>
    </tr>
    <tr style="color: #dd9090;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Kutai Gunship</td>
        <td style="font-size:9px; text-align:left;">[štát] Centaurská Republika</td>
        <td style="text-align:right;">140'706</td>
        <td style="text-align:right;">3'006</td>
        <td style="text-align:right;">76,67 mil</td>
        <td style="text-align:right;">2,4 hod</td>
        <td>8</td>
        <td style="text-align:right;">20'000</td>
        <td style="text-align:right;">3'500</td>
        <td style="text-align:right;">13'000</td>
    </tr>
    <tr style="color: #787811;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Drakh Shuttle</td>
        <td style="font-size:9px; text-align:left;">[štát] Kolónia Drakhov</td>
        <td style="text-align:right;">178'345</td>
        <td style="text-align:right;">1'651</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">0,9 hod</td>
        <td>6</td>
        <td style="text-align:right;">17'000</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">0</td>
    </tr>
    <tr style="color: #6984f9;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Olympus Corvette</td>
        <td style="font-size:9px; text-align:left;">[štát] Pozemská Aliancia</td>
        <td style="text-align:right;">69'466</td>
        <td style="text-align:right;">214</td>
        <td style="text-align:right;">77,18 mil</td>
        <td style="text-align:right;">1,7 hod</td>
        <td>0</td>
        <td style="text-align:right;">9'000</td>
        <td style="text-align:right;">16'000</td>
        <td style="text-align:right;">500</td>
    </tr>
    <tr style="color: #8a68a5;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Morshin Carrier</td>
        <td style="font-size:9px; text-align:left;">[štát] Minbarská Federácia</td>
        <td style="text-align:right;">325'035</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">230,6 mil</td>
        <td style="text-align:right;">4,9 hod</td>
        <td>32</td>
        <td style="text-align:right;">80'000</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">0</td>
    </tr>
    <tr style="color: #8a68a5;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Torotha Assault</td>
        <td style="font-size:9px; text-align:left;">[štát] Minbarská Federácia</td>
        <td style="text-align:right;">132'057</td>
        <td style="text-align:right;">3'668</td>
        <td style="text-align:right;">93,37 mil</td>
        <td style="text-align:right;">2,7 hod</td>
        <td>0</td>
        <td style="text-align:right;">11'000</td>
        <td style="text-align:right;">32'000</td>
        <td style="text-align:right;">0</td>
    </tr>
    <tr style="color: #a03b37;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Th'Nor</td>
        <td style="font-size:9px; text-align:left;">[štát] Narnský Režim</td>
        <td style="text-align:right;">107'572</td>
        <td style="text-align:right;">18'355</td>
        <td style="text-align:right;">76,84 mil</td>
        <td style="text-align:right;">3,7 hod</td>
        <td>4</td>
        <td style="text-align:right;">22'000</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">31'000</td>
    </tr>
    <tr style="color: #a03b37;">
        <td><b>K</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Sho'kar Scout</td>
        <td style="font-size:9px; text-align:left;">[štát] Narnský Režim</td>
        <td style="text-align:right;">148'694</td>
        <td style="text-align:right;">1'721</td>
        <td style="text-align:right;">101,54 mil</td>
        <td style="text-align:right;">2,7 hod</td>
        <td>0</td>
        <td style="text-align:right;">15'000</td>
        <td style="text-align:right;">25'000</td>
        <td style="text-align:right;">0</td>
    </tr>
</table><br><br>
<table width="95%" cellspacing="0" class="Tab2 center">
    <tr>
        <th>Trieda</th>
        <th>Level</th>
        <th>Meno</th>
        <th>Pôvod</th>
        <th>Titánium</th>
        <th>Q40</th>
        <th>Kredity</th>
        <th>Doba<br>výroby</th>
        <th>Kapacita<br>stíhačov</th>
        <th>HP</th>
        <th>Útok S</th>
        <th>Útok L</th>
    </tr>
    <tr style="color: #dd9090;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Primus Cruiser</td>
        <td style="font-size:9px; text-align:left;">[rasa] Centauri</td>
        <td style="text-align:right;">206'037</td>
        <td style="text-align:right;">9'416</td>
        <td style="text-align:right;">144 mil</td>
        <td style="text-align:right;">4,5 hod</td>
        <td>12</td>
        <td style="text-align:right;">39'200</td>
        <td style="text-align:right;">7'600</td>
        <td style="text-align:right;">23'200</td>
    </tr>
    <tr style="color: #888811;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Drakh Carrier</td>
        <td style="font-size:9px; text-align:left;">[rasa] Drakhovia</td>
        <td style="text-align:right;">588'350</td>
        <td style="text-align:right;">42'164</td>
        <td style="text-align:right;">34,32 mil</td>
        <td style="text-align:right;">8,1 hod</td>
        <td>10</td>
        <td style="text-align:right;">72'000</td>
        <td style="text-align:right;">12'800</td>
        <td style="text-align:right;">41'600</td>
    </tr>
    <tr style="color: #6984f9;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Nova Dreadnought</td>
        <td style="font-size:9px; text-align:left;">[rasa] Ľudia</td>
        <td style="text-align:right;">134'009</td>
        <td style="text-align:right;">6'381</td>
        <td style="text-align:right;">111,67 mil</td>
        <td style="text-align:right;">3,3 hod</td>
        <td>8</td>
        <td style="text-align:right;">32'800</td>
        <td style="text-align:right;">6'400</td>
        <td style="text-align:right;">14'400</td>
    </tr>
    <tr style="color: #8a68a5;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Sharlin</td>
        <td style="font-size:9px; text-align:left;">[rasa] Minbari</td>
        <td style="text-align:right;">214'239</td>
        <td style="text-align:right;">13'046</td>
        <td style="text-align:right;">188,83 mil</td>
        <td style="text-align:right;">5,7 hod</td>
        <td>8</td>
        <td style="text-align:right;">52'000</td>
        <td style="text-align:right;">10'400</td>
        <td style="text-align:right;">28'000</td>
    </tr>
    <tr style="color: #a03b37;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Bin'Tak</td>
        <td style="font-size:9px; text-align:left;">[rasa] Narni</td>
        <td style="text-align:right;">138'260</td>
        <td style="text-align:right;">7'374</td>
        <td style="text-align:right;">135,95 mil</td>
        <td style="text-align:right;">3,8 hod</td>
        <td>10</td>
        <td style="text-align:right;">28'000</td>
        <td style="text-align:right;">5'600</td>
        <td style="text-align:right;">24'000</td>
    </tr>
    <tr style="color: #787878;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Battle Crab</td>
        <td style="font-size:9px; text-align:left;">[rasa] Tiene</td>
        <td style="text-align:right;">1'234'308</td>
        <td style="text-align:right;">84'156</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">17,8 hod</td>
        <td>12</td>
        <td style="text-align:right;">132'000</td>
        <td style="text-align:right;">13'000</td>
        <td style="text-align:right;">115'000</td>
    </tr>
    <tr style="color: #c0cc2c;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Star Dreadnought</td>
        <td style="font-size:9px; text-align:left;">[rasa] Vorloni</td>
        <td style="text-align:right;">1'075'524</td>
        <td style="text-align:right;">73'330</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">15,6 hod</td>
        <td>6</td>
        <td style="text-align:right;">99'000</td>
        <td style="text-align:right;">8'500</td>
        <td style="text-align:right;">120'000</td>
    </tr>
    <tr style="color: #dd9090;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Covran Gunship</td>
        <td style="font-size:9px; text-align:left;">[štát] Centaurská Republika</td>
        <td style="text-align:right;">128'598</td>
        <td style="text-align:right;">21'997</td>
        <td style="text-align:right;">118,44 mil</td>
        <td style="text-align:right;">5,2 hod</td>
        <td>6</td>
        <td style="text-align:right;">30'000</td>
        <td style="text-align:right;">1'500</td>
        <td style="text-align:right;">45'000</td>
    </tr>
    <tr style="color: #787811;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Drakh Advanced</td>
        <td style="font-size:9px; text-align:left;">[štát] Kolónia Drakhov</td>
        <td style="text-align:right;">440'185</td>
        <td style="text-align:right;">55'022</td>
        <td style="text-align:right;">152,84 mil</td>
        <td style="text-align:right;">11,9 hod</td>
        <td>0</td>
        <td style="text-align:right;">70'000</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">100'000</td>
    </tr>
    <tr style="color: #6984f9;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Omega Destroyer</td>
        <td style="font-size:9px; text-align:left;">[štát] Pozemská Aliancia</td>
        <td style="text-align:right;">263'885</td>
        <td style="text-align:right;">16'859</td>
        <td style="text-align:right;">172,26 mil</td>
        <td style="text-align:right;">6,5 hod</td>
        <td>18</td>
        <td style="text-align:right;">53'000</td>
        <td style="text-align:right;">1'500</td>
        <td style="text-align:right;">39'000</td>
    </tr>
    <tr style="color: #6984f9;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Warlock Destroyer</td>
        <td style="font-size:9px; text-align:left;">[štát] Pozemská Aliancia</td>
        <td style="text-align:right;">129'979</td>
        <td style="text-align:right;">29'786</td>
        <td style="text-align:right;">67,7 mil</td>
        <td style="text-align:right;">4,9 hod</td>
        <td>0</td>
        <td style="text-align:right;">25'000</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">47'000</td>
    </tr>
    <tr style="color: #8a68a5;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Shargoti</td>
        <td style="font-size:9px; text-align:left;">[štát] Minbarská Federácia</td>
        <td style="text-align:right;">416'738</td>
        <td style="text-align:right;">38'448</td>
        <td style="text-align:right;">254,26 mil</td>
        <td style="text-align:right;">11,3 hod</td>
        <td>14</td>
        <td style="text-align:right;">88'000</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">72'000</td>
    </tr>
    <tr style="color: #a03b37;">
        <td><b>L</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>G'Quan</td>
        <td style="font-size:9px; text-align:left;">[štát] Narnský Režim</td>
        <td style="text-align:right;">237'212</td>
        <td style="text-align:right;">534</td>
        <td style="text-align:right;">165,62 mil</td>
        <td style="text-align:right;">4,5 hod</td>
        <td>20</td>
        <td style="text-align:right;">70'000</td>
        <td style="text-align:right;">2'000</td>
        <td style="text-align:right;">14'000</td>
    </tr>
</table><br><br>
<table width="95%" cellspacing="0" class="Tab2 center">
    <tr>
        <th>Trieda</th>
        <th>Level</th>
        <th>Meno</th>
        <th>Pôvod</th>
        <th>Titánium</th>
        <th>Q40</th>
        <th>Kredity</th>
        <th>Doba<br>výroby</th>
        <th>Kapacita<br>stíhačov</th>
        <th>HP</th>
        <th>Útok S</th>
        <th>Útok L</th>
    </tr>
    <tr style="color: #787811;">
        <td><b>D</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Mother Ship</td>
        <td style="font-size:9px; text-align:left;">[štát] Kolónia Drakhov</td>
        <td style="text-align:right;">1'996'295</td>
        <td style="text-align:right;">360'436</td>
        <td style="text-align:right;">138,63 mil</td>
        <td style="text-align:right;">41,9 hod</td>
        <td>120</td>
        <td style="text-align:right;">250'000</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">350'000</td>
    </tr>
    <tr style="color: #;">
        <td><b>D</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Death Cloud</td>
        <td style="font-size:9px; text-align:left;">[štát] </td>
        <td style="text-align:right;">1'855'219</td>
        <td style="text-align:right;">463'797</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">31,7 hod</td>
        <td>0</td>
        <td style="text-align:right;">230'000</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">870'000</td>
    </tr>
    <tr style="color: #8e911f;">
        <td><b>D</b></td>
        <td class="TabH1"><b>0</b></td>
        <td>Planet Killer</td>
        <td style="font-size:9px; text-align:left;">[štát] Vorlonské Impérium</td>
        <td style="text-align:right;">1'891'514</td>
        <td style="text-align:right;">472'871</td>
        <td style="text-align:right;">0</td>
        <td style="text-align:right;">31,2 hod</td>
        <td>80</td>
        <td style="text-align:right;">360'000</td>
        <td style="text-align:right;">15'000</td>
        <td style="text-align:right;">532'000</td>
    </tr>
</table> --}}

    <div style="clear:both"></div>
</div>