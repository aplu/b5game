<div id="dummyMenu" style="display: none;">
    <div class="toolbar">
        <a href="{{ route('forums') }}" class="bigBTN" title="{{ __('Forums') }}">
            <img src="/images/b5gna/icons48/forums.png" alt="" width="48" height="48" border="0"><br>
            {{ __('Forums') }}
        </a>
        <div class="smallItemsBox">
            <a class="smallBTN" href="/forum/galactic" title="{{ __('Galactic forum') }}"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>{{ __('Galactic forum') }}</div></a>
            @if (in_array('STATES', $rules))
                <a class="smallBTN" href="/forum/politic" title="{{ __('Political forum') }}"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>{{ __('Political forum') }}</div></a>
            @endif
            <a class="smallBTN" href="/forum/chronicle" title="{{ __('Chronicle') }}"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>{{ __('Chronicle') }}</div></a>
        </div>
        {{-- <div class="smallItemsBox">
            <a class="smallBTN" href="/forum/notices" title="Admin oznamy"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>Admin oznamy</div></a>
            <a class="smallBTN" href="/forum/bugs" title="Bug fórum"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>Bug fórum</div></a>
            <a class="smallBTN" href="/forum/devel" title="Vývojové fórum"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>Vývojové fórum</div></a>
        </div> --}}
        {{-- <div class="smallItemsBox">
            <a class="smallBTN" href="/forum/newbie" title="Pre nováčikov"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>Pre nováčikov</div></a>
            <a class="smallBTN" href="/forum/nation" title="Národné fórum"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>Národné fórum</div></a>
@if ('member of government' == true)
            <a class="smallBTN" href="/forum/state" title="Vládne fórum"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>Vládne fórum</div></a>
@endif
        </div> --}}
        @if (in_array('FORUMS_PRIVATE', $rules) || in_array('FORUMS_OFFTOPIC', $rules))
            <a href="{{ route('create-forum') }}" class="bigBTN" title="{{ __('Create private forum') }}"><img src="/images/b5gna/icons48/plus.png" alt="" width="48" height="48" border="0"><br>{{ __('Create forum') }}</a>
        @endif
    </div>
</div>
{{-- @if ($theme == 'b5gna') --}}
<script language="JavaScript">
    $(".panelRightBox").after($("#dummyMenu").find(".toolbar"));
</script>
{{-- @endif --}}