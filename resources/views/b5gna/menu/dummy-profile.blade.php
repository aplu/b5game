<div id="dummyMenu" style="display: none;">
	<div class="toolbar">
		<a href="{{ route('profile') }}" class="bigBTN" title="{{ __('Profile') }}"><img src="/images/b5gna/icons48/user.png" alt="" width="48" height="48" border="0"><br>{{ __('Profile') }}</a>
		<a href="{{ route('edit-profile') }}" class="bigBTN" title="{{ __('Change profile') }}"><img src="/images/b5gna/icons48/config.png" alt="" width="48" height="48" border="0"><br>{{ __('Change profile') }}</a>
		@if (in_array('PREREG', $rules))
			<a href="{{ route('prereg') }}" class="bigBTN" title="{{ __('Prereg') }}"><img src="/images/b5gna/icons48/quiz.png" alt="" width="48" height="48" border="0"><br>{{ __('Prereg') }}</a>
		@endif
	</div>
</div>
{{-- @if ($theme == 'b5gna') --}}
<script language="JavaScript">
	$(".panelRightBox").after($("#dummyMenu").find(".toolbar"));
</script>
{{-- @endif --}}