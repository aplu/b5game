		<div class="toolbar">
			<a href="/about" class="bigBTN" title="Úvod"><img src="/images/b5gna/icons48/B5.png" alt="Úvod" width="48" height="48" border="0"><br>Úvod</a>
			{{-- <a href="/rules" class="bigBTN" title="Pravidlá"><img src="/images/b5gna/icons48/meeting.png" alt="Pravidlá" width="48" height="48" border="0"><br>Pravidlá</a> --}}
			<a href="/reg" class="bigBTN" title="Registrácia"><img src="/images/b5gna/icons48/user_add.png" alt="Registrácia" width="48" height="48" border="0"><br>Registrácia</a>
			<a href="/manual" class="bigBTN" title="Manuál"><img src="/images/b5gna/icons48/lifesaver.png" alt="Manuál" width="48" height="48" border="0"><br>Manuál</a>
			<a href="/b5fans" class="bigBTN" title="Združenie"><img src="/images/b5gna/icons48/B5fans.png" alt="Združenie" width="48" height="48" border="0"><br>Združenie</a>
			<div class="smallItemsBox">
				<a class="smallBTN" href="https://discord.gg/6P52yfp" target="_blank" title="Discord"><img src="/images/icons/discord.svg" alt="Discord" width="16" height="16" border="0" style="width:20px; height:20px; padding:2px"><div>Discord</div></a>
				<a class="smallBTN" href="https://trello.com/b/56ILmnc0/b5game-open-age" target="_blank" title="Trello"><img src="/images/icons/trello.svg" alt="Trello" width="16" height="16" border="0"><div>Trello</div></a>
				<a class="smallBTN" href="https://gitlab.com/aplu/b5game" target="_blank" title="Gitlab"><img src="/images/icons/gitlab.svg" alt="Gitlab" width="16" height="16" border="0" style="width:24px; height:24px; padding:0px"><div>Gitlab</div></a>
			</div>
			{{-- <a href="/contact" class="bigBTN" title="Kontakt"><img src="/images/b5gna/icons48/email.png" alt="Kontakt" width="48" height="48" border="0"><br>Kontakt</a> --}}
			<a href="/minigames" class="bigBTN" title="Kontakt"><img src="/images/icons/icons8-game-controller-48.png" alt="Minihry" width="48" height="48" border="0"><br>Minihry</a>
		</div>
		<div class="panelRightBox">
			<form action="{{ route('login') }}" method="post" style="line-height: 24px; padding-top: 4px;">
				@csrf
				<label for="login">E-mail</label>
				<input type="text" name="email" style="width: 128px"><br>

				<label for="password">Heslo</label>
				<input type="password" name="password" style="width: 128px;; margin-left: 4px;"><br>

				<input type="submit" name="LoginSend" value="Prihlásiť sa" style="width: 176px; margin-top: 4px;">
			</form>
		</div>