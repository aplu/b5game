<div id="dummyMenu" style="display: none;">
    <div class="toolbar">
        @if (in_array('MAP', $rules))
            <a href="{{ route('map') }}" class="bigBTN" title="{{ __('Space map') }}"><img src="/images/b5gna/icons48/map.png" alt="" width="48" height="48" border="0"><br>{{ __('Space map') }}</a>
        @endif
        @if (in_array('SECTORS', $rules))
            <a href="{{ route('sectors') }}" class="bigBTN" title="{{ __('Sector detail') }}"><img src="/images/b5gna/icons48/planet.png" alt="" width="48" height="48" border="0"><br>{{ __('Detail') }}</a>
        @endif
        @if (in_array('VEHICLES', $rules))
            <a href="{{ route('intel') }}" class="bigBTN" title="{{ __('Intel Center') }}"><img src="/images/b5gna/icons48/satellite.png" alt="" width="48" height="48" border="0"><br>{{ __('Intel') }}</a>
        @endif
        @if (in_array('MAP_SHARING', $rules))
            <div class="smallItemsBox">
                <a class="smallBTN" href="{{ route('map-sharing') }}" title="{{ __('Sharing') }}"><img src="/images/b5gna/icons16/share.png" alt="" width="16" height="16" border="0">
                    <div>{{ __('Sharing') }}</div>
                </a>
            </div>
        @endif
    </div>
</div>
{{-- @if ($theme == 'b5gna') --}}
<script language="JavaScript">
    $(".panelRightBox").after($("#dummyMenu").find(".toolbar"));
</script>
{{-- @endif --}}