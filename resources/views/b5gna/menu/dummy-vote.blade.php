<div id="dummyMenu" style="display: none;">
    <div class="toolbar">
        @if (in_array('POLLS_CUSTOM', $rules))
	        <a href="{{ route('create-poll') }}" class="bigBTN" title="{{ __('Create polls') }}"><img src="/images/b5gna/icons48/star_yellow_add.png" alt="" width="48" height="48" border="0"><br>{{ __('Create polls') }}</a>
	    @endif
    </div>
</div>
{{-- @if ($theme == 'b5gna') --}}
<script language="JavaScript">
    $(".panelRightBox").after($("#dummyMenu").find(".toolbar"));
</script>
{{-- @endif --}}