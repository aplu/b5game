<div id="dummyMenu" style="display: none;">
    <div class="toolbar">
        @if (in_array('EXCHANGE', $rules))
            <a href="{{ route('exchange') }}" class="bigBTN" title="{{ __('Exchange') }}"><img src="/images/b5gna/icons48/three-bars.png" alt="" width="48" height="48" border="0"><br>{{ __('Exchange') }}</a>
        @endif
        @if (in_array('TRANSACTIONS', $rules))
            <a href="{{ route('transactions-log') }}" class="bigBTN" title="{{ __('Transactions') }}"><img src="/images/b5gna/icons48/transfer_left_right.png" alt="" width="48" height="48" border="0"><br>{{ __('Transactions') }}</a>
        @endif
        @if (in_array('MACROECO', $rules))
            <a href="{{ route('macroeco') }}" class="bigBTN" title="{{ __('Macroeco') }}"><img src="/images/b5gna/icons48/line-chart2.png" alt="" width="48" height="48" border="0"><br>{{ __('Macroeco') }}</a>
        @endif
        @if (in_array('STATES_ECO', $rules))
            <a href="{{ route('states-eco') }}" class="bigBTN" title="{{ __('Economy of states') }}"><img src="/images/b5gna/icons48/pie-chart.png" alt="" width="48" height="48" border="0"><br>{{ __('Economy of states') }}</a>
        @endif
    </div>
</div>
{{-- @if ($theme == 'b5gna') --}}
<script language="JavaScript">
    $(".panelRightBox").after($("#dummyMenu").find(".toolbar"));
</script>
{{-- @endif --}}