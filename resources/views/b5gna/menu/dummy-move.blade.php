<div id="dummyMenu" style="display: none;">
    <div class="toolbar">
        @if (in_array('GROUPS', $rules))
            <a href="/fleet-groups" class="bigBTN" title="{{ __('Groups Management') }}"><img src="/images/b5gna/icons48/group.png" alt="" width="48" height="48" border="0"><br>{{ __('Groups') }}</a>
        @endif
        @if (in_array('PLANS', $rules))
            <a href="{{ route('plans') }}" class="bigBTN" title="{{ __('Move fleet') }}"><img src="/images/b5gna/icons48/move.png" alt="" width="48" height="48" border="0"><br>{{ __('Move') }}</a>
            <a href="{{ route('create-plan') }}" class="bigBTN" title="{{ __('New flight') }}"><img src="/images/b5gna/icons48/plus.png" alt="" width="48" height="48" border="0"><br>{{ __('New flight') }}</a>
        @endif
    </div>
</div>
{{-- @if ($theme == 'b5gna') --}}
<script language="JavaScript">
    $(".panelRightBox").after($("#dummyMenu").find(".toolbar"));
</script>
{{-- @endif --}}