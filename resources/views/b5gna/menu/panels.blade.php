@if (in_array('FORUMS', $rules))
<div class="panel" id="panel-komunikacia" style="position: absolute; top:184px; left:0px; width:100%; display:none; background-image: url('/images/b5gna/menu/panel-komunikacia.png');" onMouseOver="menuOver('komunikacia');" onMouseOut="menuOut();">
	<div class="panelBox">
		<div class="toolbar">
			<a href="{{ route('forums') }}" class="bigBTN" title="{{ __('Forums') }}"><img src="/images/b5gna/icons48/forums.png" alt="" width="48" height="48" border="0"><br>{{ __('Forums') }}</a>
			<div class="smallItemsBox">
				<a class="smallBTN" href="/forum/galactic" title="{{ __('Galactic forum') }}"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>{{ __('Galactic forum') }}</div></a>
				@if (in_array('STATES', $rules))
					<a class="smallBTN" href="/forum/political" title="{{ __('Political forum') }}"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>{{ __('Political forum') }}</div></a>
				@endif
				<a class="smallBTN" href="/forum/chronicle" title="{{ __('Chronicle') }}"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>{{ __('Chronicle') }}</div></a>
			</div>
			<div class="smallItemsBox">
				<a class="smallBTN" href="/forum/newbie" title="{{ __('Newcomer forum') }}"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>{{ __('Newcomer forum') }}</div></a>
				<a class="smallBTN" href="/forum/notices" title="{{ __('Announcements') }}"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>{{ __('Announcements') }}</div></a>
			</div>
			@if (in_array('POLLS', $rules))
				<a href="{{ route('poll-admin') }}" class="bigBTN" title="{{ __('Admin polls') }}"><img src="/images/b5gna/icons48/poll.png" alt="" width="48" height="48" border="0"><br>{{ __('Polls') }}</a>
			@endif
			@if (in_array('MAIL', $rules))
				<a href="{{ route('mail') }}" class="bigBTN" title="{{ __('Mail') }}"><img src="/images/b5gna/icons48/mail.png" alt="" width="48" height="48" border="0"><br>{{ __('Mail') }}</a>
			@endif
			@if (in_array('CIS', $rules))
				<a href="{{ route('cis') }}" class="bigBTN" title="{{ __('Central Information System') }}"><img src="/images/b5gna/icons48/cis.png" alt="" width="48" height="48" border="0"><br>{{ __('Central IS') }}</a>
			@endif
		</div>
	</div>
</div>
@endif

<div class="panel" id="panel-stanica" style="position: absolute; top:184px; left:0px; width:100%; display:none; background-image: url('/images/b5gna/menu/panel-stanica.png');" onMouseOver="menuOver('stanica');" onMouseOut="menuOut();">
	<div class="panelBox">
		<div class="toolbar">
			@if (in_array('OUTPOST', $rules))
				<a href="{{ route('outpost') }}" class="bigBTN" title="{{ __('Outpost') }}"><img src="/images/b5gna/icons48/moonbus.png" alt="" width="48" height="48" border="0"><br>{{ __('Outpost') }}</a>
			@endif
			@if (in_array('TECHNOLOGIES', $rules))
				<a href="{{ route('research-vehicles') }}" class="bigBTN" title="{{ __('Techs') }}"><img src="/images/b5gna/icons48/utilities.png" alt="" width="48" height="48" border="0"><br>{{ __('Techs') }}</a>
			@endif
			@if (in_array('MINING', $rules))
				<a href="{{ route('mining') }}" class="bigBTN" title="{{ __('Mining') }}"><img src="/images/b5gna/icons48/mining.png" alt="" width="48" height="48" border="0"><br>{{ __('Mining') }}</a>
			@endif
			@if (in_array('EXCHANGE', $rules))
				<a href="{{ route('exchange') }}" class="bigBTN" title="{{ __('Exchange') }}"><img src="/images/b5gna/icons48/three-bars.png" alt="" width="48" height="48" border="0"><br>{{ __('Exchange') }}</a>
			@endif
			@if (in_array('TRANSACTIONS', $rules))
				<a href="{{ route('transactions') }}" class="bigBTN" title="{{ __('Resource transaction') }}"><img src="/images/b5gna/icons48/coins.png" alt="" width="48" height="48" border="0"><br>{{ __('Transaction') }}</a>
			@endif
			@if (in_array('MININIG', $rules) || in_array('OUTPOST_RELOCATE', $rules) || in_array('RESOURCES', $rules))
				<div class="smallItemsBox">
					@if (in_array('MINING', $rules))
						<a class="smallBTN" href="{{ route('build-mining') }}" title="{{ __('Build mining') }}"><img src="/images/b5gna/icons16/cube_yellow_add.png" alt="" width="16" height="16" border="0"><div>{{ __('Build mining') }}</div></a>
					@endif
					@if (in_array('OUTPOST_RELOCATE', $rules))
						<a class="smallBTN" href="{{ route('relocate') }}" title="{{ __('Move station') }}"><img src="/images/b5gna/icons16/transfer_left_right.png" alt="" width="16" height="16" border="0"><div>{{ __('Move station') }}</div></a>
					@endif
					@if (in_array('RESOURCES', $rules))
						<a class="smallBTN" href="{{ route('balance') }}" title="{{ __('Resource log') }}"><img src="/images/b5gna/icons16/calendar.png" alt="" width="16" height="16" border="0"><div>{{ __('Resource log') }}</div></a>
					@endif
				</div>
			@endif
		</div>
	</div>
</div>

@if (in_array('VEHICLES', $rules))
<div class="panel" id="panel-flotila" style="position: absolute; top:184px; left:0px; width:100%; display:none; background-image: url('/images/b5gna/menu/panel-flotila.png');" onMouseOver="menuOver('flotila');" onMouseOut="menuOut();">
	<div class="panelBox">
		<div class="toolbar">
			<a href="{{ route('build-vehicles') }}" class="bigBTN" title="{{ __('Build fleet') }}"><img src="/images/b5gna/icons48/build.png" alt="" width="48" height="48" border="0"><br>{{ __('Build') }}</a>
			@if (in_array('REPAIR', $rules))
				<a href="{{ route('repair') }}" class="bigBTN" title="{{ __('Repair fleet') }}"><img src="/images/b5gna/icons48/wrench.png" alt="" width="48" height="48" border="0"><br>{{ __('Repair') }}</a>
			@endif
			@if (in_array('WINGS', $rules))
				<a href="{{ route('wings') }}" class="bigBTN" title="{{ __('Wings Management') }}"><img src="/images/b5gna/icons48/hangar.png" alt="" width="48" height="48" border="0"><br>{{ __('Wings') }}</a>
			@endif
			@if (in_array('GROUPS', $rules))
				<a href="{{ route('groups') }}" class="bigBTN" title="{{ __('Groups Management') }}"><img src="/images/b5gna/icons48/group.png" alt="" width="48" height="48" border="0"><br>{{ __('Groups') }}</a>
			@endif
			@if (in_array('PLANS', $rules))
				<a href="{{ route('plans') }}" class="bigBTN" title="{{ __('Move fleet') }}"><img src="/images/b5gna/icons48/move.png" alt="" width="48" height="48" border="0"><br>{{ __('Move') }}</a>
			@endif
			<a href="{{ route('vehicles') }}" class="bigBTN" title="{{ __('Fleet overview') }}"><img src="/images/b5gna/icons48/space_pod.png" alt="" width="48" height="48" border="0"><br>{{ __('My fleet') }}</a>
			@if (in_array('FIGHTS', $rules))
				<a href="{{ route('mine-fights') }}" class="bigBTN" title="{{ __('My fights') }}"><img src="/images/b5gna/icons48/burning_globe.png" alt="" width="48" height="48" border="0"><br>{{ __('My fights') }}</a>
			@endif
			<a href="{{ route('vehicle-types') }}" class="bigBTN" title="{{ __('Fleet params') }}"><img src="/images/b5gna/icons48/address_book2.png" alt="" width="48" height="48" border="0"><br>{{ __('Params') }}</a>
		</div>
	</div>
</div>
@endif

@if (in_array('STATES', $rules))
<div class="panel" id="panel-politika" style="position: absolute; top:184px; left:0px; width:100%; display:none; background-image: url('/images/b5gna/menu/panel-politika.png');" onMouseOver="menuOver('politika');" onMouseOut="menuOut();">
	<div class="panelBox">
		<div class="toolbar">
			<a href="{{ route('citizens') }}" class="bigBTN" title="{{ __('Citizens') }}"><img src="/images/b5gna/icons48/user-group.png" alt="" width="48" height="48" border="0"><br>{{ __('Citizens') }}</a>
			@if (in_array('FORUMS', $rules) || in_array('STATES', $rules))
				<div class="smallItemsBox">
					@if (in_array('FORUMS', $rules))
						<a class="smallBTN" href="/forum/nation" title="{{ __('State forum') }}"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>{{ __('State forum') }}</div></a>
					@endif
					<a class="smallBTN" href="{{ route('citizenship') }}" title="{{ __('Citizenship') }}"><img src="/images/b5gna/icons16/id_card.png" alt="" width="16" height="16" border="0"><div>{{ __('Citizenship') }}</div></a>
				</div>
			@endif
			@if (in_array('PARTIES', $rules))
				<a href="{{ route('parties') }}" class="bigBTN" title="{{ __('Parties') }}"><img src="/images/b5gna/icons48/family.png" alt="" width="48" height="48" border="0"><br>{{ __('Parties') }}</a>
			@endif
			@if (in_array('NOTICES', $rules))
				<a href="{{ route('noticeboard') }}" class="bigBTN" title="{{ __('Notice board') }}"><img src="/images/b5gna/icons48/note.png" alt="" width="48" height="48" border="0"><br>{{ __('Notice board') }}</a>
			@endif
			@if (in_array('POLLS', $rules))
				<a href="/vote" class="bigBTN" title="{{ __('State polls') }}"><img src="/images/b5gna/icons48/poll.png" alt="" width="48" height="48" border="0"><br>{{ __('Polls') }}</a>
			@endif
			<div class="smallItemsBox">
				@if (in_array('TAXES', $rules))
					<a class="smallBTN" href="{{ route('taxes') }}" title="{{ __('Taxes') }}"><img src="/images/b5gna/icons16/percent.png" alt="" width="16" height="16" border="0"><div>{{ __('Taxes') }}</div></a>
				@endif
				@if (in_array('PENALTIES', $rules))
					<a class="smallBTN" href="{{ route('penalties') }}" title="{{ __('Penalties') }}"><img src="/images/b5gna/icons16/flash.png" alt="" width="16" height="16" border="0"><div>{{ __('Penalties') }}</div></a>
				@endif
				@if (in_array('STATIONS', $rules))
					<a class="smallBTN" href="{{ route('stations') }}" title="{{ __('Stations') }}"><img src="/images/b5gna/icons16/plus.png" alt="" width="16" height="16" border="0"><div>{{ __('Stations') }}</div></a>
				@endif
			</div>
			@if (in_array('RELATIONSHIPS', $rules))
				<a href="{{ route('relationships') }}" class="bigBTN" title="{{ __('Relationships') }}"><img src="/images/b5gna/icons48/relations.png" alt="" width="48" height="48" border="0"><br>{{ __('Relationships') }}</a>
			@endif
			@if (in_array('POLLS', $rules))
				<a href="/polls/state" class="bigBTN" title="{{ __('Votes') }}"><img src="/images/b5gna/icons48/bar-chart.png" alt="" width="48" height="48" border="0"><br>{{ __('Votes') }}</a>
			@endif
			@if (in_array('ELECTION', $rules))
				<a href="{{ route('election') }}" class="bigBTN" title="{{ __('Elections') }}"><img src="/images/b5gna/icons48/signpost.png" alt="" width="48" height="48" border="0"><br>{{ __('Elections') }}</a>
			@endif
			@if (in_array('FORUMS', $rules) || in_array('POLLS', $rules) || (in_array('RESOURCES', $rules) && in_array('STATES', $rules)))
			<div class="smallItemsBox">
				@if (in_array('FORUMS', $rules))
					<a class="smallBTN" href="/forum/gov" title="{{ __('Government forum') }}"><img src="/images/b5gna/icons16/forum.png" alt="" width="16" height="16" border="0"><div>{{ __('Government forum') }}</div></a>
				@endif
				@if (in_array('POLLS', $rules))
					<a class="smallBTN" href="/poll/gov" title="{{ __('Government polls') }}"><img src="/images/b5gna/icons16/poll.png" alt="" width="16" height="16" border="0"><div>{{ __('Government polls') }}</div></a>
				@endif
				@if (in_array('RESOURCES', $rules) && in_array('STATES', $rules))
					<a class="smallBTN" href="{{ route('state-balance') }}" title="{{ __('State resource log') }}"><img src="/images/b5gna/icons16/calendar.png" alt="" width="16" height="16" border="0"><div>{{ __('State resource log') }}</div></a>
				@endif
			</div>
			@endif
		</div>
	</div>
</div>
@endif

@if (in_array('MAP', $rules))
<div class="panel" id="panel-mapa" style="position: absolute; top:184px; left:0px; width:100%; display:none; background-image: url('/images/b5gna/menu/panel-mapa.png');" onMouseOver="menuOver('mapa');" onMouseOut="menuOut();">
	<div class="panelBox">
		<div class="toolbar">
			<a href="{{ route('map') }}" class="bigBTN" title="{{ __('Space map') }}"><img src="/images/b5gna/icons48/map.png" alt="" width="48" height="48" border="0"><br>{{ __('Space map') }}</a>
			@if (in_array('SECTORS', $rules))
				<a href="{{ route('sectors') }}" class="bigBTN" title="{{ __('Sector detail') }}"><img src="/images/b5gna/icons48/planet.png" alt="" width="48" height="48" border="0"><br>{{ __('Detail') }}</a>
			@endif
			@if (in_array('VEHICLES', $rules))
				<a href="{{ route('intel') }}" class="bigBTN" title="{{ __('Intel Center') }}"><img src="/images/b5gna/icons48/satellite.png" alt="" width="48" height="48" border="0"><br>{{ __('Intel') }}</a>
			@endif
			@if (in_array('MAP_SHARING', $rules))
			<div class="smallItemsBox">
				<a class="smallBTN" href="{{ route('map-sharing') }}" title="{{ __('Sharing') }}"><img src="/images/b5gna/icons16/share.png" alt="" width="16" height="16" border="0"><div>{{ __('Sharing') }}</div></a>
			</div>
			@endif
		</div>
	</div>
</div>
@endif

<div class="panel" id="panel-stats" style="position: absolute; top:184px; left:0px; width:100%; display:none; background-image: url('/images/b5gna/menu/panel-stats.png');" onMouseOver="menuOver('stats');" onMouseOut="menuOut();">
	<div class="panelBox">
		<div class="toolbar">
			<a href="{{ route('players') }}" class="bigBTN" title="{{ __('Players') }}"><img src="/images/b5gna/icons48/users.png" alt="" width="48" height="48" border="0"><br>{{ __('Players') }}</a>
			@if (in_array('STATES', $rules))
				<a href="{{ route('states') }}" class="bigBTN" title="{{ __('States') }}"><img src="/images/b5gna/icons48/flags.png" alt="" width="48" height="48" border="0"><br>{{ __('States') }}</a>
			@endif
			@if (in_array('TMC', $rules))
				<a href="{{ route('crystals') }}" class="bigBTN" title="{{ __('TM Crystals') }}"><img src="/images/b5gna/icons48/amethyst.png" alt="" width="48" height="48" border="0"><br>{{ __('TM Crystals') }}</a>
			@endif
			@if (in_array('MACROECO', $rules))
				<a href="{{ route('macroeco') }}" class="bigBTN" title="{{ __('Macroeco') }}"><img src="/images/b5gna/icons48/line-chart2.png" alt="" width="48" height="48" border="0"><br>{{ __('Macroeco') }}</a>
			@endif
			@if (in_array('STATES_ECO', $rules))
				<a href="{{ route('states-eco') }}" class="bigBTN" title="{{ __('Economy of states') }}"><img src="/images/b5gna/icons48/pie-chart.png" alt="" width="48" height="48" border="0"><br>{{ __('Economy of states') }}</a>
			@endif
			@if (in_array('STATS_PLAYERS', $rules))
				<a href="{{ route('players-stats') }}" class="bigBTN" title="{{ __('Players stats') }}"><img src="/images/b5gna/icons48/chart1.png" alt="" width="48" height="48" border="0"><br>{{ __('Players stats') }}</a>
			@endif
			@if (in_array('FIGHT_POINTS', $rules))
				<a href="{{ route('fight-points') }}" class="bigBTN" title="{{ __('Fight points') }}"><img src="/images/b5gna/icons48/swords.png" alt="" width="48" height="48" border="0"><br>{{ __('Fight points') }}</a>
			@endif
			@if (in_array('FIGHTS', $rules))
				<a href="{{ route('fights') }}" class="bigBTN" title="{{ __('Fights') }}"><img src="/images/b5gna/icons48/tear-gas.png" alt="" width="48" height="48" border="0"><br>{{ __('Fights') }}</a>
			@endif
			<div class="smallItemsBox">
				<a class="smallBTN" href="{{ route('players-changes') }}" title="{{ __('Players log') }}"><img src="/images/b5gna/icons16/time_machine.png" alt="" width="16" height="16" border="0"><div>{{ __('Players log') }}</div></a>
			</div>
		</div>
	</div>
</div>

<div class="panel" id="panel-menu" style="position: absolute; top:184px; left:0px; width:100%; display:none; background-image: url('/images/b5gna/menu/panel-menu.png');" onMouseOver="menuOver('menu');" onMouseOut="menuOut();">
	<div class="panelBox">
		<div class="toolbar">
			<a href="/about" class="bigBTN" title="{{ __('Intro') }}"><img src="/images/b5gna/icons48/B5.png" alt="" width="48" height="48" border="0"><br>{{ __('Intro') }}</a>
			<a href="/rules" class="bigBTN" title="{{ __('Terms and conditions') }}"><img src="/images/b5gna/icons48/meeting.png" alt="" width="48" height="48" border="0"><br>{{ __('Terms and conditions') }}</a>
			<a href="/manual" class="bigBTN" title="{{ __('Manual') }}"><img src="/images/b5gna/icons48/lifesaver.png" alt="" width="48" height="48" border="0"><br>{{ __('Manual') }}</a>
			<a href="/b5fans" class="bigBTN" title="{{ __('B5 Fans') }}"><img src="/images/b5gna/icons48/B5fans.png" alt="" width="48" height="48" border="0"><br>{{ __('B5 Fans') }}</a>
			<div class="smallItemsBox">
				<a class="smallBTN" href="https://discord.gg/6P52yfp" target="_blank" title="Discord"><img src="/images/icons/discord.svg" alt="Discord" width="16" height="16" border="0" style="width:20px; height:20px; padding:2px"><div>Discord</div></a>
				<a class="smallBTN" href="https://trello.com/b/56ILmnc0/b5game-open-age" target="_blank" title="Trello"><img src="/images/icons/trello.svg" alt="Trello" width="16" height="16" border="0"><div>Trello</div></a>
				<a class="smallBTN" href="https://gitlab.com/aplu/b5game" target="_blank" title="Gitlab"><img src="/images/icons/gitlab.svg" alt="Gitlab" width="16" height="16" border="0" style="width:24px; height:24px; padding:0px"><div>Gitlab</div></a>
			</div>
			<a href="{{ route('profile') }}" class="bigBTN" title="{{ __('Profile') }}"><img src="/images/b5gna/icons48/user.png" alt="" width="48" height="48" border="0"><br>{{ __('Profile') }}</a>
			@if (in_array('PREREG', $rules))
				<a href="{{ route('prereg') }}" class="bigBTN" title="Predregistrácia do veku"><img src="/images/b5gna/icons48/quiz.png" alt="" width="48" height="48" border="0"><br>Predregistrácia do veku</a>
			@endif
			<a href="{{ route('logout') }}" class="bigBTN" title="{{ __('Logout') }}"><img src="/images/b5gna/icons48/shutdown.png" alt="" width="48" height="48" border="0"><br>{{ __('Logout') }}</a>
		</div>
	</div>
</div>
