@extends('b5gna.layout')

@section('content')

@php
$debug['all'] = true;
@endphp


	@foreach ($pages as $page)
		<a name="{{ $page }}"></a>

		@if (View::exists('b5gna.help.'.$page))
			@section('help')
				@include('b5gna.help.'.$page)
			@endsection
		@endif

		@include('b5gna.pages.'.$page)
	@endforeach

@endsection