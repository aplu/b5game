<p>
Stíhače sú v hre organizované do letiek. Letky je ďalej možné priradiť k bojovej skupine alebo ťažbe. Letka sa skladá z určitého počtu stíhačov rovnakého typu.
Stíhače je možné medzi letkami presúvať za určitých podmienok:<br>
1. obe letky musia byť v jednom sektore<br>
2. obe letky musia byť v pokoji (nemôžu byť v presune, hyperpriestore alebo v boji)<br>
3. nie je možné presúvať stíhače v letkách, ktoré chránia ťažbu<br>
4. letky musia mať dostatok zásob kyslíka<br>
5. stíhače možno presúvať iba medzi letkami s rovnakým modelom stíhačov<br><br>
</p>
<p>
Presun sa zrealizuje výberom zdrojovej letky v stĺpci "Odkiaľ", výberom cieľovej letky v stĺpci "Kam", zadaním presúvaného počtu (dole) a kliknutím
na tlačidlo "Presunúť stíhače medzi letkami". Stíhače je možné presunúť aj do novovytvorenej letky. V takom prípade treba napísať jej meno a v stĺpci "Kam"
vybrať najspodnejšiu položku. Letka, v ktorej neostane ani jeden stíhač, je automaticky zrušená. Pri presune sa upravuje presnosť letiek váženým aritmetickým priemerom.
</p>
<p>
Stíhače potrebujú kyslík, ktorý si môžu dopĺnať v lodiach, hangároch alebo dokoch. Bez kyslíka sú schopné vydržať len obmedzený čas - 24 hodín.
Každý stíhač potrebuje svoje miesto v lodi, doku alebo hangári. Pokiaľ sa v sektore nachádza viac stíhačov ako je miest pre doplnánie zásob, začne sa odpočítavanie zostávajúceho času do vyčerpania zásob.
Zabezpečením ďalšieho miesta sa odpočítavenie zruší. Po vyčerpaní zásob pilot stíhača umiera a stíhač sa nenávratne stratí vo vesmíre.
</p>

<h2>Stratégia letky</h2>
<p>Určuje správanie jednotlivých letky počas útoku.
Môžeš určiť, v akom poradí budú stíhače útočiť na typy lodí nepriateľa.
<b>Príklad:</b> nastavenie na hodnotu "L, S, K, D" znamená, že stíhače
budú najskôr útočiť na lode, keď už nebudú žiadne lode, tak bude útočiť na stíhače,
potom na krížniky a nakoniec na destroyery. V prípade, že brániaci sa hráč nebude mať v skupine žiadnu loď (L),
automaticky sa bude útočiť na ďalšiu možnosť v poradí, v tomto príklade teda na stíhače.
</p>

<h2>Skratky</h2>
S - stíhače<br>
K - krížnik<br>
L - bojová loď<br>
D - destroyer<br>