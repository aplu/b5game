<h1 style="margin-top:0px">Stanica a jej výstavba</h1>

<p>
    Každý hráč, či už občan alebo hráč bez občianstva má svoju stanicu. Tá sa nachádza v niektorom sektore mapy, ktorý tiež nazývame HW (home world) hráča. Stanica spolu s ťažbou tvoria hlavnú ekonomickú časť v hre. Stanica poskytuje priestor pre rôzne typy budov a je tu možnosť robiť viacero vylepšení (upgradov).
</p>
<p>
    Hráč si môže za splnenia podmienok stanicu presunút do iného sektora, čím je možné vylepšiť jednak svoju ekonomickú situáciu ale aj vojensko taktickú situáciu. Vpravo hore môžes vidiet detail stanice, kde je tvoj aktuálny sektor a prepojenie na možnosť presunu stanice do iného sektora.
</p>

<h2>Budovy na stanici</h2>
<div class="highLine">
    <b>Taviaca pec</b> - pri prepočte „pretavuje“ rudu titánia (rTi) na „čisté“ titánium (Ti). Jedna taviaca pec spracuje 1000 ton rudy titánia denne.<br>

    <b>Rafinéria</b> - pri prepočte „rafinuje“ rudu quantia 40 (rQ40) na „čisté“ quantium 40 (Q40). Jedna rafinéria spracuje 1000 ton rudy q40 denne.<br>

    <b>Obchodné centrum</b> - umožňuje obchodovanie 1 obchodnej lodi (počet obchodných centier limituje maximálny počet obchodných lodí).<br>

    <b>Továreň</b> - zrýchluje stavbu všetkých lodí (bojových i ťažobných) o jedno 1% z plného času.<br>

    <b>Hangár</b> - miesto pre ťažobné plavidlá (obchodné lode, ťažobné lode a tankery), stíhače chrániace ťažbu a stíhače, ktoré v sektore tvojej stanice nemajú miesto v lodiach. Každé ťažobné plavidlo (nepatrí sem plošina) potrebuje 1 miesto v hangároch. Jeden hangár má kapacitu 10 miest.<br>

    <b>Vesmírný dok</b> - nutný pre stavbu ťažobných plošín a bojových lodí (okrem stíhačov).<br>

</div>

<h2>Parametre budov</h2>

<p>Vpravo dole pod zobrazenými budovami môžeš otvoriť "Parametre budov". Budovy majú daný svoj minimálny a maximálny počet. Dôležité je miesto, ktoré každá budova zaberá a čas výstavby konkrétnej budovy. Miesto na stanici sa dá zvyšovať (čítaj nižšie o vylepšeniach na stanici). Parameter "súčasná výstavba" hovorí koľko budov súčasne môžes postaviť za "čas výstavby".
    <b>Príklad:</b> zadáš stavbu 40 taviacich pecí. Súčasne sa môže postaviť 20 pecí za dve hodiny, takže stavba 40 pecí bude trvať 4 hodiny.
</p>

<h2>Vylepšenia na stanici</h2>
<div class="highLine">
    Na stanici je možné robiť tieto vylepšenia resp. upgrady:<br>
    <b>efektivita taviacich pecí</b> - zvyšuje efektivitu spracovania rudy titánia na „čisté“ titánium. Maximálny level je 10 = 72%.<br>
    <b>efektivita rafinerií</b> - zvyšuje efektivitu spracovania rudy quantia 40 na „čisté“ quantium 40. Maximálny level je 10 = 36%.<br>
    <b>efektivita obchodovania</b> - zvyšuje výnosnosť obchodným lodiam. Maximálny level je 10 = 90%.<br>
    <b>miesto na stanici</b> - rozširuje miesto stanice na výstavbu budov. Každý level zvyšuje miesto o 100.<br>
</div>

<h2>Možnosti vylepšení a ceny</h2>

<p>Možnosti vylepšenia, čas a aj cenu vylepšenia zobrazíš tak, že na konkrétne vylepšenie presunieš kurzor. Kliknutím vylepšenie zavedieš za uvedený čas. Cena nasledujúceho vylepšenia je vždy o 50% drahšia ako je cena predchádzajúceho vylepšenia (okrem miesta na stanici).
</p>

<h2>Výstavba budov a vylepšení</h2>

<p>Budovy ako aj vylepšenia sa stavajú (zavádzajú) v danom čase v poradí. Nie je teda možné stavať naraz viacero budov a zavádzať viacero vylepšení. Prvá položka sa nedá zrušiť, ďalšie položky v poradí zrušíš "krížikom".
</p>

<h2>Búranie budov</h2>

<p>Budovy na stanici je možné búrať zadaním záporného čísla. Za búranie sa získava 50% surovín späť. Vesmírny dok nie je možné zbúrať, obchodných centier musí zostať minimálne rovnaký počet ako máš obchodných lodí a v hangároch musí ostať dostatok miesta pre ťažbu a ochranné letky.
</p>

<h2>Pomocné tabuľky a odkazy</h2>

<div class="highLine">
    V pravo sa nachádzajú pomocné tabuľky a odkazy. Spomínali sme už "<b>Detail stanice</b>". <br>
    <br> V tabuľke "<b>Hangáre</b>" sa ti zobrazujú nasledovné údaje:<br> - voľné miesto/celkové miesto v hanároch (zvyšuje sa postavením nových hangárov)<br> - ťažobné plavidlá, ktoré práve ťažia + tie, ktoré máš k dispozícii a neťažia<br> - stíhače, ktoré sú priradené k ťažobným skupinám = ochrana ťažby a voľné stíhače na tvojej stanici<br>
    <br> V tabuľke "<b>Produkcia</b>" sa ti zobrazujú tieto údaje a prepojenia:<br> - údaj o štátnom limite ťažby vo formáte voľný/celkový (o tomto limite sa viac dočítaš v manuále)<br> - údaj o produkcii surovín a kreditov<br> - prepojenia na Výrobu ťažby a Správu ťažby
</div>