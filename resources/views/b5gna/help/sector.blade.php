- Ťažobné lode sa zobrazia iba, ak sa v sektore nachádza tažba, vojnová loď alebo obranná stanica tvojej alebo aliančnej rasy.<br>
- Bojové lode a skupiny sa zobrazia iba, ak sa v sektore nachádza vojnová loď alebo obranná stanica tvojej alebo aliančnej rasy.<br>
- Pre zobrazenie lodí a staníc potrebuješ vesmírny dok.<br>
- Počty lodí v ťažobnej skupine: [ťažobné lode, tankery, obchodné lode, plošiny]<br>
- Obsah letky je v sektore vidieť iba, ak je v skupine bez lodí alebo ak je voľná a nie sú tu iné voľné lode<br>
- Na zobrazenie obsahu sektora stačí aj prítomnosť stanice občana štátu alebo spojenca<br>
- Hráč bez občianstva nemôže útočiť na sektor, ale iba na konkretny štát. Na ťažbu môže útočiť aj bez vlastníctva sektora.<br><br>

<h2>Útok</h2>
<p>
Pri zadávaní útoku je možné posunúť začiatok prvého kola útoku, čo umožní pripojiť sa dalším hráčom od prvého kola.
Opozdenie sa zadáva v minútach a max. môže mať dĺžku 60 minút.<br>
</p>

<h2>Bránenie (podpora boja v susednom sektore)</h2>
<p>
Do režimu bránenia môže prejsť iba skupina, v ktorej je aspoň jeden krížnik/loď/destroyer. Stíhače sa bránenia nezúčasťňujú. 
Príprava na bránenie trvá 1,5 dĺžky bojového kola. Počas bránenia nie je možné pridávať do skupiny ďalšie plavidlá.
Režim bránenia sa automaticky zruší pri zadaní presunu a po skončení bojového kola, ak nebol v tomto kole boj ukončený.
</p>
<p>
Brániace lode podporujú boj, ktorý prebieha v susednom sektore. Lode musia byť v neutrálnom sektore alebo v sektore, ktorý vlastní strana zúčastnená v boji.
Podpory sa záčastňujú iba lode hráčov, ktorých štát je účastníkom boja. Podpora je dvojaká:<br>
1) za každý susediaci podporujúci sektor získa podporovaná strana bonus 5% na útok<br>
2) do útoku podporovanej strany sa pridá 30% súčtu útokov na lode všetkých lodí v susediacich sektoroch, ktoré sú v režime bránenia;
bonusový útok sa rozdelí (váhovo) do útoku na krížniky, lode a destroyeri. Maximálna výška tejto podpory je limitovaná dvojnásobkom celkového útoku flotily
(po započítaní obrannej stanice a podpory podľa bodu 1) zapojenej v boji na strane, ktorá má podporu získať.
</p>


<h2>Skenovanie okolia sektora</h2>
<p>
Existenciu susediacich sektorov k danému sektoru možno zistiť pomocou skenovania okolia. Na skenovanie je potrebná prítomnosť flotily hráča (stíhač nestačí).
Skenovanie trvá 8 hodín a o úspešnom dokončení skenovania dostane hráč správu do CIS (centrálny informačný systém).
Skenovanie sa preruší, ak v sektore hráčovi neostane žiadna flotila (okrem stíhačov).
</p>

<h2>Skratky</h2>
S - stíhače<br>
K - krížnik<br>
L - bojová loď<br>
D - destroyer