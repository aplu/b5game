<h1 style="margin-top:0px">Mapa vesmíru</h1>

<p>
Mapa vesmíru predstavuje priestor, kde prebieha hra. Je rozdelená na oblasti - <b>sektory</b>. Jednotlivé sektory môže vlastniť niektorý štát (podľa toho sú zafarbené) alebo môžu byť neutrálne (bez zafarbenia).
Sektory zo skokovou bránou majú názov napísaný bielym textom a sektory s obrannou stanicou majú červené orámovanie. Umiestnením kurzora myši nad sektor sa v hornej časti zobrazia ďalšie informácie.
Kliknutím na sektor sa zobrazí jeho detail.
</p>

<h2>Informácie o sektoroch</h2>
<p>
Hra obsahuje vlastnosť <b>Fog of War</b> (FoW). To znamená, že hráč (štát) vidí iba do sektorov, kde má flotilu alebo je splnená iná podmienka (pozri nižšie).
O ostatných sektoroch nevie nič, ani to či existujú (nie sú vôbec zobrazené).
Skumaním sektorov sa získavajú viaceré informácie, ktoré je následne možné zdieľať s inými hráčmi alebo štátmi (pozri ?Pomoc pre <b>Zdieľanie informácií</b>).
Získané informácie sa automaticky ukladajú a v prípade, že v niektorom predtým objavenom (navštívenom) sektore, hráč už nespĺňa podmienky pre priamu viditeľnosť, tak sa zobrazia uložené informácie.
Takéto sektory sú na mape zobrazené tmavšou farbou oproti sektorom s priamou viditeľnosťou.
</p>

<h2>Podmienky viditeľnosti do sektorov</h2>
Sú dva stupne viditeľnosti do sektorov. Pre viditeľnosť v danom stupni musí byť splnená aspoň jedna z podmienok.<br><br>
<b>Viditeľnosť celého obsahu sektora vrátane flotily a stanice hráča:</b><br>
- hráč má v sektore flotilu (akékoľvek plavidlo alebo stíhač okrem stíhačov pri ťažbe)<br>
- štát má v sektore postavenú obrannú stanicu a je vlastník sektora<br>
- sektor je HW hráčovho štátu<br>
<br>
<b>Viditeľnosť všetkého okrem flotily a stanice hráča:</b><br>
- hráč má v sektore ťažbu<br>
- hráč má v sektore stanicu<br>
- štát má v sektore postavenú skokovú bránu, ťažobnú alebo obchodnú stanicu a je vlastník sektora<br>
- administrátori určili, že do daného sektora budú vidieť všetci hráči<br>