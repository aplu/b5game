<p>
Pre jednoduchšiu manipuláciu sú lode a letky organizované do tzv. bojových skupín. Táto stránka slúži na ich správu.
Bojová skupina sa dá vytvoriť resp. prázdna zrušiť na konci tejto stránky. V tabuľke "Nezaradené lode a letky" je zoznam všetkých
lodí a letiek, ktoré je možné zaradovať do skupín. V stĺpci "Zaradiť" si označ lode a letky, s ktorými chceš pracovať.
Následne si na spodu tabuľky vyber skupinu, do ktorej ich chceš zaradiť a klikni na tlačidlo "Zaradiť lode".
Všetky lode a letky v jednej skupine sa musia nachádzať v tom istom sektore. Vyradenie lode zo skupiny funguje obdobne. So skupinou je
možné manipulovať iba ak je v pokoji. Podrobný obsah skupiny sa dá zobraziť kliknutím na tlačidlo "Zobraziť lode".
</p>

<p>
Stĺpec "Nosnosť stíhačov" zobrazuje, aký počet stíhačov je konkrétna loď schopná odniesť. V bojovej skupine sa môže nachádzať
aj viac stíhačov ako je celková nosnosť lodí. Avšak v takom prípade sa môže bojová skupina pohybovať len po sektoroch, v ktorých sú
skokové brány. Celkévý počet stíhačov a celková nosnosť skupiny je zobrazená v hlavičke vo formáte "počet stíhačov/celková nosnosť".
V prípade, že je v skupine viac stíhačov ako je nosnosť, je tento údaj zobrazený červenou farbou.
</p>

<p>
Pri krížnikoch, lodiach a destroyeroch sa zobrazuje aj údaj o príprave na skok do hyperpriestoru. Lode po príchode do sektora (resp. po výrobe)
začnú nabíjať skokové motory - stav tohto procesu je zobrazený v stĺpci "Príprava skoku". Loď pripravená na skok, môže presjť okamžite do
hyperpriestoru.
</p>

<h2>Stratégia</h2>
<p>Určuje správanie jednotlivých typov lodí počas útoku. Môžeš určiť, v akom poradí budú tvoje lode útočiť na typy lodí nepriateľa.
<b>Príklad:</b> nastavenie na hodnotu "L, S, K, D" znamená, že stíhače
budú najskôr útočiť na lode, keď už nebudú žiadne lode, tak bude útočiť na stíhače,
Potom na krížniky a nakoniec na destroyery. V prípade, že brániaci sa hráč nebude mať v skupine žiadnu loď (L),
automaticky sa bude útočiť na ďalšiu možnosť v poradí, v tomto príklade teda na stíhače.
</p><p>
Pri jednotlivých lodiach a letkách môžeš meniť stratégiu konkrétnej lode/letky.
Ak nastavíš stratégiu skupiny (napr.: "Krížnik - cieľ"), tak sa zadaná stratégia nastaví všetkým plavidlám daného typu (napr.: krížnikom) v danej skupine.
</p>

<h2>Bránenie</h2>
<p>
Počas režimu bránenia resp. počas prípravy na bránenie nie je možné pridávať do skupiny žiadne plavidlá.
Režim bránenia je možné zrušiť pomocou tlačidla "Zrušiť bránenie".
</p>

<h2>Presnosť lodí a letiek</h2>
<p>
Presnosť lode alebo letky a jej percentuálne vyjadrenie určuje úspešnosť útoku v boji (útok sa násobí presnosťou).
Základná presnosť každého plavidla je 50%.
<b>Príklad:</b> tvoja loď (L) má parametre útoku S/L (stíhače/lode) 20'000/46'000. Pri presnosti 50% bude teda sila útoku 10'000/23'000. 
Presnosť sa zvyšuje skúsenosťami z víťazných bojov (zničením súperovej flotily) u všetkých druhoch plavidiel.
</p><br>



<h2>Skratky</h2>
S - stíhače<br>
K - krížnik<br>
L - bojová loď<br>
D - destroyer<br>
HP - Hit points (výdrž lode)<br>