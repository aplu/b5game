<h1 style="margin-top:0px">Ťažba surovín</h1>

<p>
    Ťažba je základný prvok ekonomiky v hre. Získavajú sa ňou suroviny potrebné pre rozvoj a výstavbu flotily. V sektoroch sa ťažia tri suroviny: ruda titánia (rTi), ruda quantia 40 (rQ40) a kredity (Cr). Ťažba prebieha pomocou ťažobných lodí (rTi), tankerov (rQ40) a obchodných lodí (Cr). Ťažiť je možné iba v sektore, ktorý vlastní tvoj štát a nachádza sa tam skoková brána (niektoré rasy bránu nepotrebujú). Na ťažbu mimo sektoru, v ktorom sa nachádza tvoja stanica, potrebuješ ťažobné plošiny (jedna plošina má kapacitu 50 lodí). Ťažba v sektoroch mimo stanice trvá dlhšie (pozri nižšie). V domovskom sektore tvojho štátu (HW) môžeš ťažiť maximálne s 200 loďami, v ostatných sektoroch je počet obmedzený celkovou kapacitou sektora.
</p>
<p>
    Ťažbu ovplyvňujú vlastnosti sektora. Indexy sektora (pre každú surovinu) určujú množstvo suroviny vyťaženej jednou loďou na jeden cyklus ťažby. Udávajú sa ako percento zo základného výťažku danej lode. Kapacita ťažby v sektore určuje celkový počet lodí všetkých hráčov, ktoré tu môžu ťažiť (bez ohľadu na typ lode). Zásoby surovín v sektore (pre každú surovinu samostatne) sú celkové množstvo surovín, ktoré je ešte možné v danom sektore vyťažiť. Keď sa zásoby dostanú na nulu, nie je ďalej možné v tomto sektore danú surovinu ťažiť.
</p>

<table width="60%" cellspacing="0" class="Tab">
    <tr>
        <th>Typ lode</th>
        <th>Surovina</th>
        <th>Základný výťažok</th>
        <th>Príklad indexu</th>
        <th>Výťažok podľa príkladu</th>
    </tr>
    <tr>
        <td>ťažobná loď</td>
        <td>rTi</td>
        <td>3'000 ton</td>
        <td>83</td>
        <td>83% * 3'000 = 2'490 ton</td>
    </tr>
    <tr>
        <td>tanker</td>
        <td>rQ40</td>
        <td>500 ton</td>
        <td>32</td>
        <td>32% * 500 = 160 ton</td>
    </tr>
    <tr>
        <td>obchodná loď</td>
        <td>Cr</td>
        <td>1'000'000</td>
        <td>56</td>
        <td>56% * 1'000'000 = 560'000</td>
    </tr>
</table><br>

<p>
    V prípade obchodných lodí sa ešte výťažok násobí efektivitou obchodovania (pozri Stanicu). Takže ak vezmeme vyššie uvedený príklad a efektivitu obchodovania napríklad 80%, tak počet získaných kreditov bude 560'000 * 80% = 448'000.
</p>

<p>
    Ťažba je organizovaná do skupín. Do skupiny sa vložia lode a následne sa skupina vyšle ťažiť do zadaného sektora. Ťažbu je možné chrániť priradením ochrannej letky stíhačov ku skupine. V zátvorke za názvom letky je zobrazený počet stíhačov, ktoré sa v letke nachádzajú. Ochrannej letke je možné určiť stratégiu. Pre viac informacií pozri ?Pomoc v Flotila-&gtLetky.
</p><br>

<h2>Sektory sa zobrazujú vo formáte</h2>
skratka sektora [index rTi/rQ40/Cr]: voľná kapacita na ťažbu v sektore (udáva sa v počte lodí)<br> symbol <b>+</b> za indexom znamená prítomnosť ťažobnej resp. obchodnej stanice<br><br>

<h2>Výpočet dĺžky cyklu ťažby</h2>
<p>
    Základná dĺžka cyklu ťažby je 12 hodín v domovskom sektore hráča (HW hráča = tam kde má hráč svoju stanicu). Pri ťažbe mimo HW hráča sa k základnej dĺžke pripočítavajú podľa vzdialenosti od HW hráča za každý sektor 0.5 hodiny.
</p>

<h2>Prerušenie ťažby pred skončením cyklu</h2>
<p>
    Ťažbu je možné prerušiť aj pred skončením cyklu. Pokiaľ ťažba prebiehala minimálne jednu hodinu, dostaneš množstvo surovín prislúchajúce vykonanej dĺžke ťažby znížené o sankciu 20%.
</p>

<h2>Hráči bez občianstva</h2>
<p>
    Môžu ťažiť v sektore svojho HW a v sektoroch, kde majú svoju loď (aspoň krížnik) bez ohľadu na vlastníka sektora. Podmienkou je skoková brána (okrem rasy, ktorá ju nepotrebuje).
</p>

<h2>Ťažba v sektore iného štátu a ťažba, ak tvoje HW vlastní iný štát</h2>
<p>
    Pokiaľ nastane počas ťažby zmena vlastníka sektora, ťažba pokračuje ďalej. Avšak 20% naťažených surovín je automaticky "zhabaných" do národného skladu vlastníka sektora a 30% naťažených surovín sa "stratí z dôvodu ťažby na cudzom území". Hráčom bez občianstva sa suroviny "nestrácajú". To isté platí aj v situácii, ak tvoje HW vlastní iný štát. Avšak v takom prípade navyše nie je možné vyslať ochrannú letku s ťažbou mimo HW.
</p>

<h2>Kapacita hangárov</h2>
<p>
    Každé ťažobné plavidlo (okrem plošiny) potrebuje 1 miesto v hangároch. Jeden hangár má kapacitu 10 miest. Hangáre je možné stavať v stanici. Okrem ťažobných plavidiel zaberajú miesto v hangároch aj stíhače chrániace ťažbu a stíhače, ktoré v sektore tvojej stanice nemajú miesto v lodiach.
    <!--<b>Voľné miesto pre ochranné letky</b> je maximálny počet stíhačov, ktoré je ešte možné priradiť k ochrane ťažby 
(je to súčet voľného miesta v hangároch a počtu voľných stíhačov, ktoré zaberajú miesto v hangároch).-->
</p>

<h2>Štátny limit ťažby</h2>
<p>
    Je limit, ktorý určuje maximálny počet ťažobných plavidiel, ktoré môže jeden občan v štáte vyslať ťažiť. Vypočítava sa na základe počtu sektorov vo vlastníctve štátu a počtu občanov. Pre hráčov bez občianstva pozri podrobnejšie informácie v manuáli.
</p>

<h2>Limit obchodných lodí</h2>
<p>
    Je určený počtom obchodných centier, ktoré máš postavené na stanici. Ak ho chceš zvýšiť postav ďalšie obchodné centrá.
</p>

<br>