<h2 style="margin-top:0px">V texte možno použiť tieto formátovacie značky</h2>

<table style="width: 270px;">
    <tr>
        <td><span style="font-weight:bold;">Hrubé písmo</span></td>
        <td><span class="colHl">+b+</span> text <span class="colHl">-b-</span></td>
    </tr>
    <tr>
        <td><i>Kurzíva</i></td>
        <td><span class="colHl">+i+</span> text <span class="colHl">-i-</span></td>
    </tr>
    <tr>
        <td><u>Podčiarknuté</u></td>
        <td><span class="colHl">+u+</span> text <span class="colHl">-u-</span></td>
    </tr>
    <tr>
        <td><strong><span class="colHl">Zvýrazniť</span></strong></td>
        <td><span class="colHl">+hl+</span> text <span class="colHl">-hl-</span></td>
    </tr>
    <tr>
        <td><span style="font-size:10px">Malé písmo</span></td>
        <td><span class="colHl">+s+</span> text <span class="colHl">-s-</span></td>
    </tr>
    <tr>
        <td><span style="font-size:16px">Veľké písmo</span></td>
        <td><span class="colHl">+big+</span> text <span class="colHl">-big-</span></td>
    </tr>
</table>