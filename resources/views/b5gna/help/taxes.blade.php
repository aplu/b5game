- Tvoje dane - hodnota, ktorú zaplatíš pri najbližšom prepočte.<br><br>
<!--
- VAT (resp. DPH) - platí predajca na našej burze z príjmu z predaja; bez ohladu na rasovú prísplušnosť<br>
- CLO - platí sa pri akomkoľvek dovoze suroviny do krajiny:<br>
 a) cudzí občania pri predaji na našej burze<br>
 b) domáci občania pri dovoze suroviny z inej burzy<br>
 platí sa v dovážanej surovine.<br><br>
-->
<h2>Maximálne hodnoty daní</h2>
<table width="50%" cellspacing="0" class="Tab">
    <tr>
        <th>Druh dane</th>
        <th>Výška</th>
        <th>Jednotka</th>
    </tr>
    <tr>
        <td>Daň zo stanice</td>
        <td>10 mil</td>
        <td>Cr / deň</td>
    </tr>
    <tr>
        <td>Daň z ťažobnej lode</td>
        <td>500</td>
        <td>Ti / deň</td>
    </tr>
    <tr>
        <td>Daň z obchodnej lode</td>
        <td>200'000</td>
        <td>Cr / deň</td>
    </tr>
    <tr>
        <td>Daň z tankera</td>
        <td>50</td>
        <td>Q40 / deň</td>
    </tr>
    <tr>
        <td>Daň z vojnovej lode</td>
        <td>10 mil</td>
        <td>Cr / deň</td>
    </tr>
    <!--
<tr><td>VAT</td><td>10</td><td>%</td></tr>
<tr><td>CLO</td><td>10</td><td>%</td></tr>
-->
</table>