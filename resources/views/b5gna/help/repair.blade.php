<p>
Zapojením sa do bojov môžu byť lode poškodené. Stav lode je vyjadrený v precentách tzv. HP. 100% znamená, že lod nie je poškodená. Lode je možné opravovať.
Na tejto stránke je zoznam všetkých tvojich poškodených lodí. Ku každej lodi je vypočítaná cena a čas potrebný na opravu.
Opravovať je možné loď, ktorá je v pokoji za týchto podmienok:<br>
- poškodenie do 35% je možné opraviť kdekoľvek<br>
- poškodenie do 80% je možné opraviť len vo vlastnom sektore s obrannou stanicou<br>
- poškodenie väčšie ako 80% je možné opraviť len v tvojom HW<br>
</p>
<p>
Spustením opravy sa loď automaticky vyradí z bojovej skupiny, počas opravy ju nebude možné zaradiť späť.
V prípade útoku na opravujúcu sa loď, sa opravy prerušia, loď bude opravená iba adekvátne k už uplynutému času opravy.
Nevyužité suroviny na opravu prepadajú!<br>

<h2>Skratky:</h2>
HP - Hit points (výdrž lode)<br>
HW - Home World<br>
OS - Obranná stanica
</p>