<p>
Táto stránka zobrazuje zoznam všetkých lodí, ktoré sa nachádzajú v sektoroch, z ktorých máš k dispozícii aktuálne informácie (pozri ?Pomoc na stránke Mapa -> Zdieľanie informácií).
Stĺpec "štát" zobrazuje vlastníka lodí a stĺpec "vzťah" politický vzťah medzi ním a Tvojim štátom v tvare "naš vzťah k nim"/"ich vzťah k nám".
Ak má niektorý štát v sektore iba stiháče, je v kolonke "lode" údaj "iba stíhače".
Bližšie informácie o flotile v sektore je možné získať zobrazením detailu konkrétneho sektora (kliknutím na skratku sektora v stĺpci "sektor").
</p>