<p>
Táto stránka slúži na prevod surovín medzi hráčmi. Povolené množstvo prevedenej suroviny je limitované maximálnym množstvom, ktoré je povolené previesť za 3 dni
a rozvinutosťou (ekonomickou a vojenskou) príjemcu. Nie je možné posielať suroviny výrazne rozvinutejšiemu hráčovi. Platí pravidlo: čím je hráč menej rozvinutý voči
odosielateľovi, tým viac surovín je mu možné poslať.<br>
V tabuľke nájdeš: celkový limit na 3 dni; limit, ktorý máš momentálne k dispozícii (odrátané už prevedené suroviny); tvoje suroviny a aké maximálne množstvo surovín môžeš previesť.
Po výbere príjemcu (vyber hráča a potvrď prevod s nulovými surovinymi) sa zobrazia aj limity, ktoré možno poslať konkrétnemu hráčovi.
Riadok "<b>Maximálny prevod</b>" je najdôležitejší - zobrazuje, aké množstvo surovín môžeš v danom momente previesť.
Prevod vykonáš výberom príjemcu, zadaním množstva suroviny a kliknutím na "Odoslať suroviny" v okne "Previesť suroviny".
Na spodu stránky je prehľad prevodov a ich sumár za posledných obdobie spadajúce do limitu.
</p>