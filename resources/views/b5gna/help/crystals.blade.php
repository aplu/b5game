<p>
TM kryštály (TMK) sú, dá sa povedať, "body", ktoré majú v hre určitú hodnotu. Môžeš ich získať, stratiť alebo minúť. Ak sa chceš dozvedieť viac o ich možnostiach a použití, choď do sekcie Hlavné menu, podstránky Pravidlá, kde nižšie nájdeš "Pravidlá TM kryštálov"
<br>
Na tejto stránke je prehľad TOP 10 hráčov so ziskom TM Kryštálov (v troch štatistických pohľadoch). V každej tabuľke je aj údaj o počte tvojich TM kryštálov a pozícia medzi hráčmi. Zároveň môžes vidieť svoj Denník získavania a použitia TM kryštálov.
</p>