Najskôr pozri sekciu <b>?Pomoc</b> na stránke <b>Stanica->Ťažba</b>.<br><br>

<h2>Druhy ťažobných plavidiel</h2>

<b>Ťažobná loď</b> - ťaží rudu Titánia (rTi); jedna ťažobná loď vyťaži za jeden ťažobný cyklus <b>3'000 ton</b> rudy<br>
<b>Tanker</b> - ťaží rudu Q40 (rQ40); jeden tanker za jeden ťažobný cyklus prinesie <b>500 ton</b> rudy<br>
<b>Obchodná loď</b> - uskutočňuje obchody; jedna obchodná loď zarobí za jeden cyklus <b>1'000'000 kreditov</b> (Cr)<br>
<b>Ťažobná plošina</b> - je potrebná na ťažbu mimo sektora tvojej stanice; kapacita jednej plošiny je 50 ťažobných lodí<br><br>

<p>
    Každý sektor má však svoj ťažobný/obchodný index pre Ti/Q40/Cr. Základné množstvo 3'000 ton rudy Ti / 500 ton rudy Q40 / 1'000'000 kreditov sa teda ešte násobí indexom. Index možno zvýšiť postavením tažobnej alebo obchodnej stanice v danom sektore.<br><br>
</p><br>

<h2>Kapacita hangárov</h2>
<p>
    Každé ťažobné plavidlo (okrem plošiny) potrebuje 1 miesto v hangároch. Jeden hangár má kapacitu 10 miest. Hangáre je možné stavať v stanici. Okrem ťažobných plavidiel zaberajú miesto v hangároch aj stíhače chrániace ťažbu a stíhače, ktoré v sektore tvojej stanice nemajú miesto v lodiach.
</p>

<h2>Štátny limit ťažby</h2>
<p>
    Je to limit, ktorý určuje maximálny počet ťažobných plavidiel, ktoré môže jeden občan v štáte vyslať ťažiť. Vypočítava sa na základe počtu sektorov vo vlastníctve štátu a počtu občanov.
</p>

<h2>Limit obchodných lodí</h2>
<p>
    Je určený počtom obchodných centier, ktoré máš postavené na stanici. Ak ho chceš zvýšiť postav ďalšie obchodné centrá.
</p>

<br> - zadaním kladného čísla lode staváš, záporným búraš<br> - základná doba výroby jednej ťažobnej lode, obchodnej lode a tankera je 7 minút<br> - každá továreň znižuje túto dobu o 1% (teda 50 tovární zníži dobu výstavby o 50%)<br> - výroba jednej ťažobnej plošiny trvá 2 hodiny; počet továrni nemá na dobu výroby vplyv<br> - cenu búrania nájdeš v spodnej tabuľke; z búrania sa nezískavajú žiadne suroviny<br> - na stavbu ťažobnej plošiny potřebuješ vesmírný dok<br>