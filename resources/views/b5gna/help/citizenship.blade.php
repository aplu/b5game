<p>
Táto stránka slúži na správu Tvojej príslušnosti k štátu - občianstva. Môžeš sa ho vzdať a ak je to v danom veku povolené, tak následne požiadať o občianstvo v inom štáte.
Prípadne môžes spoločne s ďalšími hráčmi založiť nový štát (v takom prípade treba dať požiadavku na TM - administrátorov).
</p>