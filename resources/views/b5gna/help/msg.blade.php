<h1 style="margin-top:0px;">Formátovanie textu</h1>

<table>
    <tr>
        <td>+b+</td>
        <td><b>tučné písmo</b></td>
        <td>-b-</td>
    </tr>
    <tr>
        <td>+i+</td>
        <td><i>kurzíva</i></td>
        <td>-i-</td>
    </tr>
    <tr>
        <td>+u+</td>
        <td><u>podčiarknuté</u></td>
        <td>-u-</td>
    </tr>
    <tr>
        <td>+big+</td>
        <td>
            <font size="5">veľké písmo</font>
        </td>
        <td>-big-</td>
    </tr>
</table>
<br> Správy staršie ako 7 dní, ktoré nie sú v archíve a sú prečítané, budú zmazané!<br> Správy staršie ako 14 dní sa zmažú aj keď nebudú prečítané!