<h1 style="margin-top:0px">Politický systém Zemskej Aliancie</h1>
<p>
    Základom politického systému Zemskej Aliancie sú politické strany (skupina občanov s rovnakými názormi). Každý občan si môže založiť vlastnú stranu. O členstvo v strane je potrebné požiadať predsedu strany, ktorý ho môže schváliť alebo zamietnúť. Zo strany môže občan kedykoľvek vystúpiť. Predseda strany môže predsedníctvo prenechať inému členovi strany. Strana, ktorá má troch alebo viac členov, môže kandidovať v parlamentných voľbách. Kandidatúru potvrdzuje predseda strany. Parlament má 6 poslancov (členov). Troch menuje predseda víťaznej strany vo voľbách z radov členov svojej strany. Predseda druhej strany menuje dvoch a predseda tretej strany jedného poslanca. Parlament má zákonodarnú moc. Hlavou štátu je prezident. Je volený priamou voľbou. Na prezidenta môže kandidovať každý občan. Prezident na odporúčanie parlamentu menuje vládu - ministrov. Pokiaľ poslanci v parlamente získajú 5 hlasov, môžu odvolať prezidenta, čím sa začnú nové prezidentské voľby. Poslanci môžu odvolať aj jednotlivých ministrov a následne odporučiť prezidentovi vymenenovať nových. Na odvolanie ministra je potrebné získať 4 hlasy. Parlament môže vyhlásiť predčasné voľby. Potrebuje na to 4 hlasy. Vyhlásením predčasných volieb sa začínajú nové parlamentné voľby. Pred vyhlásením predčasných volieb parlament môže (ale nemusí) odvolať prezidenta a ministrov.
</p>

<h2>Politické právomoci</h2>
<b>Prezident</b> - má všetky právomoci<br>
<b>Minister zahraničia</b> - nastavuje politické a obchodné vzťahy<br>
<b>Minister financií</b> - nastavuje dane, udeľuje pokuty, zadáva stavby staníc<br>
<b>Minister vnútra</b> - vidí detaily občanov, píše na vývesku a nastavuje zdieľanie informácií<br>
<!--
<b>Minister výstavby</b> - zadáva stavby staníc<br>
-->
<h2>Priebeh hlasovania</h2>
<p>Štandartná dĺžka volieb je 3 dni. Ak nie je po tomto čase jednoznačný výsledok, voľby sa predlžujú o 1 hodinu, až kým sa nedospeje k jednoznačnému výsledku. Ak vo voľbách kandiduje menej uchádzačov, ako je počet poskytnutých postov, tieto miesta prepadajú a voľby vyhrajú iba zúčasťnení kandidáti. Vo voľbách má každý jeden hlas.
</p>