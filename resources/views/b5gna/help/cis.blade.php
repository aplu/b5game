<p>
Centrálny informačný systém (<strong>skratka CIS</strong>) poskytuje na jednom mieste informácie z rôznych častí hry.
Informácie sú rozdelené do kategórií, medzi ktorými sa dá prepínať kliknutím na názov kategórie nad tabuľkou:<br><br>

<b style="color:#ff8080;">admin</b> - správy od administrátorov hry<br>
<b style="color:#40ff70;">prepočet</b> - informácie o výsledku prepočtu<br>
<b style="color:#ddee40;">ťažba</b> - informácie o výsledku ťažby<br>
<b style="color:red;">flotila</b> - informácie vojenského charakteru<br>
<b style="color:#70baff;">politika</b> - informácie politického charakteru<br>
<b style="color:#9955FF;">vláda</b> - správy od vlády<br>
<br>
<h1>Nastavenie označovania prečítaných správ podľa kategórie</h1>
- umožňuje nastaviť spôsob upozornenia a označovania nových správ<br>
<b class="hl">Označiť ručne</b> - správa bude označená ako prečítaná až po kliknutí na ikonu obálky pri správe<br>
<b class="hl">Označiť po prvom zobrazení</b> - správa sa automaticky označí ako prečítaná po prvom zobrazení<br>
<b class="hl">Bez upozornenia na novú správu</b> - o takejto správe nebude systém informovať ikonou v paneli vedľa menu<br><br>
</p>
<p>
Správu je možné zmazať kliknutím na ikonu červeného kríža. Mazanie viacerých správ naraz sa dá vykonať zaškrtnutím políčka pod červeným krížom
a kliknutím na tlačidlo "Zmazať označené správy" pod tabuľkou.<br>
</p>