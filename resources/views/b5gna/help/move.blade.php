<p>
Ak sa v bojovej skupine nachádza viac stíhačov, ako je celková nosnosť stíhačov všetkých lodí v skupine, tak je počet stíhačov zobrazený červenou farbou.
V takomto prípade je možné presúvať skupinu iba po sektoroch, v ktorých sa nachádzajú skokové brány.
</p>

<p>
Krížniky, lode a destroyery si po príchode do sektora (resp. po výrobe) začnú nabíjať skokové motory. Stav jednotlivých lodí je vidieť v správe skupín.
Tu sa v prehľade skupín zobrazuje stav najhoršie pripraveného plavidla. Po odštartovaní presunu nastane skok do hyperpriestoru podľa najhoršej lode
zaradenej v letovom pláne. Pokial sú všetky lode pripravené na skok, prechod do hyperpriestoru nastane okamžite po odštartovaní. Skupiny zložené
iba z letiek stíhačov sa nedokážu pripravovať na skok. Po štarte presunu takejto skupiny začne odpočet plného času prípravy.
</p>

<p>
Pri presunoch je možné využívať iba skokové brány patriace štátu hráča alebo štátu, ktorý má k štátu hráča vzťah "aliancia" alebo "povolenie prechodu".
</p>

<h2>Skratky</h2>
S - stíhače<br>
K - krížnik<br>
L - bojová loď<br>
D - destroyer<br>