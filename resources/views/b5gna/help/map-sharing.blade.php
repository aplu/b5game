<h1 style="margin-top:0px">Zdieľania informácií o sektoroch</h1>

<p>
Vlastníkom informácií o sektore môže byť hráč alebo štát. Pre spôsob získavania si prečítaj ?Pomoc pre <b>Mapa vesmíru</b>.
Hráč môže získané informácie zdieľať s inými hráčmi alebo štátmi (štát ich môže ďalej zdieľať), pričom platia určité pravidlá.
</p>

<h2>Pravidlá zdieľania</h2>
- informácie získané zdieľaním hráč nemôže ďalej zdieľať<br>
- informácie, ktoré získa štát od hráča, môže ďalej zdieľať hráčom aj štátom<br>
- informácie, ktoré získa štát od iného štátu, môže ďalej zdieľať iba svojim občanom<br>

<h2>Zdieľanie od hráča</h2>
<p>
Po nastavení zdieľania hráčom, prijímateľ informácií (hráč alebo štát) bude dostávať všetky informácie získané hráčom od momentu nastavenia zdieľania.
Informácie získané pred týmto momentom nebudú zdieľané. Po zrušení zdieľania prijímateľovi ostávajú získané informácie.
</p>

<h2>Zdieľanie od štátu</h2>
<p>
Po nastavení zdieľania štátom, prijímateľ informácií (hráč alebo štát) získa všetky informácie, ktoré vlastní štát (aj z minulosti).
Po zrušení zdieľania prijímateľ príde o všetky informácie, ktoré mu boli daným štátom zdieľané.<br>
<b>POZOR:</b> Ak hráč príde o informácie o sektoroch, ktoré sú súčasťou letového plánu, tieto sektory budú z letového plánu vyradené a let bude zastavený v poslednom známom sektore!
</p>

<h2>Obmedzenia zdieľania informácií</h2>
<b>Pre občana štátu</b> (okrem politického systému Drakhov)<br>
- občan môže zdieľať informácie iba so svojim štátom<br>
<br>
<b>Pre občanov štátu s politickým systémom Drakhov</b><br>
- občan môže zdieľať informácie s inými občanmi vlastného štátu alebo štátov, s ktorými je v aliancii<br>
- občan môže zdieľať informácie so štátmi v aliancii<br>
<br>
<b>Pre hráčov bez občianstva</b><br>
- hráč môže zdieľat informácie s ktorýmkoľvek hráčom alebo štátom<br>
<br>
<b>Pre štáty</b> (okrem štátov s politickým systémom Drakhov)<br>
- štát môže zdieľat informácie s ktorýmkoľvek hráčom alebo štátom<br>
<br>
<b>Pre štáty s politickým systémom Drakhov</b><br>
- tieto štáty nemôžu vlastniť informácie o sektoroch a preto ich nemôžu ani zdieľať<br>