<b>Dane:</b><br> - VAT - platí predajca z príjmu z predaja štátu, v ktorom sa nachádza burza<br> - CLO - platí sa pri akomkoľvek dovoze suroviny do krajiny (cudzí občania pri predaji na burze; domáci občania pri dovoze suroviny z inej burzy); platí sa v dovážanej surovine.<br> - na Galaktickej burze nie sú žiadne dane<br><br>

<b>Ponuka:</b><br> - umiestni sa na burzu ponúknuté množstvo suroviny<br> - umiestnenie trvá určitý náhodný čas<br><br>

<b>Dopyt:</b><br> - vloží sa do systému dopyt na surovinu<br> - za dopyt sa skladá záloha v celkovej výške dopytu (v prípade lacnejšieho nákupu sa rozdiel vráti)<br> - v momente prítomnosti vyhovujúcej ponuky k dopytu sa zrealizuje predaj<br><br>

<b>Minimálne a maximálne ceny:</b><br>
<br>
<table width="60%" cellspacing="0" class="Tab">
    <tbody>
        <tr>
            <td class="TabH1">komodita</td>
            <th>ti</th>
            <th>q40</th>
            <th>rti</th>
            <th>rq40</th>
        </tr>
        <tr>
            <th>minimum<br>maximum</th>
            <td>
                <font color="lime">200</font><br>
                <font color="red">3'000</font>
            </td>
            <td>
                <font color="lime">1'800</font><br>
                <font color="red">15'000</font>
            </td>
            <td>
                <font color="lime">50</font><br>
                <font color="red">1'000</font>
            </td>
            <td>
                <font color="lime">500</font><br>
                <font color="red">5'000</font>
            </td>
        </tr>
    </tbody>
</table><br>