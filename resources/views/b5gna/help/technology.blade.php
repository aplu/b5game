<p>
    Na tejto stránke je zoznam technológií, ktoré máš k dispozícii. Technológie sú viazané na rasu, štát, alebo konkrétneho hráča, ktorý ich môže získať za splnenie špeciálnych úloh. Technológiu treba brať ako "návod" alebo "manuál" na výrobu niečoho alebo zlepšenie nejakého procesu. Samotný prístup k "návodu" ešte neznamená automatické využitie technológie. Využitie vyžaduje úpravu výrobnej linky, zaškolenie personálu, zmenu technického vybavenia a pod. Zavedenie technológie do praxe niečo stojí a trvá určitý čas. Práve to je funkcia tejto stránky - zavádzať technológie do praxe konkrétneho hráča.
</p>
<p>
    Prístupnosť niektorých technológií je viazaná na dosiahnutý level hráča. Garantovaný level predstavuje level hráča, ktorý má vyšší level ako 40% hráčov. Hráč má prístup ku všetkým technológiám, ktoré vyžadujú menší alebo rovnaký level ako je garantovaný a to aj v prípade, že level hráča je nižší.
</p>
<p>
    Rasové technológie, či už sú alebo nie sú zavedené, zostanú každému hráčovi podľa jeho rasy (rasa sa počas veku nemení). Štátne technológie a aj možnosť ich zavedenia hráčovi po zmene občianstva nezostávajú, okrem dvoch výnimiek - viac ti povie manuál v časti Stanica - Technológie.
</p>
