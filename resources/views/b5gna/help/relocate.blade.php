<p>
    Táto funkcia umožňuje presúvať stanicu medzi sektormi. Na presun je potrebné splniť viacero podmienok. Tie sa zobrazia po zadaní cieľového sektora. V prípade ich splnenia je v ďalšom kroku umožnený presun stanice. Cena za presun sa vypočítava podľa celkového miesta v stanici a zvyšuje sa za každých „300“ voľného miesta, čiže za každé 3 lvl miesta na stanici (začína na lvl 6, potom 9, 12 atď.). Pri každom zvýšení sa pripočíta k aktuálnej cene suma vo výške základnej ceny.

</p>
