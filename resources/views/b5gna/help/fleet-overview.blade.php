<p>
V tejto tabuľke nájdeš prehľad všetkých tvojích lodí a letiek podľa skupín, do ktorých sú zaradené. Pri skupinách v pohybe je zobrazený sektor najbližieho skoku aj cieľový sektor.
V stĺpeci "súčet" sa nachádzajú počty lodí a stíhačov v skupine podľa triedy (Stíhače, Križnik, Loď, Destroyer). Stĺpec "obsah" zobrazuje počet lodí a stíhačov podľa modelu.
Je tu možné vidieť aj priemerný stav v % (100% predstavuje lode bez poškodenia). Vládny predstavitelia s dostatočnou funkciou vidia lode všetkých občanov.<br>
</p>

<h2>Skratky</h2>
S - stíhače<br>
K - krížnik<br>
L - bojová loď<br>
D - destroyer<br>