<p>
Toto miesto slúži na výrobu flotily. Na spodu stránky je zobrazený zoznam parametrov a cien všetkých modelov lodí a stíhačov, ktoré môžeš vyrábať.
Výrobu spustíť v tabuľke "Zadaj stavbu" napísaním mena novej lode resp. zadaním počtu stíhačov a následním kliknutím na tlačidlo "Postaviť".
Pri stíhačoch môžeš zadať akýkoľvek počet, avšak naraz sa môže vyrábať max. 12 stíhačov. Pri zadaní väčšieho počtu sa výroba rozdelí na
viac výrobných dávok po 12 stíhačov. V jednom okamžiku sa môže vyrábať iba jedna loď alebo jedna dávka stíhačov. Ostatné sa zaraďujú do
fronty, ktorá je zobrazená v stĺpci "Vo výstavbe". Práve prebiehajúcu výrobu nie je možné zrušiť, ale lode/stíhače, ktoré sa ešte nezačali vyrábať,
sa dajú zrušiť kliknutím na "Zrušiť výrobu".<br>
</p>
<p>
Dobu výroby flotily je možné skrátiť postavením továrni v sekcii výstavby stanice. Každá továreň skracuje čas výroby o 1% času. Skracovanie platí
pri výrobe stíhačov aj pri výrobe lodí.
</p>

<h2>Skratky</h2>
S - stíhač<br>
K - krížnik<br>
L - loď<br>
D - destroyer<br>
HP - hit point (výdrž lode)<br>
Útok S/L - sila útoku na stíhače/lode<br>