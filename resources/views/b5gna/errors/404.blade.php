@if ($debug['all'] ?? false)
<div class="mainTitle"><a href="/404">Chyba 404: Nenalezeno</a></div>
@else
<div class="mainTitle">Chyba 404: Nenalezeno</div>
@endif
<div class="mainTitleFade"></div>
@include('b5gna.help.__button')

<div class="content" style="text-align: center;">

	<div class="warningBox">
		<span style="color:red">Neexistující obsah</span>
	</div>

	<div style="clear:both"></div>
</div>