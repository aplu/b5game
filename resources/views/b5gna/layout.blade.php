<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<title>Babylon 5 Game: Open Age</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="author" content="B5fans, Lukáš Hríbik - e.lucas">
	<meta name="keywords" content="online game, online hra, babylon 5 game, B5, B5Game">
	<meta name="description" content="online hra na motivy serialu Babylon 5">
	<meta http-equiv="content-language" content="{{ app()->getLocale() }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" href="/images/b5gna/favicon-normal-32x32.png" type="image/png">
	<link href="{{ asset('css/b5gna/style.css') }}?8" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script src="https://code.jquery.com/jquery-1.4.4.min.js"></script>
	<script src="{{ asset('js/b5gna/app.js') }}?9"></script>
	{{-- @include('b5gna.parts.fb-pixel') --}}
	@stack('head')
</head>
<body>

<div class="pageTop">
	<div class="infoBox">
		@if (Auth::user())
		<a href="{{ route('profile') }}">
			@if ($player)
			{{-- Od <strong>{{ config('b5g.since') }}</strong> prebieha vek <strong>{{ config('b5g.age') }}</strong><br> --}}
				{{ __('Age') }}: <strong>{{ /*config('b5g.age')*/ $player->age->name }}</strong><br>
			@else
				<a href="{{ route('profile') }}">{{ __('No player selected') }}</a><br>
			@endif
			@if (isset($player->name))
				{{ __('Player') }}: <strong>{{ $player->name }}</strong><br>
			@endif
		</a><br>
		@endif

		Je tu prítomných <strong id="usersOnline">?</strong> z <strong id="usersCount">??</strong> hráčov

		<div class="indBox">
			@if (isset($player->msg_count))
			<a name="indMsg" id="indMsg" href="{{ route('mail') }}" class="indIcon" title="{{ $player->msg_count ? 'Nových správ: ' . $player->msg_count : 'Nemáš žiadnu novú správu.' }}" style="background-image: url('/images/b5gna/ind-msg{{ $player->msg_count ? '' : '-gray' }}.png')">
				<div class="indCount"{!! $player->msg_count ? '' : ' style="display: none"' !!}>{{ $player->msg_count ? $player->msg_count : '' }}</div>
			</a>
			@endif
			@if (isset($player->cis_count))
			<a name="indCis" id="indCis" href="{{ route('cis') }}" class="indIcon" title="{{ $player->cis_count ? 'Počet neprečítaných informácii v CIS: ' . $player->cis_count : 'Nemáš žiadnu neprečítanu informácii v CIS.' }}" style="background-image: url('/images/b5gna/ind-cis{{ $player->cis_count ? '' : '-gray' }}.png')">
				<div class="indCount"{!! $player->cis_count ? '' : ' style="display: none"' !!}>{{ $player->cis_count ? $player->cis_count : '' }}</div>
			</a>
			@endif
			@if (isset($player->fight_count))
			<a name="indFight" id="indFight" href="{{ route('mine-fights') }}" class="indIcon" title="{{ $player->fight_count ? 'Počet bojov: ' . $player->fight_count : 'Momentálne sa nezúčastňuješ žiadneho boja.' }}" style="background-image: url('/images/b5gna/ind-attack{{ $player->fight_count ? '' : '-gray' }}.png')">
				<div class="indCount"{!! $player->fight_count ? '' : ' style="display: none"' !!}>{{ $player->fight_count ? $player->fight_count : '' }}</div>
			</a>
			@endif
		</div>
		<div id="B5time"></div>
		<script language="javascript">ShowB5Time({{ time() }}, 254);</script>
	</div>
</div>

<div class="menuPanel">
@auth
	@if ($player)
		@if (in_array('FORUMS', $rules))
			<div class="menuBTN" id="menu-komunikacia" style="background-image: url('/images/b5gna/menu/komunikacia.png'); float: left;" onMouseOver="menuOver('komunikacia');" onMouseOut="menuOut();" onClick="location.href='/forum'">{{ __('Communication') }}</div>
		@endif
		<div class="menuBTN" id="menu-stanica" style="background-image: url('/images/b5gna/menu/stanica.png'); float: left;" onMouseOver="menuOver('stanica');" onMouseOut="menuOut();" onClick="location.href='{{ route('outpost') }}'">{{ __('Outpost') }}</div>
		@if (in_array('VEHICLES', $rules))
			<div class="menuBTN" id="menu-flotila" style="background-image: url('/images/b5gna/menu/flotila.png'); float: left;" onMouseOver="menuOver('flotila');" onMouseOut="menuOut();" onClick="location.href='/fleet-groups'">{{ __('Fleet') }}</div>
		@endif
		@if (in_array('STATES', $rules))
			<div class="menuBTN" id="menu-politika" style="background-image: url('/images/b5gna/menu/politika.png'); float: left;" onMouseOver="menuOver('politika');" onMouseOut="menuOut();" onClick="location.href='/citizens'">{{ __('Politics') }}</div>
		@endif
		@if (in_array('MAP', $rules))
			<div class="menuBTN" id="menu-mapa" style="background-image: url('/images/b5gna/menu/mapa.png'); float: left;" onMouseOver="menuOver('mapa');" onMouseOut="menuOut();" onClick="location.href='/map'">{{ __('Map') }}</div>
		@endif
		<div class="menuBTN" id="menu-stats" style="background-image: url('/images/b5gna/menu/stats.png'); float: left;" onMouseOver="menuOver('stats');" onMouseOut="menuOut();" onClick="location.href='/statistics'">{{ __('Stats') }}</div>
	@endif
	<div class="menuBTN" id="menu-menu" style="background-image: url('/images/b5gna/menu/menu.png'); float: right;" onMouseOver="menuOver('menu');" onMouseOut="menuOut();" >{{ __('Main menu') }}</div>
@endauth
</div>

<div class="panelTop"></div>
<div class="panel" id="panel">
	<div class="panelBox">
	@auth
		@if (isset($player->ti))
		<div class="panelRightBox">
			@php
			$resourceMenu = [];
			foreach (\App\Models\Resource::where('age_id', player()->age->id)->where('menu', '>', 0)->get() as $res) {
				if (!isset($resourceMenu[$res->menu])) {
					$resourceMenu[$res->menu] = [];
				}
				$resourceMenu[$res->menu][] = $res;
			}
			ksort($resourceMenu);
			@endphp
			@foreach ($resourceMenu as $col => $resources)
			<div style="padding-top: 4px; margin-left: 2em; text-align: right; float:right; line-height:16px">
				@foreach ($resources as $res)
				<div class="panelResources" id="res-{{ $res->code }}" style="{{ $res->image ? "background: url('{$res->image}') no-repeat 99% center; " : '' }}{{--color:white--}}" title="{{ $res->name }}">
					<label class="resLabel"{{-- style="color:white"--}}>{{ $res->short }}</label>
					<span class="colRes{{ $res->highlight ? 'Light' : 'Dark' }}">
						{{ amount($player->{$res->code}) . (isset($res->max) ? ' / ' . amount($player->{'max_' . $res->code}) : '') }}
					</span>
				</div>
				@endforeach
			</div>
			@endforeach

			{{-- <div style="padding-top: 4px; text-align: right; float:right">
			@foreach (\App\Models\Resource::where('menu', true)->get() as $res)
				<div class="panelResources" id="res-{{ $res->code }}" style="background-image: url('/images/b5gna/icons16/res-{{ $res->code }}.png'); color:white" title="{{ $res->name }}"><label class="resLabel" style="color:white">{{ $res->short }}</label><span class="colResLight">{{ amount($player->{$res->code}) . ($res->max ? ' / ' . amount($player->{'max_' . $res->code}) : '') }}</span></div>
			@endforeach --}}
				{{-- <div class="panelResources" id="res-ti" style="background-image: url('/images/b5gna/icons16/res-ti.png');" title="titánium"><label class="resLabel">Ti</label><span class="colResLight">{{ amount($player->ti) }}</span></div>
				<div class="panelResources" id="res-q40" style="background-image: url('/images/b5gna/icons16/res-q40.png');" title="quantium 40"><label class="resLabel">Q40</label><span class="colResLight">@amount($player->q40)</span></div>
				<div class="panelResources" id="res-cr" style="background-image: url('/images/b5gna/icons16/res-cr.png');" title="kredity"><label class="resLabel">Cr</label><span class="colResLight">@amount($player->cr)</span></div>
				<div class="panelResources" id="res-rti" style="background-image: url('/images/b5gna/icons16/res-rti.png');" title="ruda titánia"><label class="resLabel">rTi</label><span class="colResDark">@amount($player->rti)</span></div>
				<div class="panelResources" id="res-rq40" style="background-image: url('/images/b5gna/icons16/res-rq40.png');" title="ruda quantia 40"><label class="resLabel">rQ40</label><span class="colResDark">@amount($player->rq40)</span></div> --}}
			{{-- </div> --}}
		</div>
		@endif
	@endauth
	@guest
		@include('b5gna.menu.guest')
	@endguest
	</div>
</div>
<div class="panelBottom"></div>

@auth
	@include('b5gna.menu.panels')
@endauth

<div class="mainDoc" style="min-height:auto">

	@hasSection('help')
	<div class="helpBox" style="display: none;">
		<div class="helpContent">
			@yield('help')
		</div>
	</div>
	@endif

	@hasSection('mainTitle')
		<div class="mainTitle">@yield('mainTitle')</div>
		<div class="mainTitleFade"></div>
	@endif

	@yield('content')

		<div style="position:absolute; z-index:10; left:50%; width: 950px; margin-left:-475px; height:30px; line-height:30px; margin-top:-10px; font-size:12pt; background:#5a135c; color:grey; text-align:center">
			<a href="/rules" style="color:white">{{ __('Terms and conditions') }}</a> &bullet;
			<a href="/manual" style="color:white">{{ __('Manual') }}</a> &bullet;
			<a href="/contact" style="color:white">{{ __('Contact admin') }}</a>
		</div>

</div><!-- /.mainDoc -->

<div class="pageBottom">
	<div class="pageBottomBG"></div>
	<div class="mainDocBottom"></div>
	<div class="pageBottomInfo" style="margin-top:1em; font-size:10pt">

		<!-- Vygenerované za <strong>0,028</strong> sekúnd. SQL[11]: 0,008 sekúnd. Verzia: <strong>3.3</strong> -->
		<div style="color:#a0a0ff">Minbari: bledí, studení. Pozri sa do ich očí, vidíš len zrkadlá, nekonečné odrazy. Nenechajú nás im pomôcť, nie.<br><i>Lovec duší č.1</i> (Soul Hunter)</div>

		<div style="text-align:left;margin-top:1em; color:black; text-align:center">
			{{ __('Credits') }} &bullet;
			{{ __('Icons from') }} <a target="_blank" href="https://icons8.com" style="color:black"><b>Icons8</b></a><br>
		</div>

	</div>
</div>
<div style="color:white; text-align:center; font-size:10pt; margin:60px auto 20px">
	{!! strtr(__('Copyright'), ["\n"=>'<br>']) !!}
</div>


@stack('footer')

</body>
</html>
