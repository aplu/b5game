@extends('b5gna.layout')

@section('content')

@php
$theme = 'b5gna';
@endphp

    @if (View::exists('b5gna.help.'.$page))
        @section('help')
            @include('b5gna.help.'.$page)
        @endsection
    @endif

    @if (View::exists('b5gna.pages.'.$page))
        @include('b5gna.pages.'.$page)
    @else
        @include('b5gna.errors.404')
    @endif

@endsection