@extends('layout')

@section('content')
@php

$icons = [
	'users' => ['user-astronaut', 'street-view', 'users', 'users-cog', 'user-cog', 'user-tag', 'user-circle', 'birthday-cake'],

	'monsters' => ['mask', 'spider', 'dragon', 'bug', 'pastafarianism', 'gitkraken', 'phoenix-framework', 'optin-monster', 'ghost', 'snapchat-ghost'],

	'clouds' => ['soundcloud', 'mixcloud', 'cloud', 'cloud-showes-heavy', 'cloud-meatball', 'cloud-sun', 'cloud-moon', 'cloud-rain', 'cloud-sun-rain', 'cloud-moon-rain', 'cloud-download-alt', 'cloudversify', 'jsfiddle', 'poo-storm'],

	'symbols' => ['battle-net', 'meteor', 'egg', 'ankh', 'bahai', 'battle-net', 'galactic-senate', 'old-republic', 'galactic-republic', 'jedi', 'trade-federation',
'poo', 'poop', 'bezier-curve'],

	'battle' => ['crosshairs', 'biohazard', 'bomb', 'bity', 'bolt', 'burn', 'fire', 'firealt', 'superpowers'],

	'travel' => ['location-arrow', 'airbnb', 'space-shuttle', 'rocket', 'megaport', 'satellite', 'satellite-dish',
				 'fighter-jet', 'helicopter',],

	'papers' => ['book', 'book-dead', 'book-medical',

	'brain', 'peace', 'plus-circle', 'plus-square', 'map-pin', 'map', 'map-marker', 'map-marker-alt', 'map-marked', 'map-marked-alt', 'bullhorn', 'broadcast-tower', 'id-badge', 'id-badge-alt', 'id-card', 'id-card-alt', 'portrait', 'passport', 'portrait', 'rocketchat', 'journal-whills'],

	'smilies' => ['angly', 'smile', 'smile-wink', 'smile-beam', 'sad', 'sad-tear', ],

	'buildings' => ['building', 'warehouse', 'torii-gate', 'synagogue', 'store-alt', 'store', 'school', 'place-of-worship', 'mosque', 'monument', 'landmark', 'kaaba', 'industry', 'house-damage', 'hotel', 'hospital-alt', 'hospital', 'hospital', 'gopuram', 'city', 'church', 'shapes', 'university', 'home', 'dungeon', 'vihara', 'igloo', 'clinic-medical', 'campground', 'archway', 'simplybuilt'],
];

@endphp
@foreach ($icons as $category => $caticons)
<div class="container"><big style="font-size: xx-large; color:white; text-shadow: 0 0 2px black">
	<h5>{{ $category }}</h5>
	@foreach ($caticons as $category => $icon)
	<i class="fas fa-{{$icon}}"></i>
	@endforeach
</big>
</div>
@endforeach

@endsection
