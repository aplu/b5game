{{-- @extends('errors::minimal') --}}
@extends('b5gna.layout')

@section('title', __('Unauthorized'))
@section('code', '401')
@section('message', __('Unauthorized'))
