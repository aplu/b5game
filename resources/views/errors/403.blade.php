{{-- @extends('errors::minimal') --}}
@extends('b5gna.layout')

@section('title', __('Forbidden'))
@section('code', '403')
@section('message', __($exception->getMessage() ?: 'Forbidden'))
