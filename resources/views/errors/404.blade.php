{{-- @extends('errors::minimal') --}}
@extends('b5gna.layout')

@section('title', __('Not Found'))
@section('code', '404')
@section('message', __('Not Found'))

@section('content')
	@include('b5gna.errors.404')
@endsection
