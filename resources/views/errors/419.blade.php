{{-- @extends('errors::minimal') --}}
@extends('b5gna.layout')

@section('title', __('Page Expired'))
@section('code', '419')
@section('message', __('Page Expired'))
