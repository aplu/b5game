{{-- @extends('errors::minimal') --}}
@extends('b5gna.layout')

@section('title', __('Too Many Requests'))
@section('code', '429')
@section('message', __('Too Many Requests'))
