<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute musí být přijat.',
    'active_url' => ':attribute není platnou URL adresou.',
    'after' => ':attribute musí být datum po :date.',
    'after_or_equal' => ':attribute musí být datum od :date.',
    'alpha' => ':attribute může obsahovat pouze písmena.',
    'alpha_dash' => ':attribute může obsahovat pouze písmena (bez háčků a čárek), číslice, pomlčky a podtržítka.',
    'alpha_num' => ':attribute může obsahovat pouze písmena (bez háčků a čárek) a číslice.',
    'array' => ':attribute musí být pole.',
    'before' => ':attribute musí být datum před :date.',
    'before_or_equal' => ':attribute musí být datum do :date.',
    'between' => [
        'numeric' => ':attribute musí být číslo mezi :min a :max.',
        'file' => ':attribute musí být větší než :min a menší než :max kilobajtů.',
        'string' => ':attribute musí být delší než :min a kratší než :max znaků.',
        'array' => ':attribute musí obsahovat nejméně :min a nejvíce :max prvků.',
    ],
    'boolean' => ':attribute musí být true nebo false',
    'confirmed' => ':attribute nebylo potvrzeno.',
    'date' => ':attribute není správné datum.',
    'date_equals' => ':attribute musí být datum :date.',
    'date_format' => ':attribute není správně podle formátu datumu :format.',
    'different' => ':attribute a :other se musí lišit.',
    'digits' => ':attribute musí být :digits čísel dlouhé.',
    'digits_between' => ':attribute musí být nejméně :min a nejvíce :max čísel.',
    'dimensions' => ':attribute nemá správné rozměry.',
    'distinct' => ':attribute má duplicitní hodnotu.',
    'email' => ':attribute není platný email.',
    'ends_with' => ':attribute musí končit něčím z následujících: :values',
    'exists' => 'Hodnota v :attribute neexistuje.',
    'file' => ':attribute musí být soubor.',
    'filled' => ':attribute musí být vyplněno.',
    'gt' => [
        'numeric' => ':attribute musí být větší než :value.',
        'file' => ':attribute musí být větší než :value kB.',
        'string' => ':attribute musí být větší než :value znaků.',
        'array' => ':attribute musí mít více než :value položek.',
    ],
    'gte' => [
        'numeric' => ':attribute musí být větší nebo roven :value.',
        'file' => ':attribute musí být větší nebo roven :value kB.',
        'string' => ':attribute musí být větší nebo roven :value znaků.',
        'array' => ':attribute musí mít :value položek nebo více.',
    ],
    'image' => ':attribute musí být obrázek.',
    'in' => 'Výběr :attribute je neplatný.',
    'in_array' => ':attribute není obsažen v :other.',
    'integer' => ':attribute musí být celé číslo.',
    'ip' => ':attribute musí být platná IP adresa.',
    'ipv4' => ':attribute musí být platná IPv4 adresa.',
    'ipv6' => ':attribute musí být platná IPv6 adresa.',
    'json' => ':attribute musí být platný JSON řetězec.',
    'lt' => [
        'numeric' => ':attribute musí být menší než :value.',
        'file' => ':attribute musí být menší než :value kB.',
        'string' => ':attribute musí být menší než :value znaků.',
        'array' => ':attribute musí mít méně než :value položek.',
    ],
    'lte' => [
        'numeric' => ':attribute musí být menší nebo roven :value.',
        'file' => ':attribute musí být menší nebo roven :value kB.',
        'string' => ':attribute musí být menší nebo roven :value znaků.',
        'array' => ':attribute nesmí mít více než :value položek.',
    ],
    'max' => [
        'numeric' => ':attribute nesmí být větší než :max.',
        'file' => ':attribute nesmí být větší než :max kilobajtů.',
        'string' => ':attribute nesmí být delší než :max znaků.',
        'array' => ':attribute nesmí obsahovat více než :max položek.',
    ],
    'mimes' => ':attribute musí být jeden z následujících datových typů :values.',
    'mimetypes' => ':attribute musí být jeden z následujících datových typů :values.',
    'min' => [
        'numeric' => ':attribute musí být nejméně :min.',
        'file' => ':attribute musí mít nejméně :min kilobajtů.',
        'string' => ':attribute musí mít nejméně :min znaků.',
        'array' => ':attribute musí mít nejméně :min prvků.',
    ],
    'not_in' => 'Výběr :attribute je neplatný.',
    'not_regex' => 'Pole :attribute má špatný formát.',
    'numeric' => ':attribute musí být číslo.',
    'present' => ':attribute musí být uvedeno.',
    'regex' => ':attribute nemá správný formát.',
    'required' => ':attribute je vyžadováno.',
    'required_if' => ':attribute je vyžadováno pokud :other je :value.',
    'required_unless' => ':attribute je vyžadováno dokud :other je v :values.',
    'required_with' => ':attribute je vyžadováno pokud :values je uvedeno.',
    'required_with_all' => ':attribute je vyžadováno pokud :values jsou uvedeny.',
    'required_without' => ':attribute je vyžadováno pokud :values není uvedeno.',
    'required_without_all' => ':attribute je vyžadováno pokud ani jedno z :values není uvedeno.',
    'same' => ':attribute se musí shodovat s :other.',
    'size' => [
        'numeric' => ':attribute musí být přesně :size.',
        'file' => ':attribute musí být přesně :size kilobajtů.',
        'string' => ':attribute musí být přesně :size znaků.',
        'array' => ':attribute musí obsahovat :size prvků.',
    ],
    'starts_with' => ':attribute musí začínat něčím z následujících: :values',
    'string' => ':attribute musí být řetězec.',
    'timezone' => ':attribute musí být platná časová zóna.',
    'unique' => ':Attribute je již obsazeno.',
    'uploaded' => 'Nepodařilo se nahrát :attribute.',
    'url' => ':Attribute nemá platný formát URL adresy.',
    'uuid' => ':Attribute musí být platné UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
