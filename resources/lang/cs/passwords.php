<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Vaše heslo bylo obnoveno!',
    'sent' => 'E-mail s odkazem na obnovení hesla byl odeslán!',
    'token' => 'Klíč pro obnovu hesla je neplatný.',
    'user' => 'Nepodařilo se najít uživatele s touto e-mailovou adresou.',

];
