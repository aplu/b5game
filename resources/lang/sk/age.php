<?php

return [

    'type_title' => 'Typ',
    'type' => [
        'PUBLIC'  => 'Veřejný',
        'PRIVATE' => 'Soukromý',
    ],

    'rules_title' => 'Pravidla',
    'rules' => [
        'RECALC'        => 'Přepočet',
        'FIGHT_POINTS'  => 'Bojové body',
        'FIGHT_DELAY'       => 'Odložený útok',
        'CUSTOM_STATES' => 'Vlastní štáty',
        'SPECIAL'       => 'SPECIAL',
        'EXTREME'       => 'eXtrémní',
    ],

    'since' => 'Začíná',
    'until' => 'Končí',
    'since_until' => 'Začíná a končí',
    'unknown_date' => 'neurčito',
    'recalc_after' => 'Přepočet po',
    'author' => 'Autor',
    'num_of_players' => 'Hráčů',

    'no_age' => 'Momentálně neexistuje žádný věk.',
];
