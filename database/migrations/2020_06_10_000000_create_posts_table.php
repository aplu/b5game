<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
	function up()
	{
		Schema::create('posts', function (Blueprint $table) {
			$table->bigIncrements('id')->nullable();
			$table->text('content');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	function down()
	{
		Schema::dropIfExists('posts');
	}
}

