<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpgradesTable extends Migration
{
	function up()
	{
		Schema::create('upgrades', function (Blueprint $table) {
			$table->bigIncrements('id')->nullable();
			$table->string('name');
			$table->unsignedSmallInteger('start_lvl')->nullable();
			$table->unsignedTinyInteger('max_lvl')->nullable();
			$table->string('ti_formula')->nullable();
			$table->string('q40_formula')->nullable();
			$table->string('cr_formula')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	function down()
	{
		Schema::dropIfExists('upgrades');
	}
}

