<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipsTable extends Migration
{
	function up()
	{
		Schema::create('ships', function (Blueprint $table) {
			$table->bigIncrements('id')->nullable();
			$table->string('name');
			$table->string('short');
			$table->enum('class', ['S', 'K', 'L', 'D'])->nullable();
			$table->unsignedTinyInteger('lvl')->nullable();
			$table->enum('src_type', ['race', 'state'])->nullable();
			$table->string('src')->nullable();
			$table->unsignedInteger('ti')->nullable();
			$table->unsignedInteger('q40')->nullable();
			$table->unsignedBigInteger('cr')->nullable();
			$table->unsignedInteger('time')->nullable();
			$table->unsignedSmallInteger('slots')->nullable();
			$table->unsignedInteger('hp')->nullable();
			$table->unsignedInteger('us')->nullable();
			$table->unsignedInteger('ul')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	function down()
	{
		Schema::dropIfExists('ships');
	}
}

