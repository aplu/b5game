<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectorsTable extends Migration
{
	function up()
	{
		Schema::create('sectors', function (Blueprint $table) {
			$table->bigIncrements('id')->nullable();
			$table->string('short');
			$table->string('shorter')->nullable();
			$table->string('name')->nullable();
			$table->smallInteger('iti')->nullable()->default(50);
			$table->smallInteger('iq40')->nullable()->default(50);
			$table->smallInteger('icr')->nullable()->default(50);
			$table->unsignedInteger('rti')->nullable();
			$table->unsignedInteger('rq40')->nullable();
			$table->unsignedBigInteger('cr')->nullable();
			$table->string('points')->nullable();
			$table->unsignedSmallInteger('tx')->nullable();
			$table->unsignedSmallInteger('ty')->nullable();
			$table->tinyInteger('rot')->nullable();
			$table->smallInteger('cx')->nullable();
			$table->smallInteger('cy')->nullable();
			$table->smallInteger('dx')->nullable();
			$table->smallInteger('dy')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	function down()
	{
		Schema::dropIfExists('sectors');
	}
}

