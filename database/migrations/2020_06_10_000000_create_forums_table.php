<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForumsTable extends Migration
{
	function up()
	{
		Schema::create('forums', function (Blueprint $table) {
			$table->bigIncrements('id')->nullable();
			$table->string('name');
			$table->enum('type', ['0', '1', '2']);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	function down()
	{
		Schema::dropIfExists('forums');
	}
}

