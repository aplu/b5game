<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgesTable extends Migration
{
	function up()
	{
		Schema::create('ages', function (Blueprint $table) {
			$table->bigIncrements('id')->nullable();
			$table->string('name');
			$table->timestamp('since')->default("2020-02-02");
			$table->timestamp('until')->nullable();
			$table->enum('type', ['PUBLIC', 'PRIVATE']);
			$table->set('rules', ['DELAYED', 'SPECIAL'])->nullable();
			$table->unsignedInteger('recalc')->default("86400");
			$table->unsignedInteger('mining_time')->default("43200");
			$table->unsignedInteger('mining_jump')->default("1800");
			$table->unsignedInteger('jump_time')->default("28800");
			$table->unsignedInteger('gate_time')->default("7200");
			$table->unsignedInteger('max_fight_delay')->default("1800");
			$table->unsignedInteger('turn_time')->default("21600");
			$table->unsignedInteger('defense_time')->default("32400");
			$table->float('build_speed')->default("1");
			$table->float('tech_speed')->default("1");
			$table->float('production_speed')->default("1");
			$table->float('mining_production_speed')->default("1");
			$table->unsignedInteger('start_ti')->nullable();
			$table->unsignedInteger('start_q40')->nullable();
			$table->unsignedBigInteger('start_cr')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	function down()
	{
		Schema::dropIfExists('ages');
	}
}

