<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
	function up()
	{
		Schema::create('states', function (Blueprint $table) {
			$table->bigIncrements('id')->nullable();
			$table->boolean('playable')->nullable()->default(0);
			$table->string('name');
			$table->string('short');
			$table->string('color')->nullable();
			$table->string('bg')->nullable();
			$table->string('stroke')->nullable();
			$table->string('hw')->nullable();
			$table->string('url')->nullable();
			$table->string('discord_channel')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	function down()
	{
		Schema::dropIfExists('states');
	}
}

