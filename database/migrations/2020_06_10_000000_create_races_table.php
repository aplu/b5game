<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRacesTable extends Migration
{
	function up()
	{
		Schema::create('races', function (Blueprint $table) {
			$table->bigIncrements('id')->nullable();
			$table->boolean('playable')->nullable()->default(0);
			$table->string('name');
			$table->string('short');
			$table->string('states')->nullable();
			$table->smallInteger('extinct')->nullable();
			$table->string('url')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	function down()
	{
		Schema::dropIfExists('races');
	}
}

