<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuildingsTable extends Migration
{
	function up()
	{
		Schema::create('buildings', function (Blueprint $table) {
			$table->bigIncrements('id')->nullable();
			$table->string('name');
			$table->unsignedInteger('init')->nullable();
			$table->unsignedInteger('space')->nullable();
			$table->unsignedInteger('ti')->nullable();
			$table->unsignedInteger('q40')->nullable();
			$table->unsignedBigInteger('cr')->nullable();
			$table->unsignedTinyInteger('min')->nullable()->default(0);
			$table->unsignedTinyInteger('max')->nullable();
			$table->unsignedTinyInteger('at_once')->nullable();
			$table->unsignedInteger('time')->nullable();
			$table->boolean('demolishable')->nullable()->default(1);
			$table->string('formula')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	function down()
	{
		Schema::dropIfExists('buildings');
	}
}

