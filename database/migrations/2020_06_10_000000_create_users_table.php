<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
	function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->bigIncrements('id')->nullable();
			$table->string('email')->unique();
			$table->unsignedBigInteger('race_id')->nullable()->index();
			$table->unsignedBigInteger('state_id')->nullable()->index();
			$table->boolean('email_visible')->nullable()->default(0);
			$table->boolean('email_notify')->nullable()->default(0);
			$table->timestamp('email_verified_at')->nullable();
			$table->string('password');
			$table->rememberToken();
			$table->unsignedInteger('login_count')->nullable()->default(1);
			$table->timestamp('last_login')->nullable();
			$table->string('name')->nullable();
			$table->set('sex', ['m', 'f'])->nullable();
			$table->decimal('birth_year', 4, 0)->nullable();
			$table->decimal('birth_month', 2, 0)->nullable();
			$table->decimal('birth_day', 2, 0)->nullable();
			$table->boolean('tablink')->nullable()->default(1);
			$table->string('url')->nullable();
			$table->text('bio')->nullable();
			$table->string('phone')->nullable();
			$table->string('discord')->nullable();
			$table->string('skype')->nullable();
			$table->string('icq')->nullable();
			$table->string('avatar')->nullable();
			$table->unsignedBigInteger('hw_id')->nullable()->index();
			$table->unsignedInteger('ti')->nullable()->default(200000);
			$table->unsignedInteger('rti')->nullable()->default(0);
			$table->unsignedInteger('q40')->nullable()->default(2500);
			$table->unsignedInteger('rq40')->nullable()->default(0);
			$table->unsignedBigInteger('cr')->nullable()->default(200000000);
			$table->unsignedInteger('crystals')->nullable()->default(1);
			$table->timestamps();
			$table->softDeletes();
			$table->foreign('race_id')->references('id')->on('races');
			$table->foreign('state_id')->references('id')->on('states');
			$table->foreign('hw_id')->references('id')->on('sectors');
		});
	}

	function down()
	{
		Schema::dropIfExists('users');
	}
}

