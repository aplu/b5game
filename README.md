# Babylon 5 Game: Open Age

This is community open-source rework of original **Babylon 5 Game: New Age** (originally created by Lucas) in modern PHP framework Laravel 6.x.

## Download
Download git repository & composer packages
```
git clone https://gitlab.com/aplu/b5game.git
cd b5game
composer install
php artisan migrate
```

## Run
Run with PHP's internal webserver just by:
```
php artisan serve
```

---

## or Install
It needs to configure any webserver (Apache, NGINX...) with support for PHP.

### Example configuration for NGINX with PHP-FPM

Add or edit your `/etc/nginx/sites-available/b5game.conf` (replace the `/path/to/b5game` in configuration by path to your **b5game** directory).
```
server {
    listen       80;
    listen       [::]:80;
    server_name  .b5game.localhost;
    root         /path/to/b5game/public;

    index index.php index.html index.htm;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_intercept_errors on;
        fastcgi_index  index.php;
        include        fastcgi_params;
        #fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        fastcgi_param  PHP_ADMIN_VALUE  "open_basedir=/path/to/b5game:/tmp";
        #fastcgi_param  PHP_ADMIN_VALUE  "xdebug.port=9002";
        #fastcgi_read_timeout 20m;
        fastcgi_pass   unix:/run/php-fpm/b5game.sock;
    }
}
```

### Example configuration of PHP-FPM pool

Add or edit `/etc/php-fpm.d/b5game.conf` (path depends on your system and configuration).

```
[b5game]
user = www-data
group = www-data
listen = /run/php/b5game.sock
pm = ondemand
pm.max_children = 5
pm.process_idle_timeout = 10s
```

## How it is made

**@TODO**

- See *bin/build-sandbox*
- See prototype of b5gadmin
- Learn [Laravel](https://laravel.com/) from [Laracasts](https://laracasts.com/series/laravel-6-from-scratch) ;-)

### Controllers
There are 3 Base Controllers:
- **BaseController** - main base controller, directly inherited by controllers for non-authenticated users (eg. PageController)
- **BaseAuthController** - base controller for controllers with authenticated user (eg. ProfileController)
- **BasePlayerController** - base controller for most of the game controllers with player joined into age (eg. StationController)

## Credits

### Thanks for old Babylon 5 Game (since 2004)

- UjoDuro & his team

### Many thanks for the original [__Babylon 5 Game: New Age__](http://b5game.icsp.sk) (since 2009)

- **Lucas**
- Sir_Chislehurst, Bard, Shaade

### Contributors of the [__Babylon 5 Game: Open Age__](http://b5game.duoverso.com) (since 2020)

- Lucas
- wajrou
- SOAD
- Sir_Chislehurst
- Dobyvatel
- Kakarrot
- ShalTok
- Ulkesh

... and thanks to many others of Slovak & Czech B5Game community

## License

The *Babylon 5 Game: Open Age* is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
