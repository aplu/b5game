<div class="mapLeftBox"><b class="resLabel">Sektor a jeho vlastník</b><br>
    <div class="mapShort colHl">ear</div>
    <div class="mapName">Earth<br><span style="color: #6984f9">Pozemská Aliancia</span></div>
</div>
<div class="mapRightBox">
    <div class="mapIndex colResLight" title="index rudy titánia"><?= rand(0, 100) ?></div>
    <div class="mapIndex colResLight" title="index rudy quantia 40"><?= rand(0, 100) ?></div>
    <div class="mapIndex colResLight" title="index kreditov"><?= rand(0, 100) ?></div>
</div>
<div class="mapRightBox">
    <div class="panelResources" style="background-image: url('/images/b5gna/icons16/res-rti.png');" title="zásoby rudy titánia"><label class="resLabel">rTi</label><span class="colResDark"><?= number_format(rand(0, 9999999999), 0, ',', '\'') ?></span></div>
    <div class="panelResources" style="background-image: url('/images/b5gna/icons16/res-rq40.png');" title="zásoby rudy quantia 40"><label class="resLabel">rQ40</label><span class="colResDark"><?= number_format(rand(0, 9999999999), 0, ',', '\'') ?></span></div>
    <div class="panelResources" style="background-image: url('/images/b5gna/icons16/res-cr.png');" title="zásoby kreditov"><label class="resLabel">Cr</label><span class="colResDark"><?= number_format(rand(0, 9999999999), 0, ',', '\'') ?></span></div>
</div>
<div class="mapRightBox"><b class="resLabel">Kapacita sektora</b>
    <div class="mapCap" title="voľná / celková kapacita"><?= number_format(rand(0, 100000), 0, ',', ' ') ?> / <span style="font-size:12px;">100 000</span></div>
</div>
<div class="mapRightBox"><b class="resLabel">Stavby v sektore</b><br>
    <div class="mapStation" style="color: #40ff40">ťažobná</div>
    <div style="clear: right;"></div>
    <div class="mapStation" style="color: #00c0c0">skoková brána</div>
</div>
<div class="mapRightBox"><b class="resLabel">Zdroj dát</b><br>
    <div class="mapSource hl">aktuálny stav</div>
</div>
