<?php header('Content-Type: application/xml'); ?>
<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<ajax>
<resources>
<res-ti><?= number_format(rand(0, 9999999999), 0, ',', '\'') ?></res-ti>
<res-q40><?= number_format(rand(0, 9999999999), 0, ',', '\'') ?></res-q40>
<res-cr><?= number_format(rand(0, 9999999999), 0, ',', '\'') ?></res-cr>
<res-rti><?= number_format(rand(0, 9999999999), 0, ',', '\'') ?></res-rti>
<res-rq40><?= number_format(rand(0, 9999999999), 0, ',', '\'') ?></res-rq40>
</resources>
<indicators>
<msg_count><?= rand(0, 100) ?></msg_count>
<cis_count><?= rand(0, 100) ?></cis_count>
<fight_count><?= rand(0, 100) ?></fight_count>
</indicators>
<users><?= rand(1, 10000) ?></users>
<online><?= rand(1, 1000) ?></online>
<notify></notify>
<alert><?= rand(0, 100) > 80 ? '1' : '' ?></alert>
</ajax>
