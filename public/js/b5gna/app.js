/** ========== date.format.js ========== */
/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function() {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function(val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len)
                val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function(date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date))
            throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d: d,
                dd: pad(d),
                ddd: dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m: m + 1,
                mm: pad(m + 1),
                mmm: dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy: String(y).slice(2),
                yyyy: y,
                h: H % 12 || 12,
                hh: pad(H % 12 || 12),
                H: H,
                HH: pad(H),
                M: M,
                MM: pad(M),
                s: s,
                ss: pad(s),
                l: pad(L, 3),
                L: pad(L > 99 ? Math.round(L / 10) : L),
                t: H < 12 ? "a" : "p",
                tt: H < 12 ? "am" : "pm",
                T: H < 12 ? "A" : "P",
                TT: H < 12 ? "AM" : "PM",
                Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default": "ddd mmm dd yyyy HH:MM:ss",
    shortDate: "m/d/yy",
    mediumDate: "mmm d, yyyy",
    longDate: "mmmm d, yyyy",
    fullDate: "dddd, mmmm d, yyyy",
    shortTime: "h:MM TT",
    mediumTime: "h:MM:ss TT",
    longTime: "h:MM:ss TT Z",
    isoDate: "yyyy-mm-dd",
    isoTime: "HH:MM:ss",
    isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};

// For convenience...
Date.prototype.format = function(mask, utc) {
    return dateFormat(this, mask, utc);
};








/** ========== functions.js ========== */
function ShowHelp() {
    obj = document.getElementById('help');
    if (obj.style.display == "none")
        obj.style.display = ""
    else
        obj.style.display = "none";
}

function ToggleDIV(name) {
    obj = document.getElementById(name);
    if (obj.style.display == "none")
        obj.style.display = ""
    else
        obj.style.display = "none";
}

function slideDIV(id) {
    if ($(id).css('display') == 'none')
        $(id).slideDown();
    else
        $(id).slideUp();
}

function SetAll(name) {
    obj = document.getElementById(name);
    for (i = 0; i < obj.length; i++) {
        node = obj.elements[i];
        if (node.type == "checkbox")
            node.checked = true;
    }
}


function CountDown(id, sec) {
    if (sec > 0) {
        var h = Math.floor(sec / 3600);
        var m = Math.floor((sec - h * 3600) / 60);
        var s = sec % 60;
        if (m < 10)
            m = '0' + m;
        if (s < 10)
            s = '0' + s;
        document.getElementById(id).innerHTML = "<font color=red>" + h + "<span style='font-size:80%'>h </span>" + m + "<span style='font-size:80%'>m </span>" + s + "<span style='font-size:80%'>s</span></font>";
        setTimeout("CountDown('" + id + "'," + (sec - 1) + ")", 1000);
    } else
        document.getElementById(id).innerHTML = "<font color=lime>Hotovo</font>";
}

function spinButtonUp(id) {
    var x = $('#' + id).val() * 1 + 1
    if (isNaN(x))
        x = 1;
    if (x > 999)
        x = 999;
    $('#' + id).val(x);
}

function spinButtonDown(id) {
    var x = $('#' + id).val() - 1
    if (isNaN(x))
        x = -1;
    if (x < -99)
        x = -99;
    $('#' + id).val(x);
}









/** ========== main.js ========== */
var timeB5 = new Date();
var intBlink;

$.ajaxSetup({
    // Disable caching of AJAX responses
    cache: false
});



function B5TimeCycle() {
    $('#B5time').html('<span class="colHl">' + dateFormat(timeB5, "yyyy-mm-dd") + '</span>/' + dateFormat(timeB5, "HHMM") + '<span style="font-size:13px;">' + dateFormat(timeB5, ".ss") + '</span>');
    timeB5.setTime(timeB5.valueOf() + 1000);
}

function ShowB5Time(unix, shift) {
    timeB5.setTime(unix * 1000);
    timeB5.setYear(timeB5.getFullYear() + shift);
    B5TimeCycle();
    setInterval("B5TimeCycle()", 1000);
}

function toggleHelp() {
    if ($('.helpBox').css('display') == 'none')
        $('.helpBox').slideDown();
    else
        $('.helpBox').slideUp();
}


function blinkMsg() {
    if ($('#indMsg').css('background-image').indexOf("gray") > -1)
        $('#indMsg').css('background-image', "url('/images/b5gna/ind-msg.png')");
    else
        $('#indMsg').css('background-image', "url('/images/b5gna/ind-msg-gray.png')");
}


function ajaxPageUpdate() {
    // $.post('../page-ajax.php', function(data) {
    $.get('/ajax/page', function(data) {
        $('#res-ti').find('span').text($('res-ti', data).text());
        $('#res-q40').find('span').text($('res-q40', data).text());
        $('#res-cr').find('span').text($('res-cr', data).text());
        $('#res-rti').find('span').text($('res-rti', data).text());
        $('#res-rq40').find('span').text($('res-rq40', data).text());

        x = $('msg_count', data).text();
        $('#indMsg').css('background-image', "url('/images/b5gna/ind-msg" + (x > 0 ? '' : '-gray') + ".png')");
        $('#indMsg').find('div').text(x);
        $('#indMsg').find('div').css('display', (x > 0 ? '' : 'none'));
        if (x > 0) $('#indMsg').attr('title', 'Počet nových správ: ' + x);
        else $('#indMsg').attr('title', 'Nemáš žiadnu novú správu.');

        if (document.getElementById("indMsg")) {
            clearInterval(intBlink);
            if (x > 0) intBlink = setInterval("blinkMsg()", 1000);
        }

        x = $('cis_count', data).text();
        $('#indCis').css('background-image', "url('/images/b5gna/ind-cis" + (x > 0 ? '' : '-gray') + ".png')");
        $('#indCis').find('div').text(x);
        $('#indCis').find('div').css('display', (x > 0 ? '' : 'none'));
        if (x > 0) $('#indCis').attr('title', 'Počet neprečítaných informácii v CIS: ' + x);
        else $('#indCis').attr('title', 'Nemáš žiadnu neprečítanú informáciu v CIS.');

        x = $('fight_count', data).text();
        $('#indFight').css('background-image', "url('/images/b5gna/ind-attack" + (x > 0 ? '' : '-gray') + ".png')");
        if (x > 0) $('#indFight').attr('title', 'Zobraziť boje, v ktorých je zapojená tvoja flotila.');
        else $('#indFight').attr('title', 'Momentálne sa nezúčastòuješ žiadneho boja.');


        $('#usersOnline').text($('online', data).text());
        $('#usersCount').text($('users', data).text());

    }, "xml");
}



$(document).ready(function() {
    /*  $('#res-ti').ajaxError(function(e, xhr, settings, exception) {
        $(this).text(exception.message);
        $('.content').text(xhr.responseText);
      });*/

    ajaxPageUpdate();
    setInterval("ajaxPageUpdate()", 300*1000);
});



/** ========== menu.js ========== */
var actPanel = '';
var menuTimeoutID = 0;


function menuOver(panel) {
    if (menuTimeoutID != 0) {
        clearTimeout(menuTimeoutID);
        menuTimeoutID = 0;
    }
    if (panel == actPanel) return;

    $("#panel-" + panel).show();
    $("#menu-" + panel).css("color", "white");
    if (actPanel == '')
        $("#panel").css("visibility", "hidden")
    else {
        $("#panel-" + actPanel).css("display", "none");
        $("#menu-" + actPanel).css("color", "black");
    }

    actPanel = panel;
}


function menuFadeOut() {
    $("#panel").css("visibility", "visible");
    $("#panel-" + actPanel).fadeOut(400);
    $("#menu-" + actPanel).css("color", "black");
    actPanel = '';
}


function menuOut() {
    if (menuTimeoutID != 0) clearTimeout(menuTimeoutID);
    menuTimeoutID = setTimeout("menuFadeOut()", 500);
}




/** ========== /game/map.php ========== */
var mapSector = '';
var mapTimer = 0;

function timerExec() {
    if (mapSector != '')
        $('#sectDetail').load('map-info.php?sector=' + mapSector);
}

function showInfo(sector) {
    window.alert(sector);
    mapSector = sector;
    if (mapTimer != 0)
        clearTimeout(mapTimer);
    mapTimer = setTimeout(timerExec, 250);
}



/** ========== /main/forum.php ========== */
function favClick(id) {
    var src = $('#fav' + id).attr("src");
    if (src.indexOf("grey") > -1) {
        src = src.replace("grey", "blue");
        $.get("/forum-ajax.php?action=fav&idx=" + id + "&value=1");
    } else {
        src = src.replace("blue", "grey");
        $.get("/forum-ajax.php?action=fav&idx=" + id + "&value=0");
    }
    $('#fav' + id).attr("src", src);
}

function hideClick(id) {
    var src = $('#hide' + id).attr("src");
    if (src.indexOf("-off") > -1) {
        src = src.replace("-off", "-on");
        $.get("/forum-ajax.php?action=hide&idx=" + id + "&value=1");
    } else {
        src = src.replace("-on", "-off");
        $.get("/forum-ajax.php?action=hide&idx=" + id + "&value=0");
    }
    $('#hide' + id).attr("src", src);
}
