var timeB5 = new Date();
var intBlink;

$.ajaxSetup ({
// Disable caching of AJAX responses
cache: false
});



function B5TimeCycle(){
  $('#B5time').html( '<span class="colHl">' + dateFormat(timeB5,"yyyy-mm-dd") + '</span>/' + dateFormat(timeB5,"HHMM") + '<span style="font-size:13px;">' + dateFormat(timeB5,".ss") + '</span>');
  timeB5.setTime(timeB5.valueOf()+1000);
}

function ShowB5Time(unix,shift){
  timeB5.setTime(unix*1000);
  timeB5.setYear(timeB5.getFullYear()+shift);
  B5TimeCycle();
  setInterval("B5TimeCycle()",1000);
}

function toggleHelp(){
  if ($('.helpBox').css('display')=='none')
    $('.helpBox').slideDown();
  else
    $('.helpBox').slideUp();
}


function blinkMsg(){
  if ( $('#indMsg').css('background-image').indexOf("gray")>-1 )
    $('#indMsg').css('background-image', "url('../images/ind-msg.png')");
  else
    $('#indMsg').css('background-image', "url('../images/ind-msg-gray.png')");
}


function ajaxPageUpdate(){
  $.post('../page-ajax.php', function(data){
    $('#res-ti').find('span').text($('res-ti',data).text());
    $('#res-q40').find('span').text($('res-q40',data).text());
    $('#res-cr').find('span').text($('res-cr',data).text());
    $('#res-rti').find('span').text($('res-rti',data).text());
    $('#res-rq40').find('span').text($('res-rq40',data).text());

    x=$('msg_count',data).text();
    $('#indMsg').css('background-image', "url('../images/ind-msg" + (x>0 ? '' : '-gray') + ".png')");
    $('#indMsg').find('div').text(x);
    $('#indMsg').find('div').css('display', (x>0 ? '' : 'none'));
    if (x>0) $('#indMsg').attr('title','Počet nových správ: '+x);
      else   $('#indMsg').attr('title','Nemáš žiadnu novú správu.');

    clearInterval(intBlink);
    if (x>0) intBlink=setInterval("blinkMsg()",1000);

    x=$('cis_count',data).text();
    $('#indCis').css('background-image', "url('../images/ind-cis" + (x>0 ? '' : '-gray') + ".png')");
    $('#indCis').find('div').text(x);
    $('#indCis').find('div').css('display', (x>0 ? '' : 'none'));
    if (x>0) $('#indCis').attr('title','Počet neprečítaných informácii v CIS: '+x);
      else   $('#indCis').attr('title','Nemáš žiadnu neprečítanú informáciu v CIS.');

    x=$('fight_count',data).text();
    $('#indFight').css('background-image', "url('../images/ind-attack" + (x>0 ? '' : '-gray') + ".png')");
    if (x>0) $('#indFight').attr('title','Zobraziť boje, v ktorých je zapojená tvoja flotila.');
      else   $('#indFight').attr('title','Momentálne sa nezúčastňuješ žiadneho boja.');
      

    $('#usersOnline').text($('online',data).text());
    $('#usersCount').text($('users',data).text());
    
  },"xml");
}



$(document).ready(function(){
/*  $('#res-ti').ajaxError(function(e, xhr, settings, exception) {
    $(this).text(exception.message);
    $('.content').text(xhr.responseText);
  });*/

  ajaxPageUpdate();
  setInterval("ajaxPageUpdate()",300000);
});
