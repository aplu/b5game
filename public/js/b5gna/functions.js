function ShowHelp(){
    obj=document.getElementById('help');
    if (obj.style.display=="none") obj.style.display=""
    else obj.style.display="none";
  }
  
function ToggleDIV(name){
  obj=document.getElementById(name);
  if (obj.style.display=="none") obj.style.display=""
   else obj.style.display="none";
}

function slideDIV(id){
  if ($(id).css('display')=='none')
    $(id).slideDown();
  else
    $(id).slideUp();
}

function SetAll(name) {
  obj=document.getElementById(name);
  for (i=0; i<obj.length; i++){
    node = obj.elements[i];
    if (node.type=="checkbox") node.checked=true;
  }
}


function CountDown(id,sec){
  if (sec>0){
    var h=Math.floor(sec/3600);
    var m=Math.floor((sec-h*3600)/60);
    var s=sec%60;
    if (m<10) m='0'+m;
    if (s<10) s='0'+s;
    document.getElementById(id).innerHTML="<font color=red>"+h+"<span style='font-size:80%'>h </span>"+m+"<span style='font-size:80%'>m </span>"+s+"<span style='font-size:80%'>s</span></font>";
    setTimeout("CountDown('"+id+"',"+(sec-1)+")",1000);
  } else document.getElementById(id).innerHTML="<font color=lime>Hotovo</font>";
}

function spinButtonUp(id){
  var x=$('#'+id).val()*1+1
  if (isNaN(x)) x=1;
  if (x>999) x=999;
  $('#'+id).val(x);
}

function spinButtonDown(id){
  var x=$('#'+id).val()-1
  if (isNaN(x)) x=-1;
  if (x<-99) x=-99;
  $('#'+id).val(x);
}