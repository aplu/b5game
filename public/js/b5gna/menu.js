var actPanel='';
var menuTimeoutID=0;


function menuOver(panel){
  if (menuTimeoutID!=0){
    clearTimeout(menuTimeoutID);
    menuTimeoutID=0;
  }
  if (panel==actPanel) return;

  $("#panel-"+panel).show();
  $("#menu-"+panel).css("color","white");
  if (actPanel=='')
    $("#panel").css("visibility","hidden")
  else{
    $("#panel-"+actPanel).css("display","none");
    $("#menu-"+actPanel).css("color","black");
  }
    
  actPanel=panel;
}


function menuFadeOut(){
  $("#panel").css("visibility","visible");
  $("#panel-"+actPanel).fadeOut(400);
  $("#menu-"+actPanel).css("color","black");
  actPanel='';
}


function menuOut(){
  if (menuTimeoutID!=0) clearTimeout(menuTimeoutID);  
  menuTimeoutID=setTimeout("menuFadeOut()", 500);
}