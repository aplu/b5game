<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/ajax/page', function (Request $request) {
    return response()
        ->view('ajax.page')
        ->header('Content-Type', 'application/xml');
});

Route::get('/', 'PagesController@show')->name('intro');

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

// BaseAuthController & BasePlayerController is used instead of Route::middleware()
// Route::middleware(['auth', 'game'])->group(function() {

// Profile
// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'ProfileController@show')->name('profile');
Route::get('/switch-player/{player}', 'ProfileController@switchPlayer')->name('switch-player');
// Route::post('/join-age', 'ProfileController@joinToAge')->name('join-age');
Route::get('/profile/edit', 'ProfileController@edit')->name('edit-profile');
Route::patch('/change-password', 'ProfileController@changePassword')->name('change-password');
Route::patch('/profile', 'ProfileController@update')->name('update-profile');
// Avatar
// Route::post('/avatar', 'AvatarController@upload')->name('upload-avatar');
// Route::delete('/avatar', 'AvatarController@destroy')->name('destroy-avatar');

Route::get('/ages', 'AgesController@index')->name('ages');
Route::any('/age/choose', 'AgesController@chooseAge')->name('choose-age');
Route::any('/age/{age}/choose-race/{race?}', 'AgesController@chooseRace')->name('choose-race');
Route::get('/prereg', function() { return page('age-prereg'); })->name('prereg');


// Route::get('/choose/race', 'ChooseController@race')->name('choose-race');

// Communication
Route::get('/forums', 'ForumsController@index')->name('forums');
Route::get('/forums/create', function() { return page('forum-create'); })->name('create-forum');
Route::get('/forum/{forum}/edit', function() { return page('forum-create'); })->name('edit-forum');

Route::get('/polls', function() { return page('vote'); })->name('polls');
Route::get('/poll/admin', function() { return page('vote'); })->name('poll-admin');
Route::get('/polls/create', function() { return page('vote-create'); })->name('create-poll');
Route::get('/mail', function() { return page('msg'); })->name('mail');
Route::get('/cis', function() { return page('cis'); })->name('cis');

// Outpost
Route::get('/outpost', 'OutpostController@show')->name('outpost');
Route::post('/outpost/build/{building}', 'OutpostController@build')->name('build');
Route::post('/outpost/upgrade/{upgrade}', 'OutpostController@upgrade')->name('upgrade');
Route::get('/outpost/stop-job/{job}', 'OutpostController@stopJob')->name('stop-job');
Route::get('/relocate', 'OutpostController@sectorsToRelocate')->name('relocate');
Route::post('/relocate', 'OutpostController@relocate');

Route::get('/mining', 'MiningController@mining')->name('mining');
Route::get('/build-mining', function() { return page('miners'); })->name('build-mining');
Route::get('/exchange', function() { return page('market'); })->name('exchange');
Route::get('/exchange/log', function() { return page('transactions-log'); })->name('exchange-log');
Route::get('/transactions', function() { return page('transfer'); })->name('transactions');
Route::get('/build-mining', 'MiningController@miners')->name('build-mining');
Route::get('/balance', function() { return page('balance'); })->name('balance');

// Fleet
// Route::get('/vehicle-types', 'VehicleTypesController@index')->name('vehicle-types');
Route::get('/build-vehicles', 'VehiclesController@build')->name('build-vehicles');
Route::get('/repair', 'VehiclesController@repair')->name('repair');
Route::get('/wings', function() { return page('fleet-wings'); })->name('wings');
Route::get('/groups', function() { return page('fleet-groups'); })->name('groups');
Route::get('/plans', function() { return page('move'); })->name('plans');
Route::get('/plans/create', function() { return page('move-plan'); })->name('create-plan');
Route::get('/research-vehicles', 'VehicleTypesController@research')->name('research-vehicles');
Route::get('/plans', function() { return page('move'); })->name('plans');
Route::get('/vehicles', function() { return page('fleet-overview'); })->name('vehicles');
Route::get('/mine-fights', function() { return page('fight-mine'); })->name('mine-fights');
Route::get('/vehicle-types', 'VehicleTypesController@index')->name('vehicle-types');

// Politics
Route::get('/citizens', function() { return page('citizens'); })->name('citizens');
Route::get('/citizenship', function() { return page('citizenship'); })->name('citizenship');
Route::get('/parties', function() { return page('parties'); })->name('parties');
Route::get('/noticeboard', function() { return page('notices'); })->name('noticeboard');
Route::get('/taxes', function() { return page('taxes'); })->name('taxes');
Route::get('/penalties', function() { return page('penalty'); })->name('penalties');
Route::get('/stations', function() { return page('stations'); })->name('stations');
Route::get('/relationships', function() { return page('relations'); })->name('relationships');
Route::get('/election', function() { return page('election'); })->name('election');
Route::get('/state/balance', function() { return page('state-balance'); })->name('state-balance');

// Map
Route::get('/map', function() { return page('map'); })->name('map');
Route::get('/intel', function() { return page('intel'); })->name('intel');
Route::get('/map-sharing', function() { return page('map-sharing'); })->name('map-sharing');
Route::get('/sectors', 'SectorsController@index')->name('sectors');
Route::post('/sector', 'SectorsController@select')->name('select-sector');
Route::get('/sector/{sector?}', 'SectorsController@show')->name('sector');

// Stats
Route::get('/players', 'PlayersController@index')->name('players');
Route::get('/player/{id}', 'PlayersController@show')->name('player');
Route::get('/states', 'StatesController@index')->name('states');
Route::get('/crystals', function() { return page('crystals'); })->name('crystals');
Route::get('/macroeco', function() { return page('macroeco'); })->name('macroeco');
Route::get('/states/eco', function() { return page('states-eco'); })->name('states-eco');
Route::get('/players/stats', function() { return page('statistics'); })->name('players-stats');
Route::get('/fight-points', function() { return page('fight-points'); })->name('fight-points');
Route::get('/fights', function() { return page('fight-summary'); })->name('fights');
Route::get('/fight/{fight}', function() { return page('fight-detail'); })->name('fight');
Route::get('/players/changes', function() { return page('acclog'); })->name('players-changes');



// Management
Route::get(env('RECALC_ROUTE_PATH', '/recalc/{amount?}'), 'GameManagementController@recalc')->name('recalc');

Route::get('/icons', function() { return view('icons'); });

// Tests
Route::get('/tests', 'TestsController@index')->name('tests');
Route::get('/test/session', 'TestsController@session')->name('test-session');
Route::get('/test/xhtml', 'TestsController@xhtml')->name('test-xhtml');
// Simulators
// Route::any('/ecosim', 'EcoSimController@calc')->name('ecosim');
Route::get('/ecosim', 'EcoSimController@index')->name('ecosim');
Route::post('/ecosim', 'EcoSimController@calc')->name('calc-ecosim');
Route::any('/battlesim', 'BattleSimController@index')->name('battlesim');

// Pages
Route::get('/contact', 'PagesController@show')->name('contact');
// Route::fallback('PagesController@show');
Route::get('all', 'PagesController@all');
Route::any('{page}', 'PagesController@show');
// Route::any('{page}/{id}', 'PagesController@show');
